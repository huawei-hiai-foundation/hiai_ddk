/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2022. All rights reserved.
 * Description: remote memory allocator
 *
 */
#include <unistd.h>
#include <sys/mman.h>

#include "base/client/memory/remote_mem_allocator.h"
#include "framework/infra/log/log.h"

namespace hiai {
uint64_t g_memSize = 0;
Status RemoteMemAllocator::Init()
{
    return SUCCESS;
}

void* RemoteMemAllocator::Allocate(uint64_t size, hiai::MemoryType type, uint64_t cacheOffset)
{
    g_memSize = size;
    return malloc(size);
}

void RemoteMemAllocator::Free(void* addr, hiai::MemoryType type)
{
    if (addr != nullptr) {
        free(addr);
    }
    return;
}

ge::Status RemoteMemAllocator::Copy(
    void* dst, uint64_t dstSize, hiai::MemoryType dstType, const void* src, uint64_t srcSize, hiai::MemoryType srcType)
{
    return SUCCESS;
}

HIAI_MR_NDTensorBuffer* RemoteMemAllocator::GetHIAINDTensorBuffer(void* addr) const
{
    return nullptr;
}

#ifdef AI_SUPPORT_CACHE_CONSISTENT
Status RemoteMemAllocator::FlushCache(uint64_t addr, uint64_t size)
{
    return SUCCESS;
}

Status RemoteMemAllocator::FlushCache(void* data, uint64_t len)
{
    return SUCCESS;
}

Status RemoteMemAllocator::InvalidCache(uint64_t addr, uint64_t size)
{
    return SUCCESS;
}

int RemoteMemAllocator::GetFd(void* addr)
{
    (void)addr;
    return -1;
}
#endif
} // namespace hiai
