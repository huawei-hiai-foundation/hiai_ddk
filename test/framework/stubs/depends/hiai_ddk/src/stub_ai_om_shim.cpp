/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdlib>
#include <cstring>
#include <unistd.h> // for access
#include "framework/om/event_manager/ai_om_shim.h"
#include "framework/infra/log/log.h"

AI_OM_Config* AI_OM_SHIM_Config_Create()
{
    return AI_OM_Config_Create();
}

void AI_OM_SHIM_Config_Destroy(AI_OM_Config* config)
{
    AI_OM_Config_Destroy(config);
}

bool AI_OM_SHIM_Config_Set(AI_OM_Config* config, const char* name, const char* value)
{
    return AI_OM_Config_Set(config, name, value);
}

const char* AI_OM_SHIM_Config_Get(AI_OM_Config* config, const char* name)
{
    return AI_OM_Config_Get(config, name);
}

size_t AI_OM_SHIM_Config_ForEach(AI_OM_Config* config, AI_OM_Config_Visitor visitor, void* userData)
{
    return AI_OM_Config_ForEach(config, visitor, userData);
}

char* AI_OM_SHIM_Config_BuildString(AI_OM_Config* config)
{
    return AI_OM_Config_BuildString(config);
}

AI_OM_Config* AI_OM_SHIM_Config_ParseString(const char* configStr)
{
    return AI_OM_Config_ParseString(configStr);
}

AI_OM_EventListener* AI_OM_SHIM_EventListener_Create(AI_OM_EventHandler handler, void* userData)
{
    return AI_OM_EventListener_Create(handler, userData);
}

void AI_OM_SHIM_EventListener_Destroy(AI_OM_EventListener* listener)
{
    AI_OM_EventListener_Destroy(listener);
}

uint32_t AI_OM_SHIM_RegisterListener(AI_OM_EventListener* listener)
{
    return AI_OM_RegisterListener(listener);
}

uint32_t AI_OM_SHIM_PostEvent(const AI_OM_EventMsg* event)
{
    return AI_OM_PostEvent(event);
}
