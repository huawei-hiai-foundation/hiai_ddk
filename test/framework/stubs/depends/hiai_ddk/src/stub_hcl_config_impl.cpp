/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2023. All rights reserved.
 * Description: hcl config implementation
 */
#include <cstdlib>
#include "securec.h"

#include "model_runtime/hcl/hcl_impl/hcl_config_impl.h"

HIAI_Status HIAI_HCL_Config_SetModelPriority_Impl(uint32_t pid, uint32_t modelId, uint32_t priority)
{
    (void)pid;
    (void)modelId;
    (void)priority;
    return HIAI_SUCCESS;
}

HIAI_Status HIAI_HCL_Config_SetComputeIOBandWidthMode_Impl(uint32_t pid, uint32_t modelId, uint32_t bandMode)
{
    (void)pid;
    (void)modelId;
    (void)bandMode;
    return HIAI_SUCCESS;
}

typedef struct HIAI_ConfigOptionsHCL {
    int32_t pid = -1;
    int32_t modelId = -1;
    int32_t bandMode = -1;
} HIAI_ConfigOptionsHCL;

HIAI_MR_ConfigOptions* HIAI_HCL_ConfigOptions_Create_Impl(void)
{
    HIAI_ConfigOptionsHCL* option = (HIAI_ConfigOptionsHCL*)malloc(sizeof(HIAI_ConfigOptionsHCL));
    if (option == nullptr) {
        return nullptr;
    }
    (void)memset_s(option, sizeof(HIAI_ConfigOptionsHCL), -1, sizeof(HIAI_ConfigOptionsHCL));

    return (HIAI_MR_ConfigOptions*)(void*)option;
}

void HIAI_HCL_ConfigOptions_Destroy_Impl(HIAI_MR_ConfigOptions** config)
{
    if (config == nullptr || *config == nullptr) {
        return;
    }
    free(*config);
    *config = nullptr;
}

void HIAI_HCL_ConfigOptions_SetPid_Impl(HIAI_MR_ConfigOptions* options, int32_t pid)
{
    if (options == NULL) {
        return;
    }
    HIAI_ConfigOptionsHCL* config = (HIAI_ConfigOptionsHCL*)(void*)options;
    config->pid = pid;
}

int32_t HIAI_HCL_ConfigOptions_GetPid_Impl(HIAI_MR_ConfigOptions* options)
{
    return options == nullptr ? -1 : ((HIAI_ConfigOptionsHCL*)(void*)options)->pid;
}

void HIAI_HCL_ConfigOptions_SetModelId_Impl(HIAI_MR_ConfigOptions* options, int32_t modelId)
{
    if (options == NULL) {
        return;
    }
    HIAI_ConfigOptionsHCL* config = (HIAI_ConfigOptionsHCL*)(void*)options;
    config->modelId = modelId;
}

int32_t HIAI_HCL_ConfigOptions_GetModelId_Impl(HIAI_MR_ConfigOptions* options)
{
    return options == nullptr ? -1 : ((HIAI_ConfigOptionsHCL*)(void*)options)->modelId;
}

void HIAI_HCL_ConfigOptions_SetBandMode_Impl(HIAI_MR_ConfigOptions* options, int32_t bandMode)
{
    if (options == NULL) {
        return;
    }
    HIAI_ConfigOptionsHCL* config = (HIAI_ConfigOptionsHCL*)(void*)options;
    config->bandMode = bandMode;
}

int32_t HIAI_HCL_ConfigOptions_GetBandMode_Impl(HIAI_MR_ConfigOptions* options)
{
    return options == nullptr ? -1 : ((HIAI_ConfigOptionsHCL*)(void*)options)->bandMode;
}