#include "hiaiengine/ai_client_itf.h"
#include "hiaiengine/ai_client.h"
#include "alias/aicp.h"

namespace aicp {

void* AIClientCreate(std::string type)
{
    return nullptr;
}

AIStatus AIClientDestroy(void* agent)
{
    if (agent != nullptr) {
        free(agent);
        agent = nullptr;
    }
    return 0;
}

AIStatus AIClientInit(void* agent, const AIConfig& config, const std::vector<AIModelDescription>& modelDescs)
{
    return 0;
}

AIStatus AIClientProcess(void* agent, AIContext& context, const std::vector<std::shared_ptr<IAITensor>>& input,
    std::vector<std::shared_ptr<IAITensor>>& output, int32_t timeout)
{
    return 0;
}

AIStatus AIClientSetListener(void* agent, std::shared_ptr<IAIListener> listener)
{
    return 0;
}
}