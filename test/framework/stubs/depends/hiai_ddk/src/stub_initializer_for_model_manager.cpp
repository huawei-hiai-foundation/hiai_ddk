/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2021. All rights reserved.
 * Description: initializer.cpp
 */
#include "base/common/cl_manager/initializer.h"

namespace hiai {
Initializer* Initializer::Instance()
{
    static Initializer instance;
    return &instance;
}

Initializer::Initializer()
{
}

Initializer::~Initializer()
{
}

void Initializer::InitAllCL()
{
}

void Initializer::Init(std::map<std::string, std::string> options)
{
}

void Initializer::Begin(std::map<std::string, std::string> options)
{
}

void Initializer::Finalize()
{
}

} // namespace hiai
