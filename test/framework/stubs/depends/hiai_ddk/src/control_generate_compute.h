#ifndef CONTROL_GENERATE_COMPUTE_H
#define CONTROL_GENERATE_COMPUTE_H

#include <map>
#include <vector>

#include "framework/ge_error_code.h"
#include "model_runtime/hcl/util/model_runtime_api_export.h"

namespace hiai {

enum GenerateComputeFunc { COMPILE, RECOMPILE, INIT };

class HIAI_HMR_API_EXPORT ControlGenerateCompute {
public:
    static ControlGenerateCompute& GetInstance();

    void Clear();

    ge::Status GetStatus(GenerateComputeFunc func);

    void SetStatus(GenerateComputeFunc func, ge::Status status);
    void SetStatus(GenerateComputeFunc func, ge::Status status, std::vector<int> witchTimeError);

    void ChangeNowTimes();

private:
    std::map<GenerateComputeFunc, ge::Status> funcMap_;
    std::map<GenerateComputeFunc, std::vector<int>> expectWhichTimeErrorMap_;
    int nowTime_ {0};
};
} // namespace hiai
#endif // CONTROL_GENERATE_COMPUTE_H