/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: peft data handle.
 */

#include "partition/list_schedule/peft_data_handle.h"

namespace hiai {
namespace {
std::vector<std::vector<float>> matrix = {{0, 0}, {0, 0}};
std::map<std::string, std::vector<float>> computeMatrix = {{"a", {0, 0}}, {"b", {0, 0}}};
std::map<std::string, std::vector<TransDataFormatMode>> formatMatrix = {
    {"a", {TransDataFormatMode::FUSE, TransDataFormatMode::FUSE}},
    {"b", {TransDataFormatMode::FUSE, TransDataFormatMode::FUSE}}};
} // namespace
Status PeftDataHandle::Parse(const ge::ComputeGraphPtr& graph, const std::vector<std::string>& fileLists,
    const ExecuteDevice& deviceType, const bool& onlyRecordModel)
{
    return SUCCESS;
}
bool PeftDataHandle::checkDataValid() const
{
    return true;
}

const std::vector<std::vector<float>>& PeftDataHandle::GetCommunicationRateMatrix() const
{
    return matrix;
}
const std::map<std::string, std::vector<float>>& PeftDataHandle::GetComputationMatrix() const
{
    return computeMatrix;
}
const std::vector<std::vector<float>>& PeftDataHandle::GetCastRateMatrix() const
{
    return matrix;
}
const std::map<std::string, std::vector<TransDataFormatMode>>& PeftDataHandle::GetFormatMatrix() const
{
    return formatMatrix;
}
const std::vector<std::vector<float>>& PeftDataHandle::GetTransdataRateMatrix() const
{
    return matrix;
}
const std::vector<std::vector<float>>& PeftDataHandle::GetSoftwareMatrix() const
{
    return matrix;
}
Status PeftDataHandle::GetHeterFlag(bool& heterFlag)
{
    heterFlag = true;
    return SUCCESS;
}
} // namespace hiai
