/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2019. All rights reserved.
 * Description: vote hook
 *
 */
#include "base/common/hook/vote_hook.h"

namespace hiai {
VoteHook::VoteHook(PerfModeExtend mode, int id, uint32_t timeOut)
{
}

VoteHook::~VoteHook()
{
}

void VoteHook::OnLoadStart()
{
}

void VoteHook::OnRunStart()
{
}

void VoteHook::OnRunFinish()
{
}

void VoteHook::OnUnloadFinish()
{
}
} // namespace hiai
