/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2012-2018. All rights reserved.
 * Description: ProfilingHook for ModelExecutor, implemnented runtime model profiling.
 */

#include "base/common/hook/profiling_hook.h"
namespace hiai {
ProfilingHook::ProfilingHook(uint32_t modelId, std::string modelName) : modelId_(modelId), modelName_(modelName)
{
}

ProfilingHook::~ProfilingHook()
{
}

void ProfilingHook::OnLoadStart()
{
}

void ProfilingHook::OnLoadFinish()
{
}

void ProfilingHook::OnRunStart()
{
}

void ProfilingHook::OnRunFinish()
{
}

void ProfilingHook::OnUnloadStart()
{
}

void ProfilingHook::OnUnloadFinish()
{
}
} // namespace hiai