/*
 * Copyright (C) Huawei Technologies Co., Ltd.  2012-2021.  All rights reserved.
 * Description: predict_earliest_finish_time
 */

#include "partition/list_schedule/predict_earliest_finish_time.h"

namespace hiai {
Status PredictEarliestFinishTime::HeterTuning(const std::shared_ptr<ge::ComputeGraph>& computeGraph,
    const TuningMode& tuningMode, const TuningObjective tuningObjective, const PeftDataHandle& peftDataHandle,
    std::map<std::string, std::vector<ExecuteDevice>>& opDeviceOrder)
{
    return SUCCESS;
}
} // namespace hiai
