#include "base/common/cl_manager/initializer.h"
#include "base/common/cl_manager/ops_kernel_store_manager.h"
#include "base/server/hook/watch_dog_hook.h"
#include "common/debug/log.h"


namespace hiai {
    Initializer* Initializer::Instance()
    {
        static Initializer instance;
        return &instance;
    }

    Initializer::Initializer()
    {
    }

    Initializer::~Initializer()
    {
    }

    void Initializer::Init(std::map<std::string, std::string> options)
    {
        if (!isInit_) {
            std::lock_guard<std::mutex> lock(initLock_);
            if (!isInit_) {
                Begin(options);
                isInit_ = true;
            }
        }
    }

    void Initializer::Begin(std::map<std::string, std::string> options)
    {
        map<string, string> options;

        (void)WatchDog::GetInstance()->Start();

#ifdef HIAI_DDK
        options["cl_files"] = "libcpucl_ddk.so:libai_fmk_dnnacl.so";
#endif
        (void)OpKernelStoreManager::GetInstance()->Initialize(options, std::map<std::string, std::string>());
    }

    void Initializer::Finalize()
    {
        (void)OpKernelStoreManager::GetInstance()->Finalize();
        isInit_.store(false);
    }
}
