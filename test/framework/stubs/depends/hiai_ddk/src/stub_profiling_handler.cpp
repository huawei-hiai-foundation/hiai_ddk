/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: profile manager
 */

#include <ctime>
#include <climits>
#include <cstdlib>
#include <algorithm>

#include "om/profiling_handler.h"

using namespace NpuCore::RunTime;

namespace hiai {
static const std::string MODEL_NAME = "model_names";
static const std::string MODEL_NAME_ALL = "ALL";
ProfilingHandler* ProfilingHandler::GetInstance()
{
    static ProfilingHandler profilingHandler;
    return &profilingHandler;
}

ProfilingHandler::ProfilingHandler()
{
}

void ProfilingHandler::GetProfilingInfo(NpuCore::RunTime::EventMsg& eventMsg)
{
}

int ProfilingHandler::Handle(const NpuCore::RunTime::EventMsg& event)
{
    return 0;
}

int ProfilingHandler::StartProfiling(const std::string& modelName)
{
    return 0;
}

int ProfilingHandler::StopProfiling()
{
    return 0;
}

bool ProfilingHandler::CheckProfiling(const std::string& modelName)
{
    return true;
}
} // namespace hiai