/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: The description of ir model complete
 */
#include "complete/ir_model_complete.h"

using namespace std;
using namespace hiai;

namespace ge {

ge::Status IRModelComplete::Complete(IRModelCompleteOptions& irCompleteOptions, ComputeGraphPtr& graph)
{
    return SUCCESS;
}
} // namespace ge
