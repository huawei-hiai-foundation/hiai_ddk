/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2020. All rights reserved.
 * Description: hiai model manager
 */
#ifndef HIAI_ND_TENSOR
#define HIAI_ND_TENSOR
#include <map>
#include <string>
#include <vector>
#include <cstdint>
#include <cstring>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <iostream>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif
typedef enum HIAI_DataType {
    HIAI_DATATYPE_UINT8 = 0,
    HIAI_DATATYPE_FLOAT32 = 1,
    HIAI_DATATYPE_FLOAT16 = 2,
    HIAI_DATATYPE_INT32 = 3,
    HIAI_DATATYPE_INT8 = 4,
    HIAI_DATATYPE_INT16 = 5,
    HIAI_DATATYPE_BOOL = 6,
    HIAI_DATATYPE_INT64 = 7,
    HIAI_DATATYPE_UINT32 = 8,
    HIAI_DATATYPE_DOUBLE = 9,
} HIAI_DataType;

typedef enum {
    HIAI_FORMAT_NCHW = 0, /* NCHW */
    HIAI_FORMAT_NHWC, /* NHWC */
    HIAI_FORMAT_ND, /* Nd Tensor */
    HIAI_FORMAT_NCDHW = 40, /* NCDHW */
    HIAI_FORMAT_NDHWC, /* NDHWC */
} HIAI_Format;

struct HIAI_NDTensorDesc {
    int32_t* dims;
    size_t dimNum;
    HIAI_DataType dataType;
    HIAI_Format format;
};

struct HIAI_MR_NDTensorBuffer {
    const HIAI_NDTensorDesc* desc;
    size_t size;
    void* data;
    void* handle;
};


HIAI_NDTensorDesc* HIAI_NDTensorDesc_Create(
    int32_t* dims, size_t dimNum, HIAI_DataType dataType, HIAI_Format format);

HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateFromSize(const HIAI_NDTensorDesc* desc, size_t size);

void HIAI_NDTensorDesc_Destroy(HIAI_NDTensorDesc** tensorDesc);

const int32_t* HIAI_NDTensorDesc_GetDims(const HIAI_NDTensorDesc* tensorDesc);

#ifdef __cplusplus
}

#endif
#endif
