/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2021. All rights reserved.
 * Description: general_model_compiler.cpp
 */
#include "general_model_compiler.h"
#include "compiler/model_compiler_factory.h"
#include "control_generate_compute.h"
#include "framework/infra/log/log.h"

using namespace hiai;
using namespace std;
using namespace ge;

namespace hiai {
static std::recursive_mutex g_generatorLock;

Status GeneralModelCompiler::PreGraphSplit(const ge::GraphOptimizerOptions& options, ComputeGraphPtr& computeGraph)
{
    return ge::SUCCESS;
}

ge::Status GeneralModelCompiler::BeforeCompile(ge::GraphOptimizerOptions& optimizerOptions,
    ModelCompileOptions& options, std::shared_ptr<ComputeGraph>& computeGraph) // 拆分大函数
{
    return ge::SUCCESS;
}

ge::Status GeneralModelCompiler::Compile(ModelCompileOptions& options, std::shared_ptr<ComputeGraph>& computeGraph,
    std::shared_ptr<ICompiledModel>& compiledModel)
{
    FMK_LOGE("+++HERE+++ %s", __func__);
    hiai::ControlGenerateCompute::GetInstance().ChangeNowTimes();
    return ControlGenerateCompute::GetInstance().GetStatus(COMPILE);
}

REGISTER_MODEL_COMPILER_CREATOR(HCS_PARTITION_MODEL, GeneralModelCompiler)
#ifdef HIAI_DDK
REGISTER_MODEL_COMPILER_CREATOR(STANDARD_IR_GRAPH_MODEL, GeneralModelCompiler)
#endif
} // namespace hiai
