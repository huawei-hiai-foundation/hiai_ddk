/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2021. All rights reserved.
 * Description: dedicated_compiled_model.cpp
 */

#include "general_model_recompiler.h"
#include "compiler/model_recompiler_factory.h"
#include "control_generate_compute.h"

using namespace std;
namespace hiai {

ge::Status GeneralModelRecompiler::Recompile(
    ModelRecompileOptions& options, std::shared_ptr<hiai::ICompiledModel>& compiledModel)
{
    return ControlGenerateCompute::GetInstance().GetStatus(RECOMPILE);
    return ge::SUCCESS;
}
REGISTER_MODEL_RECOMPILER_CREATOR(HCS_PARTITION_MODEL, GeneralModelRecompiler);
} // namespace hiai
