/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2019. All rights reserved.
 * Description: watch dog hook
 *
 */
#include "base/server/hook/watch_dog_hook.h"
#include <unistd.h>
#include <sys/syscall.h>
#include <csignal>
#include <string>
#include "common/debug/log.h"
#include "common/string_util.h"

static const int MAX_WATCH_NUM = 10000;
#define gettid() syscall(SYS_gettid)

using namespace std;

namespace hiai {
#if !defined(HIAI_DDK) && !defined(PLATFORM_FPGA)
static const int OME_MONITOR_LOOPDELAYTIME_MS = 1000;
static const int OME_MONITOR_WATCHDOG_TIMEOUT_MS = 1000;
#endif
static const int ABORT_TIME_OUT_SECONDS = 25;
static const std::string LOAD = "load";
static const std::string RUN = "run";
static const std::string UNLOAD = "unload";
struct EventInfo {
    int timeoutSecond;
    int tid;
    hiai::IModelExecutor* executor;
    std::atomic<bool> isCancelled;
};

WatchDog* WatchDog::instance_ = nullptr;
std::mutex WatchDog::instanceLock_;

WatchDogHook::WatchDogHook(bool isDumpMode, hiai::IModelExecutor* executor)
    : isDumpMode_(isDumpMode), executor_(executor)
{
}

WatchDogHook::~WatchDogHook()
{
    executor_ = nullptr;
}

void WatchDogHook::HandleEvent(HandleType type, const string& eventType)
{
}

void WatchDogHook::OnLoadStart()
{
}

void WatchDogHook::OnLoadFinish()
{
}

void WatchDogHook::OnRunStart()
{
}

void WatchDogHook::OnRunFinish()
{
}

void WatchDogHook::OnUnloadStart()
{
}

void WatchDogHook::OnUnloadFinish()
{
}
} // namespace hiai
