/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: profiling data handler.
 */

#include "infra/om/profiling/profiling_data_handler.h"

namespace hiai {
Status ProfilingDataHandler::ParseProfToCsv(
    const std::vector<std::string>& fileList, const std::string& outputDir, bool supportExtParam)
{
    return hiai::SUCCESS;
}

Status ProfilingDataHandler::ParseProfToMem(const std::vector<std::string>& fileList, ProfilingData& profilingData)
{
    return hiai::SUCCESS;
}
} // namespace hiai