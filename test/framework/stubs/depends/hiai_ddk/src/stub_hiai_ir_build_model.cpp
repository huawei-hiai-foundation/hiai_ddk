/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stub_hiai_ir_build_model.h"
#include "framework/graph/utils/graph_utils.h"
#include "framework/graph/core/node/node_spec.h"
#include "framework/graph/utils/attr_utils.h"
#include "framework/graph/debug/ge_graph_attr_define.h"
#include "framework/graph/core/cgraph/graph_list_walker.h"
#include "model_builder/model_builder.h"
#include "infra/base/assertion.h"
#include "framework/infra/log/log.h"
#include "framework/compatible/ir_transformer.h"

namespace hiai {
bool VerifyIRAPI(ge::ComputeGraphPtr graph)
{
    HIAI_EXPECT_NOT_NULL_R(graph, false);
    if (hiai::IRTransformer::VerifyIrReservedField(graph) == false) {
        FMK_LOGE("ir verify reserverd filed failed!");
        return false;
    }
    return true;
}
std::shared_ptr<hiai::IBuiltModel> BuildModel(
    const hiai::ModelBuildOptions& buildOptions, const std::string& modelName, ge::Model& irModel)
{
    std::shared_ptr<IModelBuilder> modelBuilder = CreateModelBuilder();
    HIAI_EXPECT_NOT_NULL_R(modelBuilder, nullptr);

    ge::Buffer irModelBuff;
    if (irModel.Save(irModelBuff) != ge::GRAPH_SUCCESS) {
        FMK_LOGE("serialize IR model failed");
        return nullptr;
    }
    void* data = static_cast<void*>(const_cast<uint8_t*>(irModelBuff.GetData()));
    std::shared_ptr<IBuffer> inputBuffer = CreateLocalBuffer(data, irModelBuff.GetSize());
    HIAI_EXPECT_NOT_NULL_R(inputBuffer, nullptr);

    std::shared_ptr<IBuiltModel> builtModel = nullptr;
    auto ret = modelBuilder->Build(buildOptions, modelName, inputBuffer, builtModel);
    if (ret != SUCCESS || builtModel == nullptr) {
        FMK_LOGE("IR API build failed.");
    }
    return builtModel;
}
}