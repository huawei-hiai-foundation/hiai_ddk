/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef STUB_HIAI_IR_BUILD_MODEL_H
#define STUB_HIAI_IR_BUILD_MODEL_H

#include "model_builder/hiai_ir_build.h"
#include "model/built_model.h"
#include "model_builder/model_builder_types.h"

namespace  hiai {
    bool VerifyIRAPI(ge::ComputeGraphPtr graph);
    std::shared_ptr<hiai::IBuiltModel> BuildModel(const hiai::ModelBuildOptions& buildOptions,
        const std::string& modelName, ge::Model& irModel);
}

#endif