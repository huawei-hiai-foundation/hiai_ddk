/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2019. All rights reserved.
 * Description: memory manager
 *
 */
#include "executor/hcl/model_memory/model_memory_manager.h"
#include "model_runtime/hcl/util/model_memory_manager_util.h"
using namespace std;
namespace hiai {
hiai::MemInfo g_memInfo;

ModelMemoryManager::ModelMemoryManager(DeviceMemoryReusePlan deviceMemoryReusePlan, int32_t pipelineCount)
{
}

Status ModelMemoryManager::Init(const GeneralCompiledModel* model, const std::shared_ptr<MemoryAllocator> memAllocator)
{
    return SUCCESS;
}

void* ModelMemoryManager::AllocateMemory(uint64_t bytes, MemoryType type)
{
    return nullptr;
}

Status ModelMemoryManager::ModelMemoryCopy(
    void* dst, uint64_t dstSize, MemoryType dstType, const void* src, uint64_t srcSize, MemoryType srcType)
{
    return SUCCESS;
}

void ModelMemoryManager::FreeMemory(void* addr, MemoryType type)
{
    return;
}

Status ModelMemoryManager::AllocateMemory()
{
    return SUCCESS;
}

Status ModelMemoryManager::GetGraphOpInputOutputSize(const string& graphOpName, int32_t pipeLineIndex, uint64_t& size)
{
    return SUCCESS;
}

void ModelMemoryManager::Finalize()
{
    return;
}

Status ModelMemoryManager::GetOutputTensor(const GeneralCompiledModel* model,
    const ge::Node& node, int32_t pipeLineIndex, vector<ge::BaseBuffer>& tensorVec)
{
    return SUCCESS;
}

MemoryType GetMemoryType(ge::DeviceType deviceType, ge::MemUsage memUsage)
{
    return MemoryType::DDR;
}

size_t ModelMemoryManager::GetModelUsedMemory() const
{
    return 0;
}

hiai::Maybe<hiai::MemInfo> ModelMemoryManager::GetFeatureMapMemInfo(const ge::OpDesc& opDesc, int32_t pipeLineIndex)
{
    return hiai::Maybe<hiai::MemInfo>(g_memInfo);
}

Status LoadWeights(const std::string& weightDir, ge::ComputeGraphPtr graph)
{
    return SUCCESS;
}

Status GetWeightAddr(const std::string& weightName, hiai::MemInfo& weight)
{
    return SUCCESS;
}

Status FlushWeight(const std::string& weightName, size_t offset, size_t size)
{
    return SUCCESS;
}
} // namespace hiai
