#include "control_generate_compute.h"
#include <iostream>

namespace hiai {
ControlGenerateCompute& ControlGenerateCompute::GetInstance()
{
    static ControlGenerateCompute instance;
    return instance;
}

void ControlGenerateCompute::Clear()
{
    funcMap_.clear();
    expectWhichTimeErrorMap_.clear();
    nowTime_ = 0;
}

ge::Status ControlGenerateCompute::GetStatus(GenerateComputeFunc func)
{
    std::cout << nowTime_ << std::endl;
    if (funcMap_.find(func) == funcMap_.end()) {
        return ge::SUCCESS;
    }

    if (expectWhichTimeErrorMap_.find(func) == expectWhichTimeErrorMap_.end()) {
        return funcMap_[func];
    }

    for (int i = 0; i < expectWhichTimeErrorMap_[func].size(); i++) {
        if (expectWhichTimeErrorMap_[func][i] == nowTime_) {
            return funcMap_[func];
        }
    }
    return ge::SUCCESS;
}

void ControlGenerateCompute::SetStatus(GenerateComputeFunc func, ge::Status status)
{
    funcMap_[func] = status;
}

void ControlGenerateCompute::SetStatus(GenerateComputeFunc func, ge::Status status, std::vector<int> witchTimeError)
{
    funcMap_[func] = status;
    if (witchTimeError.size() > 0) {
        expectWhichTimeErrorMap_[func] = witchTimeError;
    }
}

void ControlGenerateCompute::ChangeNowTimes()
{
    nowTime_++;
}
} // namespace hiai