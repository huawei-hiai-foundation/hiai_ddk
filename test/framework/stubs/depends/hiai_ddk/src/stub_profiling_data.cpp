/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: profiling file parser.
 */

#include "infra/om/profiling/profiling_data.h"

namespace hiai {
bool ProfilingData::Process(const std::vector<ProfTlvMem>& profTlvInfos)
{
    return true;
}

void ProfilingData::GetModelProfInfo(std::vector<ModelProfInfo>& modelProfInfo) const
{
}

void ProfilingData::GetOpProfInfo(std::vector<OpProfInfo>& opProfInfo) const
{
}

void ProfilingData::GetPmuProfInfo(std::vector<PmuProfInfo>& pmuProfInfo) const
{
}

void ProfilingData::GetPmuItemInfo(std::vector<uint64_t>& pmuItem) const
{
}
} // namespace hiai