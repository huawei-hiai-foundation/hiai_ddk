/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2021. All rights reserved.
 * Description: hiai_maint_wrapper
 */

#include <dlfcn.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>

#include "model_runtime/hcl/hcl_impl/hiai_maint_wrapper.h"
#include "om/inc/om_log.h"

using std::cerr;
using std::cout;
using std::endl;
using std::string;

namespace hiai {
namespace {
string clientFile = "/data/local/tmp/model.prof";
string serverFile = "/data/vendor/hiai/model.prof";
} // namespace
HiAiMaintWrapper::HiAiMaintWrapper()
{
}

HiAiMaintWrapper::~HiAiMaintWrapper()
{
}

Status HiAiMaintWrapper::Init()
{
    return SUCCESS;
}

Status HiAiMaintWrapper::Maintain(HIAI_MaintType type, int32_t action, const std::string config)
{
    return SUCCESS;
}

Status HiAiMaintWrapper::DeleteFile(const string& serverFileName)
{
    return SUCCESS;
}

Status HiAiMaintWrapper::GetFileList(vector<string>& clientFiles, vector<string>& serverFiles)
{
    clientFiles.push_back(clientFile);
    serverFiles.push_back(serverFile);
    return SUCCESS;
}

Status HiAiMaintWrapper::CopyFileToClient(const string& dstFileName, const string& srcFileName)
{
    return SUCCESS;
}

Status HiAiMaintWrapper::PurgeProfData()
{
    return SUCCESS;
}
}; // namespace hiai
