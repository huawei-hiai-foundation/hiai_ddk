/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <unordered_map>
#include "graph/attr_value.h"
#include "graph/compatible/all_ops.h"
#include "graph/compatible/operator_reg.h"
#include "framework/graph/core/cgraph/compute_graph.h"
#include "graph/graph.h"
#include "graph/model.h"
#include "framework/graph/core/op/op_desc.h"
#include "graph/operator.h"
#include "framework/graph/utils/graph_utils.h"
#include "operator_impl.h"
using namespace ge;
using testing::Test;

using namespace testing;
using namespace std;
class Test_Attr : public testing::Test {
public:
protected:
    void SetUp()
    {
    }
    void TearDown()
    {
    }
};

TEST_F(Test_Attr, test_set_attr)
{
    ge::Operator op("test", "test");
    bool value = true;
    op.SetAttr("test", value);

    float value1 = 0.5;
    op.SetAttr("test1", value1);

    int64_t value2 = 1;
    op.SetAttr("test2", value2);

    std::string value3 = "test";
    op.SetAttr("test3", value3);

    std::vector<bool> listBoolvalue = {true, false};
    op.SetAttr("test4", listBoolvalue);

    std::vector<float> listFloatvalue = {0.5, 07};
    op.SetAttr("test5", listFloatvalue);

    std::vector<int64_t> listIntValue = {1, 3, 10};
    op.SetAttr("test6", listIntValue);

    std::vector<std::vector<int64_t>> listListInt = {{5, 6, 7, 8}, {7, 8, 9, 6}};
    op.SetAttr("test7", listListInt);

    std::vector<std::vector<float>> listListFloat = {{0.2, 0.5, 0.3}, {0.5, 0.6, 0.7}};
    op.SetAttr("test8", listListFloat);

    ge::AscendString attrValue;
    EXPECT_TRUE(op.GetAttr("test3", attrValue) == GRAPH_SUCCESS);
    EXPECT_TRUE(attrValue.GetString() == value3);
    op.SetName("test");
    EXPECT_TRUE(op.GetType() == value3);

    ge::AscendString attrValue1(value3.c_str(), value3.length());
    EXPECT_TRUE(attrValue.Hash() == attrValue1.Hash());
    EXPECT_TRUE(attrValue1.Find(attrValue) == 0);
    EXPECT_TRUE((attrValue < attrValue1) == false);
    EXPECT_TRUE((attrValue > attrValue1) == false);
    EXPECT_TRUE(attrValue <= attrValue1);
    EXPECT_TRUE(attrValue >= attrValue1);
}