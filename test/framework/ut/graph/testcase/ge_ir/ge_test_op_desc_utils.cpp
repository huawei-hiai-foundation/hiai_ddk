#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unordered_map>
#include "framework/graph/utils/op_desc_utils.h"

using namespace std;
using namespace ge;

class ge_test_op_desc_utils : public testing::Test {
protected:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(ge_test_op_desc_utils, Activation_Attrs)
{
    std::vector<std::string> attrList = ge::OpDescUtils::GetIrAttrNames("Activation");
    EXPECT_EQ(attrList[0], "mode");
    EXPECT_EQ(attrList[1], "coef");
    EXPECT_EQ(attrList[2], "negative_slope");
}

TEST_F(ge_test_op_desc_utils, Activation1_Attrs_failed)
{
    std::vector<std::string> attrList = ge::OpDescUtils::GetIrAttrNames("Activation1");
    EXPECT_EQ(attrList.size(), 0);
}