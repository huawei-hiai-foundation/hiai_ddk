#include <gtest/gtest.h>
#include "c/ddk/graph/handle_types.h"
#include "c/ddk/graph/op_config.h"
#include "c/ddk/graph/tensor.h"
#include "graph/csrc/resource_manager.h"
#include "c/ddk/graph/context.h"
#include "graph/shape.h"

#include "framework/graph/utils/op_desc_utils.h"
#include "framework/graph/utils/attr_utils.h"

using namespace ge;
using namespace std;
using namespace hiai;

class UTEST_c_tensor : public testing::Test {
public:
    void SetUp()
    {
        resMgr = HIAI_IR_ResourceManagerCreate();
    }

    void TearDown()
    {
        HIAI_IR_ResourceManagerDestroy(&resMgr);
    }
public:
    ResMgrHandle resMgr = nullptr;
};

TEST_F(UTEST_c_tensor, CreateTensor)
{
    // 设置测试值
    uint8_t* data = new uint8_t[4] {0x01, 0x02, 0x03, 0x04};
    size_t dataLen = 4;
    int64_t shape[] = {1, 2};
    size_t shapeSize = 2;
    HIAI_DataType typeC = HIAI_DATATYPE_FLOAT32;
    HIAI_Format formatC = HIAI_Format::HIAI_FORMAT_NCHW;

    // 调用测试函数
    TensorHandle tensor = HIAI_IR_CreateTensor(resMgr, reinterpret_cast<void*>(data), dataLen, shape, shapeSize, typeC, formatC);
    EXPECT_NE(tensor, nullptr);

    // 提取转化句柄
    auto resMgrPtr = reinterpret_cast<ResourceManager*>(resMgr);
    BasePtr tensorBasePtr = resMgrPtr->GetSrcPtr(tensor);
    std::shared_ptr<ge::Tensor> tensorPtr = std::dynamic_pointer_cast<ge::Tensor>(tensorBasePtr);

    // 验证返回值
    for (size_t i = 0; i < dataLen; ++i) {
        EXPECT_EQ(tensorPtr->GetData().data()[i], data[i]);
    }
    EXPECT_EQ(tensorPtr->GetData().size(), dataLen);

    std::vector<int64_t> shapeVec = tensorPtr->GetTensorDesc().GetShape().GetDims();
    EXPECT_EQ(shapeVec.size(), shapeSize);

    for (size_t i = 0; i < shapeSize; ++i) {
        EXPECT_EQ(shapeVec[i], shape[i]);
    }

    EXPECT_EQ(tensorPtr->GetTensorDesc().GetDataType(), ge::DataType::DT_FLOAT);
    EXPECT_EQ(tensorPtr->GetTensorDesc().GetFormat(), static_cast<ge::Format>(formatC));

    delete[] data;
}
