#include <gtest/gtest.h>
#include <mockcpp/mockcpp.hpp>
#include "graph/debug/ge_error_codes.h"
#include "framework/graph/debug/ge_util.h"
#include "framework/graph/debug/ge_op_types.h"
#include "framework/graph/core/cgraph/graph_finder.h"
#include "framework/graph/core/cgraph/graph_sorter.h"
#include "framework/graph/core/cgraph/graph_modifier.h"
#include "framework/graph/core/cgraph/graph_spec.h"
#include "framework/graph/core/cgraph/graph_list_walker.h"
#include "framework/graph/debug/ge_graph_attr_define.h"
#include "framework/graph/utils/attr_utils.h"
#include "graph/op/array_defs.h"
#include "graph/op/nn_defs.h"
#include "graph/compatible/cpt_nn_defs.h"
#define protected public
#define private public
#include "framework/graph/core/cgraph/compute_graph.h"
#include "framework/graph/utils/graph_utils.h"
#include "framework/graph/utils/op_desc_utils.h"
#include "framework/graph/core/node/node.h"
#include "framework/graph/core/node/node_sub_graph.h"
#include "framework/graph/core/edge/edge.h"
#include "framework/graph/core/infershape/op_ir_func_factory.h"
#include "framework/graph/utils/node_utils.h"
#include "framework/graph/core/infershape/graph_infershape_util.h"
#include <vector>
#undef protected
#undef private
using namespace ge;
using namespace testing;
using namespace std;
class UTEST_omg_graph_compute_graph : public testing::Test {
protected:
    void SetUp()
    {
    }

    void TearDown()
    {
        GlobalMockObject::verify();
    }

public:
};

TEST_F(UTEST_omg_graph_compute_graph, add_node_success)
{
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");

    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    graphPtr->ROLE(GraphSpec).SetName("set_test");
    std::string nameGet = graphPtr->ROLE(GraphSpec).Name();
    EXPECT_EQ(nameGet, "set_test");

    // NodePtr
    NodePtr nodePtr = nullptr;
    NodePtr nodePtrReturn = graphPtr->ROLE(GraphModifier).AddNode(nodePtr);
    EXPECT_EQ(nodePtrReturn, nullptr);

    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);
    nodePtr = graphPtr->ROLE(GraphFinder).FindNodePtr(*node);
    nodePtrReturn = graphPtr->ROLE(GraphModifier).AddNode(nodePtr);
    // node already added.
    EXPECT_EQ(nodePtrReturn, nullptr);
}

TEST_F(UTEST_omg_graph_compute_graph, add_node_by_opdesc_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");

    Node* nodePtrReturn = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    EXPECT_NE(nodePtrReturn, nullptr);
    OpDescPtr opDescNullPtr = nullptr;
    EXPECT_EQ(nullptr, graphPtr->ROLE(GraphModifier).AddNode(opDescNullPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, update_node_subgraph_ret_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("root_graph");

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test_op");

    Node* nodePtrReturn = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    EXPECT_NE(nodePtrReturn, nullptr);
    OpDescPtr opDescNullPtr = nullptr;
    EXPECT_EQ(nullptr, graphPtr->ROLE(GraphModifier).AddNode(opDescNullPtr));

    std::shared_ptr<ComputeGraph> graph2 = ge::ComputeGraph::Make("ut_test_subgraph");
    hiai::Status ret = nodePtrReturn->ROLE(NodeSubGraph).AddSubGraph(graph2);
    EXPECT_EQ(ret, hiai::SUCCESS);
    EXPECT_EQ(graph2->ROLE(GraphSpec).OwnerNode(), nodePtrReturn);

    std::shared_ptr<ComputeGraph> graph3 = ge::ComputeGraph::Make("ut_test_subgraph");
    ret = nodePtrReturn->ROLE(NodeSubGraph).UpdateSubGraph(graph3);
    EXPECT_EQ(ret, hiai::SUCCESS);
    EXPECT_EQ(graph3->ROLE(GraphSpec).OwnerNode(), nodePtrReturn);
}

TEST_F(UTEST_omg_graph_compute_graph, add_set_attr_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    AttrValue attrValue;
    AttrValue& getAttrValue = attrValue;
    graphPtr->SetAttr("int_test", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    EXPECT_EQ(
        ge::GRAPH_FAILED, graphPtr->SetAttr("int_test", AttrValue::CreateFrom(static_cast<AttrValue::FLOAT>(1))));
    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->SetAttr("int_test", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2))));

    EXPECT_EQ(ge::GRAPH_FAILED, graphPtr->GetAttr("int_test111", getAttrValue));
    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->GetAttr("int_test", getAttrValue));
    graphPtr->HasAttr("int_test");
    EXPECT_EQ(true, graphPtr->HasAttr("int_test"));
}

TEST_F(UTEST_omg_graph_compute_graph, add_get_remove_nodes_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);
    Node* node2 = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node2, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddInput(*node), hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddOutput(*node), hiai::SUCCESS);

    EXPECT_NE(graphPtr->ROLE(GraphSpec).NodesNum(), 0);
    std::size_t i = 0;
    auto ret = graphPtr->ROLE(GraphListWalker).WalkInNodes([&i](Node& innode) {
        i++;
        return hiai::SUCCESS;
    });
    EXPECT_NE(0, i);
    EXPECT_EQ(ret, hiai::SUCCESS);

    i = 0;
    ret = graphPtr->ROLE(GraphListWalker).WalkOutNodes([&i](Node& outnode) {
        i++;
        return hiai::SUCCESS;
    });
    EXPECT_NE(0, i);
    EXPECT_EQ(ret, hiai::SUCCESS);

    EXPECT_EQ(node, graphPtr->ROLE(GraphFinder).FindNode("test"));
    EXPECT_EQ(nullptr, graphPtr->ROLE(GraphFinder).FindNode("non_exist"));

    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphModifier).RemoveNode(*node));
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphModifier).RemoveNode(*node2));
    EXPECT_EQ(hiai::FAILURE, graphPtr->ROLE(GraphModifier).RemoveOutputNode(*node2));
    EXPECT_EQ(hiai::FAILURE, graphPtr->ROLE(GraphModifier).RemoveNode(*node2));
}

TEST_F(UTEST_omg_graph_compute_graph, remove_duplicated_nodes_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr opDescPtr1 = std::make_shared<OpDesc>("input1", "Data");
    Node* input1 = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr1);
    ASSERT_NE(input1, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddInput(*input1), hiai::SUCCESS);

    OpDescPtr opDescPtr2 = std::make_shared<OpDesc>("input2", "Data");
    Node* input2 = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr2);
    ASSERT_NE(input2, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddInput(*input2), hiai::SUCCESS);

    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphModifier).RemoveNode(*input1));

    OpDescPtr opDescPtr3 = std::make_shared<OpDesc>("input3", "Data");
    Node* input3 = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr3);
    ASSERT_NE(input3, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddInput(*input3), hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddInput(*input3), hiai::SUCCESS);

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("output", "output");
    Node* output = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(output, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddOutput(*output), hiai::SUCCESS);
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphModifier).RemoveNode(*output));

    std::size_t inNodesCnt = 0;
    std::size_t node3Cnt = 0;
    std::size_t node2Index = 0;
    std::size_t node3Index = 0;
    auto ret = graphPtr->ROLE(GraphListWalker).WalkInNodes(
        [&inNodesCnt, &node3Cnt, &node2Index, &node3Index](Node& innode) {
        if (innode.ROLE(NodeSpec).Name() == "input2") {
            node2Index = inNodesCnt;
        }
        if (innode.ROLE(NodeSpec).Name() == "input3") {
            node3Index = inNodesCnt;
            node3Cnt++;
        }
        inNodesCnt++;
        return hiai::SUCCESS;
    });

    EXPECT_EQ(inNodesCnt, 2);
    EXPECT_EQ(node3Cnt, 1);
    EXPECT_EQ(node2Index, 0);
    EXPECT_EQ(node3Index, 1);
}

TEST_F(UTEST_omg_graph_compute_graph, add_and_remove_sub_graph_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    std::shared_ptr<ComputeGraph> subGraphPtr = ge::ComputeGraph::Make("sub_graph");
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);
    Node* sub_node = subGraphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(sub_node, nullptr);

    std::size_t all_nodes = graphPtr->ROLE(GraphSpec).NodesNum();
    EXPECT_NE(0, all_nodes);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);

    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->ROLE(GraphSorter).SortNodesDFS());
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphSpec).IsValid());
    GraphUtils::Dump(graphPtr);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_failed)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);

    EXPECT_EQ(ge::GRAPH_FAILED, graphPtr->ROLE(GraphSorter).SortNodesDFS());
    EXPECT_EQ(ge::GRAPH_FAILED, graphPtr->ROLE(GraphSpec).IsValid());
    GraphUtils::Dump(graphPtr);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_2_success)
{
    // make graph data --> Conv-->Add
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNode = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNode, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNode = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNode, nullptr);

    graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*node, 0), Endpoint(*convNode, 0));
    graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convNode, 0), Endpoint(*addNode, 0));

    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->ROLE(GraphSorter).SortNodesDFS());
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphSpec).IsValid());
    GraphUtils::Dump(graphPtr);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_3_success)
{
    // make graph data --> Conv-->Add
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* node = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(node, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNode = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(node, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNode = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNode, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*node, -1), Endpoint(*convNode, -1)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convNode, -1), Endpoint(*addNode, -1)),
        hiai::SUCCESS);

    OpDescPtr convInDesc0 = std::make_shared<OpDesc>("Data0", "Data");
    convInDesc0->AddInputDesc(tensorDesc);
    convInDesc0->AddOutputDesc(tensorDesc);
    Node* convInNode0 = graphPtr->ROLE(GraphModifier).AddNode(convInDesc0);
    ASSERT_NE(convInNode0, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convInNode0, 0), Endpoint(*convNode, 0)),
        hiai::SUCCESS);

    OpDescPtr addInDesc0 = std::make_shared<OpDesc>("Data1", "Data");
    addInDesc0->AddInputDesc(tensorDesc);
    addInDesc0->AddOutputDesc(tensorDesc);
    Node* addInNode0 = graphPtr->ROLE(GraphModifier).AddNode(addInDesc0);
    ASSERT_NE(addInNode0, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*addInNode0, 0), Endpoint(*addNode, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->ROLE(GraphSorter).SortNodesDFS());
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphSpec).IsValid());
    GraphUtils::Dump(graphPtr);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_4_success)
{
    // make graph data  Conv-->Add
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddInputDesc(tensorDesc);
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNodePtr = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNodePtr = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convNodePtr, -1), Endpoint(*addNodePtr, -1)),
        hiai::SUCCESS);

    OpDescPtr convInDesc0 = std::make_shared<OpDesc>("Data0", "Data");
    convInDesc0->AddInputDesc(tensorDesc);
    convInDesc0->AddOutputDesc(tensorDesc);
    Node* convInNode0 = graphPtr->ROLE(GraphModifier).AddNode(convInDesc0);
    ASSERT_NE(convInNode0, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convInNode0, 0), Endpoint(*convNodePtr, 0)),
        hiai::SUCCESS);

    OpDescPtr addInDesc0 = std::make_shared<OpDesc>("Data1", "Data");
    addInDesc0->AddInputDesc(tensorDesc);
    addInDesc0->AddOutputDesc(tensorDesc);
    Node* addInNode0 = graphPtr->ROLE(GraphModifier).AddNode(addInDesc0);
    ASSERT_NE(addInNode0, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*addInNode0, 0), Endpoint(*addNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->ROLE(GraphSorter).SortNodesDFS());
    EXPECT_EQ(hiai::SUCCESS, graphPtr->ROLE(GraphSpec).IsValid());
    GraphUtils::Dump(graphPtr);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_5_failed)
{
    // make graph data --> Conv, Add:as alone node
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddInputDesc(tensorDesc);
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNodePtr = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNodePtr = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*nodePtr, 0), Endpoint(*convNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, graphPtr->ROLE(GraphSorter).SortNodesDFS());
}

TEST_F(UTEST_omg_graph_compute_graph, graph_topologicsort_6_failed)
{
    // make graph Conv-->Add
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNodePtr = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNodePtr = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*convNodePtr, -1), Endpoint(*addNodePtr, -1)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_FAILED, graphPtr->ROLE(GraphSorter).SortNodesDFS());
}

TEST_F(UTEST_omg_graph_compute_graph, graph_isolate_node_success)
{
    // make graph data --> Conv-->Add->Sub
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddInputDesc(tensorDesc);
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNodePtr = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNodePtr = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNodePtr, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*nodePtr, 0), Endpoint(*convNodePtr, 0)),
        hiai::SUCCESS);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_isolate_node_2_success)
{
    // make graph data --> Conv-->Add->Sub
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddInputDesc(tensorDesc);
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNodePtr = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNodePtr = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*nodePtr, -1), Endpoint(*convNodePtr, -1)),
        hiai::SUCCESS);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_get_subgraph_success)
{
    string subGraphName = "sub";
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    std::shared_ptr<ComputeGraph> subGraphPtr = ge::ComputeGraph::Make(subGraphName);
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("test", "test");
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);
    Node* sub_nodePtr = subGraphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(sub_nodePtr, nullptr);
}

TEST_F(UTEST_omg_graph_compute_graph, graphutil_insert_transnode_success)
{
    OpDescPtr descPtr = std::make_shared<OpDesc>("name1", "type1");
    EXPECT_EQ(descPtr->AddInputDesc("x", TensorDesc(Shape({1, 16, 16, 16}), FORMAT_NCHW)), GRAPH_SUCCESS);
    EXPECT_EQ(descPtr->AddInputDesc("w", TensorDesc(Shape({1, 1, 1, 1}), FORMAT_NCHW)), GRAPH_SUCCESS);
    EXPECT_EQ(descPtr->AddOutputDesc("y", TensorDesc(Shape({1, 32, 8, 8}), FORMAT_NCHW)), GRAPH_SUCCESS);

    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    Node* n1 = graphPtr->ROLE(GraphModifier).AddNode(descPtr);
    ASSERT_NE(n1, nullptr);
    Node* n2 = graphPtr->ROLE(GraphModifier).AddNode(descPtr);
    ASSERT_NE(n2, nullptr);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*n1, 0), Endpoint(*n2, 0)),
        hiai::SUCCESS);

    OpDescPtr descPtr2 = std::make_shared<OpDesc>("name2", "type2");
    EXPECT_EQ(descPtr2->AddInputDesc("x", TensorDesc(Shape({1, 16, 16, 16}), FORMAT_NCHW)), GRAPH_SUCCESS);
    EXPECT_EQ(descPtr2->AddOutputDesc("y", TensorDesc(Shape({1, 32, 8, 8}), FORMAT_NCHW)), GRAPH_SUCCESS);

    descPtr2->SetAttr("output_format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    descPtr2->SetAttr("input_format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    OpDescPtr descPtr3 = std::make_shared<OpDesc>("name3", "type3");
    if (descPtr3->GetInputsDescSize() == 0 && descPtr3->GetOutputsSize() == 0) {
        descPtr3->AddInputDesc(TensorDesc());
        descPtr3->AddOutputDesc(TensorDesc());
    }

    Node* insertNode1 = graphPtr->ROLE(GraphModifier).AddNode(descPtr2);
    Node* insertNode2 = graphPtr->ROLE(GraphModifier).AddNode(descPtr3);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).InsertAfter(*n1, 0, *insertNode2), hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).InsertAfter(*n1, 0, *insertNode1), hiai::SUCCESS);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).InsertBefore(*n2, 0, *insertNode1), hiai::FAILURE);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).InsertBefore(*n2, 0, *insertNode2), hiai::FAILURE);
}

TEST_F(UTEST_omg_graph_compute_graph, graph_infershape_success1)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_infershape_failed1)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr dataOpDescPtr = std::make_shared<OpDesc>("Data", "Data");
    dataOpDescPtr->AddInputDesc(tensorDesc);
    dataOpDescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpDescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr softmaxOpDescPtr = std::make_shared<OpDesc>("Softmax", "Softmax");
    softmaxOpDescPtr->AddInputDesc(tensorDesc);
    softmaxOpDescPtr->AddOutputDesc(tensorDesc);
    Node* softmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(softmaxOpDescPtr);
    ASSERT_NE(softmaxNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*softmaxNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_SPECIFIC)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 6, 512, 512}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // notset
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SPECIFIC")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[16 * 6 * 3 * 3];
    TensorDesc tensorDesc1(Shape({16, 6, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 16 * 6 * 3 * 3 * sizeof(float));

    float data2[16];
    TensorDesc tensorDesc2(Shape({1, 16, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 16 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(16, out.GetDim(1));
    EXPECT_EQ(512, out.GetDim(2));
    EXPECT_EQ(512, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success2_NCHW_SPECIFIC)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 6, 512, 512}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // notset
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SPECIFIC")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[16 * 6 * 3 * 3];
    TensorDesc tensorDesc1(Shape({16, 6, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 16 * 6 * 3 * 3 * sizeof(float));

    float data2[16];
    TensorDesc tensorDesc2(Shape({16})); // 1-D
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 16 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(16, out.GetDim(1));
    EXPECT_EQ(512, out.GetDim(2));
    EXPECT_EQ(512, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_failed_NCHW_bias_shape_failed)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 6, 512, 512}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // notset
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SPECIFIC")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[16 * 6 * 3 * 3];
    TensorDesc tensorDesc1(Shape({16, 6, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 16 * 6 * 3 * 3 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1})); // shape failed
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_NE(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_failed_NCHW_bias_shape_failed2)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 6, 512, 512}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // notset
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SPECIFIC")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[16 * 6 * 3 * 3];
    TensorDesc tensorDesc1(Shape({16, 6, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 16 * 6 * 3 * 3 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 2, 2, 1})); // shape failed
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_NE(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_failed_NCHW_bias_shape_failed3)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 6, 512, 512}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // notset
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SPECIFIC")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[16 * 6 * 3 * 3];
    TensorDesc tensorDesc1(Shape({16, 6, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 16 * 6 * 3 * 3 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({2, 2})); // shape failed
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_SAME)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // SAME
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("SAME")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(32, out.GetDim(2));
    EXPECT_EQ(32, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_VALID)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {1, 1};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // VAILD
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("VALID")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(31, out.GetDim(2));
    EXPECT_EQ(31, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_VALID_s22)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);
    AttrValue::LIST_INT dilations = {1, 1};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {2, 2};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    // NCHW

    tempOpDescPtr->SetAttr(

        "format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // VAILD
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("VALID")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(16, out.GetDim(2));
    EXPECT_EQ(16, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_VALID_s22_dila22)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    AttrValue::LIST_INT dilations = {2, 2};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {2, 2};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));

    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // VAILD
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("VALID")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(15, out.GetDim(2));
    EXPECT_EQ(15, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_success_NCHW_VALID_s55_dila22)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);
    AttrValue::LIST_INT dilations = {2, 2};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {5, 5};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    // NCHW
    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // VAILD
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("VALID")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(6, out.GetDim(2));
    EXPECT_EQ(6, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_Conv2D_infershape_FAILED_not_support_offsetx_1)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Convolution", "Convolution");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);
    AttrValue::LIST_INT dilations = {2, 2};
    tempOpDescPtr->SetAttr("dilations", AttrValue::CreateFrom(dilations));
    AttrValue::LIST_INT strides = {5, 5};
    tempOpDescPtr->SetAttr("strides", AttrValue::CreateFrom(strides));
    AttrValue::LIST_INT pads = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pads", AttrValue::CreateFrom(pads));

    tempOpDescPtr->SetAttr("format", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    tempOpDescPtr->SetAttr("groups", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // VAILD
    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::STR>("VALID")));
    tempOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    float data1[4 * 3 * 2 * 2];
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * 3 * 2 * 2 * sizeof(float));

    float data2[4];
    TensorDesc tensorDesc2(Shape({1, 4, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data2, 4 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*tempNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_power_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({1, 4, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr powerOpDescPtr = std::make_shared<OpDesc>("Power", "Power");
    TensorDesc outDesc;
    powerOpDescPtr->AddInputDesc(tensorDesc1);
    powerOpDescPtr->AddOutputDesc(outDesc);
    Node* powerNodePtr = graphPtr->ROLE(GraphModifier).AddNode(powerOpDescPtr);
    ASSERT_NE(powerNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*powerNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = powerNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_activation_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({1, 4, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr activationOpDescPtr = std::make_shared<OpDesc>("Activation", "Activation");
    TensorDesc outDesc;
    activationOpDescPtr->AddInputDesc(tensorDesc1);
    activationOpDescPtr->AddOutputDesc(outDesc);
    Node* activationNodePtr = graphPtr->ROLE(GraphModifier).AddNode(activationOpDescPtr);
    ASSERT_NE(activationNodePtr, nullptr);
    activationOpDescPtr->SetAttr("mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*activationNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = activationNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_concat_infershape_failed)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);
    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 2, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);
    OpDescPtr dataOpdescPtr2 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc2(Shape({3, 4, 5, 5}));
    tensorDesc2.SetDataType(DT_FLOAT);
    dataOpdescPtr2->AddInputDesc(tensorDesc2);
    dataOpdescPtr2->AddOutputDesc(tensorDesc2);
    Node* dataNodePtr2 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr2);
    ASSERT_NE(dataNodePtr2, nullptr);
    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Activation", "Activation");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddInputDesc(tensorDesc2);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr2, 0), Endpoint(*tempNodePtr, 2)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_concat_infershape_failed_NoneRequiredAttr_concat_dim)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 2, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);

    OpDescPtr dataOpdescPtr2 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc2(Shape({3, 4, 5, 5}));
    tensorDesc2.SetDataType(DT_FLOAT);
    dataOpdescPtr2->AddInputDesc(tensorDesc2);
    dataOpdescPtr2->AddOutputDesc(tensorDesc2);
    Node* dataNodePtr2 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr2);
    ASSERT_NE(dataNodePtr2, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("ConcatD", "ConcatD");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddInputDesc(tensorDesc2);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr2, 0), Endpoint(*tempNodePtr, 2)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_reshape_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({4, 3, 3, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr reshapeOpDescPtr = std::make_shared<OpDesc>("Reshape", "Reshape");
    TensorDesc outDesc;
    reshapeOpDescPtr->AddInputDesc(tensorDesc1);
    reshapeOpDescPtr->AddOutputDesc(outDesc);
    Node* reshapeNodePtr = graphPtr->ROLE(GraphModifier).AddNode(reshapeOpDescPtr);
    ASSERT_NE(reshapeNodePtr, nullptr);

    AttrValue::LIST_INT shape = {-1};
    reshapeOpDescPtr->SetAttr("shape", AttrValue::CreateFrom(shape));
    reshapeOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    reshapeOpDescPtr->SetAttr("num_axes", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*reshapeNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = reshapeNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(72, out.GetDim(0));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_concat_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 2, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);

    OpDescPtr dataOpdescPtr2 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc2(Shape({3, 4, 5, 5}));
    tensorDesc2.SetDataType(DT_FLOAT);
    dataOpdescPtr2->AddInputDesc(tensorDesc2);
    dataOpdescPtr2->AddOutputDesc(tensorDesc2);
    Node* dataNodePtr2 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr2);
    ASSERT_NE(dataNodePtr2, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("ConcatD", "ConcatD");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddInputDesc(tensorDesc2);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("concat_dim", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr2, 0), Endpoint(*tempNodePtr, 2)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(3, out.GetDim(0));
    EXPECT_EQ(9, out.GetDim(1));
    EXPECT_EQ(5, out.GetDim(2));
    EXPECT_EQ(5, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_argmax_infershape_caffe_axis_negtive)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({2, 2, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr argmaxOpDescPtr = std::make_shared<OpDesc>("ArgMaxExt2", "ArgMaxExt2");
    TensorDesc outDesc;
    argmaxOpDescPtr->AddInputDesc(tensorDesc1);
    argmaxOpDescPtr->AddOutputDesc(outDesc);
    Node* argmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(argmaxOpDescPtr);
    ASSERT_NE(argmaxNodePtr, nullptr);

    argmaxOpDescPtr->SetAttr("output_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(3)));
    argmaxOpDescPtr->SetAttr("axis_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    argmaxOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*argmaxNodePtr, 0)),
        hiai::SUCCESS);

    int32_t axisInput[1] = {-1};
    TensorDesc tensorDescAxis(Shape({1}));
    tensorDescAxis.SetDataType(DT_INT32);
    TensorPtr tensorAxis = std::make_shared<Tensor>(tensorDescAxis, (const uint8_t*)axisInput, 1 * sizeof(int));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorAxis);
    OpDescUtils::SetWeights(*argmaxNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = argmaxNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);
    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(2, out.GetDim(1));
    EXPECT_EQ(2, out.GetDim(2));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_argmax_infershape_caffe_axis_type_2)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({2, 2, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr argmaxOpDescPtr = std::make_shared<OpDesc>("ArgMaxExt2", "ArgMaxExt2");
    TensorDesc outDesc;
    argmaxOpDescPtr->AddInputDesc(tensorDesc1);
    argmaxOpDescPtr->AddOutputDesc(outDesc);
    Node* argmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(argmaxOpDescPtr);
    ASSERT_NE(argmaxNodePtr, nullptr);

    argmaxOpDescPtr->SetAttr("output_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(3)));
    argmaxOpDescPtr->SetAttr("axis_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));
    argmaxOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));
    argmaxOpDescPtr->SetAttr("topk", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*argmaxNodePtr, 0)),
        hiai::SUCCESS);

    int32_t axisInput[1] = {-1};
    TensorDesc tensorDescAxis(Shape({1}));
    tensorDescAxis.SetDataType(DT_INT32);
    TensorPtr tensorAxis = std::make_shared<Tensor>(tensorDescAxis, (const uint8_t*)axisInput, 1 * sizeof(int));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorAxis);
    OpDescUtils::SetWeights(*argmaxNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
    TensorDesc output = argmaxNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);
    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(2, out.GetDim(1));
    EXPECT_EQ(2, out.GetDim(2));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_argmax_infershape_tf_IR_keepdims_true)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({2, 1, 2, 4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr argmaxOpDescPtr = std::make_shared<OpDesc>("ArgMaxExt2", "ArgMaxExt2");
    TensorDesc outDesc;
    argmaxOpDescPtr->AddInputDesc(tensorDesc1);
    argmaxOpDescPtr->AddOutputDesc(outDesc);
    Node* argmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(argmaxOpDescPtr);
    ASSERT_NE(argmaxNodePtr, nullptr);

    argmaxOpDescPtr->SetAttr("axis_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));
    argmaxOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));
    argmaxOpDescPtr->SetAttr("topk", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    argmaxOpDescPtr->SetAttr("output_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(3)));
    argmaxOpDescPtr->SetAttr("keep_dims", AttrValue::CreateFrom(true));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*argmaxNodePtr, 0)),
        hiai::SUCCESS);

    int32_t axisInput[1] = {2};
    TensorDesc tensorDescAxis(Shape({1}));
    tensorDescAxis.SetDataType(DT_INT32);
    TensorPtr tensorAxis = std::make_shared<Tensor>(tensorDescAxis, (const uint8_t*)axisInput, 1 * sizeof(int));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorAxis);
    OpDescUtils::SetWeights(*argmaxNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
    TensorDesc output = argmaxNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);
    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(1, out.GetDim(1));
    EXPECT_EQ(1, out.GetDim(2));
    EXPECT_EQ(4, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_argmax_infershape_tf_IR_keepdims_false)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({2, 2, 1, 1}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);


    OpDescPtr argmaxOpDescPtr = std::make_shared<OpDesc>("ArgMaxExt2", "ArgMaxExt2");
    TensorDesc outDesc;
    argmaxOpDescPtr->AddInputDesc(tensorDesc1);
    argmaxOpDescPtr->AddOutputDesc(outDesc);
    Node* argmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(argmaxOpDescPtr);
    ASSERT_NE(argmaxNodePtr, nullptr);

    argmaxOpDescPtr->SetAttr("axis_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));
    argmaxOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));
    argmaxOpDescPtr->SetAttr("topk", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    argmaxOpDescPtr->SetAttr("output_type", AttrValue::CreateFrom(static_cast<AttrValue::INT>(3)));
    argmaxOpDescPtr->SetAttr("keep_dims", AttrValue::CreateFrom(false));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*argmaxNodePtr, 0)),
        hiai::SUCCESS);

    int32_t axisInput[1] = {2};
    TensorDesc tensorDescAxis(Shape({1}));
    tensorDescAxis.SetDataType(DT_INT32);
    TensorPtr tensorAxis = std::make_shared<Tensor>(tensorDescAxis, (const uint8_t*)axisInput, 1 * sizeof(float));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorAxis);
    OpDescUtils::SetWeights(*argmaxNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
    TensorDesc output = argmaxNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);
    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(2, out.GetDim(1));
    EXPECT_EQ(1, out.GetDim(2));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_interp_infershape_tf_IR_keepdims_false)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr interpOpDescPtr = std::make_shared<OpDesc>("Interp", "Interp");
    TensorDesc outDesc;
    TensorDesc tensorDesc1(Shape({2, 2, 1, 1}));
    tensorDesc1.SetDataType(DT_FLOAT);
    interpOpDescPtr->AddInputDesc(tensorDesc1);
    interpOpDescPtr->AddOutputDesc(outDesc);
    Node* interpNodePtr = graphPtr->ROLE(GraphModifier).AddNode(interpOpDescPtr);
    ASSERT_NE(interpNodePtr, nullptr);

    interpOpDescPtr->SetAttr("pad_begin", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    interpOpDescPtr->SetAttr("pad_end", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    interpOpDescPtr->SetAttr("height", AttrValue::CreateFrom(static_cast<AttrValue::INT>(10)));
    interpOpDescPtr->SetAttr("width", AttrValue::CreateFrom(static_cast<AttrValue::INT>(10)));

    float data1[4] = {1, 2, 3, 4};
    TensorPtr tensor1 = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)data1, 4 * sizeof(float));

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor1);

    TensorDesc inOutTensorDesc1 = ge::TensorDesc();
    auto inputDesc1 = std::make_shared<OpDesc>("Data", "Data");
    inputDesc1->AddInputDesc(tensorDesc1);
    inputDesc1->AddOutputDesc(tensorDesc1);
    auto inputNode1 = graphPtr->ROLE(GraphModifier).AddNode(inputDesc1);
    ASSERT_NE(inputNode1, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*inputNode1, 0), Endpoint(*interpNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
    TensorDesc output = interpNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);
    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(2, out.GetDim(1));
    EXPECT_EQ(10, out.GetDim(2));
    EXPECT_EQ(10, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_elwise_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 3, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Eltwise", "Eltwise");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    tempOpDescPtr->SetAttr("N", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(3, out.GetDim(0));
    EXPECT_EQ(3, out.GetDim(1));
    EXPECT_EQ(5, out.GetDim(2));
    EXPECT_EQ(5, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_elwise_infershape_failed_N_not_equal_inputsize)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 3, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Eltwise", "Eltwise");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    tempOpDescPtr->SetAttr("N", AttrValue::CreateFrom(static_cast<AttrValue::INT>(3))); // N != input.size

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_elwise_infershape_failed_nothas_RequiredAttrN)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({3, 3, 5, 5}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr dataOpdescPtr1 = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({3, 3, 5, 5}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr1->AddInputDesc(tensorDesc1);
    dataOpdescPtr1->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr1 = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr1);
    ASSERT_NE(dataNodePtr1, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("Eltwise", "Eltwise");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc1);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr1, 0), Endpoint(*tempNodePtr, 1)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_createscaleinput_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    scaleOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    scaleOpDescPtr->SetAttr("filler_value", AttrValue::CreateFrom(static_cast<AttrValue::FLOAT>(1.0)));
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = scaleNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_createscaleinput_twoinput_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    scaleOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    scaleOpDescPtr->SetAttr("filler_value", AttrValue::CreateFrom(static_cast<AttrValue::FLOAT>(1.0)));
    scaleOpDescPtr->SetAttr("bias_term", AttrValue::CreateFrom(true));
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);
    // input1 const
    float scale[4] = {0.0};
    TensorDesc tensorDesc1(Shape({4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensorScale = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)scale, 4 * sizeof(float));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorScale);
    OpDescUtils::SetWeights(*scaleNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = scaleNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}


TEST_F(UTEST_omg_graph_compute_graph, graph_scale_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);
    // input1 const
    float scale[4] = {0.0};
    TensorDesc tensorDesc1(Shape({4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensorScale = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)scale, 4 * sizeof(float));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorScale);
    OpDescUtils::SetWeights(*scaleNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = scaleNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_scale_infershape2_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);
    // input1 const
    float scale[4] = {0.0};
    TensorDesc tensorDesc1(Shape({4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensorScale = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)scale, 4 * sizeof(float));

    float bias[4] = {0.0};
    TensorDesc tensorDesc2(Shape({4}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensorBias = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)bias, 4 * sizeof(float));

    vector<ge::TensorPtr> weights;
    weights.push_back(tensorScale);
    weights.push_back(tensorBias);
    OpDescUtils::SetWeights(*scaleNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = scaleNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_scale_infershape_failed_Attr_scale_from_blob_not_false)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    // only support false
    scaleOpDescPtr->SetAttr("scale_from_blob", AttrValue::CreateFrom(true));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);
    // input1 const
    float scale[4] = {0.0};
    TensorDesc tensorDesc1(Shape({4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensorScale = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)scale, 4 * sizeof(float));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorScale);
    OpDescUtils::SetWeights(*scaleNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_scale_infershape_failed_Attr_num_axes_not_1)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");

    // input 0
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr scaleOpDescPtr = std::make_shared<OpDesc>("Scale", "Scale");
    TensorDesc outDesc;
    scaleOpDescPtr->AddInputDesc(tensorDesc);
    scaleOpDescPtr->AddOutputDesc(outDesc);
    Node* scaleNodePtr = graphPtr->ROLE(GraphModifier).AddNode(scaleOpDescPtr);
    ASSERT_NE(scaleNodePtr, nullptr);

    scaleOpDescPtr->SetAttr("scale_from_blob", AttrValue::CreateFrom(false));
    // only support 1
    scaleOpDescPtr->SetAttr("num_axes", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*scaleNodePtr, 0)),
        hiai::SUCCESS);
    // input1 const
    float scale[4] = {0.0};
    TensorDesc tensorDesc1(Shape({4}));
    tensorDesc1.SetDataType(DT_FLOAT);
    TensorPtr tensorScale = std::make_shared<Tensor>(tensorDesc1, (const uint8_t*)scale, 4 * sizeof(float));
    vector<ge::TensorPtr> weights;
    weights.push_back(tensorScale);
    OpDescUtils::SetWeights(*scaleNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_softmax_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({1, 4, 3, 3}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr softmaxOpDescPtr = std::make_shared<OpDesc>("Softmax", "Softmax");
    TensorDesc outDesc;
    softmaxOpDescPtr->AddInputDesc(tensorDesc1);
    softmaxOpDescPtr->AddOutputDesc(outDesc);
    Node* softmaxNodePtr = graphPtr->ROLE(GraphModifier).AddNode(softmaxOpDescPtr);
    ASSERT_NE(softmaxNodePtr, nullptr);

    softmaxOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(-1)));
    softmaxOpDescPtr->SetAttr("algo", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*softmaxNodePtr, 0)),
        hiai::SUCCESS);
    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = softmaxNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_fc_infershape_failed_not_has_RequiredAttr_num_output)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr fullConnectionOpDescPtr = std::make_shared<OpDesc>("FullyConnection", "FullyConnection");
    TensorDesc outDesc;
    fullConnectionOpDescPtr->AddInputDesc(tensorDesc1);
    fullConnectionOpDescPtr->AddOutputDesc(outDesc);
    Node* fullConnectionNodePtr = graphPtr->ROLE(GraphModifier).AddNode(fullConnectionOpDescPtr);
    ASSERT_NE(fullConnectionNodePtr, nullptr);

    float data1[48];
    TensorDesc tensorDesc2(Shape({4, 3, 2, 2}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data1, 48 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*fullConnectionNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*fullConnectionNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_fc_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data",  std::string{ hiai::op::Data::TYPE });
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr fullConnectionOpDescPtr = std::make_shared<OpDesc>("FullyConnection",
        std::string{ hiai::op::FullyConnection::TYPE });
    (void)ge::AttrUtils::SetInt(fullConnectionOpDescPtr, hiai::ATTR_NAME_INFERRED_FORMAT, 1);
    (void)ge::AttrUtils::SetInt(fullConnectionOpDescPtr, hiai::op::FullyConnection::num_output, 4);

    TensorDesc outDesc;
    fullConnectionOpDescPtr->AddInputDesc(tensorDesc1);
    fullConnectionOpDescPtr->AddOutputDesc(outDesc);
    Node* fullConnectionNodePtr = graphPtr->ROLE(GraphModifier).AddNode(fullConnectionOpDescPtr);
    ASSERT_NE(fullConnectionNodePtr, nullptr);

    float data1[12];
    TensorDesc tensorDesc2(Shape({4, 3, 1, 1}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data1, 12 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*fullConnectionNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*fullConnectionNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));
}

Node* AddDataNode(std::shared_ptr<ComputeGraph> &graphPtr, TensorDesc tensorDesc)
{
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    return dataNodePtr;
}

TEST_F(UTEST_omg_graph_compute_graph, graph_fc_infershape_failed_Attr_transpose_not_false)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);

    Node* dataNodePtr = AddDataNode(graphPtr, tensorDesc1);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr fullConnectionOpDescPtr = std::make_shared<OpDesc>("FullyConnection", "FullyConnection");
    TensorDesc outDesc;
    fullConnectionOpDescPtr->AddInputDesc(tensorDesc1);
    fullConnectionOpDescPtr->AddOutputDesc(outDesc);
    Node* fullConnectionNodePtr = graphPtr->ROLE(GraphModifier).AddNode(fullConnectionOpDescPtr);
    ASSERT_NE(fullConnectionNodePtr, nullptr);

    fullConnectionOpDescPtr->SetAttr("num_output", AttrValue::CreateFrom(static_cast<AttrValue::INT>(4)));
    // transpose not use default false,is failed.
    fullConnectionOpDescPtr->SetAttr("transpose", AttrValue::CreateFrom(true));

    float data1[48];
    TensorDesc tensorDesc2(Shape({4, 3, 2, 2}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data1, 48 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*fullConnectionNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*fullConnectionNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_fc_infershape_failed_Attr_axis_not_1)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);

    Node* dataNodePtr = AddDataNode(graphPtr, tensorDesc1);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr fullConnectionOpDescPtr = std::make_shared<OpDesc>("FullyConnection", "FullyConnection");
    TensorDesc outDesc;
    fullConnectionOpDescPtr->AddInputDesc(tensorDesc1);
    fullConnectionOpDescPtr->AddOutputDesc(outDesc);
    Node* fullConnectionNodePtr = graphPtr->ROLE(GraphModifier).AddNode(fullConnectionOpDescPtr);
    ASSERT_NE(fullConnectionNodePtr, nullptr);

    fullConnectionOpDescPtr->SetAttr("num_output", AttrValue::CreateFrom(static_cast<AttrValue::INT>(4)));
    fullConnectionOpDescPtr->SetAttr("transpose", AttrValue::CreateFrom(false));
    // only support 1
    fullConnectionOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(2)));

    float data1[48];
    TensorDesc tensorDesc2(Shape({4, 3, 2, 2}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data1, 48 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*fullConnectionNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*fullConnectionNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_fc_infershape_failed_Attr_offset_x_not_0)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc1(Shape({4, 3, 2, 2}));
    tensorDesc1.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc1);
    dataOpdescPtr->AddOutputDesc(tensorDesc1);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr fullConnectionOpDescPtr = std::make_shared<OpDesc>("FullyConnection", "FullyConnection");
    TensorDesc outDesc;
    fullConnectionOpDescPtr->AddOutputDesc(outDesc);
    fullConnectionOpDescPtr->AddInputDesc(tensorDesc1);
    Node* fullConnectionNodePtr = graphPtr->ROLE(GraphModifier).AddNode(fullConnectionOpDescPtr);
    ASSERT_NE(fullConnectionNodePtr, nullptr);

    fullConnectionOpDescPtr->SetAttr("num_output", AttrValue::CreateFrom(static_cast<AttrValue::INT>(4)));
    fullConnectionOpDescPtr->SetAttr("transpose", AttrValue::CreateFrom(false));
    fullConnectionOpDescPtr->SetAttr("axis", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));
    // only support 0
    fullConnectionOpDescPtr->SetAttr("offset_x", AttrValue::CreateFrom(static_cast<AttrValue::INT>(1)));

    float data1[48];
    TensorDesc tensorDesc2(Shape({4, 3, 2, 2}));
    tensorDesc2.SetDataType(DT_FLOAT);
    TensorPtr tensor2 = std::make_shared<Tensor>(tensorDesc2, (const uint8_t*)data1, 48 * sizeof(float));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*fullConnectionNodePtr, 0)),
        hiai::SUCCESS);

    vector<ge::TensorPtr> weights;
    weights.push_back(tensor2);
    OpDescUtils::SetWeights(*fullConnectionNodePtr, weights);

    EXPECT_EQ(ge::GRAPH_FAILED, GraphInfershapeUtil::InferShape(*graphPtr));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_netoutput_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({1, 4, 3, 3}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr netoutputOpDescPtr = std::make_shared<OpDesc>("NetOutput", "NetOutput");
    TensorDesc outDesc;
    netoutputOpDescPtr->AddInputDesc(dataOpdescPtr->GetOutputDesc(0));
    netoutputOpDescPtr->AddOutputDesc(outDesc);
    Node* netoutputNodePtr = graphPtr->ROLE(GraphModifier).AddNode(netoutputOpDescPtr);
    ASSERT_NE(netoutputNodePtr, nullptr);

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*netoutputNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = netoutputNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(1, out.GetDim(0));
    EXPECT_EQ(4, out.GetDim(1));
    EXPECT_EQ(3, out.GetDim(2));
    EXPECT_EQ(3, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_pooling_pad_same_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("PoolingD", "PoolingD");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(6)));
    AttrValue::LIST_INT stride = {1, 1};
    tempOpDescPtr->SetAttr("stride", AttrValue::CreateFrom(stride));
    AttrValue::LIST_INT window = {2, 2};
    tempOpDescPtr->SetAttr("window", AttrValue::CreateFrom(window));
    AttrValue::LIST_INT pad = {0, 0, 0, 0};
    tempOpDescPtr->SetAttr("pad", AttrValue::CreateFrom(pad));
    tempOpDescPtr->SetAttr("global_pooling", AttrValue::CreateFrom(false));
    tempOpDescPtr->SetAttr("ceil_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(3, out.GetDim(1));
    EXPECT_EQ(32, out.GetDim(2));
    EXPECT_EQ(32, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_pooling_pad_not_set_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("PoolingD", "PoolingD");
    TensorDesc outDesc;
    tempOpDescPtr->AddOutputDesc(outDesc);
    tempOpDescPtr->AddInputDesc(tensorDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));
    AttrValue::LIST_INT stride = {1, 1};
    tempOpDescPtr->SetAttr("stride", AttrValue::CreateFrom(stride));
    AttrValue::LIST_INT window = {2, 2};
    tempOpDescPtr->SetAttr("window", AttrValue::CreateFrom(window));
    AttrValue::LIST_INT pad = {1, 1, 1, 1};
    tempOpDescPtr->SetAttr("pad", AttrValue::CreateFrom(pad));
    tempOpDescPtr->SetAttr("global_pooling", AttrValue::CreateFrom(false));
    tempOpDescPtr->SetAttr("ceil_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    Shape out = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0).GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(3, out.GetDim(1));
    EXPECT_EQ(33, out.GetDim(2));
    EXPECT_EQ(33, out.GetDim(3));
}

TEST_F(UTEST_omg_graph_compute_graph, graph_pooling_pad_valid_infershape_success)
{
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    OpDescPtr dataOpdescPtr = std::make_shared<OpDesc>("Data", "Data");
    TensorDesc tensorDesc(Shape({2, 3, 32, 32}));
    tensorDesc.SetDataType(DT_FLOAT);
    dataOpdescPtr->AddInputDesc(tensorDesc);
    dataOpdescPtr->AddOutputDesc(tensorDesc);
    Node* dataNodePtr = graphPtr->ROLE(GraphModifier).AddNode(dataOpdescPtr);
    ASSERT_NE(dataNodePtr, nullptr);

    OpDescPtr tempOpDescPtr = std::make_shared<OpDesc>("PoolingD", "PoolingD");
    TensorDesc outDesc;
    tempOpDescPtr->AddInputDesc(tensorDesc);
    tempOpDescPtr->AddOutputDesc(outDesc);
    Node* tempNodePtr = graphPtr->ROLE(GraphModifier).AddNode(tempOpDescPtr);
    ASSERT_NE(tempNodePtr, nullptr);

    tempOpDescPtr->SetAttr("pad_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(5)));
    AttrValue::LIST_INT stride = {1, 1};
    tempOpDescPtr->SetAttr("stride", AttrValue::CreateFrom(stride));
    AttrValue::LIST_INT window = {2, 2};
    tempOpDescPtr->SetAttr("window", AttrValue::CreateFrom(window));
    AttrValue::LIST_INT pad = {0, 0, 0, 0};
    tempOpDescPtr->SetAttr("global_pooling", AttrValue::CreateFrom(false));
    tempOpDescPtr->SetAttr("ceil_mode", AttrValue::CreateFrom(static_cast<AttrValue::INT>(0)));

    EXPECT_EQ(graphPtr->ROLE(GraphModifier).AddEdge(Endpoint(*dataNodePtr, 0), Endpoint(*tempNodePtr, 0)),
        hiai::SUCCESS);

    EXPECT_EQ(ge::GRAPH_SUCCESS, GraphInfershapeUtil::InferShape(*graphPtr));

    TensorDesc output = tempNodePtr->ROLE(NodeSpec).OpDesc().GetOutputDesc(0);

    Shape out = output.GetShape();
    EXPECT_EQ(2, out.GetDim(0));
    EXPECT_EQ(3, out.GetDim(1));
    EXPECT_EQ(31, out.GetDim(2));
    EXPECT_EQ(31, out.GetDim(3));
}
TEST_F(UTEST_omg_graph_compute_graph, graph_utill)
{
    // make graph data --> Conv-->Add
    std::shared_ptr<ComputeGraph> graphPtr = ge::ComputeGraph::Make("ut_test_graph");
    TensorDesc tensorDesc;

    OpDescPtr opDescPtr = std::make_shared<OpDesc>("Data", "Data");
    opDescPtr->AddInputDesc(tensorDesc);
    opDescPtr->AddOutputDesc(tensorDesc);
    Node* nodePtr = graphPtr->ROLE(GraphModifier).AddNode(opDescPtr);
    ASSERT_NE(nodePtr, nullptr);

    OpDescPtr convOpDescPtr = std::make_shared<OpDesc>("Conv", "Conv");
    convOpDescPtr->AddInputDesc(tensorDesc);
    convOpDescPtr->AddOutputDesc(tensorDesc);
    Node* convNode = graphPtr->ROLE(GraphModifier).AddNode(convOpDescPtr);
    ASSERT_NE(convNode, nullptr);
    NodePtr convNodePtr = graphPtr->ROLE(GraphFinder).FindNodePtr(*convNode);
    ASSERT_NE(convNodePtr, nullptr);

    OpDescPtr addOpDescPtr = std::make_shared<OpDesc>("Add", "Add");
    addOpDescPtr->AddInputDesc(tensorDesc);
    addOpDescPtr->AddOutputDesc(tensorDesc);
    Node* addNode = graphPtr->ROLE(GraphModifier).AddNode(addOpDescPtr);
    ASSERT_NE(addNode, nullptr);
    NodePtr addNodePtr = graphPtr->ROLE(GraphFinder).FindNodePtr(*addNode);
    ASSERT_NE(addNodePtr, nullptr);

    graphPtr->ROLE(GraphModifier).AddEdge({*convNodePtr, 2}, {*addNodePtr, 1});
    Edge e(Endpoint(*convNodePtr, 0), Endpoint(*addNodePtr, 0));
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).RemoveEdge(e), hiai::FAILURE);
    EXPECT_EQ(graphPtr->ROLE(GraphModifier).RemoveEdge(Endpoint(*convNodePtr, 0), Endpoint(*addNodePtr, 0)),
        hiai::FAILURE);
    std::vector<std::string> namesTmp;
    GraphUtils::RecordOriginalNames(namesTmp, addNodePtr);
    EXPECT_EQ(NodeUtils::SetAllAnchorStatus(*addNodePtr), GRAPH_SUCCESS);
    std::shared_ptr<ComputeGraph> graphNew = GraphUtils::Clone(graphPtr);
    EXPECT_EQ(graphNew != nullptr, true);
}
