#include <gtest/gtest.h>
#include <iostream>
#include "framework/graph/core/cgraph/graph_spec.h"
#define protected public
#define private public
//#include "framework/graph/operator_reg.h"
#include "framework/graph/graph.h"
#include "framework/graph/model.h"
#include "framework/graph/compatible/all_ops.h"
#include "framework/graph/utils/tensor_utils.h"
#undef protected
#undef private

using namespace std;
using namespace ge;
using namespace op;

class ge_test_operator_reg : public testing::Test {
protected:
    void SetUp()
    {
    }

    void TearDown()
    {
    }
};

TEST_F(ge_test_operator_reg, ge_test_operator_reg_test)
{
    TensorDesc desc(Shape({1,3,224,224}));
    uint32_t size = desc.GetShape().GetTotalDimNum();
    ge::TensorUtils::SetSize(desc, size);
    auto data = op::Data("Data").set_attr_index(0);
    data.update_input_desc_data(desc);

    auto flatten = op::Flatten("Flatten").set_input_x(data, data.OUTPUT_NAME_out);

    std::vector<Operator> inputs{data};
    std::vector<Operator> outputs{flatten};

    Graph graph("test_graph");
    graph.SetInputs(inputs).SetOutputs(outputs);
    EXPECT_EQ(true, graph.ROLE(GraphSpec).IsValid());
}
