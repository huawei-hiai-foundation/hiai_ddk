#!/usr/bin/env python
# encoding: utf-8
# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import os
import sys
import zipfile
import tarfile
import argparse

sys.path.append("build/script")
from handle_config import *
from handle_ddk import *

CONFIG_FILE = "build/config/build.conf"

def parse_args():
    parser = argparse.ArgumentParser(description="parse")
    parser.add_argument('--ddk',  action='store_true', help='run build ddk')
    parser.add_argument('--test',  action='store_true', help='run ut test')
    parser.add_argument('-t', '--target_os', type=str, choices=['android', 'ohos'])

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    parse_args = parse_args()
    print(parse_args)

    is_run_build = parse_args.ddk
    is_run_test = parse_args.test
    target_os = parse_args.target_os

    if not is_run_build and not is_run_test:
        print("[error] please specify the specific compilation command, use --ddk or --test")
        sys.exit(-1)

    config = HandleConfig(CONFIG_FILE, target_os)

    # read build config
    if not config.get_config():
        print("get_config error.")
        sys.exit(-1)

    # init
    ddk = HandleDDK(config, target_os)

    # run build and package
    if is_run_build:
        if not target_os:
            print("[error] please specify the target_os.")
            sys.exit(-1)
        ddk.run_build()
        ddk.run_repack()

    # run test
    if is_run_test:
        ddk.run_test()
