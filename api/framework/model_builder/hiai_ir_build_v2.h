/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_API_HIAI_IR_BUILD_V2_H
#define HIAI_API_HIAI_IR_BUILD_V2_H

#include "hiai_ir_build.h"
#include "model_builder/model_builder_types.h"
#include "model/built_model.h"

namespace hiai {
class HiaiIrBuildV2 {
public:
    Status Build(const hiai::ModelBuildOptions& options, const std::string& modelName,
        const std::shared_ptr<ge::Model>& model, std::shared_ptr<hiai::IBuiltModel>& builtModel);
};
}

#endif