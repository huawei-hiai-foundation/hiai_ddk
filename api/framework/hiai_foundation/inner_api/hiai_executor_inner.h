/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

/**
 * @addtogroup HiAIFoundation
 * @{
 *
 * @brief Provides APIs for HiAI Foundation model inference.
 *
 * @syscap SystemCapability.AI.HiAIFoundation
 * @since 4.1.0(11)
 */

/**
 * @file hiai_executor_inner.h
 * @kit HiAIFoundationKit
 * @library libhiai_foundation.so
 * @syscap SystemCapability.AI.HiAIFoundation
 *
 * @brief Auxiliary APIs
 *
 *
 * @since 5.0.0(12)
 */

#ifndef HIAI_FOUNDATION_EXECUTOR_INNER_H
#define HIAI_FOUNDATION_EXECUTOR_INNER_H
#include "neural_network_runtime/neural_network_runtime_type.h"

#ifdef __cplusplus
extern "C" {
#endif

OH_NN_ReturnCode HMS_HiAIExecutor_InitWeights(OH_NNExecutor *executor, const char* weightDir);

OH_NN_ReturnCode HMS_HiAIExecutor_GetWeightBuffer(OH_NNExecutor *executor, const char* weightName, void** data, size_t* size);

OH_NN_ReturnCode HMS_HiAIExecutor_FlushWeight(OH_NNExecutor *executor, const char* weightName, size_t offset, size_t size);

OH_NN_ReturnCode HMS_HiAIExecutor_GetModelID(OH_NNExecutor *executor, uint32_t* modelId);

#ifdef __cplusplus
}
#endif

/** @} */
#endif // HIAI_FOUNDATION_EXECUTOR_INNER_H
