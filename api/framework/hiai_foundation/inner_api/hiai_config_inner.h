/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

/**
 * @addtogroup HiAIFoundation
 * @{
 *
 * @brief Provides APIs for HiAI Foundation model inference.
 *
 * @syscap SystemCapability.AI.HiAIFoundation
 * @since 4.1.0(11)
 */

/**
 * @file hiai_config_inner.h
 * @kit HiAIFoundationKit
 * @library libhiai_foundation.so
 * @syscap SystemCapability.AI.HiAIFoundation
 *
 * @brief Auxiliary APIs
 *
 *
 * @since 5.0.0(12)
 */

#ifndef HIAI_FOUNDATION_CONFIG_INNER_H
#define HIAI_FOUNDATION_CONFIG_INNER_H
#include "neural_network_runtime/neural_network_runtime_type.h"

#ifdef __cplusplus
extern "C" {
#endif

OH_NN_ReturnCode HMS_HiAIConfig_SetComputeIOBandWidthMode(uint32_t pid, uint32_t modelId, uint32_t bandMode);

#ifdef __cplusplus
}
#endif

/** @} */
#endif // HIAI_FOUNDATION_CONFIG_INNER_H
