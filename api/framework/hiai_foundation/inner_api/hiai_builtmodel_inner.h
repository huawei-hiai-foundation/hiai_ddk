/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

/**
 * @addtogroup HiAIFoundation
 * @{
 *
 * @brief Provides APIs for HiAI Foundation model inference.
 *
 * @syscap SystemCapability.AI.HiAIFoundation
 * @since 4.1.0(11)
 */

/**
 * @file hiai_builtmodel_inner.h
 * @kit HiAIFoundationKit
 * @library libhiai_foundation.so
 * @syscap SystemCapability.AI.HiAIFoundation
 *
 * @brief Defines the API for builtmodel.
 *
 * You can call the following APIs to get model feature map memory size.
 *
 * @since 5.0.0(12)
 */
#ifndef HIAI_FOUNDATION_BUILT_MODEL_INNER_H
#define HIAI_FOUNDATION_BUILT_MODEL_INNER_H
#include "neural_network_runtime/neural_network_runtime_type.h"
#ifdef __cplusplus
extern "C" {
#endif
 
/**
 * @brief Queries the feature map size of a built model.
 *
 * @param [in] compilation Pointer to {@link OH_NNCompilation}. The value cannot be null. Otherwise, a null pointer is
 * returned.
 * @param [out] fmSize Pointer to a variable where the size of the feature map will be stored.
 * @return Function execution result. Returns OH_NN_SUCCESS if the operation is successful; returns an error code
 * otherwise. For details about the error codes, see {@link OH_NN_ReturnCode}.
 */
OH_NN_ReturnCode HMS_HiAIBuiltModel_GetFmMemorySize(OH_NNCompilation* compilation, uint64_t* fmSize);

#ifdef __cplusplus
}
#endif

/** @} */
#endif // HIAI_FOUNDATION_BUILTMODEL_INNER_H