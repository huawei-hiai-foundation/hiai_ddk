/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HIAI_ERROR_TYPES_H
#define FRAMEWORK_HIAI_ERROR_TYPES_H
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t HIAI_Status;

/** 执行成功 */
static const HIAI_Status HIAI_SUCCESS = 0;
/** 执行失败 */
static const HIAI_Status HIAI_FAILURE = 1;
/** 未初始化 */
static const HIAI_Status HIAI_UNINITIALIZED = 2;
/** 非法参数 */
static const HIAI_Status HIAI_INVALID_PARAM = 3;
/** 任务超时 */
static const HIAI_Status HIAI_TIMEOUT = 4;
/** 不支持方法 */
static const HIAI_Status HIAI_UNSUPPORTED = 5;
/** 内存异常 */
static const HIAI_Status HIAI_MEMORY_EXCEPTION = 6;
/** API使用错误 */
static const HIAI_Status HIAI_INVALID_API = 7;
/** 非法指针 */
static const HIAI_Status HIAI_INVALID_POINTER = 8;
/** 计算异常 */
static const HIAI_Status HIAI_CALC_EXCEPTION = 9;
/** 文件不存在 */
static const HIAI_Status HIAI_FILE_NOT_EXIST = 10;
/** 运行失败 */
static const HIAI_Status HIAI_COMM_EXCEPTION = 11;
/** 数据溢出 */
static const HIAI_Status HIAI_DATA_OVERFLOW = 12;
/** 连接异常，出现此情况建议释放所有HiAI Foundation资源，并重新载入模型 */
static const HIAI_Status HIAI_CONNECT_EXCEPTION = 19;

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HIAI_ERROR_TYPES_H
