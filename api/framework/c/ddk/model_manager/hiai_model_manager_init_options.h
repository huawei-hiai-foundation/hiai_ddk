/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_C_MM_HIAI_MODEL_MANAGER_INIT_OPTIONS_H
#define FRAMEWORK_C_MM_HIAI_MODEL_MANAGER_INIT_OPTIONS_H

#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_execute_option_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建模型加载参数指针实例
 *
 * 该方法用于模型加载接口{@link HIAI_MR_ModelManager_Init}，需要设置模型加载参数{@link HIAI_MR_ModelInitOptions}时，创建模型加载参数指针实例。
 * 模型加载完成后，模型加载参数{@link HIAI_MR_ModelInitOptions}不再使用时，需要使用{@link HIAI_MR_ModelInitOptions_Destroy}进行释放，否则将造成内存泄漏。
 *
 * @return 成功返回{@link HIAI_MR_ModelInitOptions}指针，失败返回空指针
 */
AICP_C_API_EXPORT HIAI_MR_ModelInitOptions* HIAI_MR_ModelInitOptions_Create(void);

/**
 * @brief 设置模型加载参数中DDR的性能模式
 *
 * 请根据需要设置合适的值，高性能与功耗成正比，高性能也意味着高功耗
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] devPerf 性能模式，取值参见{@link HIAI_PerfMode}
 */
AICP_C_API_EXPORT void HIAI_MR_ModelInitOptions_SetPerfMode(HIAI_MR_ModelInitOptions* options, HIAI_PerfMode devPerf);

/**
 * @brief 获取模型加载参数中DDR的性能模式
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @return 成功返回性能模式，失败返回{@link HIAI_PERFMODE_UNSET}
 */
AICP_C_API_EXPORT HIAI_PerfMode HIAI_MR_ModelInitOptions_GetPerfMode(const HIAI_MR_ModelInitOptions* options);

/**
 * @brief 释放模型加载参数
 *
 * 该方法用于释放{@link HIAI_MR_ModelInitOptions_Create}创建的指针实例，若不调用，将造成内存泄漏。
 *
 * @param options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例的引用，非空。
 */
AICP_C_API_EXPORT void HIAI_MR_ModelInitOptions_Destroy(HIAI_MR_ModelInitOptions** options);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_MM_HIAI_MODEL_MANAGER_INIT_OPTIONS_H
