/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_C_MM_HIAI_MODEL_BUILDER_H
#define FRAMEWORK_C_MM_HIAI_MODEL_BUILDER_H

#include <stddef.h>

#include "c/hiai_c_api_export.h"
#include "c/hiai_error_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 编译模型
 *
 * 用于将保存的模型数据在设备上进行编译，生成编译后模型{@link HIAI_MR_BuiltModel}指针实例。
 * {@link HIAI_MR_BuiltModel}不使用时，使用{@link HIAI_MR_BuiltModel_Destroy}进行释放，否则将造成内存泄漏。
 *
 * @param [in] options {@link HIAI_MR_ModelBuildOptions}指针实例，可选（不需要时填入空指针）
 * @param [in] modelName 模型的名称，非空，否则返回HIAI_FAILURE
 * @param [in] inputModelData 模型的数据地址，非空，否则返回HIAI_FAILURE
 * @param [in] inputModelSize 模型的数据大小，非0，否则返回HIAI_FAILURE
 * @param [out] builtModel 编译后的模型，存放在{@link HIAI_MR_BuiltModel}的指针实例，非空，否则返回HIAI_FAILURE
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_ModelBuilder_Build(const HIAI_MR_ModelBuildOptions* options,
    const char* modelName, const void* inputModelData, size_t inputModelSize, HIAI_MR_BuiltModel** builtModel);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_MM_HIAI_MODEL_BUILDER_H
