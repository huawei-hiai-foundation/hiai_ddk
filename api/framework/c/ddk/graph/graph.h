/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_GRAPH_H_
#define GE_C_GRAPH_H_

#include "handle_types.h"
#include "c/hiai_c_api_export.h"
#include "c/hiai_error_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建图指针实例。
 *
 * 本方法提供IR构图中输入算子指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] name 图的名称，非空，否则返回失败。
 * @return 执行成功返回{@link ModelHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT GraphHandle HIAI_IR_GraphCreate(ResMgrHandle resMgr, const char* name);

/**
 * @brief 设置图的输入算子。
 *
 * 本方法为IR构图设置图的输入算子。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] graph {@link ConstGraphHandle}的指针实例，非空，否则返回失败。
 * @param [in] inputs[] {@link ConstOpHandle}指针实例数组，非空，否则返回失败。
 * @param [in] inputNum 输入算子数组的大小，不为0，否则返回失败。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_SetInputs(ResMgrHandle resMgr, ConstGraphHandle graph,
    ConstOpHandle inputs[], uint32_t inputNum);

/**
 * @brief 设置图的输出算子。
 *
 * 本方法为IR构图设置图的输出算子。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] graph {@link ConstGraphHandle}的指针实例，非空，否则返回失败。
 * @param [in] outputs[] {@link ConstOpHandle}指针实例数组，非空，否则返回失败。
 * @param [in] outputNum 输出算子数组的大小，不为0，否则返回失败。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_SetOutputs(ResMgrHandle resMgr, ConstGraphHandle graph,
    ConstOpHandle outputs[], uint32_t outputNum);

#ifdef __cplusplus
}
#endif
#endif  // GE_C_GRAPH_H_