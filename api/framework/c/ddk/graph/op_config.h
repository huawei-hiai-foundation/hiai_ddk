/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_OP_CONFIG_H_
#define GE_C_OP_CONFIG_H_

#include "handle_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief {@link HIAI_OpConfig}的参数。
 */
typedef struct HIAI_IR_Input {
    /** 指向{@link ConstOpHandle}对象指针。该值不能为空指针，否则接口调用失败。*/
    ConstOpHandle op;
    /** 输入算子在对端的输出索引。 */
    unsigned int outIndex;
} HIAI_IR_Input;

/**
 * @brief {@link HIAI_OpConfig}的参数。
 */
typedef struct HIAI_IR_Params {
    /** 属性名称。*/
    const char* name;
    /** 指向{@link ConstAttrHandle}对象指针。*/
    ConstAttrHandle attrValue;
} HIAI_IR_Params;

/**
 * @brief {@link HIAI_IR_CreateOp}的输入参数。
 */
typedef struct HIAI_IR_OpConfig {
    /** 算子类型。*/
    const char* opType;
    /** 算子名称。*/
    const char* opName;
    /** 算子输入结构体数组。*/
    HIAI_IR_Input* input;
    /** 算子输入结构体数组大小。*/
    unsigned int inputNums;
    /** 属性结构体数组。*/
    HIAI_IR_Params* params;
    /** 属性结构体数组大小。*/
    unsigned int paramsNums;
    /** 算子输出个数，当算子为动态输出时必须指定。*/
    unsigned int outputNums;
} HIAI_IR_OpConfig;

#ifdef __cplusplus
}
#endif
#endif  // GE_C_OP_CONFIG_H_
