/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_CONTEXT_H_
#define GE_C_CONTEXT_H_

#include "handle_types.h"
#include "c/hiai_c_api_export.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建资源管理器。
 *
 * 本方法用于保存构图过程中指针实例，如ModelHandle, GraphHandle, OpHandle, AttrHandle, TensorHandle。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}进行资源释放，否则将造成内存泄漏。
 *
 * @return 执行成功返回{@link ResMgrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT ResMgrHandle HIAI_IR_ResourceManagerCreate();

/**
 * @brief 销毁资源管理器。
 *
 * 本方法用于销毁资源管理器。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用该函数进行资源释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 */
AICP_C_API_EXPORT void HIAI_IR_ResourceManagerDestroy(ResMgrHandle* resMgr);

#ifdef __cplusplus
}
#endif
#endif  // GE_C_CONTEXT_H_