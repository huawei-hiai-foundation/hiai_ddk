/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_OPERATOR_H_
#define GE_C_OPERATOR_H_

#include <stdint.h>

#include "handle_types.h"
#include "op_config.h"
#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_base_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建输入算子指针实例。
 *
 * 本方法提供IR构图中输入算子指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] opName 所创建算子的名称，非空，否则返回失败。
 * @param [in] format {@link HIAI_Format}格式。
 * @param [in] dataType {@link HIAI_DataType}数据类型。
 * @param [in] inputShape 输入数组，支持最大为5维。
 * @param [in] inputShapeNums 输入数组大小，最大为5。
 * @return 执行成功返回{@link OpHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT OpHandle HIAI_IR_CreatePlaceHodler(ResMgrHandle resMgr, const char* opName,
    HIAI_Format format, HIAI_DataType dataType, int64_t inputShape[], uint32_t inputShapeNums);

/**
 * @brief 创建通用算子指针实例。
 *
 * 本方法提供IR构图中通用算子指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] opConfig {@link HIAI_IR_OpConfig}指针实例，包含所创建算子的输入、属性、输出信息，非空，否则返回失败。
 * @return 执行成功返回{@link OpHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT OpHandle HIAI_IR_CreateOp(ResMgrHandle resMgr, HIAI_IR_OpConfig* opConfig);

#ifdef __cplusplus
}
#endif
#endif  // GE_C_OPERATOR_H_