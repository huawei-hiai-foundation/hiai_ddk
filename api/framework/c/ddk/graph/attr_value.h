/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_ATTR_VALUE_H
#define GE_C_ATTR_VALUE_H

#include <stdint.h>
#include <stddef.h>

#include "context.h"
#include "handle_types.h"
#include "c/hiai_c_api_export.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建int64_t类型的算子属性指针实例。
 *
 * 本方法提供IR构图中int64_t类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateInt64Attr(ResMgrHandle resMgr, int64_t value);

/**
 * @brief 创建bool类型的算子属性指针实例。
 *
 * 本方法提供IR构图中bool类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateBoolAttr(ResMgrHandle resMgr, bool value);

/**
 * @brief 创建float类型的算子属性指针实例。
 *
 * 本方法提供IR构图中float类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateFloatAttr(ResMgrHandle resMgr, float value);

/**
 * @brief 创建string类型的算子属性指针实例。
 *
 * 本方法提供IR构图中string类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateStringAttr(ResMgrHandle resMgr, const char* value);

/**
 * @brief 创建Tensor类型的算子属性指针实例。
 *
 * 本方法提供IR构图中Tensor类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateTensorAttr(ResMgrHandle resMgr, TensorHandle value);

/**
 * @brief 创建int64_t数组类型的算子属性指针实例。
 *
 * 本方法提供IR构图中int64_t数组类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateInt64LstAttr(ResMgrHandle resMgr, int64_t value[], uint32_t num);

/**
 * @brief 创建bool数组类型的算子属性指针实例。
 *
 * 本方法提供IR构图中bool数组属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateBoolLstAttr(ResMgrHandle resMgr, bool value[], uint32_t num);

/**
 * @brief 创建float数组类型的算子属性指针实例。
 *
 * 本方法提供IR构图中float数组类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateFloatLstAttr(ResMgrHandle resMgr, float value[], uint32_t num);

/**
 * @brief 创建string数组类型的算子属性指针实例。
 *
 * 本方法提供IR构图中string数组类型属性指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] value 属性值
 * @return 执行成功返回{@link AttrHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT AttrHandle HIAI_IR_CreateStringLstAttr(ResMgrHandle resMgr, const char *value[], size_t size);

#ifdef __cplusplus
}
#endif

#endif