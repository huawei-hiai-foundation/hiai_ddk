/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_TENSOR_H_
#define GE_C_TENSOR_H_

#include <stdint.h>
#include <stddef.h>

#include "handle_types.h"
#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_base_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建Tensor指针实例。
 *
 * 本方法提供IR构图中输入算子指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] data 数据首地址。
 * @param [in] data_len 数据长度。
 * @param [in] shape 输入数组，支持最大为5维。
 * @param [in] shape_size 输入数组大小，支持最大为5维。
 * @param [in] typeC {@link HIAI_DataType}数据类型。
 * @param [in] formatC {@link HIAI_Format}排布格式。
 * @return 执行成功返回{@link TensorHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT TensorHandle HIAI_IR_CreateTensor(ResMgrHandle resMgr, void* data, size_t dataLen,
    const int64_t shape[], size_t shapeSize, HIAI_DataType typeC, HIAI_Format formatC);

#ifdef __cplusplus
}
#endif
#endif  // GE_C_TENSOR_H_