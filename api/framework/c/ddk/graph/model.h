/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_MODEL_H_
#define GE_C_MODEL_H_

#include "handle_types.h"
#include "c/hiai_c_api_export.h"
#include "c/hiai_error_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建模型指针实例。
 *
 * 本方法提供IR构图中输入算子指针实例的创建。
 * 生成编译后模型{@link HIAI_MR_BuiltModel}指针实例后，使用{@link ResourceManagerDestroy}统一进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] name 模型的名称，非空，否则返回失败。
 * @return 执行成功返回{@link ModelHandle}指针实例，失败返回空指针。
 */
AICP_C_API_EXPORT ModelHandle HIAI_IR_CreateModel(ResMgrHandle resMgr, const char* name);

/**
 * @brief 创建编译后模型。
 *
 * 用于将IR构图后的model通过在线编译，生成编译后模型{@link HIAI_MR_BuiltModel}指针实例。
 * {@link HIAI_MR_BuiltModel}不使用时，使用{@link HIAI_MR_BuiltModel_Destroy}进行释放，否则将造成内存泄漏。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] model {@link ConstModelHandle}的指针实例。
 * @param [in] options {@link HIAI_MR_ModelBuildOptions}模型编译选项选项指针实例。
 * @param [out] builtModel {@link HIAI_MR_BuiltModel}的指针实例。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_CreateBuiltModel(ResMgrHandle resMgr, ConstModelHandle model,
    const HIAI_MR_ModelBuildOptions* options, HIAI_MR_BuiltModel** builtModel);

/**
 * @brief 将模型指针实例转储到文件。
 *
 * 用于将IR构图后的model转储到文件。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] model {@link ConstModelHandle}的指针实例。
 * @param [out] file 保存图模型路径和模型名称，非空，否则返回失败。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_DumpModel(ResMgrHandle resMgr, ConstModelHandle model, const char* file);

/**
 * @brief 将图指针实例设置到模型。
 *
 * 用于设置Graph图到模型。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] graph {@link ConstGraphHandle}的指针实例，非空，否则返回失败。
 * @param [out] model {@link ModelHandle}的指针实例，非空，否则返回失败。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_SetGraph(ResMgrHandle resMgr, ConstGraphHandle graph, ModelHandle model);

/**
 * @brief 将模型指针实例转储到文件。
 *
 * 用于将IR构图后的model转储到文件，与{@link HIAI_IR_DumpModel}区别为转储后的文件包含模型头信息等。
 * 该接口保存的IR模型仅供调试使用，推理请使用{@link HIAI_IR_CreateBuiltModel}。
 *
 * @param [in] resMgr {@link ResMgrHandle}指针实例，用于保存已分配的资源，非空，否则返回失败。
 * @param [in] model {@link ConstModelHandle}的指针实例。
 * @param [out] file 保存图模型路径和模型名称，非空，否则返回失败。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_IR_SaveModel(ResMgrHandle resMgr, ConstModelHandle model, const char* file);

#ifdef __cplusplus
}
#endif
#endif  // GE_C_MODEL_H_