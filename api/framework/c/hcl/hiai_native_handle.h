/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_HIAI_NATIVE_HANDLE_H
#define FRAMEWORK_HCL_HIAI_NATIVE_HANDLE_H

#include "c/hiai_c_api_export.h"
#include "hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 根据buffer_handle_t中的fd、size和offset信息创建HIAI_NativeHandle指针实例。
 *
 * 不使用{@link HIAI_NativeHandle}指针实例时，需要使用{@link HIAI_NativeHandle_Destroy}进行销毁，若不调用，将造成内存泄漏。
 *
 * @param [in] fd buffer_handle_t结构中的文件描述符，大于0
 * @param [in] size buffer_handle_t结构中总内存的大小，以字节为单位，大于0
 * @param [in] offset 使用的数据的偏移，以字节为单位，取值范围[0, size]
 * @return 成功时返回{@link HIAI_NativeHandle}指针实例，失败返回空指针。
 * @see    HIAI_NativeHandle_Destroy
 */
AICP_C_API_EXPORT HIAI_NativeHandle* HIAI_NativeHandle_Create(int fd, int size, int offset);

/**
 * @brief 销毁HIAI_NativeHandle指针实例。
 *
 * 销毁由{@link HIAI_NativeHandle_Create}接口创建的{@link HIAI_NativeHandle}指针实例，传入指针的引用即可。若不调用，将造成内存泄漏。。
 *
 * @param [in] handle {@link HIAI_NativeHandle}指针实例的引用，
 * 当handle不是空指针的时候，接口会释放*handle指向的内存，并使*handle指向空指针；如果handle或者*handle为空指针，直接退出，不执行任何操作。
 */
AICP_C_API_EXPORT void HIAI_NativeHandle_Destroy(HIAI_NativeHandle** handle);

/**
 * @brief 查询HIAI_NativeHandle指针实例中的fd。
 *
 * @param [in] handle {@link HIAI_NativeHandle}指针实例，非空，否则返回-1
 * @return 成功时返回fd的值，失败时返回-1
 */
AICP_C_API_EXPORT int HIAI_NativeHandle_GetFd(const HIAI_NativeHandle* handle);

/**
 * @brief 查询HIAI_NativeHandle指针实例中size，以字节为单位。
 *
 * @param [in] handle {@link HIAI_NativeHandle}指针实例，非空，否则返回0
 * @return 成功时返回size的值，失败时返回0
 */
AICP_C_API_EXPORT int HIAI_NativeHandle_GetSize(const HIAI_NativeHandle* handle);

/**
 * @brief 查询HIAI_NativeHandle指针实例中的offset，以字节为单位
 *
 * @param [in] handle {@link HIAI_NativeHandle}指针实例，非空，否则返回0
 * @return 成功时返回offset的值，失败时返回0
 */
AICP_C_API_EXPORT int HIAI_NativeHandle_GetOffset(const HIAI_NativeHandle* handle);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_HIAI_NATIVE_HANDLE_H
