/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_INNER_H
#define FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_INNER_H

#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_types.h"
#include "c/hiai_error_types.h"

#include "hcl_built_model_restore_options_inner.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 加载前可变shape模型从buffer恢复，仅vendor使用
 *
 * 用于将保存的模型数据及恢复选项信息在设备上重新载入，生成编译后模型{@link HIAI_MR_BuiltModel}指针实例。
 * {@link HIAI_MR_BuiltModel}不使用时，使用{@link HIAI_HCL_BuiltModel_Destroy}进行释放，否则将造成内存泄漏。
 *
 * @param [in] data 模型的数据地址，非空，否则返回失败
 * @param [in] size 模型的数据大小，大于0，否则返回失败
 * @param [in] options {@link HIAI_HCL_RestoreOptions}模型恢复选项指针实例
 * @param [out] builtModel {@link HIAI_MR_BuiltModel}的指针实例
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}
 * @see HIAI_HCL_BuiltModel_Destroy
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_BuiltModel_RestoreBufferWithOptions(
    const void* data, size_t size, HIAI_HCL_RestoreOptions* options, HIAI_MR_BuiltModel** builtModel);


/**
 * @brief 在设备上进行恢复文件里的编译后的模型，并指定恢复的档位。
 *
 * 此方法用于将有多档位的编译后的模型在设备上重新恢复，得到{@link HIAI_MR_BuiltModel}指针实例，
 * 为下一步模型加载{@link HIAI_HCL_ModelManager_InitV2}做准备。建议文件路径为绝对路径，且确保用户进程对其有读权限。
 * {@link HIAI_MR_BuiltModel}指针实例不使用时，使用{@link HIAI_HCL_BuiltModel_Destroy}进行释放，否则会出现内存泄漏。
 *
 * @param [in] file 传入的模型文件，使用绝对路径且进程有读权限，非空，否则返回失败
 * @param [in] shapeIndex 档位值，取值区间[1,16]，需要在模型内的档位数量以内，否则返回失败
 * @return 成功时返回{@link HIAI_MR_BuiltModel}的指针实例，失败返回空指针
 * @see HIAI_HCL_BuiltModel_Destroy
 */
AICP_C_API_EXPORT HIAI_MR_BuiltModel* HIAI_HCL_BuiltModel_RestoreFromFileWithShapeIndex(
    const char* path, uint8_t shapeIndex);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_INNER_H
