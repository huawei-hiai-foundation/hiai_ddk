/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INIT_OPTIONS_INNER_H
#define FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INIT_OPTIONS_INNER_H

#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 设置模型加载参数中调度配置参数中的调度路径
 *
 * ffrtConfig为0表示：软件调度，用于动态调度CPU任务
 * ffrtConfig为1表示：硬件调度，用于动态调度NPU/GPU任务
 * ffrtConfig为2表示：硬件调度，用于调度加速器任务，通过Event消息，调度异构IP间任务
 * ffrtConfig为3表示：硬件调度，HTS通路，用于静态调度异构IP间任务
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] ffrtConfig {@link HIAI_FFRT_Config}调度配置参数中的调度路径
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetFFRTConfig(
    HIAI_MR_ModelInitOptions* options, uint32_t ffrtConfig);

/**
 * @brief 获取模型加载参数中调度配置参数中的调度路径
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @return 成功返回实际调度通路编号，默认返回编号通路[0]
 */
AICP_C_API_EXPORT uint32_t HIAI_HCL_ModelInitOptions_GetFFRTConfig(const HIAI_MR_ModelInitOptions* options);

/**
 * @brief 设置模型加载参数中调度配置参数中的等待事件集
 *
 * 目前只用于ffrtConfig为3的场景
 * 等待事件集通过FFRT的接口{@link ffrt_graph_register} 和 {@link ffrt_graph_enable} 获得FFRT_STATIC_GRAPH_USER结构体中的目标事件集
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] eventIds 调度配置参数中的等待事件集
 * @param [in] size 等待事件个数
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetHTSWaitEventIds(HIAI_MR_ModelInitOptions* options,
    const int* eventIds, size_t size);

/**
 * @brief 设置模型加载参数中调度配置参数中的的发送事件集
 *
 * 目前只用于ffrtConfig为3的场景
 * 发送事件集通过FFRT的接口{@link ffrt_graph_register} 和 {@link ffrt_graph_enable}获得FFRT_STATIC_GRAPH_USER结构体中的源事件集
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] eventIds 调度配置参数中的发送事件集
 * @param [in] size 发送事件个数
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetHTSSendEventIds(HIAI_MR_ModelInitOptions* options,
    const int* eventIds, size_t size);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INIT_OPTIONS_INNER_H
