/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INNER_H
#define FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INNER_H

#include "c/hiai_c_api_export.h"
#include "c/hiai_error_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief FFRT异构场景创建FFRT Task调度任务
 *
 * 根据模型输入输出情况，申请tensor的内存，输入输出tensor的内存申请可使用{@link
 * HIAI_MR_NDTensorBuffer}下的create方法，根据需求选择合适的接口。 创建FFRT
 * Task调度任务完成，用户不需使用tensor内存后， 需调用{@link
 * HiAI_NDTensorBufferDestroy}销毁申请的每一个tensor内存，否则将造成内存泄漏。
 *
 * @param [in] manager {@link HIAI_MR_ModelManager}指针实例，非空，否则返回失败
 * @param [in] input 模型输入的指针数组，非空，否则返回HIAI_INVALID_PARAM
 * @param [in] inputNum 模型输入的个数
 * @param [in] output 模型输出的指针数组，非空，否则返回HIAI_INVALID_PARAM
 * @param [in] outputNum 模型输出的个数
 * @param [out] ffrtTask 返回创建的FFRT Task任务
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_ModelManager_CreateFFRTTask(HIAI_MR_ModelManager* manager,
    HIAI_MR_NDTensorBuffer* input[], int32_t inputNum, HIAI_MR_NDTensorBuffer* output[], int32_t outputNum,
    void* ffrtTask);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_INNER_API_HCL_MODEL_MANAGER_INNER_H