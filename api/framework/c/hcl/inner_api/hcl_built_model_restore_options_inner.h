/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_RESTORE_OPTIONS_INNER_H
#define FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_RESTORE_OPTIONS_INNER_H

#include "c/hiai_c_api_export.h"
#include "c/hiai_error_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HIAI_HCL_RestoreOptions HIAI_HCL_RestoreOptions;

/**
 * @brief 创建模型恢复选项，仅vendor使用
 *
 * 模型恢复结束后，使用{@link HIAI_HCL_RestoreOptions_Destroy}对恢复选项进行删除。
 *
 * @param [out] options {@link HIAI_HCL_RestoreOptions}模型恢复选项指针实例
 * @see HIAI_HCL_RestoreOptions_Destroy
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_RestoreOptions_Create(HIAI_HCL_RestoreOptions** options);

/**
 * @brief 向模型恢复选项中设置加载前可变shape的shapeIndex，仅vendor使用
 *
 * @param [out] options {@link HIAI_HCL_RestoreOptions}模型恢复参数指针实例
 * @param [in] shapeIndex shape的档位，支持1-16。
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_RestoreOptions_SetMultiShapeIndex(
    HIAI_HCL_RestoreOptions* options, uint8_t shapeIndex);

/**
 * @brief 释放模型恢复选项，仅vendor使用
 *
 * 该方法用于释放{@link HIAI_HCL_RestoreOptions_Create}创建的指针实例，若不调用，将造成内存泄漏。
 *
 * @param options {@link HIAI_HCL_RestoreOptions}模型恢复选项指针实例的引用，非空
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_RestoreOptions_Destroy(HIAI_HCL_RestoreOptions** options);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_INNER_API_HCL_BUILT_MODEL_RESTORE_OPTIONS_INNER_H
