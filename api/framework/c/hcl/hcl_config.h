/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_HCL_CONFIG_H
#define FRAMEWORK_HCL_HCL_CONFIG_H

#include "c/hiai_error_types.h"
#include "c/hiai_c_api_export.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 动态管理模型推理优先级。
 *
 * 本方法用于执行动态管理模型推理优先级，在多模型并发占用底层资源库的场景中，通过合理的设置前后台模型优先级，
 * 提升用户体验。
 *
 * @param [in] pId  {@link HIAI_MR_ModelManager}指针实例，非空，否则返回失败
 * @param [in] modelId 模型Id，指代某一客户端进程下的模型。
 * @param [in] priority 模型优先级，详见{@link HIAI_ModelPriority}
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_Config_SetModelPriority(uint32_t pid, uint32_t modelId, uint32_t priority);

/**
 * @brief 基于模型动态设置npu可占用的ddr带宽。
 *
 * 本方法用于执行动态管理模型占用ddr带宽，在多模型并发占用底层资源库的场景中，通过合理的设置带宽占用，
 * 提升用户体验。
 *
 * @param [in] pid 进程pid。
 * @param [in] modelId 模型Id，指代某一客户端进程下的模型。
 * @param [in] bandMode ddr带宽档位。（0~9档，0为最大档）
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HIAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_HCL_Config_SetComputeIOBandWidthMode(uint32_t pid, uint32_t modelId, uint32_t bandMode);

#ifdef __cplusplus
}
#endif

#endif  // FRAMEWORK_HCL_HCL_CONFIG_H
