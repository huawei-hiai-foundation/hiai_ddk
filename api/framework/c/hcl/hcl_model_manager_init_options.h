/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_HCL_MODEL_MANAGER_INIT_OPTIONS_H
#define FRAMEWORK_HCL_HCL_MODEL_MANAGER_INIT_OPTIONS_H

#include "c/hiai_c_api_export.h"
#include "hiai_types.h"
#include "hiai_execute_option_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建模型加载参数指针实例
 *
 * 该方法用于模型加载接口{@link HIAI_HCL_ModelManager_InitV2}，需要设置模型加载参数{@link HIAI_MR_ModelInitOptions}时，创建模型加载参数指针实例。
 * 模型加载完成后，模型加载参数{@link HIAI_MR_ModelInitOptions}不再使用时，需要使用{@link HIAI_HCL_ModelInitOptions_Destroy}进行释放，否则将造成内存泄漏。
 *
 * @return 成功返回{@link HIAI_MR_ModelInitOptions}指针，失败返回空指针
 */
AICP_C_API_EXPORT HIAI_MR_ModelInitOptions* HIAI_HCL_ModelInitOptions_Create(void);

/**
 * @brief 释放模型加载参数
 *
 * 该方法用于释放{@link HIAI_HCL_ModelInitOptions_Create}创建的指针实例，若不调用，将造成内存泄漏。
 *
 * @param options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例的引用，非空
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_Destroy(HIAI_MR_ModelInitOptions** options);

/**
 * @brief 设置模型加载参数中DDR的性能模式
 *
 * 请根据需要设置合适的值，高性能与功耗成正比，高性能也意味着高功耗
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] devPerf 性能模式，取值参见{@link HIAI_PerfMode}
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetPerfMode(
    HIAI_MR_ModelInitOptions* options, HIAI_PerfMode devPerf);

/**
 * @brief 获取模型加载参数中DDR的性能模式
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @return 成功返回性能模式，失败返回{@link HIAI_PERFMODE_UNSET}
 */
AICP_C_API_EXPORT HIAI_PerfMode HIAI_HCL_ModelInitOptions_GetPerfMode(const HIAI_MR_ModelInitOptions* options);

/**
 * @brief 设置NPU与周边IO资源的带宽模式
 *
 * 请根据需要设置合适的值，带宽与功耗成正比，高带宽也意味着高功耗。
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @param [in] bandMode 带宽模式，取值参见{@link HIAI_PerfMode}
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetBandMode(
    HIAI_MR_ModelInitOptions* options, HIAI_PerfMode bandMode);

/**
 * @brief 获取带宽模式
 *
 * @param [in] options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例，非空
 * @return 成功返回带宽模式，失败返回{@link HIAI_PERFMODE_UNSET}
 */
AICP_C_API_EXPORT HIAI_PerfMode HIAI_HCL_ModelInitOptions_GetBandMode(const HIAI_MR_ModelInitOptions* options);

/**
 * @brief 设置维测选项信息
 *
 * 该方法用于设置{@link HIAI_HCL_ModelInitOmOptions_Create}创建的指针实例到加载选项中。该选项在调试阶段用于设置维测选项。
 *
 * @param options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例的引用，非空
 * @param omOptions {@link HIAI_MR_ModelInitOmOptions}模型加载维测选项参数指针实例的引用，非空
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOptions_SetOmOptions(
    HIAI_MR_ModelInitOptions* options, HIAI_MR_ModelInitOmOptions* omOptions);

/**
 * @brief 获取加载的维测选项信息
 *
 * 该方法用于获取加载选项中的维测选项信息。该选项在调试阶段用于设置维测选项。
 *
 * @param options {@link HIAI_MR_ModelInitOptions}模型加载参数指针实例的引用，非空
 * @return 成功则返回维测选项信息实例指针地址，失败返回空指针
 */
AICP_C_API_EXPORT HIAI_MR_ModelInitOmOptions* HIAI_HCL_ModelInitOptions_GetOmOptions(
    HIAI_MR_ModelInitOptions* options);

/**
 * @brief 创建维测选项信息
 *
 * 该方法用于创建维测选项实例。维测选项在开发阶段用打开维测开关，并在推理时获取模型的维测信息。
 *
 * @return 成功则返回维测选项信息实例指针地址，失败返回空指针
 */
AICP_C_API_EXPORT HIAI_MR_ModelInitOmOptions* HIAI_HCL_ModelInitOmOptions_Create(void);

/**
 * @brief 销毁维测选项信息
 *
 * 该方法用于销毁由{@link HIAI_HCL_ModelInitOmOptions_Create}创建的维测选项实例。
 *
 * @param omOptions {@link HIAI_MR_ModelInitOmOptions}维测选项参数指针实例的地址，非空
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOmOptions_Destroy(HIAI_MR_ModelInitOmOptions** omOptions);

/**
 * @brief 设置维测选项信息中的维测类型
 *
 * 该方法用于向{@link HIAI_HCL_ModelInitOmOptions_Create}创建的维测选项实例中设置维测类型。
 *
 * @param options {@link HIAI_MR_ModelInitOmOptions}维测选项参数指针实例的地址，非空
 * @param omType 维测类型，有效类型：0：off 1：profiling 2：dump
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOmOptions_SetOmType(HIAI_MR_ModelInitOmOptions* options, int32_t omType);

/**
 * @brief 设置维测选项信息中的输出目录
 *
 * 该方法用于向{@link HIAI_HCL_ModelInitOmOptions_Create}创建的维测选项实例中设置输出目录。输出目录中用于存放维测生成的文件。
 *
 * @param options {@link HIAI_MR_ModelInitOmOptions}维测选项参数指针实例的地址，非空
 * @param outDir 输出目录，字符串，非空
 */
AICP_C_API_EXPORT void HIAI_HCL_ModelInitOmOptions_SetOmOutDir(HIAI_MR_ModelInitOmOptions* options, const char* outDir);

/**
 * @brief 设置维测选项信息中的属性信息
 *
 * 该方法用于向{@link HIAI_HCL_ModelInitOmOptions_Create}创建的维测选项实例中设置维测属性信息。
 *
 * @param options {@link HIAI_MR_ModelInitOmOptions}维测选项参数指针实例的地址，非空
 * @param key 维测属性的key信息
 * @param value 维测属性的value信息
 */
AICP_C_API_EXPORT int HIAI_HCL_ModelInitOmOptions_SetOmAttr(
    HIAI_MR_ModelInitOmOptions* options, const char* key, const char* value);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_HCL_MODEL_MANAGER_INIT_OPTIONS_H
