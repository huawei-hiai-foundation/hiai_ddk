/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_C_HIAI_EXECUTE_OPTION_TYPES_H
#define FRAMEWORK_C_HIAI_EXECUTE_OPTION_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    /** 设备类型为NPU */
    HIAI_DEVICETYPE_NPU = 0,
    /** 设备类型为CPU */
    HIAI_DEVICETYPE_CPU,
    /** 设备类型为GPU */
    HIAI_DEVICETYPE_GPU,
    /** 设备类型为DDR */
    HIAI_DEVICETYPE_DDR,
    /** 设备类型为SysCache */
    HIAI_DEVICETYPE_SC,
    /** 不支持的设备类型 */
    HIAI_DEVICETYPE_INVALID,
} HIAI_DeviceType;

typedef enum {
    /** 不使用，默认采用中级性能模式 */
    HIAI_PERFMODE_UNSET = 0,
    /** 低级性能模式 */
    HIAI_PERFMODE_LOW,
    /** 中级性能模式 */
    HIAI_PERFMODE_NORMAL,
    /** 高级性能模式 */
    HIAI_PERFMODE_HIGH,
    /** 极高性能模式 */
    HIAI_PERFMODE_EXTREME,
    /** 仅计算单元高性能模式，相比HIGH模式，性能会有微小损失但功耗有优化提升 */
    HIAI_PERFMODE_HIGH_COMPUTE_UNIT = 103,
    /** 仅计算单元中等性能模式 */
    HIAI_PERFMODE_MIDDLE_BAND_NORMAL_COMPUTE_UNIT = 202,
} HIAI_PerfMode;

typedef enum {
    /** 抢占级别，高优先级平台1*/
    HIAI_PRIORITY_HIGH_PLATFORM1 = 1,
    /** 抢占级别，高优先级平台2*/
    HIAI_PRIORITY_HIGH_PLATFORM2,
    /** 抢占级别，最高优先级 */
    HIAI_PRIORITY_HIGHEST,
    /** 抢占级别，较高优先级 */
    HIAI_PRIORITY_HIGHER,
    /** 非抢占级别，高优先级 */
    HIAI_PRIORITY_HIGH = 5,
    /** 非抢占级别，中优先级 */
    HIAI_PRIORITY_MIDDLE,
    /** 非抢占级别，低优先级 */
    HIAI_PRIORITY_LOW
} HIAI_ModelPriority;

#ifdef __cplusplus
}
#endif

#endif /* FRAMEWORK_C_HIAI_EXECUTE_OPTION_TYPES_H */
