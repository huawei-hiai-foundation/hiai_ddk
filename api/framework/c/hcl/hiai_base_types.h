/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_C_HIAI_BASE_TYPES_H
#define FRAMEWORK_C_HIAI_BASE_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    /** 数据类型为uint8_t */
    HIAI_DATATYPE_UINT8 = 0,
    /** 数据类型为float32 */
    HIAI_DATATYPE_FLOAT32 = 1,
    /** 数据类型为float16 */
    HIAI_DATATYPE_FLOAT16 = 2,
    /** 数据类型为int32_t */
    HIAI_DATATYPE_INT32 = 3,
    /** 数据类型为int8_t */
    HIAI_DATATYPE_INT8 = 4,
    /** 数据类型为int16_t */
    HIAI_DATATYPE_INT16 = 5,
    /** 数据类型为bool */
    HIAI_DATATYPE_BOOL = 6,
    /** 数据类型为int64_t */
    HIAI_DATATYPE_INT64 = 7,
    /** 数据类型为uint32_t */
    HIAI_DATATYPE_UINT32 = 8,
    /** 数据类型为double */
    HIAI_DATATYPE_DOUBLE = 9,
} HIAI_DataType;

typedef enum {
    /** YUV420SP_U8格式的图片 */
    HIAI_YUV420SP_U8 = 0,
    /** XRGB8888_U8格式的图片 */
    HIAI_XRGB8888_U8,
    /** YUV400_U8格式的图片 */
    HIAI_YUV400_U8,
    /** ARGB8888_U8格式的图片 */
    HIAI_ARGB8888_U8,
    /** YUYV_U8格式的图片 */
    HIAI_YUYV_U8,
    /** YUV422SP_U8格式的图片 */
    HIAI_YUV422SP_U8,
    /** AYUV444_U8格式的图片 */
    HIAI_AYUV444_U8,
    /** RGB888_U8格式的图片 */
    HIAI_RGB888_U8,
    /** BGR888_U8格式的图片 */
    HIAI_BGR888_U8,
    /** YUV444SP_U8格式的图片 */
    HIAI_YUV444SP_U8,
    /** YVU444SP_U8格式的图片 */
    HIAI_YVU444SP_U8,
    /** 无效格式的图片 */
    HIAI_IMAGE_FORMAT_INVALID = 255,
} HIAI_ImageFormat;

typedef enum {
    /** JPEG 色域空间类型 */
    HIAI_JPEG = 0,
    /** BT.601 video range 色域空间类型 */
    HIAI_BT_601_NARROW,
    /** BT.601 full range 色域空间类型 */
    HIAI_BT_601_FULL,
    /** BT.709 video range 色域空间类型 */
    HIAI_BT_709_NARROW,
    /** 无效图像色域类型 */
    HIAI_IMAGE_COLORSPACE_INVALID = 255,
} HIAI_ImageColorSpace;

typedef enum {
    /** Tensor数据排列格式为NCHW */
    HIAI_FORMAT_NCHW = 0,
    /** Tensor数据排列格式为NHWC */
    HIAI_FORMAT_NHWC,
    /** Tensor数据排列格式为ND */
    HIAI_FORMAT_ND,
} HIAI_Format;

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_HIAI_BASE_TYPES_H
