/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_TYPES_H
#define FRAMEWORK_C_HIAI_TYPES_H

#include <stddef.h>
#include "c/hiai_error_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HIAI_MR_ModelManager HIAI_MR_ModelManager;

typedef struct HIAI_MR_ModelInitOptions HIAI_MR_ModelInitOptions;

typedef struct HIAI_MR_ModelInitOmOptions HIAI_MR_ModelInitOmOptions;

typedef struct HIAI_MR_ModelBuildOptions HIAI_MR_ModelBuildOptions;

typedef struct HIAI_MR_BuiltModel HIAI_MR_BuiltModel;

typedef struct HIAI_NDTensorDesc HIAI_NDTensorDesc;

typedef struct HIAI_MR_NDTensorBuffer HIAI_MR_NDTensorBuffer;

typedef struct HIAI_MR_TensorAippPara HIAI_MR_TensorAippPara;

typedef struct HIAI_NativeHandle HIAI_NativeHandle;

typedef struct HIAI_MR_ModelManagerListener {
    void (*onRunDone)(void*, HIAI_Status, HIAI_MR_NDTensorBuffer* [], int32_t);
    void (*onServiceDied)(void*);
    void* userData;
} HIAI_MR_ModelManagerListener;

typedef struct HIAI_MR_ModelManagerSharedMemAllocator {
    void (*onAllocate)(void*, uint32_t requiredSize, HIAI_NativeHandle* [], size_t*);
    void (*onFree)(void*, HIAI_NativeHandle* [], size_t);
    void* userData;
} HIAI_MR_ModelManagerSharedMemAllocator;

typedef enum {
    /** 模型兼容。 */
    HIAI_BUILTMODEL_COMPATIBLE,
    /** 模型不兼容。 */
    HIAI_BUILTMODEL_INCOMPATIBLE,
} HIAI_BuiltModel_Compatibility;

#ifdef __cplusplus
}
#endif

#endif /* FRAMEWORK_C_HIAI_TYPES_H */