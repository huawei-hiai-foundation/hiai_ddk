/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_HCL_HIAI_TENSOR_AIPP_PARA_H
#define FRAMEWORK_HCL_HIAI_TENSOR_AIPP_PARA_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "hiai_types.h"
#include "hiai_base_types.h"
#include "c/hiai_error_types.h"
#include "c/hiai_c_api_export.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 根据batchNum创建动态AippPara对象。
 *
 * 本方法用于创建动态AippPara对象，根据传入的batchNum申请相应的内存，用于存储动态aipp参数。
 * 模型卸载后，不需要此指针实例时，需要调用{@link HIAI_MR_TensorAippPara_Destroy}进行释放，否则会出现内存泄漏
 *
 * @param [in] batchNum 模型输入的batch大小，取值范围为(0, 127]
 * @return 成功时返回{@link HIAI_MR_TensorAippPara}指针实例，失败返回空指针
 * @see HIAI_MR_TensorAippPara_Destroy
 */
AICP_C_API_EXPORT HIAI_MR_TensorAippPara* HIAI_MR_TensorAippPara_Create(uint32_t batchNum);

/**
 * @brief 获取AippPara申请的内存地址。
 *
 * 本方法用于获取通过{@link HIAI_MR_TensorAippPara_Create}申请的{@link HIAI_MR_TensorAippPara}的data内存地址。
 * data指向申请的aipp参数的内存。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返空指针
 * @return 成功时返回AippPara申请的内存地址，失败返回空指针
 */
AICP_C_API_EXPORT void* HIAI_MR_TensorAippPara_GetRawBuffer(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 获取AippPara申请的内存大小。
 *
 * 本方法用于获取通过{@link HIAI_MR_TensorAippPara_Create}申请的{@link HIAI_MR_TensorAippPara}的size大小。
 * size为申请的aipp参数的内存大小。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回0
 * @return 成功时返回AippPara申请的内存大小，失败返回0
 */
AICP_C_API_EXPORT int32_t HIAI_MR_TensorAippPara_GetRawBufferSize(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 查询AippPara对象所在输入的索引。
 *
 * 本方法用于在多个输入情况下，查询AippPara对象所在输入的索引。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回-1
 * @return 成功时返回该AippPara对象所在输入的索引，失败返回-1
 */
AICP_C_API_EXPORT int32_t HIAI_MR_TensorAippPara_GetInputIndex(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 设置AippPara在输入上的索引。
 *
 * 本方法用于在多个输入情况下，设置索引以确定该AippPara对象作用于第几个输入。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空
 * @param [in] inputIndex 用于标识此AIPP参数作用于模型的第几个输入，从0开始计数
 */
AICP_C_API_EXPORT void HIAI_MR_TensorAippPara_SetInputIndex(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t inputIndex);

/**
 * @brief 查询AippPara对象在该输入的多个输出分支上的索引。
 *
 * 本方法用于在data节点有多个索引时，查询AippPara对象在data节点上的索引。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回-1
 * @return 成功时返回该AippPara对象在data节点上的索引，失败返回-1
 */
AICP_C_API_EXPORT int32_t HIAI_MR_TensorAippPara_GetInputAippIndex(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 设置AippPara对象作用于该输入的多个输出分支上的索引。
 *
 * 本方法用于在data有多个输出分支时，设置AippPara对象作用域该输入的第几个输出分支。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空
 * @param [in] inputAippIndex 用于标识AIPP配置参数在输入Data有多个输出分支时作用于第几个分支，从0开始计数
 */
AICP_C_API_EXPORT void HIAI_MR_TensorAippPara_SetInputAippIndex(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t inputAippIndex);

/**
 * @brief 释放AippPara对象。
 *
 * 本方法用于释放通过{@link HIAI_MR_TensorAippPara_Create}创建的{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] aippParas {@link HIAI_MR_TensorAippPara}指针实例，非空
 */
AICP_C_API_EXPORT void HIAI_MR_TensorAippPara_Destroy(HIAI_MR_TensorAippPara** aippParas);

/**
 * @brief 设置输入图像格式。
 *
 * 本方法用于动态AIPP推理时，设置AIPP的输入图像格式到{@link HIAI_MR_TensorAippPara}对象。
 * AIPP可配置的图像格式为{@link HiAI_ImageFormat}所支持的范围。
 * 图像像素点为Uint8类型，范围为0到255。当图像的输入为YUV类型时，无论是YUV420还是YUV422或者YUYV，AIPP自动将图像数据补齐为YUV444格式。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] inputFormat 输入图像的格式，参见{@link HiAI_ImageFormat}。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetInputFormat(
    HIAI_MR_TensorAippPara* tensorAippPara, HIAI_ImageFormat inputFormat);

/**
 * @brief 查询输入图像格式。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询输入图像格式。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回{@link HIAI_YUV420SP_U8}。
 * @return 成功返回{@link HiAI_ImageFormat}，失败返回{@link HIAI_YUV420SP_U8}
 */
AICP_C_API_EXPORT HIAI_ImageFormat HIAI_MR_TensorAippPara_GetInputFormat(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 查询输入图像宽高。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询输入图像宽高。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [out] srcImageW 输入图像的宽度
 * @param [out] srcImageH 输入图像的高度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetInputShape(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t* srcImageW, uint32_t* srcImageH);

/**
 * @brief 设置输入图像宽高。
 *
 * 本方法用于动态AIPP推理时，设置输入图像宽高到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] srcImageW 输入图像的宽度，取值范围[16, 4096]
 * @param [in] srcImageH 输入图像的高度，取值范围[16, 4096]
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetInputShape(HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t srcImageW, uint32_t srcImageH);

/**
 * @brief 设置AIPP裁剪起始位置参数。
 *
 * 本方法用于动态AIPP推理时，设置裁剪参数到{@link HIAI_MR_TensorAippPara}对象。
 * YUV类型的图像受图像自身类型的限制，当输入图像类型为YUV420SP、YUYV、YUV422SP和AYUV444时，裁剪的起始坐标需为是偶数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引。
 * @param [in] cropStartPosW 裁剪起始位置水平方向坐标，startPosW小于输入图像的宽度
 * @param [in] cropStartPosH 裁剪起始位置垂直方向坐标，startPosH小于输入图像的高度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetCropPos(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH);

/**
 * @brief 设置AIPP裁剪宽高参数。
 *
 * 本方法用于动态AIPP推理时，设置裁剪参数到{@link HIAI_MR_TensorAippPara}对象。
 * YUV类型的图像受图像自身类型的限制，当输入图像类型为YUV420SP、YUYV、YUV422SP和AYUV444时，裁剪的宽高需为是偶数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引。
 * @param [in] cropSizeW 裁剪出的图像宽度，startPosW与cropSizeW之和小于等于输入图像的宽度
 * @param [in] cropSizeH 裁剪出的图像高度，startPosH与cropSizeH之和小于等于输入图像的高度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetCropSize(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH);

/**
 * @brief 查询AIPP裁剪起始位置参数
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的裁剪起始位置参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引。
 * @param [out] cropStartPosW 裁剪起始位置水平方向坐标
 * @param [out] cropStartPosH 裁剪起始位置垂直方向坐标
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetCropPos(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH);

/**
 * @brief 查询AIPP裁剪宽高参数
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的裁剪宽高参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引。
 * @param [out] cropSizeW 裁剪出的图像宽度
 * @param [out] cropSizeH 裁剪出的图像高度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetCropSize(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH);

/**
 * @brief 设置AIPP图像缩放参数
 *
 * 本方法用于动态AIPP推理时，设置图像缩放参数到{@link HIAI_MR_TensorAippPara}对象。Resize的类型为双线性插值。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] resizeOutputSizeW 缩放后图像宽度，取值范围[16, 448]，图像宽度缩放倍数的范围[1/16, 16]
 * @param [in] resizeOutputSizeH 缩放后图像高度，取值范围[16, 4096]，图像高度缩放倍数的范围[1/16, 16]
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetResizePara(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH);

/**
 * @brief 查询AIPP图像缩放参数
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的缩放后图像宽高值。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] resizeOutputSizeW 缩放后图像宽度
 * @param [out] resizeOutputSizeH 缩放后图像高度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetResizePara(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH);

/**
 * @brief 设置AIPP数据类型转换通道像素平均值
 *
 * 数据类型转换（Data Type Conversion，简称DTC），用于将输入图像中像素值转换为模型训练时的数据类型。设置DTC参数，使得转换之后的数据在一个预期的范围，避免强制转换。
 * 数据类型转化功能，将输入的图像数据类型通过转化公式转换为FP16类型送给后续模块计算，实际为依次执行减均值、减最小值和乘方差操作。
 *
 * 计算公式为：U8->FP16:
 * pixelOutChn(i) = [pixelInChn(i)–meanChn(i)–minChn(i)] * varReciChn(i), i ∈ [0, 4)
 * 其中：pixel_out_chx(i)为DTC模块的每个通道的输出值，pixel_in_chx(i)为DTC模块的每个通道的输入值，mean_chn(i)为用户输入的每个通道的均值，
 * min_chn(i)为用户输入的每个通道的最小值， var_reci_chn(i)为用户输入的每个通道的方差。
 *
 * 该方法用于动态AIPP推理时，设置DTC数据类型转换像素平均值到{@link HIAI_MR_TensorAippPara}对象。
 * 该方法需配合{@link HIAI_MR_TensorAippPara_SetDtcPixelMinPara} {@link HIAI_MR_TensorAippPara_SetDtcPixelVarReciPara}共同使用。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] pixelMeanPara[] 通道像素平均值数组，数组size为chnNum，默认值为0。
 * @param [in] chnNum 通道数量，取值范围为[1,4]，例如chnNun=3则对应配置通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelMeanPara(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, int32_t pixelMeanPara[], uint32_t chnNum);

/**
 * @brief 设置AIPP数据类型转换通道像素最小值
 *
 * 该方法用于动态AIPP推理时，设置DTC数据类型转换像素最小值到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] pixelMinPara[] 通道像素最小值数组，数组size为chnNum，默认值为0。
 * @param [in] chnNum 通道数量，取值范围为[1,4]，例如chnNun=3则对应配置通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelMinPara(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float pixelMinPara[], uint32_t chnNum);

/**
 * @brief 设置AIPP数据类型转换通道像素方差
 *
 * 该方法用于动态AIPP推理时，设置DTC数据类型转换像素方差到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] pixelVarReciPara[] 通道像素方差数组，数组size为chnNum，默认值为1.0。
 * @param [in] chnNum 通道数量，取值范围为[1,4]，例如chnNun=3则对应配置通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelVarReciPara(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float pixelVarReciPara[], uint32_t chnNum);

/**
 * @brief 查询AIPP数据类型转换通道像素平均值
 *
 * 该方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的数据类型转换像素平均值。
 * 该方法需配合{@link HIAI_MR_TensorAippPara_SetDtcPixelMinPara} {@link HIAI_MR_TensorAippPara_SetDtcPixelVarReciPara}共同使用，
 * 来获取所有数据类型转换参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] pixelMeanPara[] 通道像素平均值数组，数组size为chnNum。
 * @param [in] chnNum  通道数量，取值范围为[1,4]，对应从chn0开始。例如chnNun等于3则对应查询通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelMeanPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, int32_t pixelMeanPara[], uint32_t chnNum);

/**
 * @brief 查询AIPP数据类型转换通道像素最小值
 *
 * 该方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的数据类型转换像素最小值
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] pixelMinPara[] 通道像素最小值数组，数组size为chnNum。
 * @param [in] chnNum 通道数量，取值范围为[1,4]，对应从chn0开始。例如chnNun等于3则对应查询通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelMinPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float pixelMinPara[], uint32_t chnNum);

/**
 * @brief 查询AIPP数据类型转换通道像素方差
 *
 * 该方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的数据类型转换像素方差值
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] pixelVarReciPara[] 通道像素方差数组，数组size为chnNum。
 * @param [in] chnNum 通道数量，取值范围为[1,4]，对应从chn0开始。例如chnNun等于3则对应查询通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelVarReciPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float pixelVarReciPara[], uint32_t chnNum);

/**
 * @brief 设置AIPP的CSC色域转换参数。
 *
 * 本方法用于动态AIPP推理时，设置aippPara色域转换参数到{@link HIAI_MR_TensorAippPara}对象。
 * 色域转换（Color Space Conversion，以下简称CSC），特指在YUV444和RGB888两种图像格式之间进行转换。
 * 输入为YUV格式图像(YUV420/YUYV/YUV422SP/AYUV444)，模型训练集可为RGB,BGR，灰度图（YUV400_U8）。
 * 输入为RGB格式图像(XRGB8888/ARGB8888)，模型训练集可为YUV444SP，YVU444SP，灰度图（YUV400_U8）。
 * 不支持从YUV400到RGB或BGR的转换。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] inputFormat 图像输入格式，可参考{@link HIAI_ImageFormat}
 * @param [in] targetFormat 图像输出格式，可参考{@link HIAI_ImageFormat}
 * @param [in] colorSpace 图像颜色空间类型，可参考{@link HIAI_ImageColorSpace}
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetCscPara(
    HIAI_MR_TensorAippPara* tensorAippPara, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace);

/**
 * @brief 查询AIPP的CSC色域转换相关参数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询推理aippPara色域转换参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [out] inputFormat 图像输入格式，可参考{@link HIAI_ImageFormat}
 * @param [out] targetFormat 图像输出格式，可参考{@link HIAI_ImageFormat}
 * @param [out] colorSpace 图像颜色空间类型，可参考{@link HIAI_ImageColorSpace}
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetCscPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, HIAI_ImageFormat* inputFormat,
    HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace);

/**
 * @brief 设置AIPP的ChannelSwap通道交换参数。
 *
 * 本方法用于动态AIPP推理时，设置aippPara通道交换参数到{@link HIAI_MR_TensorAippPara}对象。
 * AIPP支持两种类型的通道交换：RB/UV通道交换和AX通道交换。
 * RB/UV通道交换丰富了输入图像的格式，开启RB/UV通道交换后，AIPP支持的图像输入格式比可配置的输入类型丰富了一倍。
 * |配置类型        |可接受图像类型
 * |YUV420SP_U8    |YUV420，YVU420 + rbuv_swap_switch
 * |XRGB8888_U8    |XRGB，XBGR + rbuv_swap_switch
 * |ARGB8888_U8    |ARGB，ABGR + rbuv_swap_switch
 * |RGB888_U8      |BGR + rbuv_swap_switch
 * |YUYV_U8        |YUYV，YVYU + rbuv_swap_switch
 * |YUV422SP_U8    |YUV422，YVU422 + rbuv_swap_switch
 * |AYUV444_U8     |AYUV + rbuv_swap_switch
 *
 * 当配置的图像输入格式为XRGB、ARGB或AYUV时，支持开启AX通道交换。开启后，图像第一个通道的输入被搬移到第四个通道上，即当XRGB、ARGB和AYUV开启AX通道交换后，转变为RGBX、RGBA和YUVA。
 * 当模型训练集为RGB格式的图像，而推理时的图像输入为XRGB或者ARGB时，可以通过使能AX通道交换，将RGB通道前移，实现兼容。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] rbuvSwapSwitch RB/UV通道交换标识
 * @param [in] axSwapSwitch AX通道交换标识
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetChannelSwapPara(
    HIAI_MR_TensorAippPara* tensorAippPara, bool rbuvSwapSwitch, bool axSwapSwitch);

/**
 * @brief 查询AIPP的ChannelSwap通道交换参数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询通道交换参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [out] rbuvSwapSwitch RB/UV通道交换标识
 * @param [out] axSwapSwitch AX通道交换标识
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetChannelSwapPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, bool* rbuvSwapSwitch, bool* axSwapSwitch);

/**
 * @brief 设置AIPP对输入图像上侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，设置对输入图像上侧的Padding像素数到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeTop 图像上侧Padding像素数，经过padding后的图像高度，需与原始模型维度中高度保持一致
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetPadTopSize(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t paddingSizeTop);

/**
 * @brief 设置AIPP对输入图像下侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，设置对输入图像下侧的Padding像素数到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeBottom 图像下侧Padding像素数，经过padding后的图像高度，需与原始模型维度中高度保持一致
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetPadBottomSize(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t paddingSizeBottom);

/**
 * @brief 设置AIPP对输入图像左侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，设置对输入图像左侧的Padding像素数到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败。
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeLeft 图像左侧Padding像素数，经过padding后的图像宽度，需与原始模型维度中宽度保持一致。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetPadLeftSize(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t paddingSizeLeft);

/**
 * @brief 设置AIPP对输入图像右侧Padding像素数。
 *
 * * 本方法用于动态AIPP推理时，设置对输入图像右侧的Padding像素数到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败。
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeRight 图像右侧Padding像素数，经过padding后的图像宽度，需与原始模型维度中宽度保持一致。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetPadRightSize(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t paddingSizeRight);

/**
 * @brief 查询AIPP对输入图像上侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的对输入图像的填充像素数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败。
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeTop 图像上侧Padding像素数
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetPadTopSize(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t* paddingSizeTop);

/**
 * @brief 查询AIPP对输入图像下侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的对输入图像的填充像素数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeBottom 图像下侧Padding像素数
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetPadBottomSize(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t* paddingSizeBottom);

/**
 * @brief 查询AIPP对输入图像左侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的对输入图像的填充像素数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeLeft 图像左侧Padding像素数
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetPadLeftSize(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t* paddingSizeLeft);

/**
 * @brief 查询AIPP对输入图像右侧Padding像素数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的对输入图像的填充像素数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] paddingSizeRight 图像右侧Padding像素数
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetPadRightSize(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t* paddingSizeRight);

/**
 * @brief 设置AIPP的CSC色域转换矩阵参数。
 *
 * 本方法用于动态AIPP推理时，设置aippPara色域转换矩阵参数到{@link HIAI_MR_TensorAippPara}对象。
 * CSC色域转换矩阵参数包含：
 * CSC矩阵元素：matrix_r0c0 matrix_r0c1 matrix_r0c2
 *             matrix_r1c0 matrix_r1c1 matrix_r1c2
 *             matrix_r2c0 matrix_r2c1 matrix_r2c2
 * RGB转YUV时的输出偏移: output_bias_0 output_bias_1 output_bias_2
 * YUV转RGB时的输入偏移: input_bias_0 input_bias_1 input_bias_2
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] cscMatrixPara[] 色域转换矩阵，数组size为paraNum。
 * @param [in] paraNum 色域转换矩阵维度，取值范围为[1, 15]。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetCscMatrixPara(
    HIAI_MR_TensorAippPara* tensorAippPara, int32_t cscMatrixPara[], uint32_t paraNum);

/**
 * @brief 查询AIPP的CSC色域转换矩阵参数。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询aippPara色域转换矩阵参数。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [out] cscMatrixPara[] 色域转换矩阵，数组size为paraNum。
 * @param [in] paraNum 色域转换矩阵维度，取值范围为[1, 15]。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetCscMatrixPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, int32_t cscMatrixPara[], uint32_t paraNum);

/**
 * @brief 查询AIPP的CSC色域转换开关状态。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询CSC色域转换开关状态。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @return CSC开关状态。
 */
AICP_C_API_EXPORT bool HIAI_MR_TensorAippPara_GetCscSwitch(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 查询AIPP图像裁剪开关状态。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询图像裁剪开关状态。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引
 * @return 图像裁剪开关状态。
 */
AICP_C_API_EXPORT bool HIAI_MR_TensorAippPara_GetCropSwitch(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex);

/**
 * @brief 查询AIPP图像缩放开关状态。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询图像缩放开关状态。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引
 * @return 图像缩放开关状态。
 */
AICP_C_API_EXPORT bool HIAI_MR_TensorAippPara_GetResizeSwitch(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex);

/**
 * @brief 查询AIPP图像填充开关状态。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询图像填充开关状态。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应即将crop出来的图像索引
 * @return 图像填充开关状态。
 */
AICP_C_API_EXPORT bool HIAI_MR_TensorAippPara_GetPadSwitch(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex);

/**
 * @brief 查询aippPara设置的图像数量。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询推理aippPara设置图像数量。
 * 在单batch多crop场景为一张图像多个crop的子图像的数量。在多batch单crop场景为输入的batch值。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @return 成功返回图像数量，失败返回0
 */
AICP_C_API_EXPORT uint32_t HIAI_MR_TensorAippPara_GetBatchCount(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 设置AIPP的单张图像多crop场景标识。
 *
 * 本方法用于动态AIPP推理时，设置aippPara单张图片多crop标识到{@link HIAI_MR_TensorAippPara}对象。
 * 对于单个batch的图像输入的场景，支持一次性传入多组crop等aipp参数，一次推理即能得到全部人脸等的关键点信息。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] singleBatchMutiCrop 是否为单张图像多crop场景
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetSingleBatchMultiCrop(
    HIAI_MR_TensorAippPara* tensorAippPara, bool singleBatchMutiCrop);

/**
 * @brief 查询AIPP的单张图像多crop场景标识。
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询是否为单张图像多crop场景。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @return 是否为AIPP的单张图像多crop场景。
 */
AICP_C_API_EXPORT bool HIAI_MR_TensorAippPara_GetSingleBatchMultiCrop(const HIAI_MR_TensorAippPara* tensorAippPara);

/**
 * @brief 设置AIPP通道padding的填充值
 *
 * 本方法用于动态AIPP推理时，设置AIPP通道padding的填充值到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] chnValue[] 通道填充值数组，填充值范围[-65504, 65504]，默认填充值为0
 * @param [in] chnNum 通道填充数，当前支持[1,4]，例如chnNun=3则对应配置通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetPadChannelValue(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t chnValue[], uint32_t chnNum);

/**
 * @brief 查询AIPP通道padding的填充值
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的通道padding的填充值
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] chnValue[] 通道填充值数组，填充值范围[-65504, 65504]，默认填充值为0
 * @param [in] chnNum 通道填充数，当前支持[1,4]，例如chnNun=3则对应配置通道chn0, chn1, chn2的数据。
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetPadChannelValue(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, uint32_t chnValue[], uint32_t chnNum);

/**
 * @brief 设置AIPP旋转角度
 *
 * 本方法用于动态AIPP推理时，设置旋转角度到{@link HIAI_MR_TensorAippPara}对象。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [in] rotateAngle 旋转角度，仅支持0, 90, 180, 270
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_SetRotateAngle(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float rotateAngle);

/**
 * @brief 查询AIPP旋转角度
 *
 * 本方法用于动态AIPP推理时，根据{@link HIAI_MR_TensorAippPara}对象查询对应索引的图像旋转角度。
 *
 * @param [in] tensorAippPara {@link HIAI_MR_TensorAippPara}指针实例，非空，否则返回失败
 * @param [in] batchIndex 对于多batch单crop场景，对应输入的图像索引；对于单batch多crop场景，对应crop出来的图像索引。
 * @param [out] rotateAngle 旋转角度
 * @return 函数执行结果状态。执行成功返回HIAI_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link HiAI_Status}
 */
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TensorAippPara_GetRotateAngle(
    const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float* rotateAngle);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_HCL_HIAI_TENSOR_AIPP_PARA_H
