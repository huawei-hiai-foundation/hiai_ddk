/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _HIAI_CACHE_STATUS_H_
#define _HIAI_CACHE_STATUS_H_

enum HiAICacheStatus {
    HIAI_CACHE_STATUS_UNSET = 0,
    HIAI_CACHE_STATUS_DIRTY = 1,
    HIAI_CACHE_STATUS_CINV = 2,
    HIAI_CACHE_STATUS_CLEAN = 3,
    HIAI_CACHE_STATUS_NOP = 4,
    HIAI_CACHE_STATUS_MAX,
};

#endif