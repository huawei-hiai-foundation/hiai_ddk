## DDK C接口目录及描述

开放接口目录：

| 文件名及目录      |接口描述|
| -------------------------------------------------- |---------------|
|ddk/ai_ddk_lib/include/c/graph/attr_value.h|构造算子属性|
|ddk/ai_ddk_lib/include/c/graph/context.h|构造资源管理器|
|ddk/ai_ddk_lib/include/c/graph/graph.h|构造graph及设置图输入输出算子|
|ddk/ai_ddk_lib/include/c/graph/handle_types.h|handle类型定义|
|ddk/ai_ddk_lib/include/c/graph/model.h|模型创建、设置及模型dump|
|ddk/ai_ddk_lib/include/c/graph/op_config.h|算子配置类型定义|
|ddk/ai_ddk_lib/include/c/graph/operator.h|data及其他算子创建|
|ddk/ai_ddk_lib/include/c/graph/tensor.h|tensor创建|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_built_model.h|编译后模型相关方法|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_model_build_options.h|模型编译参数相关方法|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_execute_option_types.h|执行参数类型定义|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_model_builder.h|编译模型|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_model_manager.h|模型管家创建、加载、推理和销毁等|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_model_manager_init_options.h|加载参数相关方法|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_nd_tensor_buffer.h|NDTensor buffer相关方法|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_nd_tensor_desc.h|NDTensorDesc相关方法|
|ddk/ai_ddk_lib/include/c/model_manager/hiai_version.h|HiAI版本号查询|

编译所在so:
DDK C接口编译在libhiai_c.so中。编译方式同其他ddk so。

## 支持的算子列表
|支持的算子|
| ------------- |
|[Acos](#acos)|
|[Activation](#activation)|
|[Add](#add)|
|[ArgMaxExt2](#argmaxext2)|
|[Asin](#asin)|
|[Atan](#atan)|
|[AvgPoolV2](#avgpoolv2)|
|[BatchMatMul](#batchmatmul)|
|[BatchToSpaceND](#batchtospacend)|
|[BiasAdd](#biasadd)|
|[BNInference](#bninference)|
|[BroadcastTo](#broadcastto)|
|[CastT](#castt)|
|[Ceil](#ceil)|
|[ClipByValue](#clipbyvalue)|
|[ConcatD](#concatd)|
|[Const](#const)|
|[Convolution](#convolution)|
|[ConvolutionDepthwise](#convolutiondepthwise)|
|[ConvTranspose](#convtranspose)|
|[Cos](#cos)|
|[Crop](#crop)|
|[CropAndResize](#cropandresize)|
|[Data](#data)|
|[DepthToSpace](#depthtospace)|
|[DequantizeV2](#dequantizev2)|
|[Eltwise](#eltwise)|
|[Equal](#equal)|
|[Erf](#erf)|
|[Exp](#exp)|
|[ExpandDims](#expanddims)|
|[Expm1](#expm1)|
|[FakeQuantWithMinMaxVars](#fakequantwithminmaxvars)|
|[Fill](#fill)|
|[Flatten](#flatten)|
|[Floor](#floor)|
|[FloorDiv](#floordiv)|
|[FloorMod](#floormod)|
|[FullyConnection](#fullyconnection)|
|[GatherNd](#gathernd)|
|[GatherV2D](#gatherv2d)|
|[GemmD](#gemmd)|
|[Greater](#greater)|
|[GreaterEqual](#greaterequal)|
|[GridSampler2D](#gridsampler2d)|
|[HardSwish](#hardswish)|
|[InstanceNorm](#instancenorm)|
|[LayerNorm](#layernorm)|
|[Less](#less)|
|[LessEqual](#lessequal)|
|[Log](#log)|
|[Log1p](#log1p)|
|[LogicalAnd](#logicaland)|
|[LogicalNot](#logicalnot)|
|[LogicalOr](#logicalor)|
|[LogicalXor](#logicalxor)|
|[LogSoftmax](#logsoftmax)|
|[MatMul](#matmul)|
|[Maximum](#maximum)|
|[Minimum](#minimum)|
|[MirrorPad](#mirrorpad)|
|[Mish](#mish)|
|[Mul](#mul)|
|[Neg](#neg)|
|[NonMaxSuppressionV6](#nonmaxsuppressionv6)|
|[NotEqual](#notequal)|
|[OneHot](#onehot)|
|[Pack](#pack)|
|[Pad](#pad)|
|[Permute](#permute)|
|[PoolingD](#poolingd)|
|[Pow](#pow)|
|[Power](#power)|
|[PRelu](#prelu)|
|[QuantizeV2](#quantizev2)|
|[Range](#range)|
|[Rank](#rank)|
|[RealDiv](#realdiv)|
|[Reciprocal](#reciprocal)|
|[ReduceLogSumExp](#reducelogsumexp)|
|[ReduceMax](#reducemax)|
|[ReduceMean](#reducemean)|
|[ReduceMin](#reducemin)|
|[ReduceProdD](#reduceprodd)|
|[ReduceSum](#reducesum)|
|[Reshape](#reshape)|
|[Resize](#resize)|
|[ResizeBicubic](#resizebicubic)|
|[ResizeBilinear](#resizebilinear)|
|[ResizeNearestNeighborV2](#resizenearestneighborv2)|
|[Rint](#rint)|
|[ROIAlignV2](#roialignv2)|
|[Round](#round)|
|[Rsqrt](#rsqrt)|
|[Scale](#scale)|
|[ScatterNd](#scatternd)|
|[ScatterNdUpdate](#scatterndupdate)|
|[Select](#select)|
|[Shape](#shape)|
|[Sign](#sign)|
|[Sin](#sin)|
|[Size](#size)|
|[Slice](#slice)|
|[Softmax](#softmax)|
|[SpaceToBatchND](#spacetobatchnd)|
|[SpaceToDepth](#spacetodepth)|
|[SparseToDense](#sparsetodense)|
|[SplitD](#splitd)|
|[SplitV](#splitv)|
|[Sqrt](#sqrt)|
|[Square](#square)|
|[SquaredDifference](#squareddifference)|
|[Squeeze](#squeeze)|
|[StridedSliceV2](#stridedslicev2)|
|[Sub](#sub)|
|[Swish](#swish)|
|[Tan](#tan)|
|[Threshold](#threshold)|
|[Tile](#tile)|
|[TopK](#topk)|
|[TruncateDiv](#truncatediv)|
|[Unpack](#unpack)|
|[Xlogy](#xlogy)|
## 算子定义说明
### Acos
Computes acos of input 'x'.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor, of range [-1, 1]
##### Output:
* <strong><em>y: </em></strong>float
Output tensor of range [0, pi]. Has the same type as 'x'.

### Activation
The Activation op provides different types of nonlinearities for use in neural networks.
These include smooth nonlinearities (sigmoid, tanh, and softplus), continuous but not everywhere
differentiable functions (ReLU), and random regularization.
##### Attributes:
* <strong><em>mode: </em></strong>int (default is 1)
Activation mode, with options as follows:
0 : Sigmoid
1 : ReLU
2 : Tanh
3 : Clipped ReLU
4 : ELU
5 : LeakyRelu
6 : Abs
7 : Relu1
8 : Softsign
9 : Softplus
10 : Hardsigmoid
11 : Threshold ReLU
12 : Selu
14 : Relu6
15 : GeLU.
Defaults to 1 (ReLU). 1.
* <strong><em>coef: </em></strong>float (default is 0.0)
If mode is set to Clipped ReLU(3), ELU(4), Hardsigmoid(10), Threshold ReLU(11), coef can be specified.
otherwise, the value of coef must use default value.
* <strong><em>negative_slope: </em></strong>float (default is 0.0)
If mode is set to Leaky_Relu(5), negative_slope can be specified.
otherwise, the value of negative_slope must use default value.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor with the same type as the input tensor

### Add
Performs tensor addition.
Inputs 'x1' and 'x2' must meet the following constraints:
    1. The input shape can be 1D-4D.
        1D: 1 is added to the front and 2 is added in the back. For example, 5 is padded into: 1,5,1,1
        2D: 1 is added to the front and back. For example, 5,5 is padded into: 1,5,5,1
        3D: 1 is added to the front. For example, 5,5,5 is padded into: 1,5,5,5
    2. For the two inputs, the corresponding dimensions must have the same value, or one of them is 1.
##### Input:
* <strong><em>x1: </em></strong>float, int32, int64
First operand
* <strong><em>x2: </em></strong>float, int32, int64
Second operand
##### Output:
* <strong><em>y: </em></strong>float, int32, int64
Result of same element type as the two inputs

### ArgMaxExt2
Determine the largest value of inputs. Returns the index with the largest value across axis of a tensor.
##### Attributes:
* <strong><em>output_type: </em></strong>int (default is 3)
Data type of output tensor
* <strong><em>keep_dims: </em></strong>bool (default is false)
If false,the rank of the tensor is reduced by 1 for each entry in axis.
If true, the reduced dimensions are retained with length 1.
* <strong><em>outmaxval: </em></strong>bool (default is false)
If true, the max value is returned; if false, the max value index or value is returned.
Only outmaxval = false is supported.
* <strong><em>topk: </em></strong>int (default is 1)
The number k of maximal items to output.
##### Input:
* <strong><em>x: </em></strong>float, int32
First input tensor
* <strong><em>axis: </em></strong>int32
Second input tensor, must be const op. Axis of the input tensor to reduce
##### Output:
* <strong><em>y: </em></strong>int32, int64
Output tensor, max value index or max value

### Asin
Computes asin of input 'x'.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor, of range [-1, 1]
##### Output:
* <strong><em>y: </em></strong>float
Output tensor of range [-pi/2, pi/2]. Has the same type as 'x'.

### Atan
Computes the trignometric inverse tangent of x input.
##### Input:
* <strong><em>x: </em></strong>float
the input tensor.
##### Output:
* <strong><em>y: </em></strong>float
the output tensor.

### AvgPoolV2
Pools the input tensors by taking the average, etc. within regions.
##### Attributes:
* <strong><em>ksize: </em></strong>list of ints(required attr)
ksize size, specifying the height and width of each filter. Here the size must be 2 or 1
and value >= 1.
* <strong><em>strides: </em></strong>list of ints(required attr)
Stride size, specifying the intervals at which to apply the filters to the input.
Here the size must be 2 or 1 and value >= 1.
* <strong><em>padding_mode: </em></strong>string (default is NOTSET)
Pad mode, either NOTSET, VALID or SAME_UPPER. Defaults to NOTSET.
* <strong><em>pads: </em></strong>list of ints (default is {0, 0, 0, 0})
Pad size, specifying the number of pixels to (implicitly) add to each side of the input.
Here the size must be 4 or 2 and value >= 0.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
* <strong><em>global_pooling: </em></strong>bool (default is false)
Defaults to false. When global_pooling is true, ksize values are ignored.
* <strong><em>ceil_mode: </em></strong>bool (default is false)
Default false (floor: The largest integer not greater than the argument),
or true (ceil: The smallest integer greater than argument)
* <strong><em>exclusive: </em></strong>bool (default is true)
Ignore padding area or not when calculating average.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### BatchMatMul
Multiplies slices of two tensors in batches.
##### Attributes:
* <strong><em>adj_x1: </em></strong>bool (default is false)
adj_x1 is true, the input tensor x1  is  transposed, otherwise it will not be transposed.
Default is false.
* <strong><em>adj_x2: </em></strong>bool (default is false)
adj_x2 is true, the input tensor x2  is  transposed, otherwise it will not be transposed.
Default is false.
##### Input:
* <strong><em>x1: </em></strong>float
The input tensor
* <strong><em>x2: </em></strong>float
The input tensor
##### Output:
* <strong><em>y: </em></strong>float
The output tensor

### BatchToSpaceND
This operation reshapes the "batch" dimension 0 into height and width that divided by block.
Interleaves these blocks back into the grid defined by the spatial dimensions [batch, depth, height, width],
to obtain a result with the same rank as the input. The spatial dimensions of this intermediate result
are then optionally cropped according to crops to produce the output.
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor with shape [batch, depth, height, width]. It is required that the elements of x.dimension[0]
must be divided by block_shape.dimension[0] * block_shape.dimension[1].
* <strong><em>block_shape: </em></strong>int32
Const OP, 1-D with shape [M], where all values must be >= 1
* <strong><em>crops: </em></strong>int32
Const OP, 2-D with shape [M, 2], where all values must be >= 0. crops[i] = [crop_start, crop_end]
specifies the amount to crop from input dimension i + 1, which corresponds to spatial dimension i.
It is required that crop_start[i] + crop_end[i] <= block_shape[i] * input_shape[i + 1].
##### Output:
* <strong><em>y: </em></strong>float
Tensor of the same type as 'x'

### BiasAdd
Adds bias to 'x'.
##### Attributes:
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
##### Input:
* <strong><em>x: </em></strong>float
Input tensor of any number of dimensions
* <strong><em>bias: </em></strong>float
1D tensor of size equal to the C dimension of 'x'
##### Output:
* <strong><em>y: </em></strong>float
Broadcast sum of 'x' and 'bias'

### BNInference
Normalizes the input to have 0-mean and/or unit (1) variance across the batch (batch normalization).
##### Attributes:
* <strong><em>momentum: </em></strong>float (default is 0.9f)
Momentum for the running mean and running variance
running_mean is equal to  [ running_mean * momentum + mean * (1 - momentum)].
* <strong><em>epsilon: </em></strong>float (default is 1e-5f)
Small float number added to the variance of 'x'
* <strong><em>mode: </em></strong>int (default is 1)
BatchNorm mode.
0: The bnScale and bnBias tensors are of size 1xCxHxW.
1: The bnScale and bnBias tensors are of size 1xCx1x1.
* <strong><em>use_global_stats: </em></strong>bool (default is true)
Must be true.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
* <strong><em>mean: </em></strong>float
Tensor for population mean. Used for inference only.
* <strong><em>variance: </em></strong>float
Tensor for population variance. Used for inference only.
* <strong><em>scale: </em></strong>float(optional input)
Tensor for scaling factor, to scale the normalized 'x'
* <strong><em>offset: </em></strong>float(optional input)
Tensor for bias, to shift to the normalized 'x'
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### BroadcastTo
Broadcasts an array for a compatible shape.
##### Input:
* <strong><em>x: </em></strong>float, int8, uint8, bool, int32
A Tensor to broadcast.
* <strong><em>shape: </em></strong>int32
The shape of the desired output, must be const op.
##### Output:
* <strong><em>y: </em></strong>float, int8, uint8, bool, int32
Result, has the same type as x.

### CastT
Casts 'x' of input type 'src_dtype' to 'y' of 'dst_dtype'.
##### Attributes:
* <strong><em>src_dtype: </em></strong>int(required attr)
Data type of the input tensor, same as x
DT_FLOAT(0), DT_INT8(2), DT_INT32(3), DT_UINT8(4), DT_INT64(9), DT_BOOL(12)
* <strong><em>dst_dtype: </em></strong>int(required attr)
Data type which will be cast to, same as y
DT_FLOAT(0), DT_INT8(2), DT_INT32(3), DT_UINT8(4), DT_INT64(9), DT_BOOL(12)
##### Input:
* <strong><em>x: </em></strong>bool, int32, uint8, float, int64, int8
Input tensor to be cast. Must be non const OP.
##### Output:
* <strong><em>y: </em></strong>bool, int32, uint8, float, int64, int8
Output tensor with the same shape as input

### Ceil
Computes smallest integer not less than x.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### ClipByValue
Given a tensor x, this operation returns a tensor of the same type and shape as x with its values clipped to
clip_value_min and clip_value_max. Any values less than clip_value_min are set to clip_value_min.
Any values greater than clip_value_max are set to clip_value_max.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor or IndexedSlices
* <strong><em>clip_value_min: </em></strong>float
The value is a 0-D (scalar) Tensor, or a Tensor with the same shape as x.
The minimum value to clip by.
* <strong><em>clip_value_max: </em></strong>float
The value is a 0-D (scalar) Tensor, or a Tensor with the same shape as x.
The maximum value to clip by.
##### Output:
* <strong><em>y: </em></strong>float
the output tensor.

### ConcatD
Concatenates a list of tensors into a single tensor along one dimension.
The number of dimensions of the input tensors must match, and all dimensions except axis must be equal.
##### Attributes:
* <strong><em>concat_dim: </em></strong>int(required attr)
Dimension along which to concatenate
* <strong><em>N: </em></strong>int (default is 1)
Number of inputs
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, int8, bool(dynamic input)
List of tensors for concatenation
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, int8, bool
Concatenated tensor

### Const
Constant tensor
##### Attributes:
* <strong><em>value: </em></strong>tensor (new (std::nothrow) Tensor(TensorDesc()))
Value for the elements of the output tensor
##### Output:
* <strong><em>y: </em></strong>float, int8, int32, bool
Output tensor containing the same value of the provided tensor

### Convolution
Consumes an input tensor and a filter, and computes the output.
##### Attributes:
* <strong><em>strides: </em></strong>list of ints(required attr)
Stride along each axis.
* <strong><em>dilations: </em></strong>list of ints (default is {1, 1})
Dilation value along each axis of the filter.
* <strong><em>pads: </em></strong>list of ints (default is {0, 0, 0, 0})
Padding for the beginning and ending along each axis [hh, ht, wh, wt] or [wh, wt].
* <strong><em>pad_mode: </em></strong>string (default is SPECIFIC)
Pad mode, SPECIFIC(Default): not set, using pads; SAME, SAME_UPPER, SAME_LOWER or VALID.
The padding is split between the two sides equally or
almost equally (depending on whether it is even or odd).
In case the padding is an odd number, the extra padding is added at the end for SAME_UPPER or SAME and
at the beginning for SAME_LOWER
* <strong><em>groups: </em></strong>int (default is 1)
Number of groups input channels and output channels are divided into.
When groups = 1, traditional convolution will be performed;
When groups > 1, feature maps are grouped by group_count, and then each groups
is convoluted separately. Specially, 'groups' equal to the number of input feature
maps indicates Depthwise Convolution.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
* <strong><em>offset_x: </em></strong>int (default is 0)
Reserved. For quantized.
##### Input:
* <strong><em>x: </em></strong>float, uint8
Input tensor with size [N, Ci, Hi, Wi] or [N, Ci, Wi] or [N, Hi, Wi, Ci] or [N, Wi, Ci].
* <strong><em>filter: </em></strong>float, int8
With shape [Co, Ci/group, Hk, Wk] or [Co, Ci/group, Wk], must be a const op when data_format is 'NHWC'.
* <strong><em>bias: </em></strong>float, int32(optional input)
With shape [Co].
* <strong><em>offset_w: </em></strong>int8(optional input)
Reserved. For quantized.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### ConvolutionDepthwise
Computes a depthwise convolution from given input and filter tensors.
##### Attributes:
* <strong><em>strides: </em></strong>list of ints(required attr)
Stride along each axis.
* <strong><em>dilations: </em></strong>list of ints (default is {1, 1})
Dilation value along each axis of the filter.
* <strong><em>pads: </em></strong>list of ints (default is {0, 0, 0, 0})
Padding for the beginning and ending along each axis [hh, ht, wh, wt].
* <strong><em>pad_mode: </em></strong>string (default is SAME)
Pad mode, 'SPECIFIC'(not set, only support in CPUCL); 'SAME', or 'VALID'.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
* <strong><em>offset_x: </em></strong>int (default is 0)
Reserved. For quantized.
##### Input:
* <strong><em>x: </em></strong>float, uint8
Input tensor with size [N, Ci, Hi, Wi].
* <strong><em>filter: </em></strong>float, int8
With shape [Co, 1, Hk, Wk].
* <strong><em>bias: </em></strong>float, int32(optional input)
With shape [Co], must be a Const-OP.
* <strong><em>offset_w: </em></strong>int8(optional input)
Reserved. For quantized.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### ConvTranspose
Computes the gradients of convolution with respect to the output_shape.
If the HiaiVersion is earlier than 320, the input output_shape is required and the bias is not supported.
##### Attributes:
* <strong><em>strides: </em></strong>list of ints(required attr)
Stride along each axis.
* <strong><em>pads: </em></strong>list of ints (default is {0, 0, 0, 0})
Padding for the beginning and ending along each axis [hh, ht, wh, wt] or [wh, wt].
* <strong><em>pad_mode: </em></strong>string (default is SPECIFIC)
Pad mode, SPECIFIC(Default): not set, using pads; SAME, or VALID
* <strong><em>dilations: </em></strong>list of ints (default is {1, 1})
Dilation value along each axis of the filter.
* <strong><em>groups: </em></strong>int (default is 1)
Number of groups input channels and output channels are divided into.
When groups = 1, traditional convolution will be performed;
When groups > 1, feature maps are grouped by group_count, and then each groups
is convoluted separately. Specially, 'groups' equal to the number of input feature
maps indicates Depthwise Convolution.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
* <strong><em>offset_x: </em></strong>int (default is 0)
Reserved. For quantized.
##### Input:
* <strong><em>output_shape: </em></strong>int32(optional input)
The output shape, which is [batch, output_channels, height, width] or
[batch, output_channels, width] tensor
* <strong><em>filter: </em></strong>float, int8
when data_format is 'NCHW', with shape [Ci, Co/group, Hk, Wk] or [Ci, Co/group, Wk]
must be a Const-OP.
* <strong><em>x: </em></strong>float, int64, uint8
Input tensor with size [N, Ci, Hi, Wi] or [N, Ci, Wi].
* <strong><em>bias: </em></strong>float, int32(optional input)
With shape [Co], must be a Const-OP.
* <strong><em>offset_w: </em></strong>int8(optional input)
Reserved. For quantized.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### Cos
Computes cos of 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### Crop
To crop ,elements of the first input are selected to fit the dimensions of the second input.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is 2)
The Dimension of input which to be croped.
* <strong><em>offsets: </em></strong>list of ints(required attr)
The offsets of input x.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, bool
The tensor to be croped.
* <strong><em>size: </em></strong>int32
The size of the input x to be croped.
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
The output tensor.

### CropAndResize
Extracts crops from the input image tensor and resizes them.
##### Attributes:
* <strong><em>extrapolation_value: </em></strong>float (default is 0)
Value for extrapolation, default 0.
* <strong><em>method: </em></strong>string (default is bilinear)
Sampling method for resizing either bilinear or nearest. Defaults to bilinear.
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor
* <strong><em>boxes: </em></strong>float
2-D tensor. boxes[1] must be equal to 4.
* <strong><em>box_index: </em></strong>int32
1-D tensor. The value of box_index[i] specifies the image that the i-th box refers to.
box_index[0] must be equal to boxes[0].
* <strong><em>crop_size: </em></strong>int32
weight and height.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Data
Data tensor
##### Attributes:
* <strong><em>index: </em></strong>int (default is 0)
Reserved. The index of input data in network, only support 0 currently.
##### Input:
* <strong><em>x: </em></strong>float, int8, uint8, int32, bool, int64, double
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, int8, uint8, int32, bool, int64, double
Output tensor

### DepthToSpace
Rearranges data from depth into blocks of spatial data.This is the reverse transformation of SpaceToDepth.
More specifically, this op outputs a copy of the input tensor where values from the depth dimension are moved
in spatial blocks to the height and width dimensions.
##### Attributes:
* <strong><em>block_size: </em></strong>int(required attr)
The size of the spatial block, must be greater than 0.
* <strong><em>mode: </em></strong>string (default is DCR)
DCR (default) for depth-column-row order re-arrangement.
CRD for column-row-depth order.
* <strong><em>data_format: </em></strong>string (default is NHWC)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NHWC'
##### Input:
* <strong><em>x: </em></strong>float
The input tensor with shape [ batch, height, width, channels ], channels
must be divided by block_size*block_size.
##### Output:
* <strong><em>y: </em></strong>float
The output tensor

### DequantizeV2
Dequantizes the 'input' tensor of type int32 to 'output' tensor of float.
    This op should be used after quantize op together with QuantizeV2 before quantize op.
##### Attributes:
* <strong><em>deq_scale: </em></strong>list of floats
Dequantize scale values. This attr is not used currently, it can be calculated automatically.
* <strong><em>sqrt_mode: </em></strong>bool (default is false)
Quantize with square root calculations. If true, sqrt the input data.
  Currently sqrt_mode is not supported, param is reserved.
* <strong><em>relu_flag: </em></strong>bool (default is false)
Specifing whether to perform ReLU.
##### Input:
* <strong><em>x: </em></strong>int32
Tensor of type int32.
##### Output:
* <strong><em>y: </em></strong>float
Tensor of type float.

### Eltwise
Compute all input tensors element-wise with specific mode.
##### Attributes:
* <strong><em>N: </em></strong>int(required attr)
The count of the x.
* <strong><em>mode: </em></strong>int (default is 1)
Either 0 (product), 1 (sum), 2 (max). Defaults to 1 (sum).
* <strong><em>coeff: </em></strong>list of floats
Blob-wise coefficient for sum operation. coeff can be set only when mode is set to 1.
##### Input:
* <strong><em>x: </em></strong>float(dynamic input)
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Equal
Calculates whether the input tensors 'x1' and 'x2' are equal, if equal, returns the truth value element-wise.
##### Input:
* <strong><em>x1: </em></strong>uint8, float, bool, int32
First input operand
* <strong><em>x2: </em></strong>uint8, float, bool, int32
Second input operand
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### Erf
Calculates the error function of the given input tensor, element-wise.
##### Input:
* <strong><em>x: </em></strong>uint8, float, int32
input tensor
##### Output:
* <strong><em>y: </em></strong>uint8, float, int32
Output tensor

### Exp
Computes exponential of 'x' element-wise. y = base^(scale*x+shift)
##### Attributes:
* <strong><em>base: </em></strong>float (default is -1.0)
Default -1 for a value of e
* <strong><em>scale: </em></strong>float (default is 1.0)
The scale
* <strong><em>shift: </em></strong>float (default is 0.0)
The shift
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### ExpandDims
Inserts a dimension of 1 into a tensor's shape.
This op support max realdims is 4 and support max padding dims is 3.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, bool
Input tensor
* <strong><em>axis: </em></strong>int32
one element const, dimension index at which to expand the shape of input
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
Output tensor. The dimension of y not support bigger than 4.

### Expm1
Computes exp(x) - 1 element-wise. y = exp(x) - 1
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### FakeQuantWithMinMaxVars
Fake-quantize the inputs tensor of type float via global float scalars.
##### Attributes:
* <strong><em>num_bits: </em></strong>int (default is 8)
An optional int. Defaults to 8. between 2 and 8, inclusive.
* <strong><em>narrow_range: </em></strong>bool (default is false)
An optional bool. Defaults to False.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor of type float32.
* <strong><em>min: </em></strong>float
0D (scalar), A Tensor of type float32.
* <strong><em>max: </em></strong>float
0D (scalar), A Tensor of type float32.
##### Output:
* <strong><em>y: </em></strong>float
A Tensor of type float32.

### Fill
Creates a tensor of shape 'dims' and fills it with 'value'.
##### Input:
* <strong><em>dims: </em></strong>int32
Const OP, 1-D, shape of the output tensor
* <strong><em>value: </em></strong>float, bool, int32, uint8
0-D, value to fill the returned tensor
##### Output:
* <strong><em>y: </em></strong>float, bool, int32, uint8
Output tensor

### Flatten
Flattens the input tensor into a 2D matrix.
##### Input:
* <strong><em>x: </em></strong>float
Tensor of rank
##### Output:
* <strong><em>y: </em></strong>float
2D tensor with the contents of the input tensor, with input dimensions up to axis flattened to
the outer dimension of the output and remaining input dimensions flattened into the inner dimension
of the output.

### Floor
Computes element-wise largest integer not greater than 'x'.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### FloorDiv
Divides x1/x2 element-wise, rounding toward the most negative integer.
##### Input:
* <strong><em>x1: </em></strong>float, int32
First input tensor
* <strong><em>x2: </em></strong>float, int32
Second input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### FloorMod
Computes element-wise remainder of division, discarding decimal places in the negative infinity direction.
##### Input:
* <strong><em>x1: </em></strong>float
First input tensor
* <strong><em>x2: </em></strong>float
Second input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### FullyConnection
Computes an inner product with an input tensor, a set of learned weights and adds biases.
##### Attributes:
* <strong><em>num_output: </em></strong>int(required attr)
Number of neurons output after full connection.
* <strong><em>transpose: </em></strong>bool (default is false)
Reserved. Whether the weight matrix is transposed.
* <strong><em>axis: </em></strong>int (default is 1)
Reserved. Inner product calculation on the axis.
* <strong><em>offset_x: </em></strong>int (default is 0)
Reserved. For quantized.
##### Input:
* <strong><em>x: </em></strong>float, uint8
Input tensor
* <strong><em>w: </em></strong>float, int8
Weight tensor.
* <strong><em>b: </em></strong>float, int32(optional input)
1D tensor for bias.
* <strong><em>offset_w: </em></strong>int8(optional input)
Reserved. For quantized.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor.

### GatherNd
Gathers slices from 'x' into a Tensor with shape specified by 'indices'.
##### Input:
* <strong><em>x: </em></strong>float, int32
Tensor from which to gather values.
* <strong><em>indices: </em></strong>float, int32
Input tensor.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### GatherV2D
Gathers slices from 'x' axis according to 'indices'.
##### Attributes:
* <strong><em>axis: </em></strong>int(required attr)
which axis to gather values. Accepted range is [-rank(x), rank(x)).
##### Input:
* <strong><em>x: </em></strong>float, int32
Tensor from which to gather values
* <strong><em>indices: </em></strong>int32
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### GemmD
Computes an inner product with an input tensor, a set of learned weights and adds biases.
if shape of a is M*K, shape of b is  K*N, shape of c must be N or (1,N).
##### Attributes:
* <strong><em>alpha: </em></strong>float (default is 1.0f)
Scalar multiplier for the product of input tensors a * b.
* <strong><em>beta: </em></strong>float (default is 1.0f)
Scalar multiplier for input tensor c.
* <strong><em>transpose_a: </em></strong>bool (default is false)
Whether a should be transposed.
* <strong><em>transpose_b: </em></strong>bool (default is false)
Whether b should be transposed.
##### Input:
* <strong><em>a: </em></strong>float, uint8
Input tensor.
* <strong><em>b: </em></strong>float, int8
Input tensor.
* <strong><em>c: </em></strong>float, int32(optional input)
Input tensor.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### Greater
Returns the truth value of (x1 > x2) element-wise.
##### Input:
* <strong><em>x1: </em></strong>float, int32, int8, uint8
First input tensor
* <strong><em>x2: </em></strong>float, int32, int8, uint8
Second input tensor
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### GreaterEqual
Returns the truth value of (x1 >= x2) element-wise.
##### Input:
* <strong><em>x1: </em></strong>float, int32
First input tensor
* <strong><em>x2: </em></strong>float, int32
Second input tensor
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### GridSampler2D
Performs an affine or perspective transformation on an image.
##### Attributes:
* <strong><em>interpolation_mode: </em></strong>string (default is bilinear)
Sampling method. Only bilinear is supported.
* <strong><em>padding_mode: </em></strong>string (default is zeros)
Filling mode, which is used to process the situation that the sampling exceeds the boundary of the input
image. The value can be zeros, border.Default zeros.
* <strong><em>align_corners: </em></strong>bool (default is false)
Used to specify the mapping mode between feature map coordinates and feature values.Default false.
##### Input:
* <strong><em>x: </em></strong>float
input feature map. 4-D tensor.
* <strong><em>grid: </em></strong>float
sampling grid. 4-D tensor.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor.  4-D tensor.

### HardSwish
Computes hard-swish activation function.
##### Input:
* <strong><em>x: </em></strong>float
The input tensor.
##### Output:
* <strong><em>y: </em></strong>float
The output tensor.

### InstanceNorm
Computes instance norm
##### Attributes:
* <strong><em>data_format: </em></strong>string (default is NCHW)
format of input, 'NCHW' or 'NHWC'. Default is 'NCHW'
* <strong><em>epsilon: </em></strong>float (default is 1e-7f)
A very small float number used to avoid dividing by zero.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor which supports 4D dimension format.
* <strong><em>gamma: </em></strong>float
A tesnor, multiple to result
* <strong><em>beta: </em></strong>float
A tensor, add to result
##### Output:
* <strong><em>y: </em></strong>float
Output tensor
* <strong><em>mean: </em></strong>float(optional output)
Reserved
* <strong><em>variance: </em></strong>float(optional output)
Reserved

### LayerNorm
Computes layer norm
##### Attributes:
* <strong><em>begin_norm_axis: </em></strong>int (default is 1)
Reserved.
* <strong><em>begin_params_axis: </em></strong>int (default is 1)
Reserved.
NOTE: By default, the preceding two parameters are set to 1 (standard LayerNorm).
      Other values do not take effect.
* <strong><em>epsilon: </em></strong>float (default is 1e-7f)
A very small float number used to avoid dividing by zero.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor, a 2D-4D tensor
* <strong><em>gamma: </em></strong>float
A tensor, multiple to result
* <strong><em>beta: </em></strong>float
A tensor, add to result
##### Output:
* <strong><em>y: </em></strong>float
Output tensor
* <strong><em>mean: </em></strong>float(optional output)
Reserved
* <strong><em>variance: </em></strong>float(optional output)
Reserved

### Less
Returns the truth value of (x1 < x2) element-wise.
##### Input:
* <strong><em>x1: </em></strong>float
First input tensor
* <strong><em>x2: </em></strong>float
Second input tensor
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### LessEqual
Returns the truth value of (x1 <= x2) element-wise.
##### Input:
* <strong><em>x1: </em></strong>float
Tensor of type float.
* <strong><em>x2: </em></strong>float
Tensor of the same type as 'x1'.
##### Output:
* <strong><em>y: </em></strong>bool
Tensor of type bool.

### Log
LogLayer computes outputs y = log_base(shift + scale * x), for base > 0.
Or if base is set to the default (-1), base is set to e,
so y = ln(shift + scale * x) = log_e(shift + scale * x)
##### Attributes:
* <strong><em>base: </em></strong>float (default is -1.0)
float, default is -1, which means set base to e
* <strong><em>scale: </em></strong>float (default is 1.0)
float, default is 1, which multiplies x
* <strong><em>shift: </em></strong>float (default is 0.0)
float, default is 0, which adds to scale*x
##### Input:
* <strong><em>x: </em></strong>float
Input tensor.
##### Output:
* <strong><em>y: </em></strong>float
Output Tensor.

### Log1p
Computes natural logarithm of (1 + x) element-wise.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### LogicalAnd
Returns the logical and truth value of 'x1' and 'x2' element-wise.
##### Input:
* <strong><em>x1: </em></strong>bool
First input operand
* <strong><em>x2: </em></strong>bool
Second input operand
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### LogicalNot
Returns the truth value of NOT 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>bool
Input tensor
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### LogicalOr
Computes natural logarithm of input 'x'.
##### Input:
* <strong><em>x1: </em></strong>bool
Tensor of type bool
* <strong><em>x2: </em></strong>bool
Tensor of type bool
##### Output:
* <strong><em>y: </em></strong>bool
Tensor of type bool

### LogicalXor
Returns the XOR value of 'x1' and 'x2' element-wise.
##### Input:
* <strong><em>x1: </em></strong>bool
First input operand
* <strong><em>x2: </em></strong>bool
Second input operand
##### Output:
* <strong><em>y: </em></strong>bool
Output tensor

### LogSoftmax
Computes the LogSoftmax (normalized exponential) values for each layer in the batch of the given input.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is -1)
Dimension LogSoftmax would be performed on
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output values, with the same shape as the input tensor

### MatMul
Multiplies matrix 'x1' by matrix 'x2'.
If both inputs are 2-D, the inner dimension of 'x1' (after being transposed if 'transpose_x1' is true)
must match the outer dimension of 'x2' (after being transposed if 'transposed_x2' is true).
If the first argument is 1-D, it is promoted to a matrix by prepending a 1 to its dimensions.
After matrix multiplication the prepended 1 is removed.
If the second argument is 1-D, it is promoted to a matrix by appending a 1 to its dimensions.
After matrix multiplication the appended 1 is removed.
##### Attributes:
* <strong><em>transpose_x1: </em></strong>bool (default is false)
If true, 'x1' is transposed before multiplication.
* <strong><em>transpose_x2: </em></strong>bool (default is false)
If true, 'x2' is transposed before multiplication.
##### Input:
* <strong><em>x1: </em></strong>float, uint8
First input tensor.
* <strong><em>x2: </em></strong>float, int8
Second input tensor.
* <strong><em>bias: </em></strong>float, int32(optional input)
Reserved - Optional input tensor, bias data
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### Maximum
Computes the maximum of input tensors 'x1' and 'x2' element-wise.
The dimension of 'x1' and 'x2' must be the same unless 'x2' is a scalar.
##### Input:
* <strong><em>x1: </em></strong>float, int64
Input tensor. Must be non const OP
* <strong><em>x2: </em></strong>float, int64
Input tensor. Must be non const OP
##### Output:
* <strong><em>y: </em></strong>float, int64
Maximum of 'x1' and 'x2'

### Minimum
Computes the minimum of input tensors 'x1' and 'x2' element-wise.
##### Input:
* <strong><em>x1: </em></strong>float, int32
Input tensor
* <strong><em>x2: </em></strong>float, int32
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32
Minimum of 'x1' and 'x2'

### MirrorPad
Pads a tensor with mirrored values.
##### Attributes:
* <strong><em>mode: </em></strong>string(required attr)
REFLECT or SYMMETRIC.
SYMMETRIC  paddings must be no greater than the x dimension size
REFLECT  paddings must be less than the x dimension size
##### Input:
* <strong><em>x: </em></strong>float, int32, bool, int64
The input tensor
* <strong><em>paddings: </em></strong>int32, int64
The values of paddings, as a role of dimensions to be added on the input tensor x,
must be a Const-OP.
##### Output:
* <strong><em>y: </em></strong>float, int32, bool, int64
The output tensor

### Mish
A Self Regularized Non-Monotonic Neural Activation Function
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Mul
Performs tensor multiplication.
##### Input:
* <strong><em>x1: </em></strong>float, int32, int64
A Tensor
* <strong><em>x2: </em></strong>float, int32, int64
A Tensor, Must have the same type and dimensions as x
##### Output:
* <strong><em>y: </em></strong>float, int32, int64
Result of the same element type as the two inputs

### Neg
Computes numerical negative value input.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### NonMaxSuppressionV6
Performs non-maximum suppression (NMS) on the boxes according to their intersection-over-union (IoU).
NMS iteratively removes lower scoring boxes which have an IoU greater than iou_threshold
with another (higher scoring) box.
##### Attributes:
* <strong><em>center_point_box: </em></strong>int (default is 0)
Integer indicate the format of the box data.
The default is 0. 0 - the box data is supplied as [y1, x1, y2, x2]
where (y1, x1) and (y2, x2) are the coordinates of any diagonal pair of box corners and the
coordinates can be provided as normalized (i.e., lying in the interval [0, 1]) or absolute.
Mostly used for TF models. 1 - the box data is supplied as [x_center, y_center, width, height]
Mostly used for Pytorch models.
##### Input:
* <strong><em>boxes: </em></strong>float
An input tensor with shape [num_batches, spatial_dimension, 4]. The single box data format is
indicated by center_point_box.
* <strong><em>scores: </em></strong>float
An input tensor with shape [num_batches, num_classes, spatial_dimension]
* <strong><em>max_output_boxes_per_class: </em></strong>int32(optional input)
Integer representing the maximum number of boxes to be selected per batch per class.
It is a scalar. Only positive numbers(excluding 0) are supported, 1 for default.
* <strong><em>iou_threshold: </em></strong>float(optional input)
Float representing the threshold for deciding whether boxes overlap too much with respect
to IOU. It is scalar. Value range [0, 1].
* <strong><em>score_threshold: </em></strong>float(optional input)
Float representing the threshold for deciding when to remove boxes based on score.
It is a scalar.
##### Output:
* <strong><em>selected_indices: </em></strong>int32
selected indices from the boxes tensor. [num_selected_indices, 3],
the selected index format is [batch_index, class_index, box_index].

### NotEqual
Returns the truth value of (x1 != x2) element-wise.
##### Input:
* <strong><em>x1: </em></strong>float
Tensor of type float.
* <strong><em>x2: </em></strong>float
Tensor of the same type as 'x1'.
##### Output:
* <strong><em>y: </em></strong>bool
Tensor of type bool

### OneHot
Return a one-hot tensor
##### Attributes:
* <strong><em>axis: </em></strong>int (default is -1)
The axis to fill.(default: -1)
##### Input:
* <strong><em>x: </em></strong>int32, uint8
A index tensor.
* <strong><em>depth: </em></strong>int32
A scalar defining the depth of the one hot dimension.
* <strong><em>on_value: </em></strong>uint8, int8, float, bool
A scalar defining the value to fill in output when x[j] = i. (default: 1).
* <strong><em>off_value: </em></strong>uint8, int8, float, bool
A scalar defining the value to fill in output when x[j] != i. (default: 0).
##### Output:
* <strong><em>y: </em></strong>uint8, int8, float, bool
Result, has element type as T. The dimension of y not support bigger than 4.

### Pack
Stacks a list of rank-R tensors into one rank-(R+1) tensor.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is 0)
Axis to stack along
* <strong><em>N: </em></strong>int(required attr)
Number of values
##### Input:
* <strong><em>x: </em></strong>float, int32(dynamic input)
List of Tensor objects with the same shape and type
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor. The dimension of y not support bigger than 4.

### Pad
Pads a tensor.
##### Input:
* <strong><em>x: </em></strong>float, int32
The input tensor
* <strong><em>paddings: </em></strong>int32
The values of padding, as a role of dimensions to be added on the input tensor x,
with shape [D, 2], D is the rank of x.
##### Output:
* <strong><em>y: </em></strong>float, int32
The output tensor

### Permute
Permutes the dimensions of the input according to a given pattern.
##### Attributes:
* <strong><em>order: </em></strong>list of ints (default is {0})
Tuple of dimension indices indicating the permutation pattern, list of dimension indices.
When order is -1, it means reverse order.
##### Input:
* <strong><em>x: </em></strong>float, uint8, int32, int64, bool
Input tensor.
##### Output:
* <strong><em>y: </em></strong>float, uint8, int32, int64, bool
Has the same shape as the input, but with the dimensions re-ordered according to the specified pattern.

### PoolingD
Pools the input tensors by taking the max, average, etc. within regions.
##### Attributes:
* <strong><em>mode: </em></strong>int (default is 0)
Either 0 max pooling), 1 (avg pooling), or 2 (L2 pooling)
* <strong><em>pad_mode: </em></strong>int (default is 0)
Pad mode, either 0 (NOTSET), 5 (VALID) or 6 (SAME). Defaults to 0 (NOTSET).
* <strong><em>global_pooling: </em></strong>bool (default is false)
Defaults to false. When global_pooling is true, window values are ignored.
* <strong><em>window: </em></strong>list of ints (default is {1, 1})
Window size, specifying the height and width of each filter. Here the size must be 2 or 1
and value >= 1.
* <strong><em>pad: </em></strong>list of ints (default is {0, 0, 0, 0})
Pad size, specifying the number of pixels to (implicitly) add to each side of the input.
Here the size must be 4 or 2 and value >= 0.
* <strong><em>stride: </em></strong>list of ints (default is {1, 1})
Stride size, specifying the intervals at which to apply the filters to the input.
Here the size must be 2 or 1 and value >= 1.
* <strong><em>ceil_mode: </em></strong>int (default is 0)
Default 0 (floor: The largest integer not greater than the argument),
or 1 (ceil: The smallest integer greater than argument)
* <strong><em>data_mode: </em></strong>int (default is 1)
Data mode, either 0 (rounded up) or 1 (rounded down)
##### Input:
* <strong><em>x: </em></strong>float, uint8
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, uint8
Output tensor

### Pow
Computes x1^x2 for corresponding elements in x1 and x2.
##### Input:
* <strong><em>x1: </em></strong>float, int32
the input tensor.
* <strong><em>x2: </em></strong>float, int32
the exponent value, the datatype of x1 and x2 must be equal.
##### Output:
* <strong><em>y: </em></strong>float, int32
return a tensor for the result of x1^x2.

### Power
Computes y = (alpha*x + beta)^gamma, as specified by the scale:alpha, shift:beta, and power:gamma.
##### Attributes:
* <strong><em>scale: </em></strong>float (default is 1.0)
(optional, default 1) the scale : alpha.
* <strong><em>shift: </em></strong>float (default is 0.0)
(optional, default 0) the shift : beta.
* <strong><em>power: </em></strong>float (default is 1.0)
(optional, default 1) the power : gamma.
##### Input:
* <strong><em>x: </em></strong>float
the input tensor, must be non const op.
##### Output:
* <strong><em>y: </em></strong>float
the computed outputs y = (alpha*x + beta)^gamma.

### PRelu
Performs parametric ReLU, produces one output data (Tensor)
where the function f(x) = slope * x for x < 0, f(x) = x for x >= 0
##### Attributes:
* <strong><em>data_format: </em></strong>string (default is NCHW)
format of input, 'NCHW' or 'NHWC'. Default is 'NCHW'
##### Input:
* <strong><em>x: </em></strong>float
A multi-dimensional Tensor of type float32.
* <strong><em>weight: </em></strong>float
if data_format is 'NCHW', the shape of Slope tensor must be [1, C, 1, 1] or [C, 1, 1].
if data_format is 'NHWC', the shape of Slope tensor must be [C].
if x is 1-D or 2-D tensor, the shape of Slope tensor must be [C], C is the channel of x tensor.
##### Output:
* <strong><em>y: </em></strong>float
Output values, with the same shape as the input tensor x

### QuantizeV2
Quantizes the 'input' tensor of type float to 'output' tensor of type specified.
    This op should be used before quantize op together with DequantizeV2 after quantize op.
##### Attributes:
* <strong><em>scale: </em></strong>list of floats(required attr)
Quantize scale values. Currently only support Per Layer mode, scale value size is 1.
* <strong><em>offset: </em></strong>list of floats(required attr)
Quantize offset values. offset value nums is equal to scale values nums.
* <strong><em>dtype: </em></strong>int(required attr)
Output data type after quantize.
* <strong><em>round_mode: </em></strong>string (default is Round)
Quantize round mode. Candidates:Round, Floor, Ceil.
Currently round_mode is not supported, param is reserved.
* <strong><em>sqrt_mode: </em></strong>bool (default is false)
Quantize with square root calculations. If true, sqrt the input data.
Currently sqrt_mode is not supported, param is reserved.
##### Input:
* <strong><em>x: </em></strong>float
Tensor of type float.
##### Output:
* <strong><em>y: </em></strong>int64, uint8, int8, int4
Tensor of type specified by dtype.

### Range
According to the input requirements, creates a sequence of numbers.
##### Input:
* <strong><em>start: </em></strong>float, int32
0-D Tensor (scalar). Acts as first entry in the range
* <strong><em>limit: </em></strong>float, int32
0-D Tensor (scalar). Upper limit of sequence
* <strong><em>delta: </em></strong>float, int32
0-D Tensor (scalar). Number that increments 'start'.
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### Rank
Returns the rank of a tensor
##### Input:
* <strong><em>x: </em></strong>float, int32, bool, uint8
Tensor of type float, int32, bool, uint8.
##### Output:
* <strong><em>y: </em></strong>int32
Rank of the input tensor.

### RealDiv
Returns x1/x2 element-wise for real types.
For the two inputs, the corresponding dimensions must have the same value, or one of them is 1.
##### Input:
* <strong><em>x1: </em></strong>float, int32
First input tensor
* <strong><em>x2: </em></strong>float, int32
Second input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32
Output tensor

### Reciprocal
Computes the reciprocal of the input tensor 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Tensor of the same type as 'x'

### ReduceLogSumExp
Computes log(sum(exp(elements across dimensions of a tensor))).
##### Attributes:
* <strong><em>axes: </em></strong>list of ints(required attr)
The dimensions to reduce.
Must be in the range [-rank(x), rank(x)).
* <strong><em>keepdims: </em></strong>bool (default is false)
If false,the rank of the tensor is reduced by 1 for each entry in axis.
If true, the reduced dimensions are retained with length 1.
##### Input:
* <strong><em>x: </em></strong>float
input tensor
##### Output:
* <strong><em>y: </em></strong>float
output tensor

### ReduceMax
Computes the maximum of elements across dimensions of a tensor.
##### Attributes:
* <strong><em>keep_dims: </em></strong>bool (default is false)
If true, retains reduced dimensions with length 1.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8
The tensor to reduce. Should have real numeric type.
* <strong><em>axes: </em></strong>int32
The dimensions to reduce.Must be in the range [-rank(x), rank(x)). Const Op.
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8
the output tensor.

### ReduceMean
The function of ReduceMean operator is computing the mean of elements across dimensions of a tensor.
##### Attributes:
* <strong><em>keep_dims: </em></strong>bool (default is false)
If true, retains reduced dimensions with length 1.
##### Input:
* <strong><em>x: </em></strong>float
The input tensor, with the type of Data.
* <strong><em>axes: </em></strong>int32
Const tensor. Describes the dimensions to reduce.
Must be in the range [-rank(input_tensor), rank(input_tensor)).
##### Output:
* <strong><em>y: </em></strong>float
The reduced tensor.

### ReduceMin
Computes the minimum of elements across dimensions of a tensor.
##### Attributes:
* <strong><em>keep_dims: </em></strong>bool (default is false)
If true, retains reduced dimensions with length 1.
##### Input:
* <strong><em>x: </em></strong>float
The tensor to reduce. Should have real numeric type.
* <strong><em>axes: </em></strong>int32
The dimensions to reduce.Must be in the range [-rank(x), rank(x)). Const Op.
##### Output:
* <strong><em>y: </em></strong>float
The output tensor.

### ReduceProdD
Returns the product of elements across dimensions of the input tensor.
##### Attributes:
* <strong><em>axes: </em></strong>list of ints(required attr)
Dimensions to reduce. Must be in the range [-rank(x), rank(x)).
* <strong><em>keep_dims: </em></strong>bool (default is false)
If true, retains reduced dimensions with length 1.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8
Output tensor

### ReduceSum
Returns the sum of elements across dimensions of the input tensor.
##### Attributes:
* <strong><em>keep_dims: </em></strong>bool (default is false)
If true, retains reduced dimensions with length 1.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8
Tensor to reduce
* <strong><em>axes: </em></strong>int32
Dimensions to reduce
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8
Output tensor

### Reshape
Reshape the input tensor.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is 0)
Dimension along which to reshape
* <strong><em>num_axes: </em></strong>int (default is -1)
Used to calculate the output shape
When 'num_axes' is -1, output.size() = shape.size() + axis.
When 'num_axes' is not -1, output.size() = shape.size() + tensor.size() - num_axes.
##### Input:
* <strong><em>x: </em></strong>float, int32, int64, bool
Input tensor, of the same type as Data
* <strong><em>shape: </em></strong>int32, int64
The shape to be resized, must be const op
##### Output:
* <strong><em>y: </em></strong>float, int32, int64, bool
Reshaped tensor that has the same values as Attr 'shape'

### Resize
Resize the input tensor.
##### Attributes:
* <strong><em>coordinate_transformation_mode: </em></strong>string (default is asymmetric)
This attribute describes how to transform the coordinate in the resized tensor
to the coordinate in the original tensor. Only support asymmetric.
* <strong><em>cubic_coeff_a: </em></strong>float (default is -0.75)
The coefficient 'a' used in cubic interpolation. Only support -0.75.
* <strong><em>exclude_outside: </em></strong>int (default is 0)
If set to 1, the weight of sampling locations outside the tensor will be set to 0 and
the weight will be renormalized so that their sum is 1.0. Only support 0.
* <strong><em>extrapolation_value: </em></strong>float (default is 0)
Reserved. Only support 0.
* <strong><em>mode: </em></strong>string (default is nearest)
Interpolation modes, Only support nearest.
* <strong><em>nearest_mode: </em></strong>string (default is round_prefer_floor)
It indicates how to get "nearest" pixel in input tensor from x_original.
Support round_prefer_floor or round_prefer_ceil.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor.
* <strong><em>roi: </em></strong>float(optional input)
Reserved.
* <strong><em>scales: </em></strong>float(optional input)
The scale array along each dimension. Only Support [1.0, 1.0, scaleH, scaleW].
* <strong><em>sizes: </em></strong>int32(optional input)
Target size of the output tensor. Only one of 'scales' and 'sizes' can be specified.
1-D of four elements.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### ResizeBicubic
Resize images to size using bicubic interpolation.
##### Attributes:
* <strong><em>align_corners: </em></strong>bool (default is false)
If true, the centers of the 4 corner pixels of the input and output tensors are aligned,
preserving the values at the corner pixels. Defaults to false
* <strong><em>half_pixel_centers: </em></strong>bool (default is false)
If true, the align_corners must be false. and this attr change the mapping way
to src image, default value is false.
* <strong><em>data_format: </em></strong>string (default is NCHW)
Format of operator, 'NCHW' or 'NHWC'. Default is 'NCHW'
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor
* <strong><em>size: </em></strong>int32
1-D tensor with shape [height, width]
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### ResizeBilinear
Resizes images to 'size' using bilinear interpolation.
##### Attributes:
* <strong><em>align_corners: </em></strong>bool (default is false)
If true, the centers of the 4 corner pixels of the input and output tensors
are aligned, preserving the values at the corner pixels.
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor
* <strong><em>size: </em></strong>int32, float
1-D tensor with shape [height, width], must be const op.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### ResizeNearestNeighborV2
Resizes images to 'size' using nearest neighbor interpolation.
##### Attributes:
* <strong><em>align_corners: </em></strong>bool (default is false)
If true, the centers of the 4 corner pixels of the input and output tensors are aligned,
preserving the values at the corner pixels. Defaults to false
* <strong><em>half_pixel_centers: </em></strong>bool (default is false)
If true, the align_corners must be false. and this attr change the mapping way
to src image, default value is false.
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor
* <strong><em>size: </em></strong>int32, float
1-D of two elements
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Rint
Finds and returns the integer closest to 'x', and if the result is between two representable values,
select an even representation.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### ROIAlignV2
Consumes an input tensor X and region of interests (rois) to apply pooling across each RoI.
##### Attributes:
* <strong><em>spatial_scale: </em></strong>list of floats(required attr)
A scaling factor that maps the raw image coordinates to the input feature map coordinates,
{ spatial_scale }. Generally is{ 1.0f }.
* <strong><em>pooled_height: </em></strong>int(required attr)
Pooled output y's height.
* <strong><em>pooled_width: </em></strong>int(required attr)
Pooled output y's width.
* <strong><em>sample_num: </em></strong>int (default is 0)
Number of sampling points, default 0.
* <strong><em>roi_end_mode: </em></strong>int (default is 1)
Used to determine whether roi_end_h / roi_end_w is incremented by 1,
0: roi_end_h / roi_end_w does not add one, 1: roi_end_h / roi_end_w add one. Default is 1.
* <strong><em>mode: </em></strong>string (default is avg)
The pooling method. Only support'avg'. Default is 'avg'.
##### Input:
* <strong><em>features: </em></strong>float
4-D feature map
* <strong><em>rois: </em></strong>float
2-D tensor, regions of interest to pool over
* <strong><em>rois_n: </em></strong>int32(optional input)
the number of RoI
* <strong><em>batch_indices: </em></strong>int32(optional input)
1-D tensor of shape with each element denoting the index of the corresponding image in the batch.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Round
Rounds the value of tensor to the nearest integer by element.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Rsqrt
Calculates the reciprocal of the square root for input 'x'.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
the output tensor, has the same type as x

### Scale
Computes the element-wise product of input tensors, with the shape of the scale 'broadcast' to match
the shape of 'x': y = x * scale + bias.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is 1)
Reserved. Axis of input tensor along which to apply other tensors (scale or bias).
* <strong><em>num_axes: </em></strong>int (default is 1)
Reserved. First input is the number of covered axes.
* <strong><em>scale_from_blob: </em></strong>bool (default is false)
Reserved.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
* <strong><em>scale: </em></strong>float
Scale of the input.
* <strong><em>bias: </em></strong>float(optional input)
Bias to add to the input.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor, with the same shape as 'x'

### ScatterNd
Scatter 'x' into a new tensor according to 'indices'.
##### Input:
* <strong><em>indices: </em></strong>int32
An Index tensor.
* <strong><em>x: </em></strong>float
A tensor.
* <strong><em>shape: </em></strong>int32
A 1-D const tensor.
##### Output:
* <strong><em>y: </em></strong>float
Result, has same element type as x.

### ScatterNdUpdate
Applies sparse updates to individual values or slices in a Variable.
##### Attributes:
* <strong><em>use_locking: </em></strong>bool (default is false)
Reserved. Defaults to false. If true, the operation will be protected by a lock.
##### Input:
* <strong><em>var: </em></strong>float
A tensor. Tensor of rank r > 1"
* <strong><em>indices: </em></strong>int32
An Index tensor. Tensor of rank r >= 1
* <strong><em>updates: </em></strong>float
A tensor. Tensor of rank is equal to (rank(data) + rank(indices) - indices.shape[-1] - 1)
##### Output:
* <strong><em>var: </em></strong>float
Result, has same element type as data.

### Select
Performs tensor select.
##### Input:
* <strong><em>condition: </em></strong>bool
the condition of select. Only support first dimension broadcast.
* <strong><em>x1: </em></strong>float, int32, uint8, bool
First operand.
* <strong><em>x2: </em></strong>float, int32, uint8, bool
Second operand, has the same shape of x1.
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
Result, has same element type as two inputs.

### Shape
Obtain the shape of input tensor,  expressed in the form of one-dimensional integer tensor.
##### Attributes:
* <strong><em>dtype: </em></strong>int (default is DT_INT32)
Reserved. data type of the output y.
##### Input:
* <strong><em>x: </em></strong>float, int32, bool, uint8
The input tensor.
##### Output:
* <strong><em>y: </em></strong>int32
The output tensor.

### Sign
Returns an indication of the sign of a number.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
Tensor of the same type as 'x'

### Sin
Computes sin of 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>float
A Tensor
##### Output:
* <strong><em>y: </em></strong>float
A Tensor. Has the same type as x.

### Size
Returns the size of a tensor.
##### Attributes:
* <strong><em>dtype: </em></strong>int (default is DT_INT32)
Reserved. data type of the output y.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, bool
Input tensor
##### Output:
* <strong><em>y: </em></strong>int32
Output tensor

### Slice
Extracts a slice of size 'size' from a tensor input starting at the location specified by 'offsets '.
The slice 'size' is represented as a tensor shape,
where size[i] is the number of elements of the ith dimension to slice.
The starting location (offsets) for the slice is represented as an offset in each dimension of input.
In other words, offsets[i] is the offset into the ith dimension of input to slice.
##### Input:
* <strong><em>x: </em></strong>float, int32, int64, uint8, bool
Input tensor
* <strong><em>offsets: </em></strong>int32
Const, starting location for each dimension of input
* <strong><em>size: </em></strong>int32
Const, number of elements for each dimension of input
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
Output tensor

### Softmax
Computes the softmax (normalized exponential) values for given input.
##### Attributes:
* <strong><em>axis: </em></strong>int (default is 0)
Dimension softmax would be performed on
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output values, with the same shape as the x

### SpaceToBatchND
Zero-pads and then rearranges blocks of spatial data into batch.The operation outputs
a copy of the input tensor, in which the values from the height and width dimensions
are moved to the batch dimension.
##### Input:
* <strong><em>x: </em></strong>float
4-D tensor with shape [batch, depth, height, width]. Both height and width must be divisible
by 'block_shape'.
* <strong><em>block_shape: </em></strong>int32
Const OP, 1-D with shape [M], where all values must be >= 1
* <strong><em>paddings: </em></strong>int32
Const OP, 2-D with shape [M, 2], where, all values must be >= 0
paddings = [[pad_top, pad_bottom], [pad_left, pad_right]]
The effective spatial dimensions of the zero-padded input tensor will be:
height_pad = pad_top + height + pad_bottom
width_pad = pad_left + width + pad_right
Both 'height_pad' and 'width_pad' must be divisible by 'block_size'.
##### Output:
* <strong><em>y: </em></strong>float
Tensor of the same type as 'x'

### SpaceToDepth
Rearranges blocks of spatial data, into depth. More specifically, this op outputs a copy of
the input tensor where values from the height and width dimensions are moved to the depth dimension.
The attr block_size indicates the input block size.
##### Attributes:
* <strong><em>block_size: </em></strong>int(required attr)
int (>= 1), size of the spatial block
* <strong><em>data_format: </em></strong>string (default is NHWC)
Format of input, 'NCHW' or 'NHWC'. Default is 'NHWC'
##### Input:
* <strong><em>x: </em></strong>float, uint8, int8
Input tensor with shape [n, c, h, w] or [n, h, w, c], where h and w must be divisible by 'block_size'
##### Output:
* <strong><em>y: </em></strong>float, uint8, int8
Output tensor

### SparseToDense
Converts a sparse representation into a dense tensor.
##### Attributes:
* <strong><em>validate_indices: </em></strong>bool (default is false)
Bool scalar, If True, indices are checked to make sure they are sorted in l
exicographic order and that there are no repeats. Now only supports False.
##### Input:
* <strong><em>indices: </em></strong>int32
A 0-D, 1-D, or 2-D Tensor of type int32.
* <strong><em>output_shape: </em></strong>int32
A 1-D Tensor of type int32, must be a Const-OP.
* <strong><em>values: </em></strong>float
A 0-D or 1-D Tensor of type float32.
* <strong><em>default_value: </em></strong>float
A 0-D Tensor of type float32.
##### Output:
* <strong><em>y: </em></strong>float
Tensor of shape output_shape.  Has the same type as values.

### SplitD
Splits a tensor into a list of tensors along the split_dim.
##### Attributes:
* <strong><em>split_dim: </em></strong>int(required attr)
Which axis to split on, the value should be in range of [-RANK(X), RANK(X)).
* <strong><em>num_split: </em></strong>int(required attr)
Number of outputs, x.shape[split_dim] % num_split should be 0.
##### Input:
* <strong><em>x: </em></strong>float, int8, int32, bool, uint8
Input tensor.
##### Output:
* <strong><em>y: </em></strong>float, int8, int32, bool, uint8(dynamic output)
(Mandatory) splited tensors, whose number is specified by num_split

### SplitV
Splits a tensor into 'num_split' tensors along one dimension.
##### Attributes:
* <strong><em>num_split: </em></strong>int(required attr)
int (>= 1), number of outputs
##### Input:
* <strong><em>x: </em></strong>float
Input tensor.
* <strong><em>size_splits: </em></strong>int32
Tensor of type int32, must be const op.
Optional length of each output.
Sum of the values must be equal to the dim value at 'split_dim' specified.
* <strong><em>split_dim: </em></strong>int32
Tensor of type int32, must be a scalar const op.
Which axis to split on. A negative value means counting dimensions from the back.
Accepted range is [-rank(x), rank(x)).
##### Output:
* <strong><em>y: </em></strong>float(dynamic output)
(Mandatory) spited tensors, whose number is specified by 'num_split'

### Sqrt
Computes the square root of the input tensor 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Tensor of the same type as 'x'

### Square
Computes the square of the input tensor 'x' element-wise.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### SquaredDifference
Returns a new tensor of (x1 - x2)(x1 - x2).
##### Input:
* <strong><em>x1: </em></strong>float
the first input tensor.
* <strong><em>x2: </em></strong>float
the second input tensor.
##### Output:
* <strong><em>y: </em></strong>float
the output tensor.

### Squeeze
Given a tensor input, this operation returns a tensor of the same type with all dimensions of size 1 removed.
If you do not want to remove all size with 1 dimensions,
you can remove specific size 1 dimensions by specifying axis.
##### Attributes:
* <strong><em>axis: </em></strong>list of ints
The dimensions which to remove.
##### Input:
* <strong><em>x: </em></strong>float, int32, bool, uint8
The input to be squeezed.
##### Output:
* <strong><em>y: </em></strong>float, int32, bool, uint8
The output tensor.

### StridedSliceV2
Extracts a strided slice of a tensor.
##### Attributes:
* <strong><em>begin_mask: </em></strong>int (default is 0)
reserved, default 0
* <strong><em>end_mask: </em></strong>int (default is 0)
reserved, default 0
* <strong><em>ellipsis_mask: </em></strong>int (default is 0)
reserved, default 0
* <strong><em>new_axis_mask: </em></strong>int (default is 0)
reserved, default 0
* <strong><em>shrink_axis_mask: </em></strong>int (default is 0)
reserved, default 0
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, bool
Tensor of data to extract slices from.
* <strong><em>begin: </em></strong>int32
1-D tensor of starting indices of corresponding axis in `axes`. Only support Const op.
* <strong><em>end: </em></strong>int32
1-D tensor of ending indices (exclusive) of corresponding axis in `axes`. Only support Const op.
* <strong><em>axes: </em></strong>int32(optional input)
1-D tensor of axes that `starts` and `ends` apply to. Only support Const op.
* <strong><em>strides: </em></strong>int32(optional input)
1-D tensor of slice step of corresponding axis in `axes`.. Only support Const op.
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
Output tensor

### Sub
Returns x1 - x2.
For the two inputs, the corresponding dimensions must have the same value, or one of them is 1.
##### Input:
* <strong><em>x1: </em></strong>float, int32, int64
First input tensor
* <strong><em>x2: </em></strong>float, int32, int64
Second input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32, int64
Output tensor

### Swish
Calculates the swish function value of each element in the input tensor.
##### Attributes:
* <strong><em>scale: </em></strong>float (default is 1.0)
Scaling factor
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Tan
Computes tan of input 'x'.
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Threshold
This operator is equivalent to the activation function.
If the value is greater than threshold, the output is 1. Otherwise, the output is 0.
##### Attributes:
* <strong><em>threshold: </em></strong>float (default is 0)
Threshold value
##### Input:
* <strong><em>x: </em></strong>float
Input tensor
##### Output:
* <strong><em>y: </em></strong>float
Output tensor

### Tile
Constructs a tensor by tiling a given tensor.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8, bool
Input tensor
* <strong><em>multiples: </em></strong>int32
Const OP
##### Output:
* <strong><em>y: </em></strong>float, int32, uint8, bool
Output tensor

### TopK
Calculate values and indices of the k largest entries of the last dimension and output.
##### Attributes:
* <strong><em>sorted: </em></strong>bool (default is true)
If true, the resulting k elements will be sorted by the values in descending order.
If it be set to false, just make sure the k elements no losing, the data set is whole.
##### Input:
* <strong><em>x: </em></strong>float, int32, uint8
1-D or higher Tensor with last dimension at least k
* <strong><em>k: </em></strong>int32
Const OP. Number of top elements to look for along the last dimension
##### Output:
* <strong><em>values: </em></strong>float, int32, uint8
k largest elements along each last dimensional slice
* <strong><em>indices: </em></strong>int32
indices of values within the last dimension of input

### TruncateDiv
Returns x1/x2 element-wise for integer types.
##### Input:
* <strong><em>x1: </em></strong>uint8
First input tensor.
* <strong><em>x2: </em></strong>uint8
Second input tensor, must have the same type as x1.
##### Output:
* <strong><em>y: </em></strong>uint8
Output tensor.

### Unpack
Unpacks the given dimension of a rank-R tensor into rank-(R-1) tensors.
##### Attributes:
* <strong><em>num: </em></strong>int(required attr)
Output number of tensor after split.
The value of num must be equal to x[axis].
* <strong><em>axis: </em></strong>int (default is 0)
Axis to unpack along
##### Input:
* <strong><em>x: </em></strong>float, int32
Input tensor
##### Output:
* <strong><em>y: </em></strong>float, int32(dynamic output)
List of Tensor objects unstacked from x

### Xlogy
Computes x multiplied by the logarithm of y element-wise, if x == 0, returns 0.
##### Input:
* <strong><em>x1: </em></strong>float
First input tensor of type float32.
* <strong><em>x2: </em></strong>float
Second input tensor, must have the same type as x1.
##### Output:
* <strong><em>y: </em></strong>float
Output tensor of type float32.

## 整体流程示例

### 创建Env类，提供打开libhiai_c.so、获取符号表、关闭so的方法
参考实现如下：

```c
#include "c/graph/handle_types.h"
#include "c/graph/op_config.h"
#include "c/hiai_base_types.h"
#include "c/hiai_types.h"

#include <stdint.h>
using RESOURCE_MANAGER_CREATE = ResMgrHandle (*)();
using RESOURC_EMANAGER_DESTROY = void (*)(ResMgrHandle* res_mgr);
using MODEL_CREATE = ModelHandle (*)(ResMgrHandle res_mgr, const char* name);
using IR_CREATE_PLACEHODLER = OpHandle (*)(ResMgrHandle res_mgr, const char* opName,
        HIAI_Format format, HIAI_DataType datatype, int64_t inputShape[], uint32_t inputShapeNums);
using IR_CREATEOP = OpHandle (*)(ResMgrHandle res_mgr, HIAI_IR_OpConfig* opConfig);
using IR_BUILT_MODEL = HIAI_Status (*)(ResMgrHandle res_mgr, ConstModelHandle model,
        const HIAI_MR_ModelBuildOptions* options, HIAI_MR_BuiltModel** builtModel);
using GRAPH_CREATE = GraphHandle (*)(ResMgrHandle res_mgr, const char* name);
using SET_INPUT = HIAI_Status (*)(ResMgrHandle res_mgr, ConstGraphHandle graph, ConstOpHandle inputs[], uint32_t input_num);
using SET_OUTPUT = HIAI_Status (*)(ResMgrHandle res_mgr, ConstGraphHandle graph, ConstOpHandle outputs[], uint32_t output_num);
using CREATE_INT64_ATTR = AttrHandle (*)(ResMgrHandle resMgr, int64_t value);
using SET_GRAPH = HIAI_Status (*)(ResMgrHandle resMgr, ConstGraphHandle graph, ModelHandle model);
using MODEL_DUMP = HIAI_Status (*)(ResMgrHandle resMgr, ConstModelHandle model, const char* file);
using CREATE_INT64LST_ATTR = AttrHandle (*)(ResMgrHandle resMgr, int64_t value[], uint32_t num);
using CREATE_TENSOR_ATTR = AttrHandle (*)(ResMgrHandle resMgr, TensorHandle value);
using CREATE_TENSOR = TensorHandle (*)(ResMgrHandle resMgr, void* data, size_t dataLen,
    const int64_t shape[], size_t shapeSize, HIAI_DataType typeC, HIAI_Format formatC);
using BUILT_MODEL_DESTROY = void (*)(HIAI_MR_BuiltModel** model);
using MODEL_MANAGER_CREATE = HIAI_MR_ModelManager* (*)();
using MODEL_INITOPTIONS_CREATE = HIAI_MR_ModelInitOptions* (*)();
using MODEL_INITOPTIONS_DESTROY = void (*)(HIAI_MR_ModelInitOptions** options);
using MODEL_MANAGER_DESTROY = void (*)(HIAI_MR_ModelManager** manager);
using MODEL_MANAGER_INIT = HIAI_Status (*)(HIAI_MR_ModelManager* manager,
    const HIAI_MR_ModelInitOptions* options, const HIAI_MR_BuiltModel* builtModel,
    const HIAI_MR_ModelManagerListener* listener);
using GET_INPUT_TENSOR_NUM = int32_t (*)(const HIAI_MR_BuiltModel* model);
using GET_INPUT_TENSO_RDESC = HIAI_NDTensorDesc* (*)(const HIAI_MR_BuiltModel* model, size_t index);
using CREATE_FROM_ND_TENSOR_DESC = HIAI_MR_NDTensorBuffer* (*)(const HIAI_NDTensorDesc* desc);
using ND_TENSOR_DESC_DESTROY = void (*)(HIAI_NDTensorDesc** tensorDesc);
using ND_TENSOR_BUFFER_GET_SIZE = size_t (*)(const HIAI_MR_NDTensorBuffer* tensorBuffer);
using ND_TENSOR_BUFFER_GET_DATA = void* (*)(const HIAI_MR_NDTensorBuffer* tensorBuffer);
using GET_OUTPUT_TENSOR_NUM = int32_t (*)(const HIAI_MR_BuiltModel* model);
using GET_OUTPUT_TENSOR_DESC = HIAI_NDTensorDesc* (*)(const HIAI_MR_BuiltModel* model, size_t index);
using MODEL_MANAGER_RUN = HIAI_Status (*)(HIAI_MR_ModelManager* manager, HIAI_MR_NDTensorBuffer* input[],
    int32_t inputNum, HIAI_MR_NDTensorBuffer* output[], int32_t outputNum);
using MODEL_MANAGER_DEINIT = HIAI_Status (*)(HIAI_MR_ModelManager* manager);
using ND_TENSOR_BUFFER_DESTROY = void (*)(HIAI_MR_NDTensorBuffer** tensorBuffer);
using GET_VERSION = const char* (*)();
class Env {
public:
    int InitEnv();
    void CloseSo();

private:
    int GetALLSym();

public:
    void* handle_ {nullptr};
    RESOURCE_MANAGER_CREATE resourceManagerCreate_ {nullptr};
    RESOURC_EMANAGER_DESTROY resourceManagerDestroy_ {nullptr};
    MODEL_CREATE modelCreate_ {nullptr};
    IR_CREATE_PLACEHODLER createIrDataOp_ {nullptr};
    IR_CREATEOP createIrOp_ {nullptr};
    IR_BUILT_MODEL irBuiltModel_ {nullptr};
    GRAPH_CREATE graphCreate_ {nullptr};
    SET_INPUT setInputs_ {nullptr};
    SET_OUTPUT setOutputs_ {nullptr};
    CREATE_INT64_ATTR createInt64Attr_ {nullptr};
    SET_GRAPH setGraph_ {nullptr};
    MODEL_DUMP modelDump_ {nullptr};
    CREATE_INT64LST_ATTR createInt64LstAttr_ {nullptr};
    CREATE_TENSOR_ATTR createTensorAttr_ {nullptr};
    CREATE_TENSOR createTensor_ {nullptr};
    BUILT_MODEL_DESTROY builtModelDestroy_ {nullptr};
    MODEL_MANAGER_CREATE modelManagerCreate_ {nullptr};
    MODEL_INITOPTIONS_CREATE modelInitOptionsCreate_ {nullptr};
    MODEL_INITOPTIONS_DESTROY modelInitOptionsDestroy_ {};
    MODEL_MANAGER_DESTROY modelManagerDestroy_ {nullptr};
    MODEL_MANAGER_INIT modelManagerInit_ {nullptr};
    GET_INPUT_TENSOR_NUM getInputTensorNum_ {nullptr};
    GET_INPUT_TENSO_RDESC getInputTensorDesc_ {nullptr};
    CREATE_FROM_ND_TENSOR_DESC createFromNDTensorDesc_ {nullptr};
    ND_TENSOR_DESC_DESTROY ndTensorDescDestroy_ {nullptr};
    ND_TENSOR_BUFFER_GET_SIZE ndTensorBufferGetSize_ {nullptr};
    ND_TENSOR_BUFFER_GET_DATA ndTensorBufferGetData_ {nullptr};
    GET_OUTPUT_TENSOR_NUM getOutputTensorNum_ {nullptr};
    GET_OUTPUT_TENSOR_DESC getOutputTensorDesc_ {nullptr};
    MODEL_MANAGER_RUN modelManagerRun_ {nullptr};
    MODEL_MANAGER_DEINIT modelManagerDeinit_ {nullptr};
    ND_TENSOR_BUFFER_DESTROY ndTensorBufferDestroy_ {nullptr};
    GET_VERSION getVersion_ {nullptr};
};
```

Env类的实现：

```c
static void* GetHandle()
{
    void* handle = dlopen("/data/local/tmp/libhiai_c.so", RTLD_LAZY);
    if (handle == nullptr) {
        printf("dlopen failed, %s", dlerror());
    }
    return handle;
}
int Env::GetALLSym()
{
    if (handle_ == nullptr) {
        printf("handle is null");
        return -1;
    }
    resourceManagerCreate_ = reinterpret_cast<RESOURCE_MANAGER_CREATE>(dlsym(handle_, "HIAI_IR_ResourceManagerCreate"));
    HIAI_EXPECT_NOT_NULL_VOID(resourceManagerCreate_);
    resourceManagerDestroy_ = reinterpret_cast<RESOURC_EMANAGER_DESTROY>(dlsym(handle_, "HIAI_IR_ResourceManagerDestroy"));
    HIAI_EXPECT_NOT_NULL_VOID(resourceManagerDestroy_);
    modelCreate_ = reinterpret_cast<MODEL_CREATE>(dlsym(handle_, "HIAI_IR_CreateModel"));
    HIAI_EXPECT_NOT_NULL_VOID(modelCreate_);
    createIrDataOp_ = reinterpret_cast<IR_CREATE_PLACEHODLER>(dlsym(handle_, "HIAI_IR_CreatePlaceHodler"));
    HIAI_EXPECT_NOT_NULL_VOID(createIrDataOp_);
    createIrOp_ = reinterpret_cast<IR_CREATEOP>(dlsym(handle_, "HIAI_IR_CreateOp"));
    HIAI_EXPECT_NOT_NULL_VOID(createIrOp_);
    irBuiltModel_ = reinterpret_cast<IR_BUILT_MODEL>(dlsym(handle_, "HIAI_IR_CreateBuiltModel"));
    HIAI_EXPECT_NOT_NULL_VOID(irBuiltModel_);
    graphCreate_ = reinterpret_cast<GRAPH_CREATE>(dlsym(handle_, "HIAI_IR_GraphCreate"));
    HIAI_EXPECT_NOT_NULL_VOID(graphCreate_);
    setInputs_ = reinterpret_cast<SET_INPUT>(dlsym(handle_, "HIAI_IR_SetInputs"));
    HIAI_EXPECT_NOT_NULL_VOID(setInputs_);
    setOutputs_ = reinterpret_cast<SET_OUTPUT>(dlsym(handle_, "HIAI_IR_SetOutputs"));
    HIAI_EXPECT_NOT_NULL_VOID(setOutputs_);
    createInt64Attr_ = reinterpret_cast<CREATE_INT64_ATTR>(dlsym(handle_, "HIAI_IR_CreateInt64Attr"));
    HIAI_EXPECT_NOT_NULL_VOID(createInt64Attr_);
    setGraph_ = reinterpret_cast<SET_GRAPH>(dlsym(handle_, "HIAI_IR_SetGraph"));
    HIAI_EXPECT_NOT_NULL_VOID(setGraph_);
    modelDump_ = reinterpret_cast<MODEL_DUMP>(dlsym(handle_, "HIAI_IR_DumpModel"));
    HIAI_EXPECT_NOT_NULL_VOID(modelDump_);
    createInt64LstAttr_ = reinterpret_cast<CREATE_INT64LST_ATTR>(dlsym(handle_, "HIAI_IR_CreateInt64LstAttr"));
    HIAI_EXPECT_NOT_NULL_VOID(createInt64LstAttr_);
    createTensorAttr_ = reinterpret_cast<CREATE_TENSOR_ATTR>(dlsym(handle_, "HIAI_IR_CreateTensorAttr"));
    HIAI_EXPECT_NOT_NULL_VOID(createTensorAttr_);
    createTensor_ = reinterpret_cast<CREATE_TENSOR>(dlsym(handle_, "HIAI_IR_CreateTensor"));
    HIAI_EXPECT_NOT_NULL_VOID(createTensor_);
    builtModelDestroy_ = reinterpret_cast<BUILT_MODEL_DESTROY>(dlsym(handle_, "HIAI_MR_BuiltModel_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(builtModelDestroy_);
    modelInitOptionsCreate_ = reinterpret_cast<MODEL_INITOPTIONS_CREATE>(dlsym(handle_, "HIAI_MR_ModelInitOptions_Create"));
    HIAI_EXPECT_NOT_NULL_VOID(modelInitOptionsCreate_);
    modelInitOptionsDestroy_ = reinterpret_cast<MODEL_INITOPTIONS_DESTROY>(dlsym(handle_, "HIAI_MR_ModelInitOptions_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(modelInitOptionsDestroy_);
    modelManagerCreate_ = reinterpret_cast<MODEL_MANAGER_CREATE>(dlsym(handle_, "HIAI_MR_ModelManager_Create"));
    HIAI_EXPECT_NOT_NULL_VOID(modelManagerCreate_);
    modelManagerDestroy_ = reinterpret_cast<MODEL_MANAGER_DESTROY>(dlsym(handle_, "HIAI_MR_ModelManager_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(modelManagerDestroy_);
    modelManagerInit_ = reinterpret_cast<MODEL_MANAGER_INIT>(dlsym(handle_, "HIAI_MR_ModelManager_Init"));
    HIAI_EXPECT_NOT_NULL_VOID(modelManagerInit_);
    getInputTensorNum_ = reinterpret_cast<GET_INPUT_TENSOR_NUM>(dlsym(handle_, "HIAI_MR_BuiltModel_GetInputTensorNum"));
    HIAI_EXPECT_NOT_NULL_VOID(getInputTensorNum_);
    getInputTensorDesc_ = reinterpret_cast<GET_INPUT_TENSO_RDESC>(dlsym(handle_, "HIAI_MR_BuiltModel_GetInputTensorDesc"));
    HIAI_EXPECT_NOT_NULL_VOID(getInputTensorDesc_);
    createFromNDTensorDesc_ = reinterpret_cast<CREATE_FROM_ND_TENSOR_DESC>(dlsym(handle_, "HIAI_MR_NDTensorBuffer_CreateFromNDTensorDesc"));
    HIAI_EXPECT_NOT_NULL_VOID(createFromNDTensorDesc_);
    ndTensorDescDestroy_ = reinterpret_cast<ND_TENSOR_DESC_DESTROY>(dlsym(handle_, "HIAI_NDTensorDesc_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(ndTensorDescDestroy_);
    ndTensorBufferGetSize_ = reinterpret_cast<ND_TENSOR_BUFFER_GET_SIZE>(dlsym(handle_, "HIAI_MR_NDTensorBuffer_GetSize"));
    HIAI_EXPECT_NOT_NULL_VOID(ndTensorBufferGetSize_);
    ndTensorBufferGetData_ = reinterpret_cast<ND_TENSOR_BUFFER_GET_DATA>(dlsym(handle_, "HIAI_MR_NDTensorBuffer_GetData"));
    HIAI_EXPECT_NOT_NULL_VOID(ndTensorBufferGetData_);
    getOutputTensorNum_ = reinterpret_cast<GET_OUTPUT_TENSOR_NUM>(dlsym(handle_, "HIAI_MR_BuiltModel_GetOutputTensorNum"));
    HIAI_EXPECT_NOT_NULL_VOID(getOutputTensorNum_);
    getOutputTensorDesc_ = reinterpret_cast<GET_OUTPUT_TENSOR_DESC>(dlsym(handle_, "HIAI_MR_BuiltModel_GetOutputTensorDesc"));
    HIAI_EXPECT_NOT_NULL_VOID(getOutputTensorDesc_);
    modelManagerRun_ = reinterpret_cast<MODEL_MANAGER_RUN>(dlsym(handle_, "HIAI_MR_ModelManager_Run"));
    HIAI_EXPECT_NOT_NULL_VOID(modelManagerRun_);
    modelManagerDeinit_ = reinterpret_cast<MODEL_MANAGER_DEINIT>(dlsym(handle_, "HIAI_MR_ModelManager_Run"));
    HIAI_EXPECT_NOT_NULL_VOID(modelManagerDeinit_);
    ndTensorBufferDestroy_ = reinterpret_cast<ND_TENSOR_BUFFER_DESTROY>(dlsym(handle_, "HIAI_MR_NDTensorBuffer_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(ndTensorBufferDestroy_);
    getVersion_ = reinterpret_cast<GET_VERSION>(dlsym(handle_, "HIAI_MR_GetVersion"));
    HIAI_EXPECT_NOT_NULL_VOID(getVersion_);
    return 0;
}
int Env::InitEnv()
{
    handle_ = GetHandle();
    if (GetALLSym() != 0) {
        return -1;
    }
    return 0;
}
void Env::CloseSo()
{
    if (handle_ == nullptr) {
        return;
    }
   dlclose(handle_);
}
```

### 算子构图

```c
// 创建const类型的OpHandle
OpHandle CreateWeight(Env env, ResMgrHandle& resMgr, const char* weightName, const char* weightFile,
    const int64_t shape[], size_t shapeSize, HIAI_DataType typeC)
{
    HIAI_Format formatC = HIAI_Format::HIAI_FORMAT_NCHW;

    void* data = nullptr;
    size_t size = 0;
    // 读取权重文件数据，自行实现
    ReadData(weightFile, &data, size);
    TensorHandle tensor_handle = env.createTensor_(resMgr, data, size,
        shape, shapeSize, typeC, formatC);
    if (tensor_handle == nullptr) {
        return nullptr;
    }

    AttrHandle attrHandle = env.createTensorAttr_(resMgr, tensor_handle);
    if (attrHandle == nullptr) {
        return nullptr;
    }

    HIAI_IR_Params params[1];
    params[0].name = "value";
    params[0].attrValue = attrHandle;

    HIAI_IR_OpConfig opConfig;
    opConfig.opType = "Const";
    opConfig.opName = weightName;
    opConfig.input = nullptr;
    opConfig.inputNums = 0;
    opConfig.params = params;
    opConfig.paramsNums = 1;
    opConfig.outputNums = 0;
    OpHandle constOp = env.createIrOp_(resMgr, &opConfig);

    free(data);
    data = nullptr;
    return constOp;
}
// 以data->conv->relu为例
int BuildGraph(Env env, ResMgrHandle& resMgr, ModelHandle& model)
{
    int64_t input_shape[] = {1, 3, 224, 224};
    OpHandle data_op = env.createIrDataOp_(resMgr, "Data", HIAI_FORMAT_NCHW, HIAI_DATATYPE_FLOAT32, input_shape, 4);

    // convolution
    int64_t w_shape[] = {16, 3, 1, 1};
    OpHandle conv_w_constOp = CreateWeight(env, resMgr, "w", "./input/conv_w_file.bin", w_shape, 4, HIAI_DATATYPE_FLOAT32);
    if (conv_w_constOp == nullptr) {
        printf("conv_w_constOp is nullptr\n");
        return -1;
    }

    int64_t b_shape[] = {16};
    OpHandle conv_b_constOp = CreateWeight(env, resMgr, "b","./input/conv_b_file.bin", b_shape, 1, HIAI_DATATYPE_FLOAT32);
    if (conv_b_constOp == nullptr) {
        printf("conv_b_constOp is nullptr\n");
        return -1;
    }

    HIAI_IR_Input convInOp[3];
    convInOp[0].op = data_op;
    convInOp[0].outIndex = 0;

    convInOp[1].op = conv_w_constOp;
    convInOp[1].outIndex = 0;

    convInOp[2].op = conv_b_constOp;
    convInOp[2].outIndex = 0;

    int64_t stridesShape[] = {2, 2};
    AttrHandle strides = env.createInt64LstAttr_(resMgr, stridesShape, 2);
    HIAI_IR_Params convParams[2];
    convParams[0].name = "strides";
    convParams[0].attrValue = strides;

    int64_t dilationsShape[] = {1, 1};
    AttrHandle dilations = env.createInt64LstAttr_(resMgr, dilationsShape, 2);
    convParams[1].name = "dilations";
    convParams[1].attrValue = dilations;

    HIAI_IR_OpConfig convConfig;
    convConfig.opType = "Convolution";
    convConfig.opName = "convolution";
    convConfig.input = convInOp;
    convConfig.inputNums = 3;
    convConfig.params = convParams;
    convConfig.paramsNums = 2;
    convConfig.outputNums = 0;
    OpHandle convOp = env.createIrOp_(resMgr, &convConfig);

    // activation
    HIAI_IR_Input activationInOp[1];
    activationInOp[0].op = convOp;
    activationInOp[0].outIndex = 0;

    AttrHandle modeAttr = env.createInt64Attr_(resMgr, 1); // mode:ReLU
    HIAI_IR_Params params[1];
    params[0].name = "mode";
    params[0].attrValue = modeAttr;

    HIAI_IR_OpConfig reluConfig;
    reluConfig.opType = "Activation";
    reluConfig.opName = "activation";
    reluConfig.input = activationInOp;
    reluConfig.inputNums = 1;
    reluConfig.params = params;
    reluConfig.paramsNums = 1;
    reluConfig.outputNums = 0;
    OpHandle activationOp = env.createIrOp_(resMgr, &reluConfig);

    GraphHandle graph = env.graphCreate_(resMgr, "graph_testc");
    if (graph == nullptr) {
        printf("graph is null.\n");
        return -1;
    }
    ConstOpHandle input[] = {data_op};
    env.setInputs_(resMgr, graph, input, 1);
    ConstOpHandle output[] = {activationOp};
    env.setOutputs_(resMgr, graph, output, 1);

    env.setGraph_(resMgr, graph, model);

    return 0;
}
```

### 模型编译

```c
ResMgrHandle resMgr = env.resourceManagerCreate_();

ModelHandle model = env.modelCreate_(resMgr, "model_testc");
int ret = BuildGraph(env, resMgr, model);
if (ret != 0) {
    return ret;
}
if (model == nullptr) {
    printf("model is nullptr.\n");
    env.resourceManagerDestroy_(&resMgr);
    return -1;
}

HIAI_MR_BuiltModel* builtModel = nullptr;
HIAI_MR_ModelBuildOptions* options = nullptr;
ret = env.irBuiltModel_(resMgr, model, options, &builtModel);
if (ret != 0 || builtModel == nullptr) {
    printf("irBuiltModel_ failed.\n");
    env.builtModelDestroy_(&builtModel);
    env.resourceManagerDestroy_(&resMgr);
    return -1;
}
env.resourceManagerDestroy_(&resMgr);
```

### 模型推理

```c
// 创建模型管家
HIAI_MR_ModelManager* manager = env.modelManagerCreate_();
if (manager == nullptr) {
    printf("manager is nullptr.\n");
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 创建加载编译选项（可选），有加载参数时请根据DDK指导文档进行设置
HIAI_MR_ModelInitOptions* initOption = env.modelInitOptionsCreate_();
if (initOption == nullptr) {
    printf("initOption is nullptr.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 加载模型
HIAI_MR_ModelManagerListener* listener = nullptr;
env.modelManagerInit_(manager, initOption, builtModel, listener);

env.modelInitOptionsDestroy_(&initOption);

// 获取模型的输入个数
int32_t inputTensorNum = env.getInputTensorNum_(builtModel);
if (inputTensorNum == 0) {
    printf("inputTensorNum is 0.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 创建模型的输入tensorBuffer
HIAI_MR_NDTensorBuffer** inputTensorBuffers = (HIAI_MR_NDTensorBuffer**)malloc(
    sizeof(HIAI_MR_NDTensorBuffer*) * inputTensorNum);
if (inputTensorBuffers == nullptr) {
    printf("inputTensorBuffers is nullptr.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 创建ndTensorBuffer并填充输入tensorBuffer
for (int32_t i = 0; i < inputTensorNum; ++i) {
    HIAI_NDTensorDesc* tensorDesc = env.getInputTensorDesc_(builtModel, i);
    HIAI_MR_NDTensorBuffer* tensorBuffer = env.createFromNDTensorDesc_(tensorDesc);
    env.ndTensorDescDestroy_(&tensorDesc);
    if (tensorBuffer == nullptr) {
        printf("tensorBuffer is nullptr.\n");
        env.modelManagerDestroy_(&manager);
        env.builtModelDestroy_(&builtModel);
        return -1;
    }
    inputTensorBuffers[i] = tensorBuffer;
}
// 获取模型的输出个数
int32_t outputTensorNum = env.getOutputTensorNum_(builtModel);
if (outputTensorNum == 0) {
    printf("outputTensorNum is 0.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 创建模型的输出tensorBuffer
HIAI_MR_NDTensorBuffer** outputTensorBuffers = (HIAI_MR_NDTensorBuffer**)malloc(
    sizeof(HIAI_MR_NDTensorBuffer*) * outputTensorNum);
if (outputTensorBuffers == nullptr) {
    printf("outputTensorBuffers is nullptr.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 创建ndTensorBuffer并填充输出tensorBuffer
for (int32_t i = 0; i < outputTensorNum; ++i) {
    HIAI_NDTensorDesc* tensorDesc = env.getOutputTensorDesc_(builtModel, i);
    HIAI_MR_NDTensorBuffer* tensorBuffer = env.createFromNDTensorDesc_(tensorDesc);
    env.ndTensorDescDestroy_(&tensorDesc);
    if (tensorBuffer == nullptr) {
        printf("tensorBuffer is nullptr.\n");
        env.modelManagerDestroy_(&manager);
        env.builtModelDestroy_(&builtModel);
        return -1;
    }
    outputTensorBuffers[i] = tensorBuffer;
}
// 推理
ret = env.modelManagerRun_(manager,  inputTensorBuffers, inputTensorNum, outputTensorBuffers, outputTensorNum);
if (ret != 0) {
    printf("run failed.\n");
    env.modelManagerDestroy_(&manager);
    env.builtModelDestroy_(&builtModel);
    return -1;
}
// 卸载模型
env.modelManagerDeinit_(manager);

// 释放推理资源
env.builtModelDestroy_(&builtModel);
env.modelManagerDestroy_(&manager);

if (inputTensorBuffers != nullptr && inputTensorNum != 0) {
    for (int32_t i = 0; i < inputTensorNum; ++i) {
        if (inputTensorBuffers[i] != nullptr) {
            env.ndTensorBufferDestroy_(&inputTensorBuffers[i]);
        }
    }
    free(inputTensorBuffers);
    inputTensorBuffers = nullptr;
}
if (outputTensorBuffers != nullptr && outputTensorNum != 0) {
    for (int32_t i = 0; i < outputTensorNum; ++i) {
        if (outputTensorBuffers[i] != nullptr) {
            env.ndTensorBufferDestroy_(&outputTensorBuffers[i]);
        }
    }
    free(outputTensorBuffers);
    outputTensorBuffers = nullptr;
}
```
