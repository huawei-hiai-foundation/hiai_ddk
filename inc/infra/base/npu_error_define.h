/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2021. All rights reserved.
 * Description: npu error define
 */
#ifndef __NPU_ERROR_DEFINE_H__
#define __NPU_ERROR_DEFINE_H__

typedef enum tagAicpNpuLocal {
    AICP_HOST = 1,
    AICP_DEVICE = 2,
} AicpNpuLocal;

typedef enum tagAicpNpuCodeType {
    ERROR_CODE = 1,
    EXCEPTION_CODE = 2,
} AicpNpuCodeType;

typedef enum tagAicpNpuErrLevel {
    NONE_LEVEL = 0,
    SUGGESTION_LEVEL = 1,
    NORMAL_LEVEL = 2,
    SERIOUS_LEVEL = 3,
    CRITICAL_ERROR = 4,
} AicpNpuErrLevel;

typedef enum tagAicpNpuModuleId {
    AICP_DRIVER = 1,
    AICP_CTRLCPU = 2,
    AICP_TS = 3,
    AICP_RUNTIME = 4,
    AICP_AICPU = 5,
    AICP_NPUCL = 6,
    AICP_TVM = 7,
    AICP_FRAMEWORK = 8,
    AICP_ENGINE = 9,
    AICP_DVPP = 10,
    AICP_AIPP = 11,
    AICP_LOWPOWER = 12,
    AICP_MDC = 13,
    AICP_COMPILE = 14,
    AICP_TOOLCHIAN = 15,
    AICP_ALG = 16,
    AICP_OM = 17,
    AICP_HCCL = 18,
    AICP_SIMULATION = 19,
    AICP_BIOS = 20,
    AICP_SEC = 21,
    AICP_TINY = 22,
} AicpNpuModuleId;

/* bit 31-bit30 to be aicp local */
#define AICP_NPULOCAL_MASK 0xC0000000U
#define SHIFT_LOCAL_MASK 30U
#define AICP_NPULOCAL_VAL_MASK 0x3U
/* bit 29 -bit28 to be aicp aicpu code type */
#define AICP_CODE_TYPE_MASK 0x30000000U
#define SHIFT_CODE_MASK 28U
#define AICP_CODE_TYPE_VAL_MASK 0x3U
/* bit 27 -bit25 to be aicp error level */
#define AICP_ERROR_LEVEL_MASK 0x0E000000U
#define SHIFT_ERROR_LVL_MASK 25U
#define AICP_ERROR_LEVEL_VAL_MASK 0x7U
/* bit 24 -bit17 to be aicp mod */
#define AICP_MODE_ID_MASK 0x01FE0000U
#define SHIFT_MODE_MASK 17U
#define AICP_MODE_ID_VAL_MASK 0xFFU

#define AICP_NPU_LOC_BIT(a) \
    (AICP_NPULOCAL_MASK & \
    (((unsigned int)(((unsigned int)((AicpNpuLocal)(a))) & AICP_NPULOCAL_VAL_MASK)) << SHIFT_LOCAL_MASK))
#define AICP_NPU_CODE_TYPE_BIT(a) \
    (AICP_CODE_TYPE_MASK & \
    (((unsigned int)(((unsigned int)((AicpNpuCodeType)(a))) & AICP_CODE_TYPE_VAL_MASK)) << SHIFT_CODE_MASK))
#define AICP_NPU_ERR_LEV_BIT(a) \
    (AICP_ERROR_LEVEL_MASK & \
    (((unsigned int)(((unsigned int)((AicpNpuErrLevel)(a))) & AICP_ERROR_LEVEL_VAL_MASK)) << SHIFT_ERROR_LVL_MASK))
#define AICP_NPU_MOD_ID_BIT(a) \
    (AICP_MODE_ID_MASK & \
    (((unsigned int)(((unsigned int)((AicpNpuModuleId)(a))) & AICP_MODE_ID_VAL_MASK)) << SHIFT_MODE_MASK))

#define AICP_NPU_ERR_CODE_HEAD(npuLocal, codeType, errLevel, moduleId) \
    (AICP_NPU_LOC_BIT(npuLocal) + AICP_NPU_CODE_TYPE_BIT(codeType) + AICP_NPU_ERR_LEV_BIT(errLevel) + \
        AICP_NPU_MOD_ID_BIT(moduleId))

#endif
