/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 * Description: trace define
 */
#ifndef INFRA_BASE_HIAI_TRACE_H
#define INFRA_BASE_HIAI_TRACE_H

#ifdef AI_SUPPORT_LITE_NN
#if defined(__OHOS__)
#include "hitrace_meter.h"
#define HIAI_TRACE_BEGIN(VAR) StartTrace(HITRACE_TAG_GRAPHIC_AGP, VAR)
#define HIAI_TRACE_END() FinishTrace(HITRACE_TAG_GRAPHIC_AGP)
#endif

#if defined(__ANDROID__)
#define ATRACE_TAG ATRACE_TAG_GRAPHICS
#include <utils/Trace.h>
#define HIAI_TRACE_BEGIN(VAR) ATRACE_BEGIN(VAR)
#define HIAI_TRACE_END() ATRACE_END()
#endif

#define HIAI_TRACE_MARK(VAR) \
    do { \
        HIAI_TRACE_BEGIN(VAR); \
        HIAI_TRACE_END(); \
    } while (0)

#else
#define HIAI_TRACE_BEGIN(VAR)
#define HIAI_TRACE_END()
#define HIAI_TRACE_MARK(VAR)
#endif

#endif // INFRA_BASE_HIAI_TRACE_H