/* Copyright (C) 2018. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the Apache License Version 2.0.You may not
 * use this file except in compliance with the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Apache License for more details at
 * http://www.apache.org/licenses/LICENSE-2.0
 */
#ifndef DOMI_OMG_GFLAGS_UTIL_H_
#define DOMI_OMG_GFLAGS_UTIL_H_

#include <gflags/gflags.h>

namespace hiai {
class GflagsUtils {
public:
    /*
     * @name   IsSetCommandTrue
     * @brief  判断参数是否为true
     * @param  [in] name 参数名称
     * @return bool true参数为true，false参数不为true
     */
    static bool IsSetCommandTrue(const char* name)
    {
        std::string out;
        return gflags::GetCommandLineOption(name, &out) && out == "true";
    }

    /*
     * @name   IsSetCommandNotEmpty
     * @brief  判断参数不为空
     * @param  [in] name 参数名称
     * @return bool true参数不为空，false参数为空
     */
    static bool IsSetCommandNotEmpty(const char* name)
    {
        std::string out;
        return gflags::GetCommandLineOption(name, &out) && !out.empty();
    }

    /*
     * @name   IsCommandLineNotDefault
     * @brief  判断指定的参数是否在命令行中
     * @param  [in] flag_name 参数名称，不带FLAGS_
     * @return bool true表示参数在命令行中,false表示参数不在命令行中
     */
    static bool IsCommandLineNotDefault(const char* flag_name)
    {
        google::CommandLineFlagInfo info;
        return GetCommandLineFlagInfo(flag_name, &info) && !info.is_default;
    }

    /*
     * @name   ChangeHelpFlags
     * @brief  修改gflags打印帮助信息
     * @param  [in] flags_h 传入自己定义的help入参，建议为FLAGS_h
     * @return void
     */
    static void ChangeHelpFlags(bool flags_h)
    {
        if (flags_h || IsSetCommandTrue("help") || IsSetCommandTrue("helpfull") || IsSetCommandNotEmpty("helpon") ||
            IsSetCommandNotEmpty("helpmatch") || IsSetCommandTrue("helppackage") || IsSetCommandTrue("helpxml")) {
            gflags::SetCommandLineOption("help", "false");
            gflags::SetCommandLineOption("helpfull", "false");
            gflags::SetCommandLineOption("helpon", "");
            gflags::SetCommandLineOption("helpmatch", "");
            gflags::SetCommandLineOption("helppackage", "false");
            gflags::SetCommandLineOption("helpxml", "false");
            gflags::SetCommandLineOption("helpshort", "true");
        }
    }
};
} // namespace hiai
#endif