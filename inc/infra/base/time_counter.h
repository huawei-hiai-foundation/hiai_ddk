/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
 * Description: time counter
 */

#ifndef TIME_COUNTER_H
#define TIME_COUNTER_H
#include <chrono>
#include "infra/log/ai_log.h"

namespace hiai {
class TimeCounter {
public:
    explicit TimeCounter(const std::string& timerName)
    {
        timerName_ = timerName;
        start_ = std::chrono::system_clock::now();
    }

    ~TimeCounter()
    {
        end_ = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_ - start_);
        AI_LOGD("INFRA", "TimeCounter::%s take time %lld ms", timerName_.c_str(), duration.count());
    }

private:
    std::string timerName_;
    std::chrono::time_point<std::chrono::system_clock> start_;
    std::chrono::time_point<std::chrono::system_clock> end_;
};
} // namespace hiai
#endif
