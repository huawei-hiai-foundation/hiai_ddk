/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INFRA_SO_PATH_H
#define INFRA_SO_PATH_H

#ifdef __OHOS__
    #define SO_PATH_PREFIX ""
#else
    #ifdef __arm__
        #define SO_PATH_PREFIX "/vendor/lib/"
    #else
        #define SO_PATH_PREFIX "/vendor/lib64/"
    #endif
#endif

#endif