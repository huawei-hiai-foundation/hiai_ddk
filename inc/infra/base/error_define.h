/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2021. All rights reserved.
 * Description: error define
 */

#ifndef __HIAI_ERROR_DEFINE_H__
#define __HIAI_ERROR_DEFINE_H__
#include <cstdint>

namespace hiai {
namespace error {
using Status = uint32_t;

enum class Location { UNDEFINE = 0x00 };

enum class Category { UNDEFINE = 0x00 };

enum class Level { UNDEFINE = 0x00 };

enum class Subsystem {
    COMMON = 0x00,
    DRIVER = 0x01,
    TS = 0x03,
    RUNTIME = 0x04,
    CPUCL = 0x05,
    NPUCL = 0x06,
    FRAMEWORK = 0x07,
    ENGINE = 0x09,
    TOOLS = 0x0F,
    OM = 0x11
};

constexpr uint32_t SUBSYSTEM_LOW_BIT = 17;
constexpr uint32_t MODULE_LOW_BIT = 12;

// define the partition range size, partition belong to subsystem
// the errorcode range in subsystem is [SubSystem * ERRORCODE_PARTITION_RANGE_SIZE, (SubSystem + 1) *
// ERRORCODE_PARTITION_RANGE_SIZE)
constexpr uint32_t ERRORCODE_PARTITION_RANGE_SIZE = 128;

inline Status MakeErrorCode(uint32_t subSysID, uint32_t moduleID, uint32_t err)
{
    return (subSysID << SUBSYSTEM_LOW_BIT) | (moduleID << MODULE_LOW_BIT) | err;
}

// common errorcode define, range [0, 127)
constexpr Status SUCCESS = 0;
constexpr Status FAILURE = 1;
constexpr Status UNINITIALIZED = 2;
constexpr Status INVALID_PARAM = 3;
constexpr Status TIMEOUT = 4;
constexpr Status UNSUPPORTED = 5;
constexpr Status MEMORY_EXCEPTION = 6;
constexpr Status INVALID_API = 7;
constexpr Status INVALID_POINTER = 8;
constexpr Status CALC_EXCEPTION = 9; // end common errorcode define
constexpr Status FILE_NOT_EXIST = 10;
constexpr Status COMM_EXCEPTION = 11;
}; // namespace error
}; // namespace hiai

#endif
