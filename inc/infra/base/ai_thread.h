/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 * Description: ai thread
 */
#ifndef INFRA_BASE_AI_THREAD_H
#define INFRA_BASE_AI_THREAD_H

#include <memory>

namespace hiai {
class AIThread;

typedef void (*HiaiTimerCallback)(void *data);

std::shared_ptr<AIThread> AIThread_Create(void *(*callBack)(void *), void *arg);

int AIThread_Join(const std::shared_ptr<AIThread> &aiThread);

int AIThread_Detach(const std::shared_ptr<AIThread> &aiThread);

void AIThread_Usleep(int period);

void AIThread_Sleep_For(int period);

int AIThread_Timer_Start(uint64_t timeout, void *data, HiaiTimerCallback cb, bool repeat);

int AIThread_Timer_Stop(int handle);
}  // namespace hiai
// #endif
#endif  // INFRA_BASE_AI_THREAD_H