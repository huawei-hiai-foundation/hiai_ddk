/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INFRA_BASE_SCOPE_GUARD_H
#define INFRA_BASE_SCOPE_GUARD_H

#include <functional>
#define HIAI_MAKE_GUARD(var, callback) hiai::ScopeGuard make_guard_##var(callback)
#define HIAI_DISMISS_GUARD(var) make_guard_##var.Dismiss()

namespace hiai {
class ScopeGuard {
public:
    // noncopyable
    ScopeGuard(ScopeGuard const&) = delete;
    ScopeGuard& operator=(ScopeGuard const&) = delete;

    explicit ScopeGuard(std::function<void()> onExitScope) : onExitScope_(onExitScope), dismissed_(false)
    {
    }

    ~ScopeGuard()
    {
        if ((!dismissed_) && onExitScope_ != nullptr) {
            onExitScope_();
        }
    }

    void Dismiss()
    {
        dismissed_ = true;
    }

private:
    std::function<void()> onExitScope_;
    bool dismissed_;
};
} // hiai

#endif // INFRA_BASE_SCOPE_GUARD_H