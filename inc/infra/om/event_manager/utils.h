/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2020. All rights reserved.
 * Description: utils
 */

#ifndef INC_OM_UTILS_H
#define INC_OM_UTILS_H

#include <vector>
#include <string>

#include "infra/base/api_export.h"

namespace om {
INFRA_API_EXPORT void ListDir(const std::string& path, std::vector<std::string>& result);

INFRA_API_EXPORT bool PathExists(const std::string& path);
} // namespace om

#endif // INC_OM_UTILS_H