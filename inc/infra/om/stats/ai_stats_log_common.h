/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INFRA_OM_STATS_LOG_COMMON_H
#define INFRA_OM_STATS_LOG_COMMON_H

#include <string>
#include <map>

#include "infra/base/api_export.h"
#include "infra/log/ai_log.h"

namespace hiai {
// 这个枚举值和 vendor/hisi/ap/include/ai/HIAIStats.h 中的AI_EngineType 与
// vendor/huawei/interfaces/ai/1.2/types.hal中的EngineType 保持一致
enum INFRA_API_EXPORT AiStatsEngineType {
    AI_STATS_START = 0,
    AI_STATS_HIAI_MNGR,
    AI_STATS_HIAI_ENGINE,
    AI_STATS_ANN,
    AI_STATS_HIAI_HCS,
    AI_STATS_HIAI_DDK,
    // 新增业务类型在此处增加
    AI_STATS_END
};

// 定义Stats内容结构体
struct HIAI_Stats_Info {
    unsigned int uid;
    AiStatsEngineType engineType;
    const char* interfaceName;
    const char* processName;
    const char* ddkVersion;
    int result;
    int runTime;
    uint64_t callTime;
};

struct AiStatsData {
    pid_t callPkg;
    std::string interfaceName;
    std::string ddkVersion;
    uint64_t callTime;
    std::string processName;
    AiStatsEngineType engineType;
    int allCnt;
    int failCnt;
    int runTime;
    std::map<int, int> result;
};

constexpr int AI_STATS_MODULEID = 20;
constexpr int32_t CMD_AI_STATS_WRITE_FILE = 1;
const char* const STATS_MODULE_NAME = "AI_INFRA";

#define STATS_LOGD(...) AI_LOGD(STATS_MODULE_NAME, __VA_ARGS__)
#define STATS_LOGI(...) AI_LOGI(STATS_MODULE_NAME, __VA_ARGS__)
#define STATS_LOGW(...) AI_LOGW(STATS_MODULE_NAME, __VA_ARGS__)
#define STATS_LOGE(...) AI_LOGE(STATS_MODULE_NAME, __VA_ARGS__)
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_COMMON_H
