/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2023. All rights reserved.
 * Description: ai stats log timer class
 */

#ifndef INFRA_OM_STATS_LOG_TIMER_H
#define INFRA_OM_STATS_LOG_TIMER_H

#include <thread>
#include <mutex>

#include "infra/base/api_export.h"
#include "infra/om/stats/ai_stats_log_timer_out_proc.h"

namespace hiai {
class INFRA_API_EXPORT AiStatsLogTimer {
public:
    static AiStatsLogTimer* GetInstance();
    inline void SetTime(int seconds)
    {
        this->seconds_ = seconds;
    }
    inline void SetTimerOutProc(AiStatsLogTimerOutProc* pProc)
    {
        this->pProc_ = pProc;
    }

public:
    void StartStatsLogTimer();

private:
    AiStatsLogTimer() = default;
    virtual ~AiStatsLogTimer()
    {
        pProc_ = nullptr;
    }
    static void* Start(void* arg);

private:
    static AiStatsLogTimer* instance_;
    static std::mutex instanceLock_;
    int seconds_ {20 * 60};
    AiStatsLogTimerOutProc* pProc_ {nullptr};
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_TIMER_H