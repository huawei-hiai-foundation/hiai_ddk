/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2023. All rights reserved.
 * Description: ai stats log utils class
 */

#ifndef INFRA_OM_STATS_LOG_UTILS_H
#define INFRA_OM_STATS_LOG_UTILS_H

#include <string>
#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <vector>
#include <mutex>
#include <map>
#include <atomic>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include "infra/base/api_export.h"
#include "infra/om/stats/ai_stats_log_common.h"

namespace hiai {
class INFRA_API_EXPORT AiStatsLogUtils {
public:
    AiStatsLogUtils() = default;
    ~AiStatsLogUtils() = default;

public:
    static std::vector<std::string>& GetFilesName(const std::string& dir);
    static std::string GetAiDDKVersion();
    static std::string GetTodayLogFileName(AiStatsEngineType type);
    static void DeleteStatsLog(std::string filename);
    static AiStatsData AddAiStatsData(AiStatsData& data1, AiStatsData& data2);
    static std::string BuildString(const AiStatsData& statsData);

private:
    // 获取目录下的所有文件名
    static void GetFileName(const char* dir);
    static std::vector<std::string> filesName_;
    static std::mutex filesNameLock_;

    // 通过属性获取DDK版本号
    static std::mutex gDdkVersionLock_;
};

class AiStatsLogCfgUtils {
public:
    // 如果打点配置文件"/data/vendor/hiai/ddk/stats.cfg"不存在，则创建，并写入默认值，否则将配置读取到内存中
    static void CreatOrGetCfg(); // only called by server when server launch
    static int GetStatsLogReserveDays(AiStatsEngineType type);
    static bool WriteStatsLogSwitchSettingToCfg(AiStatsEngineType type, bool isOpen);
    static bool WriteStatsLogReserveDaysSettingToCfg(AiStatsEngineType type, uint32_t days);
    static bool HasEngineStatsLog(AiStatsEngineType enginetype);
    static uint32_t GetEngineStatsLogSize(AiStatsEngineType enginetype);
    static int32_t GetEngineStatsLog(char* pStatsLog, uint32_t logSize, AiStatsEngineType enginetype);
    static int32_t DeleteEngineStatsLog(AiStatsEngineType enginetype);
    static uint32_t GetFileSize(std::string filename);
    static std::map<AiStatsEngineType, std::atomic<bool>> isMMLogSwitchOpen_;
    static std::string GetSwitchNameWithType(AiStatsEngineType type);
    static std::string GetReserveNameWithType(AiStatsEngineType type);
    static std::string GetLogPathWithType(AiStatsEngineType type);

private:
    AiStatsLogCfgUtils() = default;
    ~AiStatsLogCfgUtils() = default;
    static bool UpdateCfgFile();
    static void StoreStatsLogSwitchToAtomicFlag();
    static std::map<std::string, std::string> statsCfgMap_; // 存放配置文件信息
    static std::mutex statsCfgMapLock_; // 保护数据 statsCfgMap_
    static void CreatAndGetDefaultCfg();
    // 用于存储getlogsize、getlog、deletelog时的共同的文件名
    static std::map<AiStatsEngineType, std::string> logFileMap_;
    static std::mutex logFileMapLock_; // 保护数据 logFileMap_
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_UTILS_H
