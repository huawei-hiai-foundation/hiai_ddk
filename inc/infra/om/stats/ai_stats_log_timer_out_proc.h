/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2023. All rights reserved.
 * Description: ai stats log timer out proc class
 */

#ifndef INFRA_OM_STATS_LOG_TIMER_OUT_PROC_H
#define INFRA_OM_STATS_LOG_TIMER_OUT_PROC_H

#include <mutex>
#include <vector>

#include "infra/base/api_export.h"
#include "infra/om/stats/ai_stats_log_common.h"
#include "infra/om/stats/ai_stats_log_manager.h"

namespace hiai {
class INFRA_API_EXPORT AiStatsLogTimerOutProc {
public:
    static AiStatsLogTimerOutProc* GetInstance();
    void Timeout();

private:
    AiStatsLogTimerOutProc();
    ~AiStatsLogTimerOutProc() = default;
    static AiStatsLogTimerOutProc* instance_;
    static std::mutex instanceLock_;

private:
    std::vector<AiStatsLogWriter*> statsLogWriters_;
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_TIMER_OUT_PROC_H