/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2023. All rights reserved.
 * Description: ai stats log builder class
 */

#ifndef INFRA_OM_STATS_LOG_BUILDER_H
#define INFRA_OM_STATS_LOG_BUILDER_H

#include "infra/base/api_export.h"
#include "infra/om/stats/ai_stats_time.h"
#include "infra/om/stats/ai_stats_log_common.h"

namespace hiai {
class INFRA_API_EXPORT AiStatsLogBuilder {
public:
    AiStatsLogBuilder() = default;
    virtual ~AiStatsLogBuilder() = default;
    AiStatsLogBuilder& StartStats(
        unsigned int uid, AiStatsEngineType engineType, const char* clientInterfaceName, const char* processName);
    void EndStats(int result);
    void Stats(unsigned int uid, AiStatsEngineType engineType, const char* clientInterfaceName, const char* processName,
        int result);
    void Stats(unsigned int uid, AiStatsEngineType engineType, const char* clientInterfaceName, const char* processName,
        const char* ddkVersion, int result);
    void Stats(unsigned int uid, AiStatsEngineType engineType, const char* clientInterfaceName, const char* processName,
        const char* ddkVersion, int result, int runTime, uint64_t callTime);

private:
    bool CheckStatus(AiStatsEngineType engineType);
    AiStatsLogBuilder& CallUid(unsigned int uid);
    AiStatsLogBuilder& CallTimeNow();
    AiStatsLogBuilder& SetCallTime(uint64_t callTime);
    AiStatsLogBuilder& SetEngineType(AiStatsEngineType engineType);
    AiStatsLogBuilder& InterfaceName(const char* clientInterfaceName);
    AiStatsLogBuilder& ProcessResult(int result); // result is 0 if process success,result is other if process failed.
    AiStatsLogBuilder& RunTime();
    AiStatsLogBuilder& StartProcess();
    AiStatsLogBuilder& EndProcess();
    AiStatsLogBuilder& SetProcessName(const char* processName);
    AiStatsLogBuilder& SetDdkVersion(const char* ddkVersion);
    void Build();
    AiStatsData statsData_ {0, "", "", 0, "", AiStatsEngineType::AI_STATS_START, 0, 0, 0, {}};
    AiStatsTime time_;
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_BUILDER_H
