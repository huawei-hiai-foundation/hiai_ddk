/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 * Description: ai stats time class
 */

#ifndef INFRA_OM_STATS_AI_STATS_TIME_H
#define INFRA_OM_STATS_AI_STATS_TIME_H

#include <string>
#include <chrono>
#include <ctime>

namespace hiai {
class AiStatsTime {
public:
    AiStatsTime() = default;
    ~AiStatsTime() = default;
    inline void StartProcess()
    {
        this->start_ = std::chrono::system_clock::now();
    }
    inline void EndProcess()
    {
        this->end_ = std::chrono::system_clock::now();
    }
    int GetRunTime();
    static std::string Now();
    static uint64_t TimeNow();
    static std::string TimeToString(uint64_t t);
    static std::string Today();

private:
    std::chrono::system_clock::time_point start_;
    std::chrono::system_clock::time_point end_;
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_UTILS_H
