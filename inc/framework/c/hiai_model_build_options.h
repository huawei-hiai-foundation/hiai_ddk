/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_MODEL_BUILD_OPTIONS_H
#define FRAMEWORK_C_HIAI_MODEL_BUILD_OPTIONS_H
#include "c/hiai_c_api_export.h"
#include "hiai_nd_tensor_desc.h"
#include "c/hiai_error_types.h"
#include "c/ddk/model_manager/hiai_model_build_options.h"

#ifdef __cplusplus
extern "C" {
#endif
// build option begin

// inputTensorDescs will free in HIAI_ModelBuilderOptions_Destroy
AICP_C_API_EXPORT void HIAI_MR_ModelBuildOptions_SetInputTensorDescs(
    HIAI_MR_ModelBuildOptions* options, size_t inputNum, HIAI_NDTensorDesc** inputTensorDescs);
AICP_C_API_EXPORT size_t HIAI_MR_ModelBuildOptions_GetInputSize(const HIAI_MR_ModelBuildOptions* options);
AICP_C_API_EXPORT HIAI_NDTensorDesc** HIAI_MR_ModelBuildOptions_GetInputTensorDescs(
    const HIAI_MR_ModelBuildOptions* options);

typedef enum {
    HIAI_DYNAMIC_SHAPE_CACHE_BUILDED_MODEL,
    HIAI_DYNAMIC_SHAPE_CACHE_LOADED_MODEL
} HIAI_DYNAMIC_SHAPE_CACHE_MODE;

typedef enum { HIAI_DYNAMIC_SHAPE_DISABLE = 0, HIAI_DYNAMIC_SHAPE_ENABLE = 1 } HIAI_DYNAMIC_SHAPE_ENABLE_MODE;

typedef struct HIAI_MR_DynamicShapeConfig HIAI_MR_DynamicShapeConfig;

AICP_C_API_EXPORT HIAI_MR_DynamicShapeConfig* HIAI_MR_DynamicShapeConfig_Create(void);

AICP_C_API_EXPORT void HIAI_MR_DynamicShapeConfig_SetEnableMode(
    HIAI_MR_DynamicShapeConfig* config, HIAI_DYNAMIC_SHAPE_ENABLE_MODE mode);
AICP_C_API_EXPORT HIAI_DYNAMIC_SHAPE_ENABLE_MODE HIAI_MR_DynamicShapeConfig_GetEnableMode(
    const HIAI_MR_DynamicShapeConfig* config);

// maxCacheNum should be between 1 and 10.
AICP_C_API_EXPORT void HIAI_MR_DynamicShapeConfig_SetMaxCacheNum(
    HIAI_MR_DynamicShapeConfig* config, size_t maxCacheNum);
AICP_C_API_EXPORT size_t HIAI_MR_DynamicShapeConfig_GetMaxCacheNum(const HIAI_MR_DynamicShapeConfig* config);

AICP_C_API_EXPORT void HIAI_MR_DynamicShapeConfig_SetCacheMode(
    HIAI_MR_DynamicShapeConfig* config, HIAI_DYNAMIC_SHAPE_CACHE_MODE mode);
AICP_C_API_EXPORT HIAI_DYNAMIC_SHAPE_CACHE_MODE HIAI_MR_DynamicShapeConfig_GetCacheMode(
    const HIAI_MR_DynamicShapeConfig* config);

AICP_C_API_EXPORT void HIAI_MR_DynamicShapeConfig_Destroy(HIAI_MR_DynamicShapeConfig** config);

AICP_C_API_EXPORT void HIAI_MR_ModelBuildOptions_SetDynamicShapeConfig(
    HIAI_MR_ModelBuildOptions* options, HIAI_MR_DynamicShapeConfig* dynamicShapeConfig);
AICP_C_API_EXPORT HIAI_MR_DynamicShapeConfig* HIAI_MR_ModelBuildOptions_GetDynamicShapeConfig(
    const HIAI_MR_ModelBuildOptions* options);

typedef enum {
    HIAI_DEVICE_CONFIG_MODE_AUTO,
    HIAI_DEVICE_CONFIG_MODE_MODEL_LEVEL,
    HIAI_DEVICE_CONFIG_MODE_OP_LEVEL
} HIAI_DEVICE_CONFIG_MODE;

typedef enum { HIAI_FALLBACK_MODE_ENABLE, HIAI_FALLBACK_MODE_DISABLE } HIAI_FALLBACK_MODE;

typedef enum {
    HIAI_EXECUTE_DEVICE_NPU = 0,
    HIAI_EXECUTE_DEVICE_CPU = 1,
    HIAI_EXECUTE_DEVICE_GPU = 2
} HIAI_EXECUTE_DEVICE;

typedef enum {
    HIAI_DEVICE_MEMORY_REUSE_PLAN_UNSET = 0,
    HIAI_DEVICE_MEMORY_REUSE_PLAN_LOW = 1,
    HIAI_DEVICE_MEMORY_REUSE_PLAN_HIGH = 2
} HIAI_DEVICE_MEMORY_REUSE_PLAN;

// op device select config begin
typedef struct HIAI_MR_OpDeviceOrder HIAI_MR_OpDeviceOrder;

AICP_C_API_EXPORT HIAI_MR_OpDeviceOrder* HIAI_MR_OpDeviceOrder_Create(void);
AICP_C_API_EXPORT void HIAI_MR_OpDeviceOrder_SetOpName(HIAI_MR_OpDeviceOrder* config, const char* opName);
AICP_C_API_EXPORT const char* HIAI_MR_OpDeviceOrder_GetOpName(const HIAI_MR_OpDeviceOrder* config);
AICP_C_API_EXPORT void HIAI_MR_OpDeviceOrder_SetDeviceOrder(
    HIAI_MR_OpDeviceOrder* config, size_t supportedDeviceNum, HIAI_EXECUTE_DEVICE* supportedDevices);
AICP_C_API_EXPORT size_t HIAI_MR_OpDeviceOrder_GetSupportedDeviceNum(const HIAI_MR_OpDeviceOrder* config);
AICP_C_API_EXPORT HIAI_EXECUTE_DEVICE* HIAI_MR_OpDeviceOrder_GetSupportedDevices(const HIAI_MR_OpDeviceOrder* config);

AICP_C_API_EXPORT void HIAI_MR_OpDeviceOrder_Destroy(HIAI_MR_OpDeviceOrder** config);
// op device select config end

// cl Customizations select config begin
typedef struct HIAI_MR_CLCustomization HIAI_MR_CLCustomization;

AICP_C_API_EXPORT HIAI_MR_CLCustomization* HIAI_MR_CLCustomization_Create(void);
AICP_C_API_EXPORT void HIAI_MR_CLCustomization_SetOpName(HIAI_MR_CLCustomization* config, const char* opName);
AICP_C_API_EXPORT const char* HIAI_MR_CLCustomization_GetOpName(const HIAI_MR_CLCustomization* config);
AICP_C_API_EXPORT void HIAI_MR_CLCustomization_SetCustomization(
    HIAI_MR_CLCustomization* config, const char* customization);
AICP_C_API_EXPORT const char* HIAI_MR_CLCustomization_GetCustomization(const HIAI_MR_CLCustomization* config);
AICP_C_API_EXPORT void HIAI_MR_CLCustomization_Destroy(HIAI_MR_CLCustomization** config);
// cl Customizations select config end

// device select config begin
typedef struct HIAI_MR_ModelDeviceConfig HIAI_MR_ModelDeviceConfig;

AICP_C_API_EXPORT HIAI_MR_ModelDeviceConfig* HIAI_MR_ModelDeviceConfig_Create(void);

AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetDeviceConfigMode(
    HIAI_MR_ModelDeviceConfig* config, HIAI_DEVICE_CONFIG_MODE deviceConfigMode);
AICP_C_API_EXPORT HIAI_DEVICE_CONFIG_MODE HIAI_MR_ModelDeviceConfig_GetDeviceConfigMode(
    const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetFallBackMode(
    HIAI_MR_ModelDeviceConfig* config, HIAI_FALLBACK_MODE fallBackMode);
AICP_C_API_EXPORT HIAI_FALLBACK_MODE HIAI_MR_ModelDeviceConfig_GetFallBackMode(const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetModelDeviceOrder(
    HIAI_MR_ModelDeviceConfig* config, size_t configModelNum, HIAI_EXECUTE_DEVICE* modelDeviceOrder);
AICP_C_API_EXPORT size_t HIAI_MR_ModelDeviceConfig_GetConfigModelNum(const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT HIAI_EXECUTE_DEVICE* HIAI_MR_ModelDeviceConfig_GetModelDeviceOrder(
    const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetOpDeviceOrder(
    HIAI_MR_ModelDeviceConfig* config, size_t configOpNum, HIAI_MR_OpDeviceOrder** opDeviceOrder);
AICP_C_API_EXPORT size_t HIAI_MR_ModelDeviceConfig_GetConfigOpNum(const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT HIAI_MR_OpDeviceOrder** HIAI_MR_ModelDeviceConfig_GetOpDeviceOrder(
    const HIAI_MR_ModelDeviceConfig* config);

AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetDeviceMemoryReusePlan(
    HIAI_MR_ModelDeviceConfig* config, HIAI_DEVICE_MEMORY_REUSE_PLAN deviceMemoryReusePlan);
AICP_C_API_EXPORT HIAI_DEVICE_MEMORY_REUSE_PLAN HIAI_MR_ModelDeviceConfig_GetDeviceMemoryReusePlan(
    const HIAI_MR_ModelDeviceConfig* config);
AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_SetCLCustomization(
    HIAI_MR_ModelDeviceConfig* config, HIAI_MR_CLCustomization** clCustomization);
AICP_C_API_EXPORT HIAI_MR_CLCustomization** HIAI_MR_ModelDeviceConfig_GetCLCustomization(
    const HIAI_MR_ModelDeviceConfig* config);

AICP_C_API_EXPORT void HIAI_MR_ModelDeviceConfig_Destroy(HIAI_MR_ModelDeviceConfig** config);
// device select config end

AICP_C_API_EXPORT void HIAI_MR_ModelBuildOptions_SetModelDeviceConfig(
    HIAI_MR_ModelBuildOptions* options, HIAI_MR_ModelDeviceConfig* modelDeviceConfig);
AICP_C_API_EXPORT HIAI_MR_ModelDeviceConfig* HIAI_MR_ModelBuildOptions_GetModelDeviceConfig(
    const HIAI_MR_ModelBuildOptions* options);

AICP_C_API_EXPORT void HIAI_MR_ModelBuildOptions_SetEstimatedOutputSize(
    HIAI_MR_ModelBuildOptions* options, size_t size);
AICP_C_API_EXPORT size_t HIAI_MR_ModelBuildOptions_GetEstimatedOutputSize(const HIAI_MR_ModelBuildOptions* options);

typedef struct HIAI_MR_ConfigBuffer HIAI_MR_ConfigBuffer;

typedef enum {
    HIAI_TUNING_MODE_UNSET = 0,
    HIAI_TUNING_MODE_AUTO,
    HIAI_TUNING_MODE_HETER,
    HIAI_TUNING_MODE_CLOUD_TUNING_EXPORT
} HIAI_TUNING_MODE;

typedef enum {
    HIAI_TUNING_OBJECTIVE_PERFORMANCE = 0
} HIAI_TUNING_OBJECTIVE;
typedef struct HIAI_MR_TuningConfig HIAI_MR_TuningConfig;
AICP_C_API_EXPORT HIAI_MR_TuningConfig* HIAI_MR_TuningConfig_Create(void);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TuningConfig_SetTuningMode(
    HIAI_MR_TuningConfig* config, HIAI_TUNING_MODE tuningMode);
AICP_C_API_EXPORT HIAI_TUNING_MODE HIAI_MR_TuningConfig_GetTuningMode(const HIAI_MR_TuningConfig* config);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TuningConfig_SetTuningObjective(
    HIAI_MR_TuningConfig* config, HIAI_TUNING_OBJECTIVE tuningObjective);
AICP_C_API_EXPORT HIAI_TUNING_OBJECTIVE HIAI_MR_TuningConfig_GetTuningObjective(const HIAI_MR_TuningConfig* config);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TuningConfig_SetCacheDir(HIAI_MR_TuningConfig* config, const char* cacheDir);
AICP_C_API_EXPORT const char* HIAI_MR_TuningConfig_GetCacheDir(const HIAI_MR_TuningConfig* config);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_TuningConfig_SetDeviceMemoryReusePlan(
    HIAI_MR_TuningConfig* config, HIAI_DEVICE_MEMORY_REUSE_PLAN deviceMemoryReusePlan);
AICP_C_API_EXPORT HIAI_DEVICE_MEMORY_REUSE_PLAN HIAI_MR_TuningConfig_GetDeviceMemoryReusePlan(
    const HIAI_MR_TuningConfig* config);
AICP_C_API_EXPORT void HIAI_MR_TuningConfig_Destroy(HIAI_MR_TuningConfig** config);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_ModelBuildOptions_SetTuningConfig(
    HIAI_MR_ModelBuildOptions* options, HIAI_MR_TuningConfig* config);
AICP_C_API_EXPORT HIAI_MR_TuningConfig* HIAI_MR_ModelBuildOptions_GetTuningConfig(
    const HIAI_MR_ModelBuildOptions* options);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_ModelBuildOptions_SetCustomOpPath(
    HIAI_MR_ModelBuildOptions* options, const char* customPath);

// build option end

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_HIAI_MODEL_BUILD_OPTIONS_H
