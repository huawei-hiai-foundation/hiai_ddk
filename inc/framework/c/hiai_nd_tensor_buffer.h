/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_C_HIAI_MR_ND_TENSOR_BUFFER_H
#define FRAMEWORK_C_HIAI_MR_ND_TENSOR_BUFFER_H
#include "c/hiai_c_api_export.h"
#include "c/hcl/hiai_base_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

AICP_C_API_EXPORT HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateNoCopy(
    const HIAI_NDTensorDesc* desc, const void* data, size_t dataSize);

AICP_C_API_EXPORT uint8_t HIAI_MR_NDTensorBuffer_GetCacheStatus(const HIAI_MR_NDTensorBuffer* tensorBuffer);
AICP_C_API_EXPORT uint8_t HIAI_MR_NDTensorBuffer_GetUpdateCount(const HIAI_MR_NDTensorBuffer* tensorBuffer);
AICP_C_API_EXPORT void HIAI_MR_NDTensorBuffer_SetCacheStatus(HIAI_MR_NDTensorBuffer* tensorBuffer, uint8_t cacheStatus);

AICP_C_API_EXPORT void* HIAI_MR_NDTensorBuffer_GetHandle(const HIAI_MR_NDTensorBuffer* tensorBuffer);
AICP_C_API_EXPORT int32_t HIAI_MR_NDTensorBuffer_GetOriginFd(const HIAI_MR_NDTensorBuffer* tensorBuffer);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_HIAI_MR_ND_TENSOR_BUFFER_H
