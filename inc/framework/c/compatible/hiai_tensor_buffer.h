/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_COMPATIBLE_HIAI_TENSOR_BUFFER_H
#define FRAMEWORK_C_COMPATIBLE_HIAI_TENSOR_BUFFER_H

#include <stdint.h>

#include "hiai_tensor_desc.h"
#include "tensor/cache_status.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HIAI_TensorBuffer {
    HIAI_TensorDescription desc;
    int size;
    void* data;
    void* impl; /* DON'T MODIFY */
    const char* name;
    enum HiAICacheStatus cacheStatus;
    uint8_t updateCount;
} HIAI_TensorBuffer;

#ifdef __cplusplus
} // extern "C"
#endif

#endif // FRAMEWORK_C_COMPATIBLE_HIAI_TENSOR_BUFFER_H
