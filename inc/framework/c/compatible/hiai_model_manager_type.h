/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef HIAI_MODEL_MANAGER_TYPE_H
#define HIAI_MODEL_MANAGER_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum { HIAI_MODELTYPE_ONLINE = 0, HIAI_MODELTYPE_OFFLINE } HIAI_ModelType;

typedef enum {
    HIAI_FRAMEWORK_NONE = 0,
    HIAI_FRAMEWORK_TENSORFLOW = 1,
    HIAI_FRAMEWORK_CAFFE = 3,
    HIAI_FRAMEWORK_TENSORFLOW_8BIT,
    HIAI_FRAMEWORK_CAFFE_8BIT,
    HIAI_FRAMEWORK_OFFLINE,
    HIAI_FRAMEWORK_IR,
    HIAI_FRAMEWORK_INVALID,
} HIAI_Framework;

typedef enum {
    HIAI_DEVPERF_UNSET = 0,
    HIAI_DEVPREF_LOW,
    HIAI_DEVPREF_NORMAL,
    HIAI_DEVPREF_HIGH,
    HIAI_DEVPREF_EXTREME,
    HIAI_DEVPREF_LOW_COMPUTE_UNIT = 101, // Reserved.
    HIAI_DEVPREF_MIDDLE_COMPUTE_UNIT = 102, // Reserved.
    HIAI_DEVPREF_HIGH_COMPUTE_UNIT = 103,
    HIAI_DEVPREF_EXTREME_HIGH_COMPUTE_UNIT = 104, // Reserved.
} HIAI_DevPerf;

#ifdef __cplusplus
}
#endif

#endif
