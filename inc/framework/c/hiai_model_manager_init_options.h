/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_MODEL_MANAGER_INIT_OPTIONS_H
#define FRAMEWORK_C_HIAI_MODEL_MANAGER_INIT_OPTIONS_H
#include "c/hiai_c_api_export.h"
#include "hiai_model_build_options.h"
#include "c/hcl/hiai_execute_option_types.h"

#ifdef __cplusplus
extern "C" {
#endif

AICP_C_API_EXPORT void HIAI_MR_ModelInitOptions_SetBandMode(HIAI_MR_ModelInitOptions* options, HIAI_PerfMode bandMode);
AICP_C_API_EXPORT HIAI_PerfMode HIAI_MR_ModelInitOptions_GetBandMode(const HIAI_MR_ModelInitOptions* options);
AICP_C_API_EXPORT void HIAI_MR_ModelInitOptions_SetBuildOptions(
    HIAI_MR_ModelInitOptions* options, HIAI_MR_ModelBuildOptions* buildOptions);
AICP_C_API_EXPORT HIAI_MR_ModelBuildOptions* HIAI_MR_ModelInitOptions_GetBuildOptions(
    const HIAI_MR_ModelInitOptions* options);

AICP_C_API_EXPORT HIAI_MR_ModelInitOmOptions* HIAI_MR_ModelInitOmOptions_Create(void);
AICP_C_API_EXPORT void HIAI_MR_ModelInitOmOptions_SetOmType(HIAI_MR_ModelInitOmOptions* omOptions, int omType);
AICP_C_API_EXPORT void HIAI_MR_ModelInitOmOptions_SetOmOutDir(
    HIAI_MR_ModelInitOmOptions* omOptions, const char* omOutDir);
AICP_C_API_EXPORT int HIAI_MR_ModelInitOmOptions_SetOmAttr(
    HIAI_MR_ModelInitOmOptions* omOptions, const char* key, const char* value);
AICP_C_API_EXPORT void HIAI_MR_ModelInitOmOptions_Destroy(HIAI_MR_ModelInitOmOptions** omOptions);
AICP_C_API_EXPORT void HIAI_MR_ModelInitOptions_SetOmOptions(
    HIAI_MR_ModelInitOptions* options, HIAI_MR_ModelInitOmOptions* omOptions);
AICP_C_API_EXPORT HIAI_MR_ModelInitOmOptions* HIAI_MR_ModelInitOptions_GetOmOptions(
    HIAI_MR_ModelInitOptions* options);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_HIAI_MODEL_MANAGER_INIT_OPTIONS_H
