/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_BUILT_MODEL_H
#define FRAMEWORK_C_HIAI_BUILT_MODEL_H
#include <stdint.h>
#include <stddef.h>

#include "c/hiai_c_api_export.h"
#include "hiai_nd_tensor_desc.h"
#include "c/hiai_error_types.h"
#include "c/hcl/hiai_types.h"

#ifdef __cplusplus
extern "C" {
#endif

AICP_C_API_EXPORT HIAI_Status HIAI_MR_BuiltModel_Save(const HIAI_MR_BuiltModel* model, void** data, size_t* size);

AICP_C_API_EXPORT HIAI_MR_BuiltModel* HIAI_MR_BuiltModel_RestoreFromFileWithShapeIndex(
    const char* file, uint8_t shapeIndex);

AICP_C_API_EXPORT HIAI_Status HIAI_MR_BuiltModel_CheckCompatibility(
    const HIAI_MR_BuiltModel* model, HIAI_BuiltModel_Compatibility* compatibility);

typedef enum {
    HIAI_MODEL_OFFLINE_TUNING = 0,
    HIAI_MODEL_ONLINE_TUNING,
} HIAI_MODEL_TUNING_TYPE;
AICP_C_API_EXPORT HIAI_Status HIAI_MR_BuiltModel_CheckUpdatability(
    const HIAI_MR_BuiltModel* model, int32_t* updatability);
AICP_C_API_EXPORT HIAI_Status HIAI_MR_BuiltModel_GetLibraryTimestamp(const HIAI_MR_BuiltModel *model,
    char* currentModelLibraryTimestamp, int32_t currentTimestampSize, char* availableModelLibraryTimestamp,
    int32_t availableTimestampSize);

AICP_C_API_EXPORT HIAI_Status HIAI_MR_BuiltModel_GetFmMemorySize(const HIAI_MR_BuiltModel* model, uint64_t* fmSize);

#ifdef __cplusplus
}
#endif

#endif // FRAMEWORK_C_HIAI_BUILT_MODEL_H
