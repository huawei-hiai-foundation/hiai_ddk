/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 * Description: aipp_defs
 */
#ifndef INC_FRAMEWORK_GRAPH_OP_AIPP_DEFS_H
#define INC_FRAMEWORK_GRAPH_OP_AIPP_DEFS_H

#include "graph/operator_hiai_reg.h"

namespace hiai {
/*
 * Extract crop from the input image tensor.
 * <Input>
 *    x : the input tensor
 *    param : the crop param
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.600.020.010
 * <Examples>
 *    TensorDesc xDesc(Shape({2, 3, 128, 128}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    // static
 *    CropPara crop0;
 *    crop0.batchIndex = 0;
 *    crop0.cropStartPosW = 0;
 *    crop0.cropStartPosH = 0;
 *    crop0.cropSizeW = 64;
 *    crop0.cropSizeH = 64;
 *
 *    CropPara crop1;
 *    crop1.batchIndex = 1;
 *    crop1.cropStartPosW = 0;
 *    crop1.cropStartPosH = 0;
 *    crop1.cropSizeW = 64;
 *    crop1.cropSizeH = 64;
 *
 *    hiai::MutilCropPara multiCrop;
 *    multiCrop.imageFormat = ImageFormat::RGB888;
 *    multiCrop.cropParas = {crop0, crop1};
 *
 *    hiai::op::Const cropConst = ImageParaUtil::CreateConfigConst(multiCrop, "cropConst");
 *    auto imageMultiCrop = hiai::op::ImageMultiCrop("imageMultiCrop")
 *                      .set_input_x(x)
 *                      .set_input_param(cropConst);
 *
 *    // dynamic
 *    TensorDesc xDesc(Shape({2, 3, 128, 128}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    CropPara crop0;
 *    crop0.batchIndex = 0;
 *    crop0.cropStartPosW = 0;
 *    crop0.cropStartPosH = 0;
 *    crop0.cropSizeW = 64;
 *    crop0.cropSizeH = 64;
 *
 *    CropPara crop1;
 *    hiai::MutilCropPara multiCrop;
 *    multiCrop.imageFormat = ImageFormat::RGB888;
 *    multiCrop.cropParas = {crop0, crop1};
 *    hiai::op::ConfigData cropConfig =
 *    ImageParaUtil::CreateConfigData(multiCrop, "cropConfig", hiai::op::ImageMultiCrop::TYPE);
 *    auto imageMultiCrop = hiai::op::ImageMultiCrop("imageMultiCrop")
 *                        .set_input_x(x)
 *                        .set_input_param(cropConfig);
 */
REG_OP(ImageMultiCrop)
.INPUT(x, TensorType({ DT_UINT8 }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8 }))
.OP_END()

/*
 * Resize the input image tensor, It use bilinear or nearest neighbor algorithm to support scale up and down
 * <Input>
 *    x : the input tensor
 *    param : resize parameters
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.600.020.010
 * <Examples>
 *    TensorDesc xDesc(Shape({2, 3, 128, 128}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    // static
 *    hiai::ResizePara resize0;
 *    resize0.batchIndex = 0;
 *    resize0.resizeOutputSizeW = 64;
 *    resize0.resizeOutputSizeW = 64;
 *
 *    hiai::ResizePara resize1;
 *    resize1.batchIndex = 1;
 *    resize1.resizeOutputSizeW = 64;
 *    resize1.resizeOutputSizeW = 64;
 *
 *    MultiResizePara multiResizePara;
 *    multiResizePara.imageFormat = ImageFormat::RGB888;
 *    multiResizePara.resizeParas = {resize0, resize1};
 *
 *    hiai::op::Const resizeConst = ImageParaUtil::CreateConfigConst(multiResizePara, "resizeConst");
 *    auto imageMultiResize = hiai::op::ImageMultiResize("imageMultiResize")
 *                      .set_input_x(x)
 *                      .set_input_param(resizeConst);
 *
 *    // dynamic
 *    hiai::ResizePara resize0;
 *    resize0.batchIndex = 0;
 *    resize0.resizeOutputSizeW = 64;
 *    resize0.resizeOutputSizeW = 64;
 *
 *    hiai::ResizePara resize1;
 *    MultiResizePara multiResizePara;
 *    multiResizePara.imageFormat = ImageFormat::RGB888;
 *    multiResizePara.resizeParas = {resize0, resize1};
 *    hiai::op::ConfigData resizeConfig = ImageParaUtil::CreateConfigData(
 *                                          multiResizePara, "resizeConfig", hiai::op::ImageMultiResize::TYPE);
 *    auto imageMultiResize = hiai::op::ImageMultiResize("imageMultiResize")
 *                      .set_input_x(x)
 *                      .set_input_param(resizeConfig);
 */
REG_OP(ImageMultiResize)
.INPUT(x, TensorType({ DT_UINT8 }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8 }))
.OP_END()

/*
 * Change image channel before Color Space Convertion
 * <Input>
 *    x : the input tensor
 *    param : channel swap parameters
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.520.020.100
 * <Examples>
 *    TensorDesc xDesc(Shape({1, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    hiai::ChannelSwapPara channelSwap;
 *    channelSwap.imageFormat = ImageFormat::RGB888;
 *    channelSwap.rbuvSwapSwitch = true;
 *    channelSwap.axSwapSwitch = true;
 *
 *    // static
 *    hiai::op::Const swapConst =
 *          ImageParaUtil::CreateConfigConst(channelSwap, "swapConst", hiai::op::ImageChannelSwapV2::TYPE);
 *    auto channelSwapV2 = hiai::op::ImageChannelSwapV2("channelSwapV2")
 *                       .set_input_x(x)
 *                       .set_input_param(swapConst);
 *
 *    // dynamic
 *    hiai::op::ConfigData swapConfig =
 *          ImageParaUtil::CreateConfigData(channelSwap, "swapConfig", hiai::op::ImageChannelSwapV2::TYPE);
 *    auto channelSwapV2 = hiai::op::ImageChannelSwapV2("channelSwapV2")
 *                       .set_input_x(x)
 *                       .set_input_param(swapConfig);
 */
REG_OP(ImageChannelSwapV2)
.INPUT(x, TensorType({ DT_UINT8 }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8 }))
.OP_END()

/*
 * AI core support feature map data type are: int8 and fp16. But image RGB888 or YUV444 data are all uint8 data type.
 * So we need data type conversion.
 * uint8->int8: pixel_out_chx(i) = pixel_in_chx(i) - mean_chn_i
 * uint8->fp16: pixel_out_chx(i) = (pixel_in_chx(i) - mean_chn_i - min_chn_i) * var_reci_chn
 * <Input>
 *    x : the input tensor
 *    param : dataType convertion parameters
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.600.020.010
 * <Examples>
 *    TensorDesc xDesc(Shape({1, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *    // static
 *    hiai::DtcPara dtcPara;
 *    dtcPara.batchIndex = 0;
 *    dtcPara.pixelMeanChn0 = 0;
 *    dtcPara.pixelMeanChn1 = 1;
 *    dtcPara.pixelMeanChn2 = 2;
 *    dtcPara.pixelMeanChn3 = 3;
 *    dtcPara.pixelMinChn0 = 4.0f;
 *    dtcPara.pixelMinChn1 = 5.0f;
 *    dtcPara.pixelMinChn2 = 6.0f;
 *    dtcPara.pixelMinChn3 = 7.0f;
 *    dtcPara.pixelVarReciChn0 = 8.0f;
 *    dtcPara.pixelVarReciChn1 = 9.0f;
 *    dtcPara.pixelVarReciChn2 = 10.0f;
 *    dtcPara.pixelVarReciChn3 = 11.0f;
 *
 *    MultiDtcPara multiDtcPara;
 *    multiDtcPara.imageFormat = ImageFormat::RGB888;
 *    multiDtcPara.dtcParas = {dtcPara};
 *    hiai::op::Const dtcConst = ImageParaUtil::CreateConfigConst(multiDtcPara, "dtcConst");
 *    auto multiDTC = hiai::op::ImageMultiDataTypeConvertion("multiDTC")
 *                      .set_input_x(x)
 *                      .set_input_param(dtcConst);
 *
 *    // dynamic
 *    MultiDtcPara multiDtcPara;
 *    multiDtcPara.imageFormat = ImageFormat::RGB888;
 *    multiDtcPara.dtcParas.resize(1);
 *    hiai::op::ConfigData dtcConfig =
 *        ImageParaUtil::CreateConfigData(multiDtcPara, "dtcConfig", hiai::op::ImageMultiDataTypeConvertion::TYPE);
 *    auto multiDTC = hiai::op::ImageMultiDataTypeConvertion("multiDTC")
 *                      .set_input_x(x)
 *                      .set_input_param(dtcConfig);
 */
REG_OP(ImageMultiDataTypeConvertion)
.INPUT(x, TensorType({ DT_UINT8 }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8, DT_INT8, DT_FLOAT }))
.OP_END()

/*
 * Support convert YUV444 to RGB888, RGB888 to YUV444.
 * Set target_format, CSC_MATRIX and CSC_BIAS will be configured by system according to input_format,
 * image_type and target_format.
 * <Input>
 *    x : the input tensor
 *    param : color space convertion parameters
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.520.020.100
 * <Examples>
 *    TensorDesc xDesc(Shape({1, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("Placeholder");
 *    x.update_input_desc_x(xDesc);
 *
 *    hiai::CscPara cscPara;
 *    cscPara.outputFormat = ImageFormat::YUV444SP;
 *    cscPara.imageColorSpace = ImageColorSpace::JPEG;
 *
 *    // static
 *    hiai::op::Const cscConst =
 *         ImageParaUtil::CreateConfigConst(cscPara, "cscConst", hiai::op::ImageColorSpaceConvertionV2::TYPE);
 *    auto colorSpaceConvertionV2 = hiai::op::ImageChannelSwapV2("channelSwapV2")
 *                       .set_input_x(x)
 *                       .set_input_param(cscConst);
 *
 *    // dynamic
 *    hiai::op::ConfigData cscConfig =
 *         ImageParaUtil::CreateConfigData(cscPara, "cscConfig", hiai::op::ImageColorSpaceConvertionV2::TYPE);
 *    auto colorSpaceConvertionV2 = hiai::op::ImageChannelSwapV2("channelSwapV2")
 *                       .set_input_x(x)
 *                       .set_input_param(cscConfig);
 */
REG_OP(ImageColorSpaceConvertionV2)
.INPUT(x, TensorType({ DT_UINT8 }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8 }))
.OP_END()

/*
 * Rotate the input image tensor, It use bilinear or nearest neighbor algorithm to support scale up and down
 * <Input>
 *    x : the input tensor.
 *    param : rotate parameters, not supported dynamic currently
 * <Output>
 *    y : the output tensor
 * <Added in HiAI version>
 *    100.600.020.010
 * <Examples>
 *    TensorDesc xDesc(Shape({2, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    // static
 *    hiai::RotatePara rotatePara0;
 *    rotatePara0.batchIndex = 0;
 *    rotatePara0.rotationAngle = 90.0f;
 *    rotatePara0.rotate = true;
 *
 *    hiai::RotatePara rotatePara1;
 *    rotatePara1.batchIndex = 0;
 *    rotatePara1.rotationAngle = 90.0f;
 *    rotatePara1.rotate = true;
 *
 *    MultiRotatePara multiRotatePara;
 *    multiRotatePara.imageFormat = ImageFormat::RGB888;
 *    multiRotatePara.rotateParas = {rotatePara0, rotatePara1};
 *
 *    hiai::op::Const rotateConst = ImageParaUtil::CreateConfigConst(multiRotatePara, "rotateConst");
 *    auto multiRotate = hiai::op::ImageMultiRotate("multiRotate")
 *                      .set_input_x(x)
 *                      .set_input_param(rotateConst);
 *
 *    // dynamic
 *    hiai::RotatePara rotatePara0;
 *    rotatePara0.batchIndex = 0;
 *    rotatePara0.rotationAngle = 90.0f;
 *
 *    hiai::RotatePara rotatePara1;
 *    MultiRotatePara multiRotatePara;
 *    multiRotatePara.imageFormat = ImageFormat::RGB888;
 *    multiRotatePara.rotateParas = {rotatePara0, rotatePara1};
 *    hiai::op::ConfigData rotateConfig =
 *        ImageParaUtil::CreateConfigData(multiRotatePara, "dtcConfig", hiai::op::ImageMultiRotate::TYPE);
 *    auto multiDTC = hiai::op::ImageMultiRotate("multiDTC")
 *                      .set_input_x(x)
 *                      .set_input_param(rotateConfig);
 */
REG_OP(ImageMultiRotate)
.INPUT(x, TensorType({ DT_UINT8, DT_INT8, DT_FLOAT }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8, DT_INT8, DT_FLOAT }))
.OP_END()

/*
 * Add padding to input image tensor
 * <Input>
 *    x : the input tensor.
 *    param : padding parameters
 * <Output>
 *    y : the output tensor.
 * <Added in HiAI version>
 *    100.600.020.010
 * <Examples>
 *    TensorDesc xDesc(Shape({2, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    // static
 *    hiai::PadPara padPara0;
 *    padPara0.batchIndex = 0;
 *    padPara0.paddingSizeTop = 10;
 *    padPara0.paddingSizeBottom = 10;
 *    padPara0.paddingSizeLeft = 20;
 *    padPara0.paddingSizeRight = 20;
 *
 *    hiai::PadPara padPara1;
 *    padPara1.batchIndex = 1;
 *    padPara1.paddingSizeTop = 10;
 *    padPara1.paddingSizeBottom = 10;
 *    padPara1.paddingSizeLeft = 20;
 *    padPara1.paddingSizeRight = 20;
 *
 *    MultiPadPara multiPadPara;
 *    multiPadPara.imageFormat = ImageFormat::RGB888;
 *    multiPadPara.padParas = {padPara0, padPara1};
 *
 *    hiai::op::Const padConst = ImageParaUtil::CreateConfigConst(multiPadPara, "padConst");
 *    auto multiPad = hiai::op::ImageMultiPad("multiPad")
 *                      .set_input_x(x)
 *                      .set_input_param(padConst);
 *
 *    // dynamic
 *    TensorDesc xDesc(Shape({2, 3, 64, 64}), FORMAT_NCHW, DT_UINT8);
 *    hiai::op::Data x = hiai::op::Data("x");
 *    x.update_input_desc_x(xDesc);
 *
 *    hiai::PadPara padPara0;
 *    padPara0.batchIndex = 0;
 *    padPara0.paddingSizeTop = 10;
 *    padPara0.paddingSizeBottom = 10;
 *    padPara0.paddingSizeLeft = 20;
 *    padPara0.paddingSizeRight = 20;
 *
 *    hiai::PadPara padPara1;
 *    MultiPadPara multiPadPara;
 *    multiPadPara.imageFormat = ImageFormat::RGB888;
 *    multiPadPara.padParas = {padPara0, padPara1};
 *    hiai::op::ConfigData padConfig =
 *        ImageParaUtil::CreateConfigData(multiPadPara, "padConfig", hiai::op::ImageMultiPad::TYPE);
 *    auto multiPad = hiai::op::ImageMultiPad("multiPad")
 *                      .set_input_x(x)
 *                      .set_input_param(padConfig);
 */
REG_OP(ImageMultiPad)
.INPUT(x, TensorType({ DT_UINT8, DT_INT8, DT_FLOAT }))
.INPUT(param, TensorType({ DT_UINT8 }))
.OUTPUT(y, TensorType({ DT_UINT8, DT_INT8, DT_FLOAT }))
.OP_END()
}
#endif