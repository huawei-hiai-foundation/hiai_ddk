#ifndef _AIPP_ATTR_DEFINE_H_
#define _AIPP_ATTR_DEFINE_H_

namespace hiai {
const char * const AIPP_MODEL_DATA_DIM_N = "aipp_model_data_dim_n";
const char * const AIPP_MODEL_DATA_DIM_W = "aipp_model_data_dim_w";
const char * const AIPP_MODEL_DATA_DIM_H = "aipp_model_data_dim_h";
const char * const AIPP_MODEL_DATA_DIM_C = "aipp_model_data_dim_c";
const char * const AIPP_ATTR_MAX_IMAGE_SIZE = "max_src_image_size";
const char * const AIPP_MODEL_DATA_TYPE = "aipp_model_data_type";
const char * const AIPP_DATA_FLAG = "aipp_data_flag";
const char * const ATTR_AIPP_COMPATIBLE = "aipp_compatible_flag";
const char * const AIPP_INPUT_NAME = "aipp_input_name";
const char * const AIPP_DYN_INPUT = "aipp_dyn_input";
const char * const AIPP_CONV_FLAG = "Aipp_Conv_Flag";
const char * const AIPP_MODE = "aipp_mode";
const char * const ATTR_NAME_AIPP = "aipp";
const char * const ATTR_NAME_WAIPP = "waipp";
const char * const AIPP_CONV_OP_NAME = "aipp_conv_op";
const char * const AIPP_CONFIG_INPUT = "AIPP_CONFIG_INPUT";
const char * const AIPP_ATTR_SINGLE_BATCH_MULTI_CROP = "single_batch_multi_crop";
}

#endif
