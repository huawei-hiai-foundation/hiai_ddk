/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TENSOR_FORMAT_INDEX_DEFINE_H
#define TENSOR_FORMAT_INDEX_DEFINE_H

namespace ge {
/*
 * NCHW索引默认值
 */
enum NCHWIndex {
    NCHW_DIM_N = 0,
    NCHW_DIM_C,
    NCHW_DIM_H,
    NCHW_DIM_W
};

/*
 * NHWC索引默认值
 */
enum NHWCIndex {
    NHWC_DIM_N = 0,
    NHWC_DIM_H,
    NHWC_DIM_W,
    NHWC_DIM_C
};

/*
 * HWCK索引默认值
 */
enum HWCKIndex {
    HWCK_DIM_H = 0,
    HWCK_DIM_W,
    HWCK_DIM_C,
    HWCK_DIM_K
};

/*
 * KCDHW索引默认值
 */
enum KCDHWIndex {
    KCDHW_DIM_K = 0,
    KCDHW_DIM_C,
    KCDHW_DIM_D,
    KCDHW_DIM_H,
    KCDHW_DIM_W
};

/*
 * NDHWC索引默认值
 */
enum NDHWCIndex {
    NDHWC_DIM_N = 0,
    NDHWC_DIM_D,
    NDHWC_DIM_H,
    NDHWC_DIM_W,
    NDHWC_DIM_C
};

/*
 * NCDHW索引默认值
 */
enum NCDHWIndex {
    NCDHW_DIM_N = 0,
    NCDHW_DIM_C,
    NCDHW_DIM_D,
    NCDHW_DIM_H,
    NCDHW_DIM_W
};

/*
 * DHWCN 索引默认值
 */
enum DHWCNIndex {
    DHWCN_DIM_D = 0,
    DHWCN_DIM_H,
    DHWCN_DIM_W,
    DHWCN_DIM_C,
    DHWCN_DIM_N
};

/*
 * CHW索引默认值
 */
enum CHWIndex {
    CHW_DIM_C = 0,
    CHW_DIM_H,
    CHW_DIM_W
};

/*
 * HWC索引默认值
 */
enum HWCIndex {
    HWC_DIM_H = 0,
    HWC_DIM_W,
    HWC_DIM_C
};

/*
 * Pad值的索引默认值
 */
enum PadHWIndex {
    PAD_H_HEAD = 0,
    PAD_H_TAIL,
    PAD_W_HEAD,
    PAD_W_TAIL
};

/*
 * Pad3D值的索引默认值
 */
enum Pad3DDHWIndex {
    PAD3D_D_HEAD = 0,
    PAD3D_D_TAIL,
    PAD3D_H_HEAD,
    PAD3D_H_TAIL,
    PAD3D_W_HEAD,
    PAD3D_W_TAIL,
};

/*
 * 宽、高索引默认值
 */
enum HWIndex {
    INDEX_H = 0,
    INDEX_W,
};

/*
 * 深、宽、高索引默认值
 */
enum DHWIndex {
    DHWINDEX_D = 0,
    DHWINDEX_H,
    DHWINDEX_W,
};

/*
 * output_padding值的索引默认值
 */
enum OutputPaddingHWIndex {
    OUT_PAD_H_TAIL = 0,
    OUT_PAD_W_TAIL,
};
}; // namespace ge

#endif // TENSOR_FORMAT_INDEX_DEFINE_H
