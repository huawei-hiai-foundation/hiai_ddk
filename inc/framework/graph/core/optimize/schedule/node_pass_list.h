/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_GRAPH_CORE_OPTIMIZE_SCHEDULE_NODE_PASS_LIST_H
#define FRAMEWORK_GRAPH_CORE_OPTIMIZE_SCHEDULE_NODE_PASS_LIST_H

#include <memory>
#include <functional>

#include "framework/graph/core/optimize/optimize_status.h"

#include "framework/graph/core/cgraph/graph_fwd.h"
#include "framework/graph/core/node/node_fwd.h"

#include "framework/common/hcs_types.h"

namespace hiai {
class INodePass;

using NodeValidater = std::function<bool(ge::Node& node)>;

class HCS_API_EXPORT NodePassList {
public:
    static std::unique_ptr<NodePassList> Make();

    virtual void Add(std::unique_ptr<INodePass> pass) = 0;
    virtual OptimizeStatus Optimize(ge::Node& node, ge::ComputeGraph& graph, NodeValidater validater) = 0;
    virtual bool Attention(const std::string& type) const = 0;

    virtual ~NodePassList() = default;
};
} // namespace hiai

#endif