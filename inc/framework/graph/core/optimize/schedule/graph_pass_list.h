/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_GRAPH_CORE_OPTIMIZE_SCHEDULE_GRAPH_PASS_LIST_H
#define FRAMEWORK_GRAPH_CORE_OPTIMIZE_SCHEDULE_GRAPH_PASS_LIST_H

#include <memory>

#include "framework/graph/core/optimize/optimize_status.h"

#include "framework/graph/core/cgraph/graph_fwd.h"
#include "framework/common/hcs_types.h"

namespace hiai {
class GraphPass;

class HCS_API_EXPORT GraphPassList {
public:
    static std::unique_ptr<GraphPassList> Make();

    virtual void Add(std::unique_ptr<GraphPass> pass) = 0;
    virtual OptimizeStatus Optimize(ge::ComputeGraph& graph) = 0;

    virtual ~GraphPassList() = default;
};
} // namespace hiai

#endif