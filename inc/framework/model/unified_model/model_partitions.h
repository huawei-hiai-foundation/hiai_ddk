/**
 * Copyright 2023-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_MODEL_MODEL_PARTITIONS_H
#define FRAMEWORK_MODEL_MODEL_PARTITIONS_H

#include <vector>
#include "securec.h"
#include "framework/common/types.h"
#include "framework/common/fmk_types.h"
#include "base/maybe.h"

namespace hiai {
struct ModelPartition {
    bool isPassedFromHidl {false};
    ModelPartitionType type;
    uint8_t* data {nullptr};
    uint32_t size {0};
};

using Status = uint32_t;
class ModelPartitions {
public:
    ModelPartitions() = default;
    ~ModelPartitions() = default;

    ModelPartitions(const ModelPartitions& other)
    {
        modelPartitions_ = other.modelPartitions_;
    }

    ModelPartitions& operator=(const ModelPartitions& other)
    {
        if (this == &other) {
            return *this;
        }
        modelPartitions_ = other.modelPartitions_;
        return *this;
    }

    Status Init(const uint8_t* partitionsData, size_t partitionsSize, bool isPassedFromHidl);

    Maybe<ModelPartition> GetPartition(ModelPartitionType type, int8_t index = -1) const;

    inline size_t GetPartitionsSize() const
    {
        return modelPartitions_.size();
    }

private:
    bool CheckModelPartitionTableValid(
        uint32_t partitionsSize, const ModelPartitionTable* mpTable, uint32_t mpTableSize);

    Status ParseModelPartitionTable(const uint8_t* partitionsData, const ModelPartitionTable* mpTable,
        uint32_t mpTableSize, bool isPassedFromHidl);

private:
    std::vector<ModelPartition> modelPartitions_ {};
};
} // namespace hiai
#endif
