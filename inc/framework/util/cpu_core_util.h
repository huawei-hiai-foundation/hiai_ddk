/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Description: The API of getting the CPU core IDs.
 */

#ifndef _CPU_CORE_UTIL_H_
#define _CPU_CORE_UTIL_H_

#include <map>
#include <vector>

#include "infra/base/hiai_types.h"

namespace hiai {
class AICP_API_EXPORT CpuCoreUtil {
public:
    static CpuCoreUtil& GetInstance();
    const std::vector<int>& GetBigCpuIDs(void) const;
    const std::vector<int>& GetMidCpuIDs(void) const;
    const std::vector<int>& GetLittleCpuIDs(void) const;

private:
    CpuCoreUtil();
    ~CpuCoreUtil() = default;
    CpuCoreUtil(const CpuCoreUtil&) = delete;
    CpuCoreUtil& operator=(const CpuCoreUtil&) = delete;
    void Init(void);
    int GetCPUCapacity(int cpuID, int& capacity);
    std::map<int, std::vector<int>> GetAllCpuIdCapatity(void);

private:
    std::vector<int> bigCpuIDs_;
    std::vector<int> midCpuIDs_;
    std::vector<int> littleCpuIDs_;
};
} // namespace hiai

#endif // _CPU_CORE_UTIL_H_
