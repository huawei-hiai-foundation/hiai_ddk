/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2021. All rights reserved.
 * Description: memory allocator
 *
 */

#ifndef GE_EXECUTOR_MEM_ALLOCATOR_H
#define GE_EXECUTOR_MEM_ALLOCATOR_H

#include <cstdint>
#include <vector>
#include "base/error_types.h"
#include "framework/common/fmk_types.h"
#include "infra/alias/aicp.h"

struct HIAI_TensorBuffer;
struct HIAI_MR_NDTensorBuffer;

namespace hiai {
class MemoryAllocator {
public:
    virtual ~MemoryAllocator() = default;

    virtual void* Allocate(uint64_t size, hiai::MemoryType type, uint64_t cacheOffset = 0) = 0;

    virtual void Free(void* addr, hiai::MemoryType type) = 0;

    virtual Status Copy(void* dst, uint64_t dstSize, hiai::MemoryType dstType, const void* src, uint64_t srcSize,
        hiai::MemoryType srcType) = 0;

    virtual Status FlushCache(uint64_t addr, uint64_t size)
    {
        (void)addr;
        (void)size;
        return SUCCESS;
    }

    virtual Status FlushCache(void* base, uint64_t len)
    {
        (void)base;
        (void)len;
        return SUCCESS;
    }

    virtual Status InvalidCache(uint64_t addr, uint64_t size)
    {
        (void)addr;
        (void)size;
        return SUCCESS;
    }

    virtual HIAI_MR_NDTensorBuffer* GetHIAINDTensorBuffer(void* addr) const
    {
        (void)addr;
        return nullptr;
    }

    virtual void* GetClientVA(const void* memAddr)
    {
        (void)memAddr;
        return nullptr;
    }

    virtual Status MmapClientVAVec(
        const std::vector<std::pair<int32_t, int32_t>>& handles, std::vector<void*>& clientVAs) const
    {
        (void)handles;
        (void)clientVAs;
        return SUCCESS;
    }

    virtual Status UnmapClientVAVec(const std::vector<std::pair<void*, int32_t>>& handles) const
    {
        (void)handles;
        return SUCCESS;
    }

    virtual int GetFd(void* addr)
    {
        (void)addr;
        return -1;
    }
};
} // namespace hiai
#endif // GE_EXECUTOR_MEM_ALLOCATOR_H
