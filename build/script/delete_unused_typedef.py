#!/usr/bin/env python3
#-*- coding: UTF-8 -*-
# Copyright © Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 
import os
import sys
import io
import re
 
SRC_DIR = "api/framework/c"
DEST_DIR = "out/hihms/ddk_include/api/framework/c"
 
def replace_include_path(read_file):
    new_content = read_file.read()
    patterns = {
        re.compile(r'#include "c/hcl/hiai_types.h') : '#include "c/hiai_types.h',
        re.compile(r'#include "c/hcl/hiai_base_types.h') : '#include "c/hiai_base_types.h',
        re.compile(r'#include "c/hcl/hiai_execute_option_types.h') : '#include "hiai_execute_option_types.h',
    }
    for pat in patterns:
        new_content = re.sub(pat, patterns[pat], new_content)
    return new_content
 
 
def handle_ddk_h():     # 调整include的头文件路径
    src_h_dir = os.path.join(SRC_DIR, "ddk/model_manager")
    dest_h_dir = os.path.join(DEST_DIR, "ddk/model_manager")
    file_list = [
        "hiai_built_model.h",
        "hiai_model_builder.h",
        "hiai_model_build_options.h",
        "hiai_model_manager.h",
        "hiai_model_manager_init_options.h",
    ]
 
    if not os.path.exists(dest_h_dir):
            os.makedirs(dest_h_dir)
 
    for h_file in file_list:
        src_h_path = os.path.join(src_h_dir, h_file)
        dest_h_path = os.path.join(dest_h_dir, h_file)
        new_content = []
        with io.open(src_h_path, encoding="utf8", errors='ignore') as read_file:
            new_content = replace_include_path(read_file)
 
        with io.open(dest_h_path, 'w') as dest_file:
            dest_file.write(new_content)
 
 
def get_reserved_typedef(typedef_def, reserved_types):
    type_name = typedef_def[-2].split(' ')[-1].split(';')[0]
    if type_name in reserved_types:
        return typedef_def
    else:
        return []
 
 
def handle_types(lines, reserved_types):
    new_context = []
    typedef_def = []
    is_typedef = False
    for line in lines:
        if not is_typedef and "typedef" in line:    # 定义语句开始标记
            typedef_def.clear()
            typedef_def.append(line)
            is_typedef = True
            continue
 
        if is_typedef:
            typedef_def.append(line)
            if line == '\n':    # 定义语句结束标记
                reserved_typedef = get_reserved_typedef(typedef_def, reserved_types)
                new_context.extend(reserved_typedef)
                is_typedef = False
                typedef_def.clear()
            continue
        new_context.append(line)
    return new_context
 
 
def get_reserved_funcdef(func_def, reserved_funcs):
    func_name = ""
    for line in func_def:
        if "AICP_C_API_EXPORT" in line:
            func_name = line.split('(')[0].split(' ')[-1]
    if func_name in reserved_funcs or func_name == "":  # func_name为""表示文件头注释
        return func_def
    else:
        return []
 
 
def replace_h_path(old_line):      # 修改 hiai_base_types.h -> c/hiai_base_types.h
    new_line = old_line
    patterns = {
        re.compile(r'#include "hiai_types.h') : '#include "c/hiai_types.h',
        re.compile(r'#include "hiai_base_types.h') : '#include "c/hiai_base_types.h',
    }
    for pat in patterns:
        new_line = re.sub(pat, patterns[pat], new_line)
    return new_line
 
 
def handle_funcs(lines, reserved_funcs):
    new_context = []
    func_def = []
    is_func = False
    for line in lines:
        if not is_func and "/**" in line:   # 定义语句开始标记
            func_def.clear()
            func_def.append(line)
            is_func = True
            continue
 
        if is_func:
            func_def.append(line)
            if line == '\n':    # 定义语句结束标记
                reserved_func = get_reserved_funcdef(func_def, reserved_funcs)
                new_context.extend(reserved_func)
                is_func = False
                func_def.clear()
            continue
        new_line = replace_h_path(line)
        new_context.append(new_line)
    return new_context
 
 
def delete_unused(read_file, h_file):
    lines = read_file.readlines()
    # "文件名"："文件中保留的类型"
    file_type_dict = {
        "hiai_base_types.h" : ["HIAI_DataType", "HIAI_Format"],
        "hiai_execute_option_types.h" : ["HIAI_PerfMode"],
        "hiai_types.h" : ["HIAI_MR_ModelManager", "HIAI_MR_ModelInitOptions", "HIAI_MR_ModelBuildOptions",
                          "HIAI_MR_BuiltModel", "HIAI_NDTensorDesc", "HIAI_MR_NDTensorBuffer",
                          "HIAI_MR_ModelManagerListener"],
    }
    # "文件名"："文件中保留的函数"
    file_func_dict = {
        "hiai_nd_tensor_desc.h" : ["HIAI_NDTensorDesc_CreateDefault", "HIAI_NDTensorDesc_Destroy",
                                "HIAI_NDTensorDesc_SetDataType", "HIAI_NDTensorDesc_GetDataType",
                                "HIAI_NDTensorDesc_SetFormat", "HIAI_NDTensorDesc_GetFormat",
                                "HIAI_NDTensorDesc_SetDims", "HIAI_NDTensorDesc_GetDimNum", "HIAI_NDTensorDesc_GetDim",],
        "hiai_nd_tensor_buffer.h" : ["HIAI_MR_NDTensorBuffer_CreateFromNDTensorDesc", "HIAI_MR_NDTensorBuffer_Destroy",
                                    "HIAI_MR_NDTensorBuffer_GetData", "HIAI_MR_NDTensorBuffer_GetSize", "HIAI_MR_NDTensorBuffer_CreateFromBuffer"]
    }
    new_context = []
    if h_file in file_type_dict.keys():
        new_context = handle_types(lines, file_type_dict[h_file])
 
    if h_file in file_func_dict.keys():
        new_context = handle_funcs(lines, file_func_dict[h_file])
 
    return new_context
 
 
def handle_hcl_h():
    src_h_dir = os.path.join(SRC_DIR, "hcl")
    dest_h_dir = os.path.join(DEST_DIR, "hcl")
    file_list = [
        "hiai_base_types.h",
        "hiai_execute_option_types.h",
        "hiai_types.h",
        "hiai_nd_tensor_desc.h",
        "hiai_nd_tensor_buffer.h",
    ]
 
    if not os.path.exists(dest_h_dir):
            os.makedirs(dest_h_dir)
 
    for h_file in file_list:
        src_hcl_h_path = os.path.join(src_h_dir, h_file)
        dest_hcl_h_path = os.path.join(dest_h_dir, h_file)
        new_content = []
        with io.open(src_hcl_h_path, encoding="utf8", errors='ignore') as read_file:
            new_content = delete_unused(read_file, h_file)
 
        with io.open(dest_hcl_h_path, 'w') as dest_file:
            for content in new_content:
                dest_file.write(content)
 
 
def handle_graph_h():
    src_h_dir = os.path.join(SRC_DIR, "ddk/graph")
    dest_h_dir = os.path.join(DEST_DIR, "ddk/graph")
    file_list = [
        "model.h",
        "operator.h",
        "tensor.h",
    ]
 
    if not os.path.exists(dest_h_dir):
            os.makedirs(dest_h_dir)
 
    for h_file in file_list:
        src_graph_h_path = os.path.join(src_h_dir, h_file)
        dest_graph_h_path = os.path.join(dest_h_dir, h_file)
        new_content = []
        with io.open(src_graph_h_path, encoding="utf8", errors='ignore') as read_file:
            new_content = replace_include_path(read_file)
 
        with io.open(dest_graph_h_path, 'w') as dest_file:
            for content in new_content:
                dest_file.write(content)
