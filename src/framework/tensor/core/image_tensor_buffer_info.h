/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TENSOR_CORE_IMAGE_TENSOR_BUFFER_INFO_H
#define TENSOR_CORE_IMAGE_TENSOR_BUFFER_INFO_H

#include <cstdint>

#include "tensor/image_format.h"

namespace hiai {
struct ImageTensorBufferInfo {
    int32_t rotation {0};
    int32_t batch {0};
    int32_t height {0};
    int32_t width {0};
    int32_t channel {0};
    ImageFormat format {ImageFormat::INVALID};
    ImageColorSpace colorSpace {ImageColorSpace::BT_601_NARROW};
};
}
#endif