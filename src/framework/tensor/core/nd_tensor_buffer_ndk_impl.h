/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_BUFFER_ND_TENSOR_BUFFER_NDK_IMPL_H
#define FRAMEWORK_BUFFER_ND_TENSOR_BUFFER_NDK_IMPL_H
#include "tensor/nd_tensor_buffer.h"
#include "tensor/tensor_api_export.h"
#include "neural_network_runtime/neural_network_core.h"

namespace hiai {

class NDTensorBufferNDKImpl : public INDTensorBuffer {
public:
    explicit NDTensorBufferNDKImpl(NN_Tensor* nnTensor_);
    NDTensorBufferNDKImpl(NN_Tensor* nnTensor_, const NDTensorDesc& desc);
    ~NDTensorBufferNDKImpl() override;

    NN_Tensor* GetNNTensor();

    void* GetData() override;

    size_t GetSize() const override;

    const NDTensorDesc& GetTensorDesc() const override
    {
        return desc_;
    }

    void SetCacheStatus(uint8_t status) override;

    uint8_t GetCacheStatus() override;

private:
    NDTensorBufferNDKImpl(const NDTensorBufferNDKImpl&) = delete;
    NDTensorBufferNDKImpl& operator=(const NDTensorBufferNDKImpl&) = delete;

private:
    NDTensorDesc desc_;
    NN_Tensor* nnTensor_ {nullptr};
};
} // namespace hiai

#endif