/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_TENSOR_AIPP_PARA_NDK_IMPL_H
#define FRAMEWORK_TENSOR_AIPP_PARA_NDK_IMPL_H
#include "tensor/aipp_para.h"
#include "tensor/image_format.h"
#include "tensor/repo/compatible/hiai_tensor_aipp_para_def.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_aipp.h"

namespace hiai {
class AIPPParaNDKImpl : public IAIPPPara {
public:
    AIPPParaNDKImpl() = default;
    ~AIPPParaNDKImpl() override;
    AIPPParaNDKImpl(const AIPPParaNDKImpl&) = delete;
    AIPPParaNDKImpl& operator=(const AIPPParaNDKImpl&) = delete;

    Status Init(uint32_t batchCount);
    Status Init(HiAI_AippParam* paraBuff);

    uint32_t GetBatchCount() override;

    Status SetInputIndex(uint32_t inputIndex) override;
    int32_t GetInputIndex() override;
    Status SetInputAippIndex(uint32_t inputAippIndex) override;
    int32_t GetInputAippIndex() override;

    Status SetCscPara(ImageFormat targetFormat, ImageColorSpace colorSpace) override;
    Status SetCscPara(CscMatrixPara cscPara) override;
    CscMatrixPara GetCscMatrixPara() override;
    CscPara GetCscPara() override;

    Status SetChannelSwapPara(ChannelSwapPara&& channelSwapPara) override;
    ChannelSwapPara GetChannelSwapPara() override;

    Status SetSingleBatchMultiCrop(bool singleBatchMutiCrop) override;
    bool GetSingleBatchMultiCrop() override;
    // end common interface

    Status SetCropPara(CropPara&& cropPara) override;
    Status SetCropPara(uint32_t batchIndex, CropPara&& cropPara) override;
    CropPara GetCropPara(uint32_t batchIndex) override;

    Status SetResizePara(ResizePara&& resizePara) override;
    Status SetResizePara(uint32_t batchIndex, ResizePara&& resizePara) override;
    ResizePara GetResizePara(uint32_t batchIndex) override;

    Status SetPaddingPara(PadPara&& paddingPara) override;
    Status SetPaddingPara(uint32_t batchIndex, PadPara&& paddingPara) override;
    PadPara GetPaddingPara(uint32_t batchIndex) override;

    Status SetDtcPara(DtcPara&& dtcPara) override;
    Status SetDtcPara(uint32_t batchIndex, DtcPara&& dtcPara) override;
    DtcPara GetDtcPara(uint32_t batchIndex) override;

    Status SetRotatePara(RotatePara&& rotatePara) override;
    Status SetRotatePara(uint32_t batchIndex, RotatePara&& rotatePara) override;
    RotatePara GetRotatePara(uint32_t batchIndex) override;

    // internel
    Status SetInputFormat(ImageFormat inputFormat) override;
    ImageFormat GetInputFormat() override;
    std::vector<int32_t> GetInputShape() override;
    Status SetInputShape(std::vector<int32_t>& shape) override;

    void* GetData() override;

    size_t GetSize() const override;

    HiAI_AippParam* GetParaBuffer();

public:
    bool GetEnableCrop(uint32_t batchIndex);

    bool GetEnableResize(uint32_t batchIndex);

    bool GetEnableCsc();

    bool GetEnablePadding(uint32_t batchIndex);

private:
    Status InitAippPara(uint32_t batchCount);

    template <typename T, typename UpdateParaFunc>
    Status SetAippFuncPara(T&& funcPara, UpdateParaFunc updateParaFunc);

    template <typename T, typename UpdateParaFunc>
    Status SetAippFuncPara(uint32_t batchIndex, T&& funcPara, UpdateParaFunc updateParaFunc);

private:
    HiAI_AippParam* paraBuff_ {nullptr};
    void* rawBuffer_ {nullptr};
    ImageFormat targetFormat_ {ImageFormat::INVALID};
    ImageColorSpace colorSpace_ {ImageColorSpace::INVALID};

    CscMatrixPara cscMatrixPara_;

    std::vector<bool> cropSwitchs_;
    std::vector<bool> resizeSwitchs_;
    bool cscSwitchs_ {false};
    std::vector<bool> paddingSwitchs_;
};

HIAI_TENSOR_API_EXPORT HiAI_AippParam* GetNNTensorAippParaFromAippPara(const std::shared_ptr<IAIPPPara>& aippPara);
} // namespace hiai
#endif