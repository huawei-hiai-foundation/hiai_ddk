/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "image_tensor_buffer_ndk_impl.h"

#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_create_itf.h"
#include "model_manager/general_model_manager/ndk/ndk_util/ndk_util.h"
#include "infra/base/securestl.h"
#include "framework/infra/log/log.h"

namespace hiai {

std::shared_ptr<IImageTensorBuffer> CreateImageTensorBufferFromNDK(ImageTensorBufferInfo bufferInfo,
    void* ndTensor, NDTensorDesc desc, ImageColorSpace colorSpace, int32_t rotation)
{
    std::shared_ptr<ImageTensorBufferNDKImpl> imageTensor =
        make_shared_nothrow<ImageTensorBufferNDKImpl>(bufferInfo, reinterpret_cast<NN_Tensor*>(ndTensor), desc);
    if (imageTensor == nullptr) {
        FMK_LOGE("HIAI_CreateImageBuffer create ImageTensorBufferNDKImpl failed");
        return nullptr;
    }
    imageTensor->SetRotation(rotation);
    imageTensor->SetColorSpace(colorSpace);
    return std::static_pointer_cast<IImageTensorBuffer>(imageTensor);
}

} // namespace hiai
