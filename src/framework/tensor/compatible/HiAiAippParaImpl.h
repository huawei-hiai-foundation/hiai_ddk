/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_TENSOR_COMPATIBLE_HIAI_AIPP_PARAM_IMPL_H
#define FRAMEWORK_TENSOR_COMPATIBLE_HIAI_AIPP_PARAM_IMPL_H

#include "tensor/aipp_para.h"
#include "compatible/HiAiAippPara.h"

namespace hiai {
class AippParaImpl {
public:
    AippParaImpl();
    ~AippParaImpl();
    AippParaImpl(const AippParaImpl& para) = delete;
    AippParaImpl& operator=(const AippParaImpl& para) = delete;

    AIStatus Init(uint32_t batchCount = 1);
    uint32_t GetBatchCount();

    AIStatus SetInputIndex(uint32_t inputIndex);
    int32_t GetInputIndex();

    AIStatus SetInputAippIndex(uint32_t inputAippIndex);
    int32_t GetInputAippIndex();

    AIStatus SetInputShape(AippInputShape inputShape);
    AippInputShape GetInputShape();

    AIStatus SetInputFormat(AiTensorImage_Format inputFormat);
    AiTensorImage_Format GetInputFormat();

    AIStatus SetCscPara(AiTensorImage_Format targetFormat, ImageType imageType = JPEG);

    AIStatus SetCscPara(AippCscPara cscPara);
    AippCscPara GetCscPara();

    AIStatus SetChannelSwapPara(AippChannelSwapPara channelSwapPara);
    AippChannelSwapPara GetChannelSwapPara();

    AIStatus SetCropPara(AippCropPara cropPara);
    AIStatus SetCropPara(uint32_t batchIndex, AippCropPara cropPara);
    AippCropPara GetCropPara(uint32_t batchIndex);

    AIStatus SetResizePara(AippResizePara resizePara);
    AIStatus SetResizePara(uint32_t batchIndex, AippResizePara resizePara);
    AippResizePara GetResizePara(uint32_t batchIndex);

    AIStatus SetPaddingPara(AippPaddingPara paddingPara);
    AIStatus SetPaddingPara(uint32_t batchIndex, AippPaddingPara paddingPara);
    AippPaddingPara GetPaddingPara(uint32_t batchIndex);

    AIStatus SetDtcPara(AippDtcPara dtcPara);
    AIStatus SetDtcPara(uint32_t batchIndex, AippDtcPara dtcPara);
    AippDtcPara GetDtcPara(uint32_t batchIndex);

    std::shared_ptr<IAIPPPara> GetAIPPPara();
    void SetAIPPPara(std::shared_ptr<IAIPPPara>& para);

private:
    std::shared_ptr<IAIPPPara> aippParaBase_;
};
}

#endif