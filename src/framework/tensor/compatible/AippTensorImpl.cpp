/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AippTensorImpl.h"
#include "framework/infra/log/log.h"

namespace hiai {
AippTensorImpl::AippTensorImpl(std::shared_ptr<AiTensor> tensor, std::vector<std::shared_ptr<AippPara>> aippParas)
    : tensor_(tensor), aippParas_(aippParas)
{
}

void* AippTensorImpl::GetBuffer() const
{
    if (tensor_ == nullptr) {
        FMK_LOGE("GetBuffer failed, tensor is null");
        return nullptr;
    }
    return tensor_->GetBuffer();
}

uint32_t AippTensorImpl::GetSize() const
{
    if (tensor_ == nullptr) {
        FMK_LOGE("GetSize failed, tensor is null");
        return 0;
    }
    return tensor_->GetSize();
}

std::shared_ptr<AiTensor> AippTensorImpl::GetAiTensor() const
{
    return tensor_;
}

std::vector<std::shared_ptr<AippPara>> AippTensorImpl::GetAippParas() const
{
    return aippParas_;
}

std::shared_ptr<AippPara> AippTensorImpl::GetAippParas(uint32_t index) const
{
    if (index >= aippParas_.size()) {
        FMK_LOGE("GetBuffer failed, index is out of the range of aippParas");
        return nullptr;
    }
    return aippParas_[index];
}

}