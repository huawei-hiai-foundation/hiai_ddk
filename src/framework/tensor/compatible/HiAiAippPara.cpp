/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compatible/HiAiAippPara.h"
#include "framework/infra/log/log_fmk_interface.h"
#include "HiAiAippParaImpl.h"
#include "infra/base/assertion.h"
#include "infra/base/securestl.h"

namespace hiai {
AippPara::AippPara()
{
    impl_ = hiai::make_shared_nothrow<AippParaImpl>();
}

AippPara::~AippPara()
{
    if (impl_ != nullptr) {
        impl_.reset();
        impl_ = nullptr;
    }
}

AIStatus AippPara::Init(uint32_t batchCount)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->Init(batchCount);
}

uint32_t AippPara::GetBatchCount()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, 0);
    return impl_->GetBatchCount();
}

AIStatus AippPara::SetInputIndex(uint32_t inputIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetInputIndex(inputIndex);
}

int32_t AippPara::GetInputIndex()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, 0);
    return impl_->GetInputIndex();
}

AIStatus AippPara::SetInputAippIndex(uint32_t inputAippIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetInputAippIndex(inputAippIndex);
}

int32_t AippPara::GetInputAippIndex()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, 0);
    return impl_->GetInputAippIndex();
}

AIStatus AippPara::SetInputShape(AippInputShape inputShape)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetInputShape(inputShape);
}

AippInputShape AippPara::GetInputShape()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippInputShape{});
    return impl_->GetInputShape();
}

AIStatus AippPara::SetInputFormat(AiTensorImage_Format inputFormat)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetInputFormat(inputFormat);
}

AiTensorImage_Format AippPara::GetInputFormat()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AiTensorImage_Format::AiTensorImage_INVALID);
    return impl_->GetInputFormat();
}

AIStatus AippPara::SetCscPara(AiTensorImage_Format targetFormat, ImageType imageType)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetCscPara(targetFormat, imageType);
}

AIStatus AippPara::SetCscPara(AippCscPara cscPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetCscPara(cscPara);
}

AippCscPara AippPara::GetCscPara()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippCscPara{});
    return impl_->GetCscPara();
}

AIStatus AippPara::SetChannelSwapPara(AippChannelSwapPara channelSwapPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetChannelSwapPara(channelSwapPara);
}

AippChannelSwapPara AippPara::GetChannelSwapPara()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippChannelSwapPara{});
    return impl_->GetChannelSwapPara();
}

AIStatus AippPara::SetCropPara(uint32_t batchIndex, AippCropPara cropPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetCropPara(batchIndex, cropPara);
}

AIStatus AippPara::SetCropPara(AippCropPara cropPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetCropPara(cropPara);
}

AippCropPara AippPara::GetCropPara(uint32_t batchIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippCropPara{});
    return impl_->GetCropPara(batchIndex);
}

AIStatus AippPara::SetResizePara(uint32_t batchIndex, AippResizePara resizePara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetResizePara(batchIndex, resizePara);
}

AIStatus AippPara::SetResizePara(AippResizePara resizePara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetResizePara(resizePara);
}

AippResizePara AippPara::GetResizePara(uint32_t batchIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippResizePara{});
    return impl_->GetResizePara(batchIndex);
}

AIStatus AippPara::SetPaddingPara(uint32_t batchIndex, AippPaddingPara paddingPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetPaddingPara(batchIndex, paddingPara);
}

AIStatus AippPara::SetPaddingPara(AippPaddingPara paddingPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetPaddingPara(paddingPara);
}

AippPaddingPara AippPara::GetPaddingPara(uint32_t batchIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippPaddingPara{});
    return impl_->GetPaddingPara(batchIndex);
}

AIStatus AippPara::SetDtcPara(uint32_t batchIndex, AippDtcPara dtcPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetDtcPara(batchIndex, dtcPara);
}

AIStatus AippPara::SetDtcPara(AippDtcPara dtcPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_FAILED);
    return impl_->SetDtcPara(dtcPara);
}

AippDtcPara AippPara::GetDtcPara(uint32_t batchIndex)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AippDtcPara{});
    return impl_->GetDtcPara(batchIndex);
}

std::shared_ptr<IAIPPPara> AippPara::GetAIPPPara()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->GetAIPPPara();
}

void AippPara::SetAIPPPara(std::shared_ptr<IAIPPPara>& para)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->SetAIPPPara(para);
}

} // namespace hiai
