/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_TENSOR_COMPATIBLE_AIPP_TENSOR_IMPL_H
#define FRAMEWORK_TENSOR_COMPATIBLE_AIPP_TENSOR_IMPL_H

#include <vector>

#include "compatible/HiAiAippPara.h"
#include "compatible/AiTensor.h"

namespace hiai {
class AippTensorImpl {
public:
    AippTensorImpl(std::shared_ptr<AiTensor> tensor, std::vector<std::shared_ptr<AippPara>> aippParas);

    virtual ~AippTensorImpl() = default;

    void* GetBuffer() const;

    uint32_t GetSize() const;

    std::shared_ptr<AiTensor> GetAiTensor() const;

    std::vector<std::shared_ptr<AippPara>> GetAippParas() const;

    std::shared_ptr<AippPara> GetAippParas(uint32_t index) const;

private:
    std::shared_ptr<AiTensor> tensor_;

    std::vector<std::shared_ptr<AippPara>> aippParas_;
};
}
#endif