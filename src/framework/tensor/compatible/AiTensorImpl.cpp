/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AiTensorImpl.h"

#include <map>

#include "framework/infra/log/log.h"
#include "tensor/image_format.h"
#include "tensor/image_tensor_buffer.h"

namespace hiai {
TensorDimensionImpl::TensorDimensionImpl()
{
}

TensorDimensionImpl::~TensorDimensionImpl()
{
}

TensorDimensionImpl::TensorDimensionImpl(uint32_t number, uint32_t channel, uint32_t height, uint32_t width)
    : n_(number), c_(channel), h_(height), w_(width)
{
}

void TensorDimensionImpl::SetNumber(const uint32_t number)
{
    n_ = number;
}

uint32_t TensorDimensionImpl::GetNumber() const
{
    return n_;
}

void TensorDimensionImpl::SetChannel(const uint32_t channel)
{
    c_ = channel;
}

uint32_t TensorDimensionImpl::GetChannel() const
{
    return c_;
}

void TensorDimensionImpl::SetHeight(const uint32_t height)
{
    h_ = height;
}

uint32_t TensorDimensionImpl::GetHeight() const
{
    return h_;
}

void TensorDimensionImpl::SetWidth(const uint32_t width)
{
    w_ = width;
}

uint32_t TensorDimensionImpl::GetWidth() const
{
    return w_;
}

bool TensorDimensionImpl::IsEqual(uint32_t number, uint32_t channel, uint32_t height, uint32_t width)
{
    return n_ == number && c_ == channel && h_ == height && w_ == width;
}

AiTensorImpl::AiTensorImpl() : tensor_(nullptr)
{
}
AiTensorImpl::~AiTensorImpl()
{
    if (tensor_ != nullptr) {
        tensor_.reset();
        tensor_ = nullptr;
    }
}

namespace {
bool IsDimensionValid(const TensorDimension* dim)
{
    if (dim == nullptr ||
        dim->GetNumber() > INT32_MAX || dim->GetChannel() > INT32_MAX ||
        dim->GetHeight() > INT32_MAX || dim->GetWidth() > INT32_MAX) {
        FMK_LOGE("invalid dim.");
        return false;
    }
    return true;
}
}

AIStatus AiTensorImpl::Init(const TensorDimension* dim)
{
    if (dim == nullptr) {
        return AI_INVALID_PARA;
    }
    HIAI_DataType dataType = static_cast<HIAI_DataType>(DataType::FLOAT32);
    return Init(dim, dataType);
}

AIStatus AiTensorImpl::Init(const TensorDimension* dim, HIAI_DataType pdataType)
{
    if (!IsDimensionValid(dim)) {
        return AI_INVALID_PARA;
    }
    if (pdataType < HIAI_DATATYPE_UINT8 || pdataType > HIAI_DATATYPE_DOUBLE) {
        FMK_LOGE("invalid pdataType.");
        return AI_INVALID_PARA;
    }
    desc_.dims = {static_cast<int32_t>(dim->GetNumber()), static_cast<int32_t>(dim->GetChannel()),
        static_cast<int32_t>(dim->GetHeight()), static_cast<int32_t>(dim->GetWidth())};
    desc_.dataType = static_cast<DataType>(pdataType);
    desc_.format = Format::NCHW;

    tensor_ = CreateNDTensorBuffer(desc_);
    if (tensor_ == nullptr) {
        return AI_FAILED;
    }
    return AI_SUCCESS;
}

static size_t GetTotalDimNum(const NDTensorDesc tensorDesc)
{
    size_t totalDimSize = 1;
    for (size_t i = 0; i < tensorDesc.dims.size(); i++) {
        int32_t dimValue = tensorDesc.dims[i];
        if (dimValue <= 0) {
            FMK_LOGE("invalid dim value, dimValue = %d", dimValue);
            return 0;
        }

        if (totalDimSize > static_cast<size_t>(SIZE_MAX / dimValue)) {
            FMK_LOGE("too large totalDimSize, current totalDimSize = %d, dimValue = %d", totalDimSize, dimValue);
            return 0;
        }
        totalDimSize *= static_cast<size_t>(static_cast<uint32_t>(dimValue));
    }

    return totalDimSize;
}

static size_t GetElementSize(DataType type)
{
    std::map<DataType, int32_t> dataTypeElementSize = {
        {DataType::UINT8, sizeof(uint8_t)},
        {DataType::FLOAT32, sizeof(float)},
        {DataType::FLOAT16, sizeof(float) / 2},
        {DataType::INT32, sizeof(int32_t)},
        {DataType::INT8, sizeof(int8_t)},
        {DataType::INT16, sizeof(int16_t)},
        {DataType::BOOL, sizeof(char)},
        {DataType::INT64, sizeof(int64_t)},
        {DataType::UINT32, sizeof(uint32_t)},
        {DataType::DOUBLE, sizeof(double)},
    };

    auto it = dataTypeElementSize.find(type);
    if (it == dataTypeElementSize.end()) {
        FMK_LOGE("invalid DataType, type = %d", static_cast<int32_t>(type));
        return 0;
    }

    return it->second;
}

AIStatus AiTensorImpl::Init(const void* data, const TensorDimension* dim, HIAI_DataType pdataType)
{
    if (data == nullptr || !IsDimensionValid(dim)) {
        return AI_INVALID_PARA;
    }
    if (pdataType < HIAI_DATATYPE_UINT8 || pdataType > HIAI_DATATYPE_DOUBLE) {
        FMK_LOGE("invalid data type, pdataType = %d.", pdataType);
        return AI_INVALID_PARA;
    }
    desc_.dims = {static_cast<int32_t>(dim->GetNumber()), static_cast<int32_t>(dim->GetChannel()),
        static_cast<int32_t>(dim->GetHeight()), static_cast<int32_t>(dim->GetWidth())};
    desc_.dataType = static_cast<DataType>(pdataType);
    desc_.format = Format::NCHW;

    size_t totalSize = 0;

    size_t totalDimNum = GetTotalDimNum(desc_);
    if (totalDimNum == 0) {
        FMK_LOGE("GetTotalDimNum error.");
        return AI_INVALID_PARA;
    }

    size_t elementSize = GetElementSize(desc_.dataType);
    if (elementSize == 0) {
        FMK_LOGE("GetElementSize error.");
        return AI_INVALID_PARA;
    }

    if (totalDimNum > SIZE_MAX / elementSize) {
        FMK_LOGE("too large totalSize, current totalSize = %d, elementSize = %d", totalDimNum, elementSize);
        return AI_INVALID_PARA;
    }

    totalSize = totalDimNum * elementSize;
    tensor_ = CreateNDTensorBufferNoCopy(desc_, data, totalSize);
    if (tensor_ == nullptr) {
        FMK_LOGE("tensor_ is nullptr.");
        return AI_FAILED;
    }

    return AI_SUCCESS;
}

AIStatus AiTensorImpl::Init(const NativeHandle& handle, const TensorDimension* dim, HIAI_DataType pdataType)
{
    if (!IsDimensionValid(dim)) {
        return AI_INVALID_PARA;
    }
    if (pdataType < HIAI_DATATYPE_UINT8 || pdataType > HIAI_DATATYPE_DOUBLE) {
        FMK_LOGE("invalid data type.");
        return AI_INVALID_PARA;
    }

    desc_.dims = {static_cast<int32_t>(dim->GetNumber()), static_cast<int32_t>(dim->GetChannel()),
        static_cast<int32_t>(dim->GetHeight()), static_cast<int32_t>(dim->GetWidth())};
    desc_.dataType = static_cast<DataType>(pdataType);
    desc_.format = Format::NCHW;

    tensor_ = CreateNDTensorBuffer(desc_, handle);
    if (tensor_ == nullptr) {
        FMK_LOGE("tensor_ is nullptr");
        return AI_FAILED;
    }
    return AI_SUCCESS;
}

AIStatus AiTensorImpl::Init(uint32_t number, uint32_t height, uint32_t width, AiTensorImage_Format format)
{
    if (number > INT32_MAX || height > INT32_MAX || width > INT32_MAX) {
        return AI_FAILED;
    }
    std::shared_ptr<IImageTensorBuffer> tensor =
        CreateImageTensorBuffer(static_cast<int32_t>(number), static_cast<int32_t>(height), static_cast<int32_t>(width),
        static_cast<ImageFormat>(format), ImageColorSpace::BT_601_NARROW, 0);
    if (tensor == nullptr) {
        return AI_FAILED;
    }
    tensor_ = tensor;
    desc_ = tensor->GetTensorDesc();
    return AI_SUCCESS;
}

void* AiTensorImpl::GetBuffer() const
{
    if (tensor_ == nullptr) {
        FMK_LOGE("tensor is not inited");
        return nullptr;
    }
    return tensor_->GetData();
}

uint32_t AiTensorImpl::GetSize() const
{
    if (tensor_ == nullptr) {
        FMK_LOGE("tensor is not inited");
        return 0;
    }
    return tensor_->GetSize();
}

AIStatus AiTensorImpl::SetTensorDimension(const TensorDimension* dim)
{
    if (!IsDimensionValid(dim)) {
        return AI_FAILED;
    }

    desc_.dims = {static_cast<int32_t>(dim->GetNumber()), static_cast<int32_t>(dim->GetChannel()),
        static_cast<int32_t>(dim->GetHeight()), static_cast<int32_t>(dim->GetWidth())};
    return AI_SUCCESS;
}

TensorDimension AiTensorImpl::GetTensorDimension() const
{
    if (desc_.dims.size() == 4) {
        return TensorDimension(desc_.dims[0], desc_.dims[1], desc_.dims[2], desc_.dims[3]);
    }

    return TensorDimension();
}
void* AiTensorImpl::GetTensorBuffer() const
{
    return GetBuffer();
}

}