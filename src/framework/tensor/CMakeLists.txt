hi_add_subdirectory(core)
hi_add_subdirectory(repo)

hi_cc_library_static(
  NAME
    ai::fmk::tensor_static
  SRCS
    core/image_tensor_buffer_impl.cpp
    core/image_tensor_buffer.cpp
    core/image_config_tensor_util.cpp
  CDEFS
    HIAI_TENSOR_API_VISIABLE
  COPTS
    -frtti
)

hi_cc_system("HI_HARMONY_NDK"
  NAME
    ai::fmk::tensor_static
  SRCS
    core/image_tensor_buffer_ndk.cpp
  DEPS
    huawei::c_sec
  COPTS
    -frtti
  INCS
    foundation/ai/neural_network_runtime/interfaces/kits/c
    foundation/ai/neural_network_runtime/frameworks/native
    foundation/ai/neural_network_runtime
)

hi_cc_function(AI_SUPPORT_LEGACY_APP_COMPATIBLE
  NAME
    ai::fmk::tensor_static
  SRCS
    compatible/AiTensor.cpp
    compatible/AiTensorImpl.cpp
)

hi_cc_function(AI_SUPPORT_AIPP_COMPATIBLE
  NAME
    ai::fmk::tensor_static
  SRCS
    compatible/AippTensor.cpp
    compatible/HiAiAippPara.cpp
    compatible/AippTensorImpl.cpp
    compatible/HiAiAippParaImpl.cpp
)
