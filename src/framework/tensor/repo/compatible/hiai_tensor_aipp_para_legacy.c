/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "hiai_tensor_aipp_para_legacy.h"

#include "framework/c/hiai_tensor_aipp_para.h"
#include "hiai_tensor_aipp_para_def.h"
#include "framework/infra/log/log.h"

#include "util/hiai_foundation_dl_helper.h"

#define HIAI_EXPECT_NOT_NULL_R(ptr, ret) \
    if ((ptr) == NULL) { \
        return ret; \
    }

void* HIAI_TensorAippPara_CreateLegacy(uint32_t batchNum)
{
    static const char* createFuncName = "HIAI_TensorAipp_create";
    void* (*createFunc)(uint32_t) = (void* (*)(uint32_t))HIAI_Foundation_GetSymbol(createFuncName);

    HIAI_EXPECT_NOT_NULL_R(createFunc, NULL);

    return createFunc(batchNum);
}

void* HIAI_TensorAippPara_GetRawBufferLegacy(const void* handle)
{
    static const char* getRawBufferFuncName = "HIAI_TensorAipp_getRawBuffer";
    void* (*getRawBufferFunc)(void*) = (void* (*)(void*))HIAI_Foundation_GetSymbol(getRawBufferFuncName);

    HIAI_EXPECT_NOT_NULL_R(getRawBufferFunc, NULL);

    return getRawBufferFunc((HIAI_MR_TensorAippPara*)handle);
}

int32_t HIAI_TensorAippPara_GetRawBufferSizeLegacy(const void* handle)
{
    int32_t size = 0;
    static const char* getRawBufferSizeFuncName = "HIAI_TensorAipp_getRawBufferSize";
    int32_t (*getRawBufferSizeFunc)(void*) = (int32_t(*)(void*))HIAI_Foundation_GetSymbol(getRawBufferSizeFuncName);
    if (getRawBufferSizeFunc != NULL) {
        size = getRawBufferSizeFunc((HIAI_MR_TensorAippPara*)handle);
    }

    if (size <= 0) {
        void* bufferPara = HIAI_TensorAippPara_GetRawBufferLegacy(handle);
        HIAI_EXPECT_NOT_NULL_R(bufferPara, 0);

        HIAI_MR_TensorAippCommPara* commPara = (HIAI_MR_TensorAippCommPara*)(bufferPara);
        if (commPara == NULL) {
            FMK_LOGE("revert to HIAI_TensorAippCommPara failed.");
            return 0;
        }
        size = (int32_t)(sizeof(HIAI_MR_TensorAippCommPara)) +
            (int32_t)(sizeof(HIAI_MR_TensorAippBatchPara) * commPara->batchNum);
    }
    return size;
}

void* HIAI_TensorAippPara_GetHandleLegacy(const void* handle)
{
    return (void*)handle;
}

int32_t HIAI_TensorAippPara_GetInputIndexLegacy(const void* handle)
{
    HIAI_EXPECT_NOT_NULL_R(handle, -1);

    static const char* getInputIndexFuncName = "HIAI_TensorAipp_getInputIndex";
    int32_t (*getInutIndexFunc)(void*) = (int32_t(*)(void*))HIAI_Foundation_GetSymbol(getInputIndexFuncName);
    HIAI_EXPECT_NOT_NULL_R(getInutIndexFunc, -1);

    return getInutIndexFunc((HIAI_MR_TensorAippPara*)handle);
}

void HIAI_TensorAippPara_SetInputIndexLegacy(void* handle, uint32_t inputIndex)
{
    if (handle == NULL) {
        return;
    }

    static const char* setInputIndexFuncName = "HIAI_TensorAipp_setInputIndex";
    void (*setInutIndexFunc)(void*, uint32_t) =
        (void (*)(void*, uint32_t))HIAI_Foundation_GetSymbol(setInputIndexFuncName);
    if (setInutIndexFunc == NULL) {
        return;
    }

    setInutIndexFunc(handle, inputIndex);
}

int32_t HIAI_TensorAippPara_GetInputAippIndexLegacy(const void* handle)
{
    HIAI_EXPECT_NOT_NULL_R(handle, -1);

    static const char* getInputIndexFuncName = "HIAI_TensorAipp_getInputAippIndex";
    int32_t (*getInutIndexFunc)(void*) = (int32_t(*)(void*))HIAI_Foundation_GetSymbol(getInputIndexFuncName);
    HIAI_EXPECT_NOT_NULL_R(getInutIndexFunc, -1);

    return getInutIndexFunc((HIAI_MR_TensorAippPara*)handle);
}

void HIAI_TensorAippPara_SetInputAippIndexLegacy(void* handle, uint32_t inputAippIndex)
{
    if (handle == NULL) {
        return;
    }

    static const char* setInputIndexFuncName = "HIAI_TensorAipp_setInputAippIndex";
    void (*setInutIndexFunc)(void*, uint32_t) =
        (void (*)(void*, uint32_t))HIAI_Foundation_GetSymbol(setInputIndexFuncName);
    if (setInutIndexFunc == NULL) {
        return;
    }

    setInutIndexFunc(handle, inputAippIndex);
}

void HIAI_TensorAippPara_DestroyLegacy(void* handle)
{
    if (handle == NULL) {
        return;
    }

    static const char* destroyFuncName = "HIAI_TensorAipp_destroy";
    void (*destroyFunc)(void*) = (void (*)(void*))HIAI_Foundation_GetSymbol(destroyFuncName);
    if (destroyFunc == NULL) {
        return;
    }

    destroyFunc(handle);
}

HIAI_Status HIAI_TensorAippPara_GetCscSwitchLegacy(void* handle, bool* cscSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cscSwitch, HIAI_FAILURE);

    static const char* getCscSwitchFuncName = "HIAI_TensorAipp_GetCscSwitch";
    bool (*getCscSwitchFunc)(void*) =
        (bool (*)(void*))HIAI_Foundation_GetSymbol(getCscSwitchFuncName);

    HIAI_EXPECT_NOT_NULL_R(getCscSwitchFunc, HIAI_INVALID_API);

    *cscSwitch = getCscSwitchFunc(handle);

    return HIAI_SUCCESS;
}

HIAI_Status HIAI_TensorAippPara_GetCropSwitchLegacy(void* handle, uint32_t batchIndex, bool* cropSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cropSwitch, HIAI_FAILURE);

    static const char* getCropSwitchFuncName = "HIAI_TensorAipp_GetCropSwitch";
    bool (*getCropSwitchFunc)(void*, uint32_t) =
        (bool (*)(void*, uint32_t))HIAI_Foundation_GetSymbol(getCropSwitchFuncName);

    HIAI_EXPECT_NOT_NULL_R(getCropSwitchFunc, HIAI_INVALID_API);

    *cropSwitch = getCropSwitchFunc(handle, batchIndex);
    return HIAI_SUCCESS;
}

HIAI_Status HIAI_TensorAippPara_GetResizeSwitchLegacy(void* handle, uint32_t batchIndex, bool* resizeSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(resizeSwitch, HIAI_FAILURE);

    static const char* getResizeSwitchFuncName = "HIAI_TensorAipp_GetResizeSwitch";
    bool (*getResizeSwitchFunc)(void*, uint32_t) =
        (bool (*)(void*, uint32_t))HIAI_Foundation_GetSymbol(getResizeSwitchFuncName);

    HIAI_EXPECT_NOT_NULL_R(getResizeSwitchFunc, HIAI_INVALID_API);

    *resizeSwitch = getResizeSwitchFunc(handle, batchIndex);
    return HIAI_SUCCESS;
}

HIAI_Status HIAI_TensorAippPara_GetPadSwitchLegacy(void* handle, uint32_t batchIndex, bool* padSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(padSwitch, HIAI_FAILURE);

    static const char* getPadSwitchFuncName = "HIAI_TensorAipp_GetPadSwitch";
    bool (*getPadSwitchFunc)(void*, uint32_t) =
        (bool (*)(void*, uint32_t))HIAI_Foundation_GetSymbol(getPadSwitchFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadSwitchFunc, HIAI_INVALID_API);

    *padSwitch = getPadSwitchFunc(handle, batchIndex);
    return HIAI_SUCCESS;
}

HIAI_Status HIAI_TensorAippPara_GetBatchCountLegacy(void* handle, uint32_t* batchCount)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(batchCount, HIAI_FAILURE);

    static const char* getBatchCountFuncName = "HIAI_TensorAipp_GetBatchCount";
    int32_t (*getBatchCountFunc)(void*) =
        (int32_t (*)(void*))HIAI_Foundation_GetSymbol(getBatchCountFuncName);

    HIAI_EXPECT_NOT_NULL_R(getBatchCountFunc, HIAI_INVALID_API);

    int32_t ret = getBatchCountFunc(handle);
    if (ret >= 0) {
        *batchCount = (uint32_t)ret;
        return HIAI_SUCCESS;
    }
    return HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetInputFormatLegacy(void* handle, HIAI_ImageFormat inputFormat)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setInputFormatFuncName = "HIAI_TensorAipp_SetInputFormat";
    int32_t (*setInputFormatFunc)(void*, HIAI_ImageFormat) =
        (int32_t (*)(void*, HIAI_ImageFormat))HIAI_Foundation_GetSymbol(setInputFormatFuncName);

    HIAI_EXPECT_NOT_NULL_R(setInputFormatFunc, HIAI_INVALID_API);

    int32_t ret = setInputFormatFunc(handle, inputFormat);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetInputFormatLegacy(void* handle, HIAI_ImageFormat* inputFormat)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(inputFormat, HIAI_FAILURE);

    static const char* getInputFormatFuncName = "HIAI_TensorAipp_GetInputFormat";
    HIAI_ImageFormat (*getInputFormatFunc)(void*) =
        (HIAI_ImageFormat (*)(void*))HIAI_Foundation_GetSymbol(getInputFormatFuncName);

    HIAI_EXPECT_NOT_NULL_R(getInputFormatFunc, HIAI_INVALID_API);

    *inputFormat = getInputFormatFunc(handle);
    return (*inputFormat != HIAI_IMAGE_FORMAT_INVALID) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetInputShapeLegacy(void* handle, uint32_t srcImageW, uint32_t srcImageH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setInputShapeFuncName = "HIAI_TensorAipp_SetInputShape";
    int32_t (*setInputShapeFunc)(void*, uint32_t, uint32_t) =
        (int32_t (*)(void*, uint32_t, uint32_t))HIAI_Foundation_GetSymbol(setInputShapeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setInputShapeFunc, HIAI_INVALID_API);

    int32_t ret = setInputShapeFunc(handle, srcImageW, srcImageH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetInputShapeLegacy(void* handle, uint32_t* srcImageW, uint32_t* srcImageH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(srcImageW, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(srcImageH, HIAI_FAILURE);

    static const char* getInputShapeFuncName = "HIAI_TensorAipp_GetInputShape";
    int32_t (*getInputShapeFunc)(void*, uint32_t*, uint32_t*) =
        (int32_t (*)(void*, uint32_t*, uint32_t*))HIAI_Foundation_GetSymbol(getInputShapeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getInputShapeFunc, HIAI_INVALID_API);

    int32_t ret = getInputShapeFunc(handle, srcImageW, srcImageH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetCscParaLegacy(void* handle, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setCscParaFuncName = "HIAI_TensorAipp_SetCscPara";
    int32_t (*setCscParaFunc)(void*, HIAI_ImageFormat, HIAI_ImageFormat, HIAI_ImageColorSpace) =
        (int32_t (*)(void*, HIAI_ImageFormat, HIAI_ImageFormat, HIAI_ImageColorSpace))
        HIAI_Foundation_GetSymbol(setCscParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(setCscParaFunc, HIAI_INVALID_API);

    int32_t ret = setCscParaFunc(handle, inputFormat, targetFormat, colorSpace);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetCscParaLegacy(void* handle, HIAI_ImageFormat* inputFormat,
    HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(inputFormat, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(targetFormat, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(colorSpace, HIAI_FAILURE);

    static const char* getCscParaFuncName = "HIAI_TensorAipp_GetCscPara";
    int32_t (*getCscParaFunc)(void*, HIAI_ImageFormat*, HIAI_ImageFormat*, HIAI_ImageColorSpace*) =
        (int32_t (*)(void*, HIAI_ImageFormat*, HIAI_ImageFormat*, HIAI_ImageColorSpace*))
        HIAI_Foundation_GetSymbol(getCscParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(getCscParaFunc, HIAI_INVALID_API);

    int32_t ret =  getCscParaFunc(handle, inputFormat, targetFormat, colorSpace);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetChannelSwapParaLegacy(void* handle, bool rbuvSwapSwitch, bool axSwapSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setChannelSwapParaFuncName = "HIAI_TensorAipp_SetChannelSwapPara";
    int32_t (*setChannelSwapParaFunc)(void*, bool, bool) =
        (int32_t (*)(void*, bool, bool))HIAI_Foundation_GetSymbol(setChannelSwapParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(setChannelSwapParaFunc, HIAI_INVALID_API);

    int32_t ret = setChannelSwapParaFunc(handle, rbuvSwapSwitch, axSwapSwitch);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetChannelSwapParaLegacy(void* handle, bool* rbuvSwapSwitch, bool* axSwapSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(rbuvSwapSwitch, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(axSwapSwitch, HIAI_FAILURE);

    static const char* getChannelSwapParaFuncName = "HIAI_TensorAipp_GetChannelSwapPara";
    int32_t (*getChannelSwapParaFunc)(void*, bool*, bool*) =
        (int32_t (*)(void*, bool*, bool*))HIAI_Foundation_GetSymbol(getChannelSwapParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(getChannelSwapParaFunc, HIAI_INVALID_API);

    int32_t ret = getChannelSwapParaFunc(handle, rbuvSwapSwitch, axSwapSwitch);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetSingleBatchMultiCropLegacy(void* handle, bool singleBatchMutiCrop)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setSingleBatchMultiCropFuncName = "HIAI_TensorAipp_SetSingleBatchMultiCrop";
    int32_t (*setSingleBatchMultiCropFunc)(void*, bool) =
        (int32_t (*)(void*, bool))HIAI_Foundation_GetSymbol(setSingleBatchMultiCropFuncName);
    if (setSingleBatchMultiCropFunc == NULL) {
        FMK_LOGE("sym %s not found.", setSingleBatchMultiCropFuncName);
        return HIAI_FAILURE;
    }

    int32_t ret = setSingleBatchMultiCropFunc(handle, singleBatchMutiCrop);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

bool HIAI_TensorAippPara_GetSingleBatchMultiCropLegacy(void* handle)
{
    HIAI_EXPECT_NOT_NULL_R(handle, false);

    static const char* getSingleBatchMultiCropFuncName = "HIAI_TensorAipp_GetSingleBatchMultiCrop";
    bool (*getSingleBatchMultiCropFunc)(void*) =
        (bool (*)(void*))HIAI_Foundation_GetSymbol(getSingleBatchMultiCropFuncName);
    if (getSingleBatchMultiCropFunc == NULL) {
        FMK_LOGE("sym %s not found.", getSingleBatchMultiCropFuncName);
        return false;
    }

    return getSingleBatchMultiCropFunc(handle);
}

HIAI_Status HIAI_TensorAippPara_SetCropPosLegacy(void* handle,
    uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setCropPosFuncName = "HIAI_TensorAipp_SetCropPos";
    int32_t (*setCropPosFun)(void*, unsigned int, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setCropPosFuncName);

    HIAI_EXPECT_NOT_NULL_R(setCropPosFun, HIAI_INVALID_API);

    int32_t ret = setCropPosFun(handle, batchIndex, cropStartPosW, cropStartPosH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetCropSizeLegacy(
    void* handle, uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setCropSizeFuncName = "HIAI_TensorAipp_SetCropSize";
    int32_t (*setCropSizeFun)(void*, unsigned int, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setCropSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setCropSizeFun, HIAI_INVALID_API);

    int32_t ret = setCropSizeFun(handle, batchIndex, cropSizeW, cropSizeH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetCropPosLegacy(void* handle,
    uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cropStartPosW, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cropStartPosH, HIAI_FAILURE);

    static const char* getCropPosFuncName = "HIAI_TensorAipp_GetCropPos";
    int32_t (*getCropPosFun)(void*, unsigned int, unsigned int*, unsigned int*) =
        (int32_t (*)(void*, unsigned int, unsigned int*, unsigned int*))HIAI_Foundation_GetSymbol(getCropPosFuncName);

    HIAI_EXPECT_NOT_NULL_R(getCropPosFun, HIAI_INVALID_API);

    int32_t ret = getCropPosFun(handle, batchIndex, cropStartPosW, cropStartPosH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetCropSizeLegacy(void* handle,
    uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cropSizeW, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(cropSizeH, HIAI_FAILURE);

    static const char* getCropSizeFuncName = "HIAI_TensorAipp_GetCropSize";
    int32_t (*getCropSizeFun)(void*, unsigned int, unsigned int*, unsigned int*) =
        (int32_t (*)(void*, unsigned int, unsigned int*, unsigned int*))HIAI_Foundation_GetSymbol(getCropSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getCropSizeFun, HIAI_INVALID_API);

    int32_t ret = getCropSizeFun(handle, batchIndex, cropSizeW, cropSizeH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetResizeParaLegacy(void* handle,
    uint32_t batchIndex, uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setResizeParaFuncName = "HIAI_TensorAipp_SetResizePara";
    int32_t (*setResizeParaFun)(void*, unsigned int, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setResizeParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(setResizeParaFun, HIAI_INVALID_API);

    int32_t ret = setResizeParaFun(handle, batchIndex, resizeOutputSizeW, resizeOutputSizeH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetResizeParaLegacy(void* handle,
    uint32_t batchIndex, uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(resizeOutputSizeW, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(resizeOutputSizeH, HIAI_FAILURE);

    static const char* getResizeParaName = "HIAI_TensorAipp_GetResizePara";
    int32_t (*getResizePara)(void*, unsigned int, unsigned int*, unsigned int*) =
        (int32_t (*)(void*, unsigned int, unsigned int*, unsigned int*))HIAI_Foundation_GetSymbol(getResizeParaName);

    HIAI_EXPECT_NOT_NULL_R(getResizePara, HIAI_INVALID_API);

    int32_t ret = getResizePara(handle, batchIndex, resizeOutputSizeW, resizeOutputSizeH);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetPadTopSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeTop)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPadTopSizeFuncName = "HIAI_TensorAipp_SetPadTopSize";
    int32_t (*setPadTopSizeFunc)(void*, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setPadTopSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPadTopSizeFunc, HIAI_INVALID_API);

    int32_t ret = setPadTopSizeFunc(handle, batchIndex, paddingSizeTop);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetPadBottomSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeBottom)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPadBottomSizeFuncName = "HIAI_TensorAipp_SetPadBottomSize";
    int32_t (*setPadBottomSizeFunc)(void*, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setPadBottomSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPadBottomSizeFunc, HIAI_INVALID_API);

    int32_t ret = setPadBottomSizeFunc(handle, batchIndex, paddingSizeBottom);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetPadLeftSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeLeft)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPadLeftSizeFuncName = "HIAI_TensorAipp_SetPadLeftSize";
    int32_t (*setPadLeftSizeFunc)(void*, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setPadLeftSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPadLeftSizeFunc, HIAI_INVALID_API);

    int32_t ret = setPadLeftSizeFunc(handle, batchIndex, paddingSizeLeft);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetPadRightSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeRight)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPadRightSizeFuncName = "HIAI_TensorAipp_SetPadRightSize";
    int32_t (*setPadRightSizeFunc)(void*, unsigned int, unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int))HIAI_Foundation_GetSymbol(setPadRightSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPadRightSizeFunc, HIAI_INVALID_API);

    int32_t ret = setPadRightSizeFunc(handle, batchIndex, paddingSizeRight);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetPadChannelValueLegacy(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPadChannelValueFuncName = "HIAI_TensorAipp_SetPadChannelValue";
    int32_t (*setPadChannelValueFunc)(void*, unsigned int,  unsigned int[], unsigned int) =
        (int32_t (*)(void*, unsigned int,  unsigned int[], unsigned int))
        HIAI_Foundation_GetSymbol(setPadChannelValueFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPadChannelValueFunc, HIAI_INVALID_API);

    int32_t ret = setPadChannelValueFunc(handle, batchIndex, chnValue, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetPadTopSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeTop)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(paddingSizeTop, HIAI_FAILURE);

    static const char* getPadTopSizeFuncName = "HIAI_TensorAipp_GetPadTopSize";
    int32_t (*getPadTopSizeFunc)(void*, unsigned int) =
        (int32_t (*)(void*, unsigned int))HIAI_Foundation_GetSymbol(getPadTopSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadTopSizeFunc, HIAI_INVALID_API);

    int32_t ret = getPadTopSizeFunc(handle, batchIndex);
    if (ret >= 0) {
        *paddingSizeTop = (uint32_t)(ret);
        return HIAI_SUCCESS;
    }
    return HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetPadBottomSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeBottom)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(paddingSizeBottom, HIAI_FAILURE);

    static const char* getPadBottomSizeFuncName = "HIAI_TensorAipp_GetPadBottomSize";
    int32_t (*getPadBottomSizeFunc)(void*, unsigned int) =
        (int32_t (*)(void*, unsigned int))HIAI_Foundation_GetSymbol(getPadBottomSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadBottomSizeFunc, HIAI_INVALID_API);

    int32_t ret = getPadBottomSizeFunc(handle, batchIndex);
    if (ret >= 0) {
        *paddingSizeBottom = (uint32_t)(ret);
        return HIAI_SUCCESS;
    }
    return HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetPadLeftSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeLeft)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(paddingSizeLeft, HIAI_FAILURE);

    static const char* getPadLeftSizeFuncName = "HIAI_TensorAipp_GetPadLeftSize";
    int32_t (*getPadLeftSizeFunc)(void*, unsigned int) =
        (int32_t (*)(void*, unsigned int))HIAI_Foundation_GetSymbol(getPadLeftSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadLeftSizeFunc, HIAI_INVALID_API);

    int32_t ret = getPadLeftSizeFunc(handle, batchIndex);
    if (ret >= 0) {
        *paddingSizeLeft = (uint32_t)(ret);
        return HIAI_SUCCESS;
    }
    return HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetPadRightSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeRight)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);
    HIAI_EXPECT_NOT_NULL_R(paddingSizeRight, HIAI_FAILURE);

    static const char* getPadRightSizeFuncName = "HIAI_TensorAipp_GetPadRightSize";
    int32_t (*getPadRightSizeFunc)(void*, unsigned int) =
        (int32_t (*)(void*, unsigned int))HIAI_Foundation_GetSymbol(getPadRightSizeFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadRightSizeFunc, HIAI_INVALID_API);

    int32_t ret = getPadRightSizeFunc(handle, batchIndex);
    if (ret >= 0) {
        *paddingSizeRight = (uint32_t)(ret);
        return HIAI_SUCCESS;
    }
    return HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetPadChannelValueLegacy(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* getPadChannelValueFuncName = "HIAI_TensorAipp_GetPadChannelValue";
    int32_t (*getPadChannelValueFunc)(void*, unsigned int, unsigned int[], unsigned int) =
        (int32_t (*)(void*, unsigned int, unsigned int[], unsigned int))
        HIAI_Foundation_GetSymbol(getPadChannelValueFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPadChannelValueFunc, HIAI_INVALID_API);

    int32_t ret = getPadChannelValueFunc(handle, batchIndex, chnValue, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetDtcPixelMeanParaLegacy(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPixelMeanParaFuncName = "HIAI_TensorAipp_SetDtcPixelMeanPara";
    int32_t (*setPixelMeanParaFun)(void*, unsigned int, int[], unsigned int) =
        (int32_t (*)(void*, unsigned int, int[], unsigned int))
        HIAI_Foundation_GetSymbol(setPixelMeanParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPixelMeanParaFun, HIAI_INVALID_API);

    int32_t ret = setPixelMeanParaFun(handle, batchIndex, pixelMeanPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetDtcPixelMinParaLegacy(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPixelMinParaFuncName = "HIAI_TensorAipp_SetDtcPixelMinPara";
    int32_t (*setPixelMinParaFun)(void*, unsigned int, float[], unsigned int) =
        (int32_t (*)(void*, unsigned int, float[], unsigned int))
        HIAI_Foundation_GetSymbol(setPixelMinParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPixelMinParaFun, HIAI_INVALID_API);

    int32_t ret = setPixelMinParaFun(handle, batchIndex, pixelMinPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetDtcPixelVarReciParaLegacy(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setPixelVarReciFuncName = "HIAI_TensorAipp_SetDtcPixelVarReciPara";
    int32_t (*setPixelVarReciFunc)(void*, unsigned int, float[], unsigned int) =
        (int32_t (*)(void*, unsigned int, float[], unsigned int))HIAI_Foundation_GetSymbol(setPixelVarReciFuncName);

    HIAI_EXPECT_NOT_NULL_R(setPixelVarReciFunc, HIAI_INVALID_API);

    int32_t ret = setPixelVarReciFunc(handle, batchIndex, pixelVarReciPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetDtcPixelMeanParaLegacy(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* getPixelMeanParaFuncName = "HIAI_TensorAipp_GetDtcPixelMeanPara";
    int32_t (*getPixelMeanParaFun)(void*, unsigned int, int[], unsigned int) =
        (int32_t (*)(void*, unsigned int, int[], unsigned int))
        HIAI_Foundation_GetSymbol(getPixelMeanParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPixelMeanParaFun, HIAI_INVALID_API);

    int32_t ret = getPixelMeanParaFun(handle, batchIndex, pixelMeanPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetDtcPixelMinParaLegacy(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* getPixelMinParaFuncName = "HIAI_TensorAipp_GetDtcPixelMinPara";
    int32_t (*getPixelMinParaFun)(void*, unsigned int, float[], unsigned int) =
        (int32_t (*)(void*, unsigned int, float[], unsigned int))
        HIAI_Foundation_GetSymbol(getPixelMinParaFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPixelMinParaFun, HIAI_INVALID_API);

    int32_t ret = getPixelMinParaFun(handle, batchIndex, pixelMinPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetDtcPixelVarReciParaLegacy(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* getPixelVarReciFuncName = "HIAI_TensorAipp_GetDtcPixelVarReciPara";
    int32_t (*getPixelVarReciFunc)(void*, unsigned int, float[], unsigned int) =
        (int32_t (*)(void*, unsigned int, float[], unsigned int))
        HIAI_Foundation_GetSymbol(getPixelVarReciFuncName);

    HIAI_EXPECT_NOT_NULL_R(getPixelVarReciFunc, HIAI_INVALID_API);

    int32_t ret = getPixelVarReciFunc(handle, batchIndex, pixelVarReciPara, chnNum);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_SetRotateAngleLegacy(void* handle, uint32_t batchIndex, float rotateAngle)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* setRotateAngleFuncName = "HIAI_TensorAipp_SetRotateAngle";
    int32_t (*setRotateAngleFunc)(void*, unsigned int, float) =
        (int32_t (*)(void*, unsigned int, float))HIAI_Foundation_GetSymbol(setRotateAngleFuncName);

    HIAI_EXPECT_NOT_NULL_R(setRotateAngleFunc, HIAI_INVALID_API);

    int32_t ret = setRotateAngleFunc(handle, batchIndex, rotateAngle);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}

HIAI_Status HIAI_TensorAippPara_GetRotateAngleLegacy(void* handle, uint32_t batchIndex, float* rotateAngle)
{
    HIAI_EXPECT_NOT_NULL_R(handle, HIAI_FAILURE);

    static const char* getRotateAngleFuncName = "HIAI_TensorAipp_GetRotateAngle";
    int32_t (*getRotateAngleFunc)(void*, unsigned int, float*) =
        (int32_t (*)(void*, unsigned int, float*))HIAI_Foundation_GetSymbol(getRotateAngleFuncName);

    HIAI_EXPECT_NOT_NULL_R(getRotateAngleFunc, HIAI_INVALID_API);

    int32_t ret = getRotateAngleFunc(handle, batchIndex, rotateAngle);
    return (ret == 0) ? HIAI_SUCCESS : HIAI_FAILURE;
}