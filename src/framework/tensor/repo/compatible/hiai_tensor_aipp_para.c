/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "framework/c/hiai_tensor_aipp_para.h"
#include <pthread.h>

#include "c/hcl/hiai_tensor_aipp_para.h"
#include "framework/infra/log/log.h"
#include "securec.h"

#include "hiai_tensor_aipp_para_def.h"
#include "hiai_tensor_aipp_para_util.h"
#include "hiai_tensor_aipp_para_local.h"
#include "hiai_tensor_aipp_para_legacy.h"
#include "util/hiai_foundation_dl_helper.h"

#define HIAI_EXPECT_NOT_NULL_R(ptr, ret) \
    if ((ptr) == NULL) { \
        return ret; \
    }

typedef struct HIAI_TensorAipp_FunctionDef {
    void* (*create)(uint32_t);
    void* (*getRawBuffer)(const void*);
    int32_t (*getRawBufferSize)(const void*);
    void* (*getHandle)(const void*);
    int32_t (*getInputIndex)(const void*);
    void (*setInputIndex)(void*, uint32_t);
    int32_t (*getInputAippIndex)(const void*);
    void (*setInputAippIndex)(void*, uint32_t);
    void (*destroy)(void*);

    HIAI_Status (*getCscSwitch)(void*, bool*);
    HIAI_Status (*getCropSwitch)(void*, uint32_t, bool*);
    HIAI_Status (*getResizeSwitch)(void*, uint32_t, bool*);
    HIAI_Status (*getPadSwitch)(void*, uint32_t, bool*);

    HIAI_Status (*getBatchCount)(void*, uint32_t*);

    HIAI_Status (*getInputFormat)(void*, HIAI_ImageFormat*);
    HIAI_Status (*setInputFormat)(void*, HIAI_ImageFormat);

    HIAI_Status (*getInputShape)(void*, uint32_t*, uint32_t*);
    HIAI_Status (*setInputShape)(void*, uint32_t, uint32_t);

    HIAI_Status (*setCscPara)(void*, HIAI_ImageFormat, HIAI_ImageFormat, HIAI_ImageColorSpace);
    HIAI_Status (*getCscPara)(void*, HIAI_ImageFormat*, HIAI_ImageFormat*, HIAI_ImageColorSpace*);
    HIAI_Status (*setChannelSwapPara)(void*, bool, bool);
    HIAI_Status (*getChannelSwapPara)(void*, bool*, bool*);

    HIAI_Status (*setSingleBatchMultiCrop)(void*, bool);
    bool (*getSingleBatchMultiCrop)(void*);

    HIAI_Status (*setCropPos)(void*, uint32_t, uint32_t, uint32_t);
    HIAI_Status (*setCropSize)(void*, uint32_t, uint32_t, uint32_t);
    HIAI_Status (*getCropPos)(void*, uint32_t, uint32_t*, uint32_t*);
    HIAI_Status (*getCropSize)(void*, uint32_t, uint32_t*, uint32_t*);

    HIAI_Status (*setResizePara)(void*, uint32_t, uint32_t, uint32_t);
    HIAI_Status (*getResizePara)(void*, uint32_t, uint32_t*, uint32_t*);

    HIAI_Status (*setPadTopSize)(void*, uint32_t, uint32_t);
    HIAI_Status (*setPadBottomSize)(void*, uint32_t, uint32_t);
    HIAI_Status (*setPadLeftSize)(void*, uint32_t, uint32_t);
    HIAI_Status (*setPadRightSize)(void*, uint32_t, uint32_t);
    HIAI_Status (*setPadChannelValue)(void*, uint32_t, uint32_t[], uint32_t);
    HIAI_Status (*getPadTopSize)(void*, uint32_t, uint32_t*);
    HIAI_Status (*getPadBottomSize)(void*, uint32_t, uint32_t*);
    HIAI_Status (*getPadLeftSize)(void*, uint32_t, uint32_t*);
    HIAI_Status (*getPadRightSize)(void*, uint32_t, uint32_t*);
    HIAI_Status (*getPadChannelValue)(void*, uint32_t, uint32_t[], uint32_t);

    HIAI_Status (*setDtcPixelMeanPara)(void*, uint32_t, int32_t[], uint32_t);
    HIAI_Status (*setDtcPixelMinPara)(void*, uint32_t, float[], uint32_t);
    HIAI_Status (*setDtcPixelVarReciPara)(void*, uint32_t, float[], uint32_t);
    HIAI_Status (*getDtcPixelMeanPara)(void*, uint32_t, int32_t[], uint32_t);
    HIAI_Status (*getDtcPixelMinPara)(void*, uint32_t, float[], uint32_t);
    HIAI_Status (*getDtcPixelVarReciPara)(void*, uint32_t, float[], uint32_t);

    HIAI_Status (*setRotateAngle)(void*, uint32_t, float);
    HIAI_Status (*getRotateAngle)(void*, uint32_t, float*);
} HIAI_TensorAipp_FunctionDef;

static pthread_mutex_t g_functionDefInitMutex = PTHREAD_MUTEX_INITIALIZER;
static HIAI_TensorAipp_FunctionDef g_tensorAippFunctionDef;

static HIAI_TensorAipp_FunctionDef* GetTensorAippFunctionDef_Local(void)
{
    static int32_t isInited = 0;
    if (isInited == 0) {
        pthread_mutex_lock(&g_functionDefInitMutex);
        if (isInited == 0) {
            (void)memset_s(&g_tensorAippFunctionDef, sizeof(HIAI_TensorAipp_FunctionDef),
                0, sizeof(HIAI_TensorAipp_FunctionDef));
            g_tensorAippFunctionDef.create = HIAI_TensorAippPara_CreateLocal;
            g_tensorAippFunctionDef.getRawBuffer = HIAI_TensorAippPara_GetRawBufferLocal;
            g_tensorAippFunctionDef.getRawBufferSize = HIAI_TensorAippPara_GetRawBufferSizeLocal;
            g_tensorAippFunctionDef.getHandle = HIAI_TensorAippPara_GetHandleLocal;
            g_tensorAippFunctionDef.getInputIndex = HIAI_TensorAippPara_GetInputIndexLocal;
            g_tensorAippFunctionDef.setInputIndex = HIAI_TensorAippPara_SetInputIndexLocal;
            g_tensorAippFunctionDef.getInputAippIndex = HIAI_TensorAippPara_GetInputAippIndexLocal;
            g_tensorAippFunctionDef.setInputAippIndex = HIAI_TensorAippPara_SetInputAippIndexLocal;

            g_tensorAippFunctionDef.getCscSwitch = HIAI_TensorAippPara_GetCscSwitchLocal;
            g_tensorAippFunctionDef.getCropSwitch = HIAI_TensorAippPara_GetCropSwitchLocal;
            g_tensorAippFunctionDef.getResizeSwitch = HIAI_TensorAippPara_GetResizeSwitchLocal;
            g_tensorAippFunctionDef.getPadSwitch = HIAI_TensorAippPara_GetPadSwitchLocal;
            g_tensorAippFunctionDef.getBatchCount = HIAI_TensorAippPara_GetBatchCountLocal;
            g_tensorAippFunctionDef.getInputFormat = HIAI_TensorAippPara_GetInputFormatLocal;
            g_tensorAippFunctionDef.setInputFormat = HIAI_TensorAippPara_SetInputFormatLocal;
            g_tensorAippFunctionDef.getInputShape = HIAI_TensorAippPara_GetInputShapeLocal;
            g_tensorAippFunctionDef.setInputShape = HIAI_TensorAippPara_SetInputShapeLocal;

            g_tensorAippFunctionDef.setCscPara = HIAI_TensorAippPara_SetCscParaLocal;
            g_tensorAippFunctionDef.getCscPara = HIAI_TensorAippPara_GetCscParaLocal;
            g_tensorAippFunctionDef.setChannelSwapPara = HIAI_TensorAippPara_SetChannelSwapParaLocal;
            g_tensorAippFunctionDef.getChannelSwapPara = HIAI_TensorAippPara_GetChannelSwapParaLocal;

            g_tensorAippFunctionDef.setSingleBatchMultiCrop = HIAI_TensorAippPara_SetSingleBatchMultiCropLocal;
            g_tensorAippFunctionDef.getSingleBatchMultiCrop = HIAI_TensorAippPara_GetSingleBatchMultiCropLocal;

            g_tensorAippFunctionDef.destroy = HIAI_TensorAippPara_DestroyLocal;
            g_tensorAippFunctionDef.setCropPos = HIAI_TensorAippPara_SetCropPosLocal;
            g_tensorAippFunctionDef.setCropSize = HIAI_TensorAippPara_SetCropSizeLocal;
            g_tensorAippFunctionDef.getCropPos = HIAI_TensorAippPara_GetCropPosLocal;
            g_tensorAippFunctionDef.getCropSize = HIAI_TensorAippPara_GetCropSizeLocal;
            g_tensorAippFunctionDef.setResizePara = HIAI_TensorAippPara_SetResizeParaLocal;
            g_tensorAippFunctionDef.getResizePara = HIAI_TensorAippPara_GetResizeParaLocal;
            g_tensorAippFunctionDef.setPadTopSize = HIAI_TensorAippPara_SetPadTopSizeLocal;
            g_tensorAippFunctionDef.setPadBottomSize = HIAI_TensorAippPara_SetPadBottomSizeLocal;
            g_tensorAippFunctionDef.setPadLeftSize = HIAI_TensorAippPara_SetPadLeftSizeLocal;
            g_tensorAippFunctionDef.setPadRightSize = HIAI_TensorAippPara_SetPadRightSizeLocal;
            g_tensorAippFunctionDef.setPadChannelValue = HIAI_TensorAippPara_SetPadChannelValueLocal;
            g_tensorAippFunctionDef.getPadTopSize = HIAI_TensorAippPara_GetPadTopSizeLocal;
            g_tensorAippFunctionDef.getPadBottomSize = HIAI_TensorAippPara_GetPadBottomSizeLocal;
            g_tensorAippFunctionDef.getPadLeftSize = HIAI_TensorAippPara_GetPadLeftSizeLocal;
            g_tensorAippFunctionDef.getPadRightSize = HIAI_TensorAippPara_GetPadRightSizeLocal;
            g_tensorAippFunctionDef.getPadChannelValue = HIAI_TensorAippPara_GetPadChannelValueLocal;

            g_tensorAippFunctionDef.setDtcPixelMeanPara = HIAI_TensorAippPara_SetDtcPixelMeanParaLocal;
            g_tensorAippFunctionDef.setDtcPixelMinPara = HIAI_TensorAippPara_SetDtcPixelMinParaLocal;
            g_tensorAippFunctionDef.setDtcPixelVarReciPara = HIAI_TensorAippPara_SetDtcPixelVarReciParaLocal;
            g_tensorAippFunctionDef.getDtcPixelMeanPara = HIAI_TensorAippPara_GetDtcPixelMeanParaLocal;
            g_tensorAippFunctionDef.getDtcPixelMinPara = HIAI_TensorAippPara_GetDtcPixelMinParaLocal;
            g_tensorAippFunctionDef.getDtcPixelVarReciPara = HIAI_TensorAippPara_GetDtcPixelVarReciParaLocal;

            g_tensorAippFunctionDef.setRotateAngle = HIAI_TensorAippPara_SetRotateAngleLocal;
            g_tensorAippFunctionDef.getRotateAngle = HIAI_TensorAippPara_GetRotateAngleLocal;

            isInited = 1;
        }
        pthread_mutex_unlock(&g_functionDefInitMutex);
    }

    return &g_tensorAippFunctionDef;
}

static HIAI_TensorAipp_FunctionDef* GetTensorAippFunctionDef_Legacy(void)
{
    static int32_t isInited = 0;
    if (isInited == 0) {
        pthread_mutex_lock(&g_functionDefInitMutex);
        if (isInited == 0) {
            (void)memset_s(&g_tensorAippFunctionDef, sizeof(HIAI_TensorAipp_FunctionDef),
                0, sizeof(HIAI_TensorAipp_FunctionDef));
            g_tensorAippFunctionDef.create = HIAI_TensorAippPara_CreateLegacy;
            g_tensorAippFunctionDef.getRawBuffer = HIAI_TensorAippPara_GetRawBufferLegacy;
            g_tensorAippFunctionDef.getRawBufferSize = HIAI_TensorAippPara_GetRawBufferSizeLegacy;
            g_tensorAippFunctionDef.getHandle = HIAI_TensorAippPara_GetHandleLegacy;
            g_tensorAippFunctionDef.getInputIndex = HIAI_TensorAippPara_GetInputIndexLegacy;
            g_tensorAippFunctionDef.setInputIndex = HIAI_TensorAippPara_SetInputIndexLegacy;
            g_tensorAippFunctionDef.getInputAippIndex = HIAI_TensorAippPara_GetInputAippIndexLegacy;
            g_tensorAippFunctionDef.setInputAippIndex = HIAI_TensorAippPara_SetInputAippIndexLegacy;

            g_tensorAippFunctionDef.getCscSwitch = HIAI_TensorAippPara_GetCscSwitchLegacy;
            g_tensorAippFunctionDef.getCropSwitch = HIAI_TensorAippPara_GetCropSwitchLegacy;
            g_tensorAippFunctionDef.getResizeSwitch = HIAI_TensorAippPara_GetResizeSwitchLegacy;
            g_tensorAippFunctionDef.getPadSwitch = HIAI_TensorAippPara_GetPadSwitchLegacy;
            g_tensorAippFunctionDef.getBatchCount = HIAI_TensorAippPara_GetBatchCountLegacy;
            g_tensorAippFunctionDef.getInputFormat = HIAI_TensorAippPara_GetInputFormatLegacy;
            g_tensorAippFunctionDef.setInputFormat = HIAI_TensorAippPara_SetInputFormatLegacy;
            g_tensorAippFunctionDef.getInputShape = HIAI_TensorAippPara_GetInputShapeLegacy;
            g_tensorAippFunctionDef.setInputShape = HIAI_TensorAippPara_SetInputShapeLegacy;

            g_tensorAippFunctionDef.setCscPara = HIAI_TensorAippPara_SetCscParaLegacy;
            g_tensorAippFunctionDef.getCscPara = HIAI_TensorAippPara_GetCscParaLegacy;
            g_tensorAippFunctionDef.setChannelSwapPara = HIAI_TensorAippPara_SetChannelSwapParaLegacy;
            g_tensorAippFunctionDef.getChannelSwapPara = HIAI_TensorAippPara_GetChannelSwapParaLegacy;

            g_tensorAippFunctionDef.setSingleBatchMultiCrop = HIAI_TensorAippPara_SetSingleBatchMultiCropLegacy;
            g_tensorAippFunctionDef.getSingleBatchMultiCrop = HIAI_TensorAippPara_GetSingleBatchMultiCropLegacy;

            g_tensorAippFunctionDef.destroy = HIAI_TensorAippPara_DestroyLegacy;
            g_tensorAippFunctionDef.setCropPos = HIAI_TensorAippPara_SetCropPosLegacy;
            g_tensorAippFunctionDef.setCropSize = HIAI_TensorAippPara_SetCropSizeLegacy;
            g_tensorAippFunctionDef.getCropPos = HIAI_TensorAippPara_GetCropPosLegacy;
            g_tensorAippFunctionDef.getCropSize = HIAI_TensorAippPara_GetCropSizeLegacy;
            g_tensorAippFunctionDef.setResizePara = HIAI_TensorAippPara_SetResizeParaLegacy;
            g_tensorAippFunctionDef.getResizePara = HIAI_TensorAippPara_GetResizeParaLegacy;

            g_tensorAippFunctionDef.setPadTopSize = HIAI_TensorAippPara_SetPadTopSizeLegacy;
            g_tensorAippFunctionDef.setPadBottomSize = HIAI_TensorAippPara_SetPadBottomSizeLegacy;
            g_tensorAippFunctionDef.setPadLeftSize = HIAI_TensorAippPara_SetPadLeftSizeLegacy;
            g_tensorAippFunctionDef.setPadRightSize = HIAI_TensorAippPara_SetPadRightSizeLegacy;
            g_tensorAippFunctionDef.setPadChannelValue = HIAI_TensorAippPara_SetPadChannelValueLegacy;
            g_tensorAippFunctionDef.getPadTopSize = HIAI_TensorAippPara_GetPadTopSizeLegacy;
            g_tensorAippFunctionDef.getPadBottomSize = HIAI_TensorAippPara_GetPadBottomSizeLegacy;
            g_tensorAippFunctionDef.getPadLeftSize = HIAI_TensorAippPara_GetPadLeftSizeLegacy;
            g_tensorAippFunctionDef.getPadRightSize = HIAI_TensorAippPara_GetPadRightSizeLegacy;
            g_tensorAippFunctionDef.getPadChannelValue = HIAI_TensorAippPara_GetPadChannelValueLegacy;

            g_tensorAippFunctionDef.setDtcPixelMeanPara = HIAI_TensorAippPara_SetDtcPixelMeanParaLegacy;
            g_tensorAippFunctionDef.setDtcPixelMinPara = HIAI_TensorAippPara_SetDtcPixelMinParaLegacy;
            g_tensorAippFunctionDef.setDtcPixelVarReciPara = HIAI_TensorAippPara_SetDtcPixelVarReciParaLegacy;
            g_tensorAippFunctionDef.getDtcPixelMeanPara = HIAI_TensorAippPara_GetDtcPixelMeanParaLegacy;
            g_tensorAippFunctionDef.getDtcPixelMinPara = HIAI_TensorAippPara_GetDtcPixelMinParaLegacy;
            g_tensorAippFunctionDef.getDtcPixelVarReciPara = HIAI_TensorAippPara_GetDtcPixelVarReciParaLegacy;

            g_tensorAippFunctionDef.setRotateAngle = HIAI_TensorAippPara_SetRotateAngleLegacy;
            g_tensorAippFunctionDef.getRotateAngle = HIAI_TensorAippPara_GetRotateAngleLegacy;
            isInited = 1;
        }
        pthread_mutex_unlock(&g_functionDefInitMutex);
    }

    return &g_tensorAippFunctionDef;
}

static HIAI_TensorAipp_FunctionDef* GetTensorAippFunctionDef(void)
{
    if (HIAI_Foundation_IsNpuSupport() == HIAI_SUPPORT_NPU &&
        HIAI_Foundation_GetSymbol("HIAI_TensorAipp_create") != NULL) {
        return GetTensorAippFunctionDef_Legacy();
    } else {
        return GetTensorAippFunctionDef_Local();
    }
}

HIAI_MR_TensorAippPara* HIAI_MR_TensorAippPara_Create(uint32_t batchNum)
{
    void* handle = GetTensorAippFunctionDef()->create(batchNum);
    if (handle == NULL) {
        handle = GetTensorAippFunctionDef_Local()->create(batchNum);
        if (handle == NULL) {
            return NULL;
        }
    }

    HIAI_MR_TensorAippPara* aippPara = (HIAI_MR_TensorAippPara*)malloc(sizeof(HIAI_MR_TensorAippPara));
    if (aippPara == NULL) {
        FMK_LOGE("malloc failed.");
        goto FREE_HANDLE;
    }
    (void)memset_s(aippPara, sizeof(HIAI_MR_TensorAippPara), 0, sizeof(HIAI_MR_TensorAippPara));

    aippPara->handle = handle;
    aippPara->data = GetTensorAippFunctionDef()->getRawBuffer(handle);
    aippPara->size = (size_t)(uint32_t)(GetTensorAippFunctionDef()->getRawBufferSize(handle));
    if (aippPara->data == NULL || aippPara->size == 0) {
        FMK_LOGE("create invalid para. size:%u", aippPara->size);
        goto FREE_PARA;
    }

    (void)HIAI_TensorAippUtil_SetBatchCount(aippPara->data, batchNum);
    return aippPara;
FREE_PARA:
    free(aippPara);

FREE_HANDLE:
    GetTensorAippFunctionDef()->destroy(handle);
    return NULL;
}

HIAI_MR_TensorAippPara* HIAI_MR_TensorAippPara_CreateWithHandle(void* data, size_t size, void* handle)
{
    HIAI_MR_TensorAippPara* aippPara = (HIAI_MR_TensorAippPara*)malloc(sizeof(HIAI_MR_TensorAippPara));
    if (aippPara == NULL) {
        FMK_LOGE("malloc HIAI_TensorAippPara failed.");
        return NULL;
    }

    aippPara->data = data;
    aippPara->size = size;
    aippPara->handle = handle;
    return aippPara;
}

void* HIAI_MR_TensorAippPara_GetRawBuffer(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, NULL);

    return tensorAippPara->data;
}

int32_t HIAI_MR_TensorAippPara_GetRawBufferSize(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, 0);

    return GetTensorAippFunctionDef()->getRawBufferSize(tensorAippPara->handle);
}

void* HIAI_MR_TensorAippPara_GetHandle(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    return tensorAippPara->handle;
}

int32_t HIAI_MR_TensorAippPara_GetInputIndex(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, -1);

    return GetTensorAippFunctionDef()->getInputIndex(tensorAippPara->handle);
}

void HIAI_MR_TensorAippPara_SetInputIndex(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t inputIndex)
{
    if (tensorAippPara == NULL) {
        return;
    }

    GetTensorAippFunctionDef()->setInputIndex(tensorAippPara->handle, inputIndex);
}

int32_t HIAI_MR_TensorAippPara_GetInputAippIndex(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, -1);

    return GetTensorAippFunctionDef()->getInputAippIndex(tensorAippPara->handle);
}

void HIAI_MR_TensorAippPara_SetInputAippIndex(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t inputAippIndex)
{
    if (tensorAippPara == NULL) {
        return;
    }

    GetTensorAippFunctionDef()->setInputAippIndex(tensorAippPara->handle, inputAippIndex);
}

void HIAI_MR_TensorAippPara_Destroy(HIAI_MR_TensorAippPara** aippParas)
{
    if (aippParas == NULL || *aippParas == NULL) {
        return;
    }

    GetTensorAippFunctionDef()->destroy((*aippParas)->handle);
    free(*aippParas);
    *aippParas = NULL;
}

bool HIAI_MR_TensorAippPara_GetCscSwitch(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, false);

    bool cscSwitch = false;
    HIAI_Status ret = GetTensorAippFunctionDef()->getCscSwitch(tensorAippPara->handle, &cscSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetCscSwitch(tensorAippPara->data);
    }
    return cscSwitch;
}

bool HIAI_MR_TensorAippPara_GetCropSwitch(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, false);

    bool cropSwitch = false;
    HIAI_Status ret = GetTensorAippFunctionDef()->getCropSwitch(tensorAippPara->handle, batchIndex, &cropSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetCropSwitch(tensorAippPara->data, batchIndex);
    }
    return cropSwitch;
}

bool HIAI_MR_TensorAippPara_GetResizeSwitch(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, false);

    bool resizeSwitch = false;
    HIAI_Status ret = GetTensorAippFunctionDef()->getResizeSwitch(tensorAippPara->handle, batchIndex, &resizeSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetResizeSwitch(tensorAippPara->data, batchIndex);
    }
    return resizeSwitch;
}

bool HIAI_MR_TensorAippPara_GetPadSwitch(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, false);

    bool padSwitch = false;
    HIAI_Status ret = GetTensorAippFunctionDef()->getPadSwitch(tensorAippPara->handle, batchIndex, &padSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadSwitch(tensorAippPara->data, batchIndex);
    }
    return padSwitch;
}

uint32_t HIAI_MR_TensorAippPara_GetBatchCount(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, 0);

    uint32_t batchCount = 0;
    HIAI_Status ret = GetTensorAippFunctionDef()->getBatchCount(tensorAippPara->handle, &batchCount);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetBatchCount(tensorAippPara->data);
    }
    return batchCount;
}

HIAI_Status HIAI_MR_TensorAippPara_SetInputFormat(HIAI_MR_TensorAippPara* tensorAippPara, HIAI_ImageFormat inputFormat)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setInputFormat(tensorAippPara->handle, inputFormat);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetInputFormat(tensorAippPara->data, inputFormat);
    }
    return ret;
}

HIAI_ImageFormat HIAI_MR_TensorAippPara_GetInputFormat(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_IMAGE_FORMAT_INVALID);

    HIAI_ImageFormat format = HIAI_IMAGE_FORMAT_INVALID;
    HIAI_Status ret = GetTensorAippFunctionDef()->getInputFormat(tensorAippPara->handle, &format);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetInputFormat(tensorAippPara->data);
    }
    return format;
}

HIAI_Status HIAI_MR_TensorAippPara_GetInputShape(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t* srcImageW, uint32_t* srcImageH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getInputShape(tensorAippPara->handle, srcImageW, srcImageH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetInputShape(tensorAippPara->data, srcImageW, srcImageH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetInputShape(HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t srcImageW, uint32_t srcImageH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setInputShape(tensorAippPara->handle, srcImageW, srcImageH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetInputShape(tensorAippPara->data, srcImageW, srcImageH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetCscPara(HIAI_MR_TensorAippPara* tensorAippPara, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setCscPara(tensorAippPara->handle, inputFormat, targetFormat, colorSpace);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetCscPara(tensorAippPara->data, inputFormat, targetFormat, colorSpace);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetCscMatrixPara(
    HIAI_MR_TensorAippPara* tensorAippPara, int32_t cscMatrixPara[], uint32_t paraNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    return HIAI_TensorAippUtil_SetCscMatrixPara(tensorAippPara->data, cscMatrixPara, paraNum);
}

HIAI_Status HIAI_MR_TensorAippPara_GetCscPara(const HIAI_MR_TensorAippPara* tensorAippPara,
    HIAI_ImageFormat* inputFormat, HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getCscPara(tensorAippPara->handle, inputFormat, targetFormat, colorSpace);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetCscPara(tensorAippPara->data, inputFormat, targetFormat, colorSpace);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetCscMatrixPara(
    const HIAI_MR_TensorAippPara* tensorAippPara, int32_t cscMatrixPara[], uint32_t paraNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    return HIAI_TensorAippUtil_GetCscMatrixPara(tensorAippPara->data, cscMatrixPara, paraNum);
}

HIAI_Status HIAI_MR_TensorAippPara_SetChannelSwapPara(HIAI_MR_TensorAippPara* tensorAippPara,
    bool rbuvSwapSwitch, bool axSwapSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setChannelSwapPara(tensorAippPara->handle, rbuvSwapSwitch, axSwapSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetChannelSwapPara(tensorAippPara->data, rbuvSwapSwitch, axSwapSwitch);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetChannelSwapPara(const HIAI_MR_TensorAippPara* tensorAippPara,
    bool* rbuvSwapSwitch, bool* axSwapSwitch)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getChannelSwapPara(tensorAippPara->handle, rbuvSwapSwitch, axSwapSwitch);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetChannelSwapPara(tensorAippPara->data, rbuvSwapSwitch, axSwapSwitch);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetSingleBatchMultiCrop(
    HIAI_MR_TensorAippPara* tensorAippPara, bool singleBatchMutiCrop)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    return GetTensorAippFunctionDef()->setSingleBatchMultiCrop(tensorAippPara->handle, singleBatchMutiCrop);
}

bool HIAI_MR_TensorAippPara_GetSingleBatchMultiCrop(const HIAI_MR_TensorAippPara* tensorAippPara)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, false);

    return GetTensorAippFunctionDef()->getSingleBatchMultiCrop(tensorAippPara->handle);
}


HIAI_Status HIAI_MR_TensorAippPara_SetCropPos(HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setCropPos(tensorAippPara->handle, batchIndex, cropStartPosW, cropStartPosH);
    if (ret == HIAI_INVALID_API) {
        /* 兼容原来非API传递参数场景，直接填充结构体 */
        return HIAI_TensorAippUtil_SetCropPos(tensorAippPara->data, batchIndex, cropStartPosW, cropStartPosH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetCropSize(HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setCropSize(tensorAippPara->handle, batchIndex, cropSizeW, cropSizeH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetCropSize(tensorAippPara->data, batchIndex, cropSizeW, cropSizeH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetCropPos(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getCropPos(tensorAippPara->handle, batchIndex, cropStartPosW, cropStartPosH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetCropPos(tensorAippPara->data, batchIndex, cropStartPosW, cropStartPosH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetCropSize(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getCropSize(tensorAippPara->handle, batchIndex, cropSizeW, cropSizeH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetCropSize(tensorAippPara->data, batchIndex, cropSizeW, cropSizeH);
    }
    return ret;
}
HIAI_Status HIAI_MR_TensorAippPara_SetResizePara(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setResizePara(tensorAippPara->handle, batchIndex,
        resizeOutputSizeW, resizeOutputSizeH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetResizePara(
            tensorAippPara->data, batchIndex, resizeOutputSizeW, resizeOutputSizeH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetResizePara(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getResizePara(tensorAippPara->handle, batchIndex,
        resizeOutputSizeW, resizeOutputSizeH);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetResizePara(
            tensorAippPara->data, batchIndex, resizeOutputSizeW, resizeOutputSizeH);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetPadTopSize(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t paddingSizeTop)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =  GetTensorAippFunctionDef()->setPadTopSize(tensorAippPara->handle, batchIndex, paddingSizeTop);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetPadTopSize(tensorAippPara->data, batchIndex, paddingSizeTop);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetPadBottomSize(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t paddingSizeBottom)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setPadBottomSize(tensorAippPara->handle, batchIndex, paddingSizeBottom);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetPadBottomSize(tensorAippPara->data, batchIndex, paddingSizeBottom);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetPadLeftSize(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t paddingSizeLeft)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setPadLeftSize(tensorAippPara->handle, batchIndex, paddingSizeLeft);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetPadLeftSize(tensorAippPara->data, batchIndex, paddingSizeLeft);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetPadRightSize(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t paddingSizeRight)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setPadRightSize(tensorAippPara->handle, batchIndex, paddingSizeRight);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetPadRightSize(tensorAippPara->data, batchIndex, paddingSizeRight);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetPadChannelValue(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->setPadChannelValue(tensorAippPara->handle, batchIndex, chnValue, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetPadChannelValue(tensorAippPara->data, batchIndex, chnValue, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetPadTopSize(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* paddingSizeTop)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getPadTopSize(tensorAippPara->handle, batchIndex, paddingSizeTop);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadTopSize(tensorAippPara->data, batchIndex, paddingSizeTop);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetPadBottomSize(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* paddingSizeBottom)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getPadBottomSize(tensorAippPara->handle, batchIndex, paddingSizeBottom);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadBottomSize(tensorAippPara->data, batchIndex, paddingSizeBottom);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetPadLeftSize(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* paddingSizeLeft)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getPadLeftSize(tensorAippPara->handle, batchIndex, paddingSizeLeft);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadLeftSize(tensorAippPara->data, batchIndex, paddingSizeLeft);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetPadRightSize(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t* paddingSizeRight)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getPadRightSize(tensorAippPara->handle, batchIndex, paddingSizeRight);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadRightSize(tensorAippPara->data, batchIndex, paddingSizeRight);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetPadChannelValue(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getPadChannelValue(tensorAippPara->handle, batchIndex, chnValue, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetPadChannelValue(tensorAippPara->data, batchIndex, chnValue, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelMeanPara(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setDtcPixelMeanPara(
        tensorAippPara->handle, batchIndex, pixelMeanPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetDtcPixelMeanPara(tensorAippPara->data, batchIndex, pixelMeanPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelMinPara(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);
    HIAI_Status ret =
        GetTensorAippFunctionDef()->setDtcPixelMinPara(tensorAippPara->handle, batchIndex, pixelMinPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetDtcPixelMinPara(tensorAippPara->data, batchIndex, pixelMinPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetDtcPixelVarReciPara(HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setDtcPixelVarReciPara(
        tensorAippPara->handle, batchIndex, pixelVarReciPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetDtcPixelVarReciPara(tensorAippPara->data, batchIndex, pixelVarReciPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelMeanPara(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, int32_t pixelMeanPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getDtcPixelMeanPara(
        tensorAippPara->handle, batchIndex, pixelMeanPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetDtcPixelMeanPara(tensorAippPara->data, batchIndex, pixelMeanPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelMinPara(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret =
        GetTensorAippFunctionDef()->getDtcPixelMinPara(tensorAippPara->handle, batchIndex, pixelMinPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetDtcPixelMinPara(tensorAippPara->data, batchIndex, pixelMinPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetDtcPixelVarReciPara(const HIAI_MR_TensorAippPara* tensorAippPara,
    uint32_t batchIndex, float pixelVarReciPara[], uint32_t chnNum)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getDtcPixelVarReciPara(
        tensorAippPara->handle, batchIndex, pixelVarReciPara, chnNum);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetDtcPixelVarReciPara(tensorAippPara->data, batchIndex, pixelVarReciPara, chnNum);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_SetRotateAngle(
    HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex, float rotateAngle)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->setRotateAngle(tensorAippPara->handle, batchIndex, rotateAngle);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_SetRotateAngle(tensorAippPara->data, batchIndex, rotateAngle);
    }
    return ret;
}

HIAI_Status HIAI_MR_TensorAippPara_GetRotateAngle(const HIAI_MR_TensorAippPara* tensorAippPara, uint32_t batchIndex,
    float* rotateAngle)
{
    HIAI_EXPECT_NOT_NULL_R(tensorAippPara, HIAI_FAILURE);

    HIAI_Status ret = GetTensorAippFunctionDef()->getRotateAngle(tensorAippPara->handle, batchIndex, rotateAngle);
    if (ret == HIAI_INVALID_API) {
        return HIAI_TensorAippUtil_GetRotateAngle(tensorAippPara->data, batchIndex, rotateAngle);
    }
    return ret;
}
