/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_AIPP_PARA_UTIL_H
#define FRAMEWORK_C_HIAI_AIPP_PARA_UTIL_H

#include "c/hiai_error_types.h"
#include "c/hcl/hiai_base_types.h"
#include "framework/c/hiai_tensor_aipp_para.h"
#include "hiai_tensor_aipp_para_def.h"

#ifdef __cplusplus
extern "C" {
#endif

HIAI_MR_TensorAippBatchPara* HIAI_TensorAippUtil_GetBatchPara(void* commPara, uint32_t batchIndex);

int32_t HIAI_TensorAippUtil_GetRawBufferSize(void* commPara);

bool HIAI_TensorAippUtil_GetCscSwitch(void* commPara);
bool HIAI_TensorAippUtil_GetCropSwitch(void* commPara, uint32_t batchIndex);
bool HIAI_TensorAippUtil_GetResizeSwitch(void* commPara, uint32_t batchIndex);
bool HIAI_TensorAippUtil_GetPadSwitch(void* commPara, uint32_t batchIndex);

HIAI_Status HIAI_TensorAippUtil_SetBatchCount(void* commPara, uint32_t batchNum);
uint32_t HIAI_TensorAippUtil_GetBatchCount(void* commPara);

HIAI_Status HIAI_TensorAippUtil_SetInputFormat(void* commPara, HIAI_ImageFormat inputFormat);
HIAI_ImageFormat HIAI_TensorAippUtil_GetInputFormat(void* commPara);

HIAI_Status HIAI_TensorAippUtil_SetInputShape(void* commPara, uint32_t srcImageW, uint32_t srcImageH);
HIAI_Status HIAI_TensorAippUtil_GetInputShape(void* commPara, uint32_t* srcImageW, uint32_t* srcImageH);

HIAI_Status HIAI_TensorAippUtil_SetCscPara(void* commPara, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace);
HIAI_Status HIAI_TensorAippUtil_SetCscMatrixPara(void* commPara, int32_t cscMatrixPara[], uint32_t paraNum);
HIAI_Status HIAI_TensorAippUtil_GetCscPara(void* commPara, HIAI_ImageFormat* inputFormat,
    HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace);
HIAI_Status HIAI_TensorAippUtil_GetCscMatrixPara(void* commPara, int32_t cscMatrixPara[], uint32_t paraNum);

HIAI_Status HIAI_TensorAippUtil_SetChannelSwapPara(void* commPara, bool rbuvSwapSwitch, bool axSwapSwitch);
HIAI_Status HIAI_TensorAippUtil_GetChannelSwapPara(void* commPara, bool* rbuvSwapSwitch, bool* axSwapSwitch);

HIAI_Status HIAI_TensorAippUtil_SetSingleBatchMultiCrop(void* commPara, bool singleBatchMutiCrop);
bool HIAI_TensorAippUtil_GetSingleBatchMultiCrop(void* commPara);

HIAI_Status HIAI_TensorAippUtil_SetCropPos(
    void* commPara, uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH);
HIAI_Status HIAI_TensorAippUtil_SetCropSize(
    void* commPara, uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH);
HIAI_Status HIAI_TensorAippUtil_GetCropPos(
    void* commPara, uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH);
HIAI_Status HIAI_TensorAippUtil_GetCropSize(
    void* commPara, uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH);

HIAI_Status HIAI_TensorAippUtil_SetResizePara(void* commPara,
    uint32_t batchIndex, uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH);
HIAI_Status HIAI_TensorAippUtil_GetResizePara(void* commPara,
    uint32_t batchIndex, uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH);

HIAI_Status HIAI_TensorAippUtil_SetPadTopSize(void* commPara, uint32_t batchIndex, uint32_t paddingSizeTop);
HIAI_Status HIAI_TensorAippUtil_SetPadBottomSize(void* commPara, uint32_t batchIndex, uint32_t paddingSizeBottom);
HIAI_Status HIAI_TensorAippUtil_SetPadLeftSize(void* commPara, uint32_t batchIndex, uint32_t paddingSizeLeft);
HIAI_Status HIAI_TensorAippUtil_SetPadRightSize(void* commPara, uint32_t batchIndex, uint32_t paddingSizeRight);
HIAI_Status HIAI_TensorAippUtil_SetPadChannelValue(void* commPara, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_GetPadTopSize(void* commPara, uint32_t batchIndex, uint32_t* paddingSizeTop);
HIAI_Status HIAI_TensorAippUtil_GetPadBottomSize(void* commPara, uint32_t batchIndex, uint32_t* paddingSizeBottom);
HIAI_Status HIAI_TensorAippUtil_GetPadLeftSize(void* commPara, uint32_t batchIndex, uint32_t* paddingSizeLeft);
HIAI_Status HIAI_TensorAippUtil_GetPadRightSize(void* commPara, uint32_t batchIndex, uint32_t* paddingSizeRight);
HIAI_Status HIAI_TensorAippUtil_GetPadChannelValue(
    void* commPara, uint32_t batchIndex, uint32_t chnValue[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippUtil_SetDtcPixelMeanPara(void* commPara, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_SetDtcPixelMinPara(void* commPara, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_SetDtcPixelVarReciPara(void* commPara, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_GetDtcPixelMeanPara(void* commPara, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_GetDtcPixelMinPara(void* commPara, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippUtil_GetDtcPixelVarReciPara(void* commPara, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippUtil_SetRotateAngle(void* commPara, uint32_t batchIndex, float rotateAngle);
HIAI_Status HIAI_TensorAippUtil_GetRotateAngle(void* commPara, uint32_t batchIndex, float* rotateAngle);

#ifdef __cplusplus
}
#endif

#endif