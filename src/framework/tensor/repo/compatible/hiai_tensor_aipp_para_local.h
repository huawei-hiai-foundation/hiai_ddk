/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_AIPP_PARA_LOCAL_H
#define FRAMEWORK_C_HIAI_AIPP_PARA_LOCAL_H
#include <stdint.h>
#include <stdbool.h>

#include "c/hiai_error_types.h"
#include "c/hcl/hiai_base_types.h"

#ifdef __cplusplus
extern "C" {
#endif

void* HIAI_TensorAippPara_CreateLocal(uint32_t batchNum);

void* HIAI_TensorAippPara_GetRawBufferLocal(const void* handle);

int32_t HIAI_TensorAippPara_GetRawBufferSizeLocal(const void* handle);

void* HIAI_TensorAippPara_GetHandleLocal(const void* handle);

int32_t HIAI_TensorAippPara_GetInputIndexLocal(const void* handle);

void HIAI_TensorAippPara_SetInputIndexLocal(void* handle, uint32_t inputIndex);

int32_t HIAI_TensorAippPara_GetInputAippIndexLocal(const void* handle);

void HIAI_TensorAippPara_SetInputAippIndexLocal(void* handle, uint32_t inputAippIndex);

void HIAI_TensorAippPara_DestroyLocal(void* handle);

HIAI_Status HIAI_TensorAippPara_GetCscSwitchLocal(void* handle, bool* cscSwitch);
HIAI_Status HIAI_TensorAippPara_GetCropSwitchLocal(void* handle, uint32_t batchIndex, bool* cropSwitch);
HIAI_Status HIAI_TensorAippPara_GetResizeSwitchLocal(void* handle, uint32_t batchIndex, bool* resizeSwitch);
HIAI_Status HIAI_TensorAippPara_GetPadSwitchLocal(void* handle, uint32_t batchIndex, bool* padSwitch);

HIAI_Status HIAI_TensorAippPara_GetBatchCountLocal(void* handle, uint32_t* batchCount);

HIAI_Status HIAI_TensorAippPara_SetInputFormatLocal(void* handle, HIAI_ImageFormat inputFormat);
HIAI_Status HIAI_TensorAippPara_GetInputFormatLocal(void* handle, HIAI_ImageFormat* inputFormat);

HIAI_Status HIAI_TensorAippPara_GetInputShapeLocal(void* handle, uint32_t* srcImageW, uint32_t* srcImageH);
HIAI_Status HIAI_TensorAippPara_SetInputShapeLocal(void* handle, uint32_t srcImageW, uint32_t srcImageH);

HIAI_Status HIAI_TensorAippPara_SetCscParaLocal(void* handle, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace);
HIAI_Status HIAI_TensorAippPara_GetCscParaLocal(void* handle, HIAI_ImageFormat* inputFormat,
    HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace);

HIAI_Status HIAI_TensorAippPara_SetChannelSwapParaLocal(void* handle, bool rbuvSwapSwitch, bool axSwapSwitch);
HIAI_Status HIAI_TensorAippPara_GetChannelSwapParaLocal(void* handle, bool* rbuvSwapSwitch, bool* axSwapSwitch);

HIAI_Status HIAI_TensorAippPara_SetSingleBatchMultiCropLocal(void* handle, bool singleBatchMutiCrop);
bool HIAI_TensorAippPara_GetSingleBatchMultiCropLocal(void* handle);


HIAI_Status HIAI_TensorAippPara_SetCropPosLocal(void* handle,
    uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH);
HIAI_Status HIAI_TensorAippPara_SetCropSizeLocal(void* handle,
    uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH);
HIAI_Status HIAI_TensorAippPara_GetCropPosLocal(void* handle,
    uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH);
HIAI_Status HIAI_TensorAippPara_GetCropSizeLocal(void* handle,
    uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH);

HIAI_Status HIAI_TensorAippPara_SetResizeParaLocal(void* handle,
    uint32_t batchIndex, uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH);
HIAI_Status HIAI_TensorAippPara_GetResizeParaLocal(void* handle,
    uint32_t batchIndex, uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH);

HIAI_Status HIAI_TensorAippPara_SetPadTopSizeLocal(void* handle, uint32_t batchIndex, uint32_t paddingSizeTop);
HIAI_Status HIAI_TensorAippPara_SetPadBottomSizeLocal(void* handle, uint32_t batchIndex, uint32_t paddingSizeBottom);
HIAI_Status HIAI_TensorAippPara_SetPadLeftSizeLocal(void* handle, uint32_t batchIndex, uint32_t paddingSizeLeft);
HIAI_Status HIAI_TensorAippPara_SetPadRightSizeLocal(void* handle, uint32_t batchIndex, uint32_t paddingSizeRight);
HIAI_Status HIAI_TensorAippPara_SetPadChannelValueLocal(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetPadTopSizeLocal(void* handle, uint32_t batchIndex, uint32_t* paddingSizeTop);
HIAI_Status HIAI_TensorAippPara_GetPadBottomSizeLocal(void* handle, uint32_t batchIndex, uint32_t* paddingSizeBottom);
HIAI_Status HIAI_TensorAippPara_GetPadLeftSizeLocal(void* handle, uint32_t batchIndex, uint32_t* paddingSizeLeft);
HIAI_Status HIAI_TensorAippPara_GetPadRightSizeLocal(void* handle, uint32_t batchIndex, uint32_t* paddingSizeRight);
HIAI_Status HIAI_TensorAippPara_GetPadChannelValueLocal(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippPara_SetDtcPixelMeanParaLocal(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_SetDtcPixelMinParaLocal(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_SetDtcPixelVarReciParaLocal(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelMeanParaLocal(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelMinParaLocal(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelVarReciParaLocal(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippPara_SetRotateAngleLocal(void* handle, uint32_t batchIndex, float rotateAngle);
HIAI_Status HIAI_TensorAippPara_GetRotateAngleLocal(void* handle, uint32_t batchIndex, float* rotateAngle);

#ifdef __cplusplus
}
#endif

#endif