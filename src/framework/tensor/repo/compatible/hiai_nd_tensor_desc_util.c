/**
 * Copyright 2023-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hiai_nd_tensor_desc_util.h"
#include "hiai_nd_tensor_desc_def.h"
#include "c/hcl/hiai_nd_tensor_desc.h"

#include "framework/infra/log/log.h"

size_t HIAI_NDTensorDesc_GetElementNum(const HIAI_NDTensorDesc* tensorDesc)
{
    if (tensorDesc == NULL) {
        return 0;
    }

    size_t totalSize = 1;
    for (size_t i = 0; i < tensorDesc->dimNum; i++) {
        int32_t dimValue = tensorDesc->dims[i];
        if (dimValue <= 0) {
            FMK_LOGE("invalid dim value.");
            return 0;
        }
        if (totalSize > SIZE_MAX / (uint32_t)(dimValue)) {
            FMK_LOGE("too large dimValue.");
            return 0;
        }
        totalSize *= (uint32_t)(dimValue);
    }

    return totalSize;
}