/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "framework/c/hiai_nd_tensor_buffer.h"
#include "c/hcl/hiai_nd_tensor_buffer.h"
#include "c/hcl/hiai_nd_tensor_desc.h"

#include "stdbool.h"

#include "securec.h"
#include "framework/infra/log/log.h"

#include "hiai_nd_tensor_buffer_local.h"
#include "hiai_nd_tensor_buffer_legacy.h"
#include "hiai_nd_tensor_buffer_util.h"

#include "util/hiai_foundation_dl_helper.h"

static void* enableNewFeaturesFunc = NULL;
static bool getSymbolTryed = false;

HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateNoCopy(
    const HIAI_NDTensorDesc* desc, const void* data, size_t dataSize)
{
    if (desc == NULL || data == NULL) {
        FMK_LOGE("desc or data is NULL.");
        return NULL;
    }

    if (dataSize == 0 || dataSize != HIAI_NDTensorDesc_GetByteSize(desc)) {
        FMK_LOGE("size is invalid");
        return NULL;
    }

    return HIAI_MR_NDTensorBuffer_Create(desc, (void*)data, dataSize, NULL, false, false);
}

HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateFromNDTensorDesc(const HIAI_NDTensorDesc* desc)
{
    if (desc == NULL) {
        FMK_LOGE("desc is NULL.");
        return NULL;
    }

    if (HIAI_Foundation_IsNpuSupport() == HIAI_SUPPORT_NPU) {
        return HIAI_NDTensorBuffer_CreateFromNDTensorDescLegacy(desc);
    }

    return HIAI_NDTensorBuffer_CreateLocalBufferFromNDTensorDesc(desc);
}

HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateFromSize(const HIAI_NDTensorDesc* desc, size_t size)
{
    if (desc == NULL) {
        FMK_LOGE("desc is NULL.");
        return NULL;
    }
    if (HIAI_Foundation_IsNpuSupport() == HIAI_SUPPORT_NPU) {
        return HIAI_NDTensorBuffer_CreateFromSizeLegacy(desc, size);
    }

    return HIAI_NDTensorBuffer_CreateLocalBufferFromSize(desc, size);
}

HIAI_MR_NDTensorBuffer* HIAI_MR_NDTensorBuffer_CreateFromBuffer(
    const HIAI_NDTensorDesc* desc, const void* data, size_t dataSize)
{
    if (desc == NULL || data == NULL) {
        FMK_LOGE("desc or data is NULL.");
        return NULL;
    }

    HIAI_MR_NDTensorBuffer* buffer = HIAI_MR_NDTensorBuffer_CreateFromNDTensorDesc(desc);
    if (buffer == NULL) {
        return NULL;
    }

    HIAI_NDTensorBufferV2* buffersV2 = (HIAI_NDTensorBufferV2*)(void*)buffer;

    if (buffersV2->size != dataSize) {
        FMK_LOGE("mismatch buffer size.");
        HIAI_MR_NDTensorBuffer_Destroy(&buffer);
        return NULL;
    }

    if (memcpy_s(buffersV2->data, buffersV2->size, data, dataSize) != 0) {
        FMK_LOGE("memcpy buffer failed.");
        HIAI_MR_NDTensorBuffer_Destroy(&buffer);
        return NULL;
    }
    return buffer;
}

HIAI_NDTensorDesc* HIAI_MR_NDTensorBuffer_GetNDTensorDesc(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return 0;
    }
    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->desc;
}

size_t HIAI_MR_NDTensorBuffer_GetSize(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return 0;
    }
    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->size;
}

void* HIAI_MR_NDTensorBuffer_GetData(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return NULL;
    }

    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->data;
}

uint8_t HIAI_MR_NDTensorBuffer_GetCacheStatus(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return 0;
    }

    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->cacheStatus;
}

uint8_t HIAI_MR_NDTensorBuffer_GetUpdateCount(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return 0;
    }

    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->updateCount;
}

void HIAI_MR_NDTensorBuffer_SetCacheStatus(HIAI_MR_NDTensorBuffer* tensorBuffer, uint8_t cacheStatus)
{
    if (tensorBuffer == NULL) {
        return;
    }

    ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->cacheStatus = cacheStatus;

    if (!getSymbolTryed) {
        enableNewFeaturesFunc = HIAI_Foundation_GetSymbol("HIAI_NDTensorBuffer_EnableNewFeatures");
        getSymbolTryed = true;
    }
    if (enableNewFeaturesFunc != NULL) {
        HIAI_NDTensorBufferV2* bufferV2 = (HIAI_NDTensorBufferV2*)(void*)tensorBuffer;
        ((void(*)(void*))enableNewFeaturesFunc)(bufferV2->handle);
    }
}

void* HIAI_MR_NDTensorBuffer_GetHandle(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return NULL;
    }
    return ((HIAI_NDTensorBufferV2*)(void*)tensorBuffer)->handle;
}

int32_t HIAI_MR_NDTensorBuffer_GetFd(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return -1;
    }

    if (HIAI_Foundation_IsNpuSupport() == HIAI_SUPPORT_NPU) {
        HIAI_NDTensorBufferV2* bufferV2 = (HIAI_NDTensorBufferV2*)(void*)tensorBuffer;
        return HIAI_NDTensorBuffer_GetFdLegacy(bufferV2->handle, bufferV2->isLegacy);
    }
    return -1;
}

int32_t HIAI_MR_NDTensorBuffer_GetOriginFd(const HIAI_MR_NDTensorBuffer* tensorBuffer)
{
    if (tensorBuffer == NULL) {
        return -1;
    }

    if (HIAI_Foundation_IsNpuSupport() == HIAI_SUPPORT_NPU) {
        HIAI_NDTensorBufferV2* bufferV2 = (HIAI_NDTensorBufferV2*)(void*)tensorBuffer;
        return HIAI_NDTensorBuffer_GetOriginFdLegacy(bufferV2->handle, bufferV2->isLegacy);
    }
    return -1;
}

void HIAI_MR_NDTensorBuffer_Destroy(HIAI_MR_NDTensorBuffer** tensorBuffer)
{
    if (tensorBuffer == NULL || *tensorBuffer == NULL) {
        return;
    }
    // 释放ndbuffer desc
    HIAI_NDTensorDesc* desc = HIAI_MR_NDTensorBuffer_GetNDTensorDesc(*tensorBuffer);
    HIAI_NDTensorDesc_Destroy(&desc);

    // 释放ndbuffer handle
    void* handle = ((HIAI_NDTensorBufferV2*)(void*)(*tensorBuffer))->handle;
    if (handle == NULL) {
        HIAI_NDTensorBuffer_ReleaseLocal(tensorBuffer);
    } else {
        HIAI_NDTensorBufferV2* bufferV2 = (HIAI_NDTensorBufferV2*)(void*)(*tensorBuffer);
        HIAI_NDTensorBuffer_DestroyLegacy(bufferV2->handle, bufferV2->isLegacy);
    }

    // 释放ndbuffer
    free(*tensorBuffer);
    *tensorBuffer = NULL;
}
