/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_C_HIAI_AIPP_PARA_LEGACY_H
#define FRAMEWORK_C_HIAI_AIPP_PARA_LEGACY_H
#include <stdint.h>
#include <stdbool.h>

#include "c/hiai_error_types.h"
#include "c/hcl/hiai_base_types.h"

#ifdef __cplusplus
extern "C" {
#endif

void* HIAI_TensorAippPara_CreateLegacy(uint32_t batchNum);

void* HIAI_TensorAippPara_GetRawBufferLegacy(const void* handle);

int32_t HIAI_TensorAippPara_GetRawBufferSizeLegacy(const void* handle);

void* HIAI_TensorAippPara_GetHandleLegacy(const void* handle);

int32_t HIAI_TensorAippPara_GetInputIndexLegacy(const void* handle);

void HIAI_TensorAippPara_SetInputIndexLegacy(void* handle, uint32_t inputIndex);

int32_t HIAI_TensorAippPara_GetInputAippIndexLegacy(const void* handle);

void HIAI_TensorAippPara_SetInputAippIndexLegacy(void* handle, uint32_t inputAippIndex);

void HIAI_TensorAippPara_DestroyLegacy(void* handle);

HIAI_Status HIAI_TensorAippPara_GetCscSwitchLegacy(void* handle, bool* cscSwitch);
HIAI_Status HIAI_TensorAippPara_GetCropSwitchLegacy(void* handle, uint32_t batchIndex, bool* cropSwitch);
HIAI_Status HIAI_TensorAippPara_GetResizeSwitchLegacy(void* handle, uint32_t batchIndex, bool* resizeSwitch);
HIAI_Status HIAI_TensorAippPara_GetPadSwitchLegacy(void* handle, uint32_t batchIndex, bool* padSwitch);

HIAI_Status HIAI_TensorAippPara_GetBatchCountLegacy(void* handle, uint32_t* batchCount);

HIAI_Status HIAI_TensorAippPara_SetInputFormatLegacy(void* handle, HIAI_ImageFormat inputFormat);
HIAI_Status HIAI_TensorAippPara_GetInputFormatLegacy(void* handle, HIAI_ImageFormat* inputFormat);

HIAI_Status HIAI_TensorAippPara_GetInputShapeLegacy(void* handle, uint32_t* srcImageW, uint32_t* srcImageH);
HIAI_Status HIAI_TensorAippPara_SetInputShapeLegacy(void* handle, uint32_t srcImageW, uint32_t srcImageH);

HIAI_Status HIAI_TensorAippPara_SetCscParaLegacy(void* handle, HIAI_ImageFormat inputFormat,
    HIAI_ImageFormat targetFormat, HIAI_ImageColorSpace colorSpace);
HIAI_Status HIAI_TensorAippPara_GetCscParaLegacy(void* handle, HIAI_ImageFormat* inputFormat,
    HIAI_ImageFormat* targetFormat, HIAI_ImageColorSpace* colorSpace);

HIAI_Status HIAI_TensorAippPara_SetChannelSwapParaLegacy(void* handle, bool rbuvSwapSwitch, bool axSwapSwitch);
HIAI_Status HIAI_TensorAippPara_GetChannelSwapParaLegacy(void* handle, bool* rbuvSwapSwitch, bool* axSwapSwitch);

HIAI_Status HIAI_TensorAippPara_SetSingleBatchMultiCropLegacy(void* handle, bool singleBatchMutiCrop);
bool HIAI_TensorAippPara_GetSingleBatchMultiCropLegacy(void* handle);

HIAI_Status HIAI_TensorAippPara_SetCropPosLegacy(void* handle,
    uint32_t batchIndex, uint32_t cropStartPosW, uint32_t cropStartPosH);
HIAI_Status HIAI_TensorAippPara_SetCropSizeLegacy(void* handle,
    uint32_t batchIndex, uint32_t cropSizeW, uint32_t cropSizeH);
HIAI_Status HIAI_TensorAippPara_GetCropPosLegacy(void* handle,
    uint32_t batchIndex, uint32_t* cropStartPosW, uint32_t* cropStartPosH);
HIAI_Status HIAI_TensorAippPara_GetCropSizeLegacy(void* handle,
    uint32_t batchIndex, uint32_t* cropSizeW, uint32_t* cropSizeH);

HIAI_Status HIAI_TensorAippPara_SetResizeParaLegacy(void* handle,
    uint32_t batchIndex, uint32_t resizeOutputSizeW, uint32_t resizeOutputSizeH);
HIAI_Status HIAI_TensorAippPara_GetResizeParaLegacy(void* handle,
    uint32_t batchIndex, uint32_t* resizeOutputSizeW, uint32_t* resizeOutputSizeH);

HIAI_Status HIAI_TensorAippPara_SetPadTopSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeTop);
HIAI_Status HIAI_TensorAippPara_SetPadBottomSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeBottom);
HIAI_Status HIAI_TensorAippPara_SetPadLeftSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeLeft);
HIAI_Status HIAI_TensorAippPara_SetPadRightSizeLegacy(void* handle, uint32_t batchIndex, uint32_t paddingSizeRight);
HIAI_Status HIAI_TensorAippPara_SetPadChannelValueLegacy(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetPadTopSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeTop);
HIAI_Status HIAI_TensorAippPara_GetPadBottomSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeBottom);
HIAI_Status HIAI_TensorAippPara_GetPadLeftSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeLeft);
HIAI_Status HIAI_TensorAippPara_GetPadRightSizeLegacy(void* handle, uint32_t batchIndex, uint32_t* paddingSizeRight);
HIAI_Status HIAI_TensorAippPara_GetPadChannelValueLegacy(void* handle, uint32_t batchIndex,
    uint32_t chnValue[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippPara_SetDtcPixelMeanParaLegacy(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_SetDtcPixelMinParaLegacy(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_SetDtcPixelVarReciParaLegacy(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelMeanParaLegacy(void* handle, uint32_t batchIndex,
    int32_t pixelMeanPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelMinParaLegacy(void* handle, uint32_t batchIndex,
    float pixelMinPara[], uint32_t chnNum);
HIAI_Status HIAI_TensorAippPara_GetDtcPixelVarReciParaLegacy(void* handle, uint32_t batchIndex,
    float pixelVarReciPara[], uint32_t chnNum);

HIAI_Status HIAI_TensorAippPara_SetRotateAngleLegacy(void* handle, uint32_t batchIndex, float rotateAngle);
HIAI_Status HIAI_TensorAippPara_GetRotateAngleLegacy(void* handle, uint32_t batchIndex, float* rotateAngle);
#ifdef __cplusplus
}
#endif

#endif