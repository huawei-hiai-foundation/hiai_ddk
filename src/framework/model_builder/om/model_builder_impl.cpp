/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "model_builder_impl.h"

#include <climits>

#include "securec.h"
#include "infra/base/assertion.h"
#include "framework/infra/log/log_fmk_interface.h"
#include "model_build_options_util.h"
#include "model/built_model/built_model_impl.h"
#include "infra/base/securestl.h"

#include "framework/infra/log/log.h"
#include "base/common/file_util/file_util.h"
#include "model/built_model/customdata_util.h"
#include "c/ddk/model_manager/hiai_model_builder.h"
#include "c/ddk/model_manager/hiai_built_model.h"

#ifdef __OHOS__
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_create_itf.h"
#include "model_manager/general_model_manager/ndk/ndk_util/ndk_util.h"
#endif

namespace hiai {

static Status BuildModel(const ModelBuildOptions& options, const std::string& modelName,
    const std::shared_ptr<IBuffer>& modelBuffer, std::shared_ptr<IBuiltModel>& builtModel)
{
    auto buildOptions = ModelBuildOptionsUtil::ConvertToCBuildOptions(options);
    HIAI_EXPECT_NOT_NULL(buildOptions);

    HIAI_MR_BuiltModel* builtModelImpl = nullptr;
    auto ret = HIAI_MR_ModelBuilder_Build(
        buildOptions, modelName.c_str(), modelBuffer->GetData(), modelBuffer->GetSize(), &builtModelImpl);
    HIAI_MR_ModelBuildOptions_Destroy(&buildOptions);
    if (ret != HIAI_SUCCESS || builtModelImpl == nullptr) {
        FMK_LOGE("build model failed.");
        return FAILURE;
    }

    builtModel = make_shared_nothrow<BuiltModelImpl>(std::shared_ptr<HIAI_MR_BuiltModel>(builtModelImpl,
                                                         [](HIAI_MR_BuiltModel* p) { HIAI_MR_BuiltModel_Destroy(&p); }),
        std::const_pointer_cast<IBuffer>(modelBuffer));
    if (builtModel != nullptr) {
        FMK_LOGI("build model success.");
        return SUCCESS;
    }

    return FAILURE;
}

Status ModelBuilderImpl::Build(const ModelBuildOptions& options, const std::string& modelName,
    const std::shared_ptr<IBuffer>& modelBuffer, std::shared_ptr<IBuiltModel>& builtModel)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    if (modelBuffer == nullptr) {
        FMK_LOGE("modelBuffer is nullptr.");
        return INVALID_PARAM;
    }

    if (modelName.length() > PATH_MAX) {
        FMK_LOGE("modelName length is too long.");
        return INVALID_PARAM;
    }

#ifdef AI_SUPPORT_AIPP_API
    std::shared_ptr<IBuffer> compatibleModelBuffer = nullptr;
    CustomModelData customData;
    if (CustomDataUtil::MakeCompatibleBuffer(compatibleModelBuffer, customData, modelBuffer) != SUCCESS) {
        return FAILURE;
    }

    if (compatibleModelBuffer != nullptr) {
        if (BuildModel(options, modelName, compatibleModelBuffer, builtModel) != SUCCESS) {
            return FAILURE;
        }
        if (builtModel != nullptr) {
            builtModel->SetCustomData(customData);
        }
        return SUCCESS;
    }
#endif
    return BuildModel(options, modelName, modelBuffer, builtModel);
}

Status ModelBuilderImpl::Build(const ModelBuildOptions& options, const std::string& modelName,
    const std::string& modelFile, std::shared_ptr<IBuiltModel>& builtModel)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    buffer_ = FileUtil::LoadToBuffer(modelFile);
    if (buffer_ == nullptr) {
        return FAILURE;
    }

    std::shared_ptr<IBuffer> localBuffer =
        CreateLocalBuffer(static_cast<void*>(buffer_->MutableData()), buffer_->GetSize(), false);

    return Build(options, modelName, localBuffer, builtModel);
}

std::shared_ptr<IModelBuilder> CreateModelBuilder()
{
#ifdef __OHOS__
    if (!NDKUtil::CanDlopenVendorSo()) {
        return CreateModelBuilderFromNDK();
    }
#endif
    return make_shared_nothrow<ModelBuilderImpl>();
}

} // namespace hiai
