/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hiai_ir_build_model.h"
#include "framework/graph/utils/graph_utils.h"
#include "framework/graph/core/node/node_spec.h"
#include "framework/graph/utils/attr_utils.h"
#include "framework/graph/debug/ge_graph_attr_define.h"
#include "framework/graph/core/cgraph/graph_list_walker.h"
#include "model_builder/model_builder.h"
#include "infra/base/assertion.h"
#include "framework/infra/log/log.h"

namespace hiai {
hiai::Status SetNetoutput(ge::Model& irModel)
{
    ge::ComputeGraphPtr graph = ge::GraphUtils::GetComputeGraph(irModel.GetGraph());
    HIAI_EXPECT_NOT_NULL(graph);

    auto isSetOutNodesVisitor = [](ge::Node& node) {
        std::vector<int64_t> output_type;
        ge::OpDesc& opdesc = node.ROLE(NodeSpec).OpDesc();
        (void)ge::AttrUtils::GetListInt(opdesc, "output_type", output_type);
        size_t outTypeLen = output_type.size();
        HIAI_EXPECT_TRUE(opdesc.GetInputsDescSize() >= 0);
        uint32_t inputSize = static_cast<uint32_t>(opdesc.GetInputsDescSize());
        size_t outSize = opdesc.GetOutputsSize();
        if (outTypeLen > 0) {
            (void)opdesc.DelAttr("output_type");
            if ((outTypeLen != inputSize) || (inputSize != outSize) || (outTypeLen != outSize)) {
                FMK_LOGE("output_type size %zu inputsnum %zu src_types nums %zu not equal",
                    outTypeLen, inputSize, outSize);
                return FAILURE;
            }
            for (size_t i = 0; i < outTypeLen; ++i)  {
                ge::TensorDescPtr curDesc = opdesc.MutableOutputDesc(i);
                (void)ge::AttrUtils::SetBool(curDesc, hiai::ATTR_NAME_IS_OUTPUT_DATATYPE_SET, true);
                curDesc->SetDataType(static_cast<ge::DataType>(output_type[i]));
            }
        }
        return SUCCESS;
    };
    return graph->ROLE(GraphListWalker).WalkOutNodes(std::move(isSetOutNodesVisitor));
}

std::shared_ptr<hiai::IBuiltModel> BuildModel(
    const hiai::ModelBuildOptions& buildOptions, const std::string& modelName, ge::Model& irModel)
{
    std::shared_ptr<IModelBuilder> modelBuilder = CreateModelBuilder();
    HIAI_EXPECT_NOT_NULL_R(modelBuilder, nullptr);
    HIAI_EXPECT_EXEC_R(SetNetoutput(irModel), nullptr);
    ge::Buffer irModelBuff;
    if (irModel.Save(irModelBuff) != ge::GRAPH_SUCCESS) {
        FMK_LOGE("serialize IR model failed");
        return nullptr;
    }
    void* data = static_cast<void*>(const_cast<uint8_t*>(irModelBuff.GetData()));
    std::shared_ptr<IBuffer> inputBuffer = CreateLocalBuffer(data, irModelBuff.GetSize());
    HIAI_EXPECT_NOT_NULL_R(inputBuffer, nullptr);

    std::shared_ptr<IBuiltModel> builtModel = nullptr;
    auto ret = modelBuilder->Build(buildOptions, modelName, inputBuffer, builtModel);
    if (ret != SUCCESS || builtModel == nullptr) {
        FMK_LOGE("IR API build failed.");
    }
    return builtModel;
}
}