/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_builder/hiai_ir_build_v2.h"
#include "hiai_ir_build_model.h"

#include <dlfcn.h>

#include "framework/infra/log/log_fmk_interface.h"
#include "framework/infra/log/log.h"
#include "infra/base/assertion.h"
#include "framework/graph/utils/graph_utils.h"
#include "framework/graph/utils/attr_utils.h"
#include "framework/compatible/ir_transformer.h"
#include "model/built_model_aipp.h"

namespace hiai {
namespace {
class ExtendedCompatibleModel {
public:
    explicit ExtendedCompatibleModel(ge::Model* model)
    {
        model_ = model;
    }

    ge::Model* model_;
    std::string aippConfig_;
};

static hiai::Status MakeAippCompatible(ExtendedCompatibleModel& extendedModel)
{
    ge::ComputeGraphPtr graph = ge::GraphUtils::GetComputeGraph(extendedModel.model_->GetGraph());
    HIAI_EXPECT_NOT_NULL(graph);

    const char* HIAI_IR_BUILD_AIPP = "libhiai_ir_build_aipp.so";
    void* handle = dlopen(HIAI_IR_BUILD_AIPP, RTLD_LAZY);
    if (handle == nullptr) {
        FMK_LOGW("have no libhiai_ir_build_aipp.so!");
        return hiai::SUCCESS;
    }
    auto func = reinterpret_cast<Status (*)(ge::ComputeGraph&, std::string&)>(
        dlsym(handle, "GenerateAippCompatibleInfoAdapter"));
    if (func == nullptr) {
        FMK_LOGW("dlsym GenerateAippCompatibleInfoAdapter failed");
        dlclose(handle);
        return hiai::FAILURE;
    }
    Status status = func(*graph, extendedModel.aippConfig_);
    dlclose(handle);
    return status;
}

static std::unique_ptr<ExtendedCompatibleModel> MakeCompatibleModel(const std::shared_ptr<ge::Model>& model)
{
    HIAI_EXPECT_NOT_NULL_R(model, nullptr);

    (void)ge::AttrUtils::SetInt(&*model, "stream_num", 1);
    std::unique_ptr<ExtendedCompatibleModel> compatibleModel = ge::make_unique<ExtendedCompatibleModel>(model.get());

    ge::ComputeGraphPtr graph = ge::GraphUtils::GetComputeGraph(model->GetGraph());
    HIAI_EXPECT_NOT_NULL_R(graph, nullptr);

    HIAI_EXPECT_TRUE_R(hiai::IRTransformer::VerifyIrReservedField(graph), nullptr);

    HIAI_EXPECT_EXEC_R(MakeAippCompatible(*compatibleModel.get()), nullptr);

    return compatibleModel;
}
} // namespace

static std::shared_ptr<hiai::IBuiltModel> BuildCompatibleModel(const hiai::ModelBuildOptions& options,
    const std::string& modelName, std::unique_ptr<ExtendedCompatibleModel>& compatibleModel)
{
    auto builtModel = BuildModel(options, modelName, *(compatibleModel->model_));
    HIAI_EXPECT_NOT_NULL_R(builtModel, nullptr);

    if (compatibleModel->aippConfig_.size() > 0) {
        hiai::CustomModelData customModelData {hiai::AIPP_PREPROCESS_TYPE, compatibleModel->aippConfig_};
        builtModel->SetCustomData(customModelData);
    }

    FMK_LOGI("Build ir model success.");
    return builtModel;
}

GRAPH_API_EXPORT Status HiaiIrBuildV2::Build(const hiai::ModelBuildOptions& options, const std::string& modelName,
    const std::shared_ptr<ge::Model>& model, std::shared_ptr<hiai::IBuiltModel>& builtModel)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    auto compatibleModel = MakeCompatibleModel(model);
    HIAI_EXPECT_NOT_NULL(compatibleModel);

    builtModel = BuildCompatibleModel(options, modelName, compatibleModel);
    HIAI_EXPECT_NOT_NULL(builtModel);

    return hiai::SUCCESS;
}
}