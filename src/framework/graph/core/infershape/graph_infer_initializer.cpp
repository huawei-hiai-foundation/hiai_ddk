/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "graph_infer_initializer.h"

#include <dlfcn.h>

#include "framework/infra/log/log.h"

namespace ge {
const char* const INFERSHAPE_LIB_NAME = "libhiai_ir_infershape.so";

GraphInferInitializer::GraphInferInitializer() noexcept
{
    std::lock_guard<std::mutex> guard(loadSoMutex);
    handle_ = dlopen(INFERSHAPE_LIB_NAME, RTLD_NOW | RTLD_NODELETE);
    if (handle_ == nullptr) {
        FMK_LOGE("load %s failed. errmsg[%s]", INFERSHAPE_LIB_NAME, dlerror());
        return;
    }
}

GraphInferInitializer::~GraphInferInitializer()
{
    std::lock_guard<std::mutex> guard(loadSoMutex);
    if (handle_ != nullptr && dlclose(handle_) != 0) {
        FMK_LOGE("unload %s failed. errmsg[%s]", INFERSHAPE_LIB_NAME, dlerror());
        return;
    }
    handle_ = nullptr;
}

GraphInferInitializer g_graphInferInitializer;
} // namespace ge