/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_C_API_SRC_RESOURCE_MANAGER_H_
#define GE_C_API_SRC_RESOURCE_MANAGER_H_

#include <unordered_map>
#include "graph/attributes_holder.h"
#include "c/ddk/graph/handle_types.h"
#include "framework/infra/log/log.h"

class ResourceManager {
public:
    ResourceManager()
    {
    }

    ~ResourceManager()
    {
        ptrResPool_.clear();
    }

    void StoreSrcPtr(const BasePtr& srcPtr)
    {
        (void)ptrResPool_.insert(std::make_pair(reinterpret_cast<Handle>(srcPtr.get()), srcPtr));
    }

    BasePtr GetSrcPtr(ConstHandle ptr)
    {
        auto iter = ptrResPool_.find(ptr);
        if (iter == ptrResPool_.end()) {
            FMK_LOGE("The key handle %p is not exist in resource pool.", ptr);
            return nullptr;
        }
        return iter->second;
    }

    void ReleaseSrcPtr(ConstHandle ptr)
    {
        auto iter = ptrResPool_.find(ptr);
        if (iter != ptrResPool_.end()) {
            (void)ptrResPool_.erase(iter);
        }
    }

private:
    std::unordered_map<ConstHandle, BasePtr> ptrResPool_{};
};

#endif  // GE_C_API_SRC_RESOURCE_MANAGER_H_