/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "proto_attr_list_int_def.h"

namespace hiai {

ProtoAttrListIntDef::ProtoAttrListIntDef(hiai::proto::AttrDef_ListListInt_ListInt &attrListIntDef)
    : attrListIntDef_(attrListIntDef)
{}

ge::AttrValue::ValueType ProtoAttrListIntDef::GetValueType() const
{
    return ge::AttrValue::VT_LIST_INT;
}

void ProtoAttrListIntDef::SetValueType(ge::AttrValue::ValueType type)
{
    (void)type; /* -Wunused-parameter */
}

SerializeType ProtoAttrListIntDef::GetSerializeType() const
{
    return PROTOBUF;
}

ProtoAttrListIntDef::~ProtoAttrListIntDef()
{}

void ProtoAttrListIntDef::CopyFrom(const IAttrListIntDef *other)
{
    if (other != nullptr && other->GetSerializeType() == PROTOBUF) {
        attrListIntDef_ = static_cast<const ProtoAttrListIntDef *>(other)->attrListIntDef_;
    }
}

IMPL_PROTO_PERSISTENCE_BASIC_LIST_MEMBER_PURE_FUNC(ProtoAttrListIntDef, attrListIntDef_, int64_t, list_i, -1);

}  // namespace hiai