/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "proto_attr_list_list_float_def.h"
#include "proto_attr_list_float_def.h"

namespace hiai {

ProtoAttrListListFloatDef::ProtoAttrListListFloatDef(
    hiai::proto::AttrDef_ListListFloat& attrListListFloatDef) : attrListListFloatDef_(attrListListFloatDef)
{
}

ge::AttrValue::ValueType ProtoAttrListListFloatDef::GetValueType() const
{
    return ge::AttrValue::VT_LIST_LIST_FLOAT;
}

void ProtoAttrListListFloatDef::SetValueType(ge::AttrValue::ValueType type)
{
    (void)type; /* -Wunused-parameter */
}

SerializeType ProtoAttrListListFloatDef::GetSerializeType() const
{
    return PROTOBUF;
}

ProtoAttrListListFloatDef::~ProtoAttrListListFloatDef()
{
    IMPL_PROTO_CUSTOM_LIST_MEMBER_FREE(list_list_f);
}

void ProtoAttrListListFloatDef::CopyFrom(const IAttrListListFloatDef* other)
{
    if (other != nullptr && other->GetSerializeType() == PROTOBUF) {
        attrListListFloatDef_ = static_cast<const ProtoAttrListListFloatDef*>(other)->attrListListFloatDef_;
    }
}

IMPL_PROTO_PERSISTENCE_CUSTOM_LIST_MEMBER_PURE_FUNC(
    ProtoAttrListListFloatDef, attrListListFloatDef_, IAttrListFloatDef, ProtoAttrListFloatDef, list_list_f);

} // namespace hiai