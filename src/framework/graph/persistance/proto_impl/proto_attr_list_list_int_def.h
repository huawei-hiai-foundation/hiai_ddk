/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_GRAH_PERSISTENCE_PROTO_PROTO_ATTR_LIST_LIST_INT_DEF_H
#define FRAMEWORK_GRAH_PERSISTENCE_PROTO_PROTO_ATTR_LIST_LIST_INT_DEF_H
#include "graph/persistance/interface/attr_list_list_int_def.h"
#include "proto_func_macro_def.h"

namespace hiai {
class ProtoAttrListListIntDef : public IAttrListListIntDef {
public:
    explicit ProtoAttrListListIntDef(hiai::proto::AttrDef_ListListInt& attrListListIntDef);
    ~ProtoAttrListListIntDef() override;

private:
    ge::AttrValue::ValueType GetValueType() const override;
    void SetValueType(ge::AttrValue::ValueType type) override;

    void CopyFrom(const IAttrListListIntDef* other) override;
    SerializeType GetSerializeType() const override;

    DEF_PROTO_PERSISTENCE_CUSTOM_LIST_MEMBER_PURE_FUNC(IAttrListIntDef, list_list_i);

private:
    hiai::proto::AttrDef_ListListInt& attrListListIntDef_;
};
} // namespace hiai

#endif