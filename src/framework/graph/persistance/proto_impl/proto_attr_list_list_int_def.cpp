/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "proto_attr_list_list_int_def.h"
#include "proto_attr_list_int_def.h"

namespace hiai {

ProtoAttrListListIntDef::ProtoAttrListListIntDef(hiai::proto::AttrDef_ListListInt &attrListListIntDef)
    : attrListListIntDef_(attrListListIntDef)
{}

ge::AttrValue::ValueType ProtoAttrListListIntDef::GetValueType() const
{
    return ge::AttrValue::VT_LIST_LIST_INT;
}

void ProtoAttrListListIntDef::SetValueType(ge::AttrValue::ValueType type)
{
    (void)type; /* -Wunused-parameter */
}

SerializeType ProtoAttrListListIntDef::GetSerializeType() const
{
    return PROTOBUF;
}

ProtoAttrListListIntDef::~ProtoAttrListListIntDef()
{
    IMPL_PROTO_CUSTOM_LIST_MEMBER_FREE(list_list_i);
}

void ProtoAttrListListIntDef::CopyFrom(const IAttrListListIntDef *other)
{
    if (other != nullptr && other->GetSerializeType() == PROTOBUF) {
        attrListListIntDef_ = static_cast<const ProtoAttrListListIntDef *>(other)->attrListListIntDef_;
    }
}

IMPL_PROTO_PERSISTENCE_CUSTOM_LIST_MEMBER_PURE_FUNC(
    ProtoAttrListListIntDef, attrListListIntDef_, IAttrListIntDef, ProtoAttrListIntDef, list_list_i);

}  // namespace hiai