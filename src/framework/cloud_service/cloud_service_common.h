/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
 * Description: cloud service common
 */
#ifndef CLOUD_SERVICE_COMMON_H
#define CLOUD_SERVICE_COMMON_H
#include "cloud_service_loader.h"

namespace hiai {
class CLOUD_API_EXPORT CloudServiceCommon {
public:
    CloudServiceCommon(const char* loadLibName);
    ~CloudServiceCommon();
    void* GetSymbol(const char* symbol);

private:
    CloudServiceCommon(const CloudServiceCommon&) = delete;
    CloudServiceCommon& operator=(const CloudServiceCommon&) = delete;

private:
    CloudServiceLoader* dlHelper_ = nullptr;
};
} // namespace hiai
#endif // CLOUD_SERVICE_COMMON_H
