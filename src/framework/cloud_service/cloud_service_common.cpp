/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2022. All rights reserved.
 * Description: cloud service common
 */

#include "cloud_service_common.h"

#include "framework/infra/log/log.h"

namespace hiai {
CloudServiceCommon::CloudServiceCommon(const char* loadLibName)
{
    dlHelper_ = new (std::nothrow) CloudServiceLoader();
    if (dlHelper_ == nullptr) {
        FMK_LOGE("dl helper is nullptr.");
        return;
    }
    if (!dlHelper_->Init(loadLibName)) {
        delete dlHelper_;
        dlHelper_ = nullptr;
        return;
    }
}

CloudServiceCommon::~CloudServiceCommon()
{
    if (dlHelper_ != nullptr) {
        dlHelper_->Deinit();
        delete dlHelper_;
        dlHelper_ = nullptr;
    }
}

void* CloudServiceCommon::GetSymbol(const char* symbol)
{
    if (dlHelper_ == nullptr) {
        return nullptr;
    }
    return dlHelper_->GetSymbol(symbol);
}
} // namespace hiai
