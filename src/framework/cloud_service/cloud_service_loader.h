#ifndef CLOUD_SERVICE_LOADER_H
#define CLOUD_SERVICE_LOADER_H
#include <string>
#include <mutex>
#include <map>

#define CLOUD_API_EXPORT __attribute__((__visibility__("default")))

namespace hiai {
class CLOUD_API_EXPORT CloudServiceLoader {
public:
    CloudServiceLoader();
    ~CloudServiceLoader();

    bool Init(const char* libName);
    void Deinit();

    bool IsValid();
    void* GetSymbol(const char* symbol);

private:
    void* handle_;
    std::mutex funcMapMutex_;
    std::map<const char*, void*> funcMap_;
};
} // namespace hiai

#endif
