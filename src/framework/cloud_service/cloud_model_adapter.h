/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Description: cloud_model_adapter
 *
 */

#ifndef CLOUD_MODEL_ADAPTER_H
#define CLOUD_MODEL_ADAPTER_H

#include "framework/graph/core/cgraph/compute_graph.h"
#include "framework/cl_itf/graph_optimizer.h"
#include "framework/ge_error_code.h"
#include "framework/cl_itf/graph_compiler.h"
#include "base/common/cl_manager/initializer.h"

#ifdef __cplusplus
extern "C" {
#endif
AICP_API_EXPORT int HIAI_GraphOptimizer_Optimize(
    ge::GraphOptimizerOptions& options, ge::ComputeGraphPtr& graphPtr, int& stage);
AICP_API_EXPORT int HIAI_OpsKernelInfoStore_CheckSupported(
    const ge::ComputeGraphPtr graphPtr, std::vector<std::string>& checkRst);
AICP_API_EXPORT int HIAI_ModelBuilder_Build(const ge::CompileOptions& options,
    ge::ComputeGraphPtr graphPtr, std::shared_ptr<ge::CompiledTarget>& compiledTarget);
AICP_API_EXPORT int HIAI_Init(std::map<std::string, std::string>& options);
AICP_API_EXPORT void HIAI_Deinit();
AICP_API_EXPORT void HIAI_Stats_PluginVersion(const char* pluginVersionNum);
#ifdef __cplusplus
} // extern "C"
#endif
namespace hiai {
class HCS_API_EXPORT CloudModelAdapter {
public:
    ge::Status CheckSupported(const ge::ComputeGraphPtr graphPtr, std::vector<std::string>& nodeNameVec);
    ge::Status Optimize(ge::GraphOptimizerOptions& options, ge::ComputeGraphPtr& graphPtr, int& stage);
    ge::Status Compile(const ge::CompileOptions& options,
        ge::ComputeGraphPtr graphPtr, std::shared_ptr<ge::CompiledTarget>& compiledTarget);
};
} // namespace hiai
#endif
