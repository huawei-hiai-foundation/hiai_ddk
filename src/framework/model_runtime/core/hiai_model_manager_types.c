/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "framework/c/hiai_model_manager_init_options.h"
#include "c/ddk/model_manager/hiai_model_manager_init_options.h"

#include <pthread.h>
#include <stdbool.h>

#ifdef AI_SUPPORT_LEGACY_ROM_COMPATIBLE
#include "hiai_model_manager_options_v1.h"
#endif

#include "hiai_model_runtime_repo.h"
#include "securec.h"
#include "framework/infra/log/log.h"

typedef struct ModelInitOptionsFuncs {
    HIAI_MR_ModelInitOptions* (*create)(void);

    void (*setPerfMode)(HIAI_MR_ModelInitOptions*, HIAI_PerfMode);
    HIAI_PerfMode (*getPerfMode)(const HIAI_MR_ModelInitOptions*);

    void (*setBuildOptions)(HIAI_MR_ModelInitOptions*, HIAI_MR_ModelBuildOptions*);
    HIAI_MR_ModelBuildOptions* (*getBuildOptions)(const HIAI_MR_ModelInitOptions*);

    void (*destroy)(HIAI_MR_ModelInitOptions**);

    void (*setBandMode)(HIAI_MR_ModelInitOptions*, HIAI_PerfMode);
    HIAI_PerfMode (*getBandMode)(const HIAI_MR_ModelInitOptions*);

    void (*setOmOptions)(HIAI_MR_ModelInitOptions*, HIAI_MR_ModelInitOmOptions*);
    HIAI_MR_ModelInitOmOptions* (*getOmOptions)(HIAI_MR_ModelInitOptions*);
} ModelInitOptionsFuncs;

typedef struct ModelInitOmOptionsFuncs {
    HIAI_MR_ModelInitOmOptions* (*createFunc)(void);
    void (*destroyFunc)(HIAI_MR_ModelInitOmOptions**);
    void (*setOmType)(HIAI_MR_ModelInitOmOptions*, int);
    void (*setOmOutDir)(HIAI_MR_ModelInitOmOptions*, const char*);
    int (*setOmAttr)(HIAI_MR_ModelInitOmOptions*, const char*, const char*);
} ModelInitOmOptionsFuncs;

static void InitModelInitOmOptionsFuncs(ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs)
{
    HIAI_ModelRuntime* runtime = HIAI_ModelRuntimeRepo_GetSutiableHclRuntime();
    if (runtime != NULL && runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_CREATE] != NULL) {
        modelInitOmOptionsFuncs->createFunc = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_CREATE];
        modelInitOmOptionsFuncs->setOmType = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_OMTYPE];
        modelInitOmOptionsFuncs->setOmOutDir = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_OMOUTDIR];
        modelInitOmOptionsFuncs->setOmAttr = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_SETATTR];
        modelInitOmOptionsFuncs->destroyFunc = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_OMOPTIONS_DESTROY];
    }
}

static pthread_mutex_t g_initOptionFuncsInitMutex = PTHREAD_MUTEX_INITIALIZER;
static ModelInitOmOptionsFuncs* GetModelInitOmOptionsFuncs(void)
{
    static ModelInitOmOptionsFuncs modelInitOmOptionsFuncs;

    static bool isInited = false;
    if (!isInited) {
        pthread_mutex_lock(&g_initOptionFuncsInitMutex);
        if (!isInited) {
            InitModelInitOmOptionsFuncs(&modelInitOmOptionsFuncs);
            isInited = true;
        }
        pthread_mutex_unlock(&g_initOptionFuncsInitMutex);
    }

    return &modelInitOmOptionsFuncs;
}

static void InitModelInitOptionsFuncs(ModelInitOptionsFuncs* modelInitOptionsFuncs)
{
    HIAI_ModelRuntime* runtime = HIAI_ModelRuntimeRepo_GetSutiableHclRuntime();
    if (runtime != NULL && runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_CREATE] != NULL) {
        modelInitOptionsFuncs->create = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_CREATE];
        modelInitOptionsFuncs->setPerfMode = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_SETPERFMODE];
        modelInitOptionsFuncs->getPerfMode = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_GETPERFMODE];
        modelInitOptionsFuncs->setBuildOptions = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_SETBUILDOPTIONS];
        modelInitOptionsFuncs->getBuildOptions = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_GETBUILDOPTIONS];
        modelInitOptionsFuncs->destroy = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_DESTROY];
        modelInitOptionsFuncs->setBandMode = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_SETBANDMODE];
        modelInitOptionsFuncs->getBandMode = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_GETBANDMODE];
        modelInitOptionsFuncs->setOmOptions = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_SETOMOPTIONS];
        modelInitOptionsFuncs->getOmOptions = runtime->optionsSymbolList[HIAI_MODELINITOPTIONS_GETOMOPTIONS];
        return;
    }
#ifdef AI_SUPPORT_LEGACY_ROM_COMPATIBLE
    // if you need add new func, don't add here.
    modelInitOptionsFuncs->create = HIAI_ModelInitOptionsV1_Create;
    modelInitOptionsFuncs->setPerfMode = HIAI_ModelInitOptionsV1_SetPerfMode;
    modelInitOptionsFuncs->getPerfMode = HIAI_ModelInitOptionsV1_GetPerfMode;
    modelInitOptionsFuncs->setBuildOptions = HIAI_ModelInitOptionsV1_SetBuildOptions;
    modelInitOptionsFuncs->getBuildOptions = HIAI_ModelInitOptionsV1_GetBuildOptions;
    modelInitOptionsFuncs->destroy = HIAI_ModelInitOptionsV1_Destroy;
#endif
}

static ModelInitOptionsFuncs* GetModelInitOptionsFuncs(void)
{
    static ModelInitOptionsFuncs modelInitOptionsFuncs;

    static bool isInited = false;
    if (!isInited) {
        pthread_mutex_lock(&g_initOptionFuncsInitMutex);
        if (!isInited) {
            InitModelInitOptionsFuncs(&modelInitOptionsFuncs);
            isInited = true;
        }
        pthread_mutex_unlock(&g_initOptionFuncsInitMutex);
    }

    return &modelInitOptionsFuncs;
}

HIAI_MR_ModelInitOptions* HIAI_MR_ModelInitOptions_Create(void)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->create != NULL) {
        return modelInitOptionsFuncs->create();
    }
    return NULL;
}

void HIAI_MR_ModelInitOptions_SetBandMode(HIAI_MR_ModelInitOptions* options, HIAI_PerfMode bandMode)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->setBandMode != NULL) {
        modelInitOptionsFuncs->setBandMode(options, bandMode);
    }
}

HIAI_PerfMode HIAI_MR_ModelInitOptions_GetBandMode(const HIAI_MR_ModelInitOptions* options)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->getBandMode != NULL) {
        return modelInitOptionsFuncs->getBandMode(options);
    }
    return HIAI_PERFMODE_UNSET;
}

void HIAI_MR_ModelInitOptions_SetPerfMode(HIAI_MR_ModelInitOptions* options, HIAI_PerfMode devPerf)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->setPerfMode != NULL) {
        modelInitOptionsFuncs->setPerfMode(options, devPerf);
    }
}

HIAI_PerfMode HIAI_MR_ModelInitOptions_GetPerfMode(const HIAI_MR_ModelInitOptions* options)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->getPerfMode != NULL) {
        return modelInitOptionsFuncs->getPerfMode(options);
    }
    return HIAI_PERFMODE_UNSET;
}

HIAI_MR_ModelInitOmOptions* HIAI_MR_ModelInitOmOptions_Create()
{
    ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs = GetModelInitOmOptionsFuncs();
    if (modelInitOmOptionsFuncs->createFunc != NULL) {
        return modelInitOmOptionsFuncs->createFunc();
    }
    return NULL;
}

void HIAI_MR_ModelInitOmOptions_SetOmType(HIAI_MR_ModelInitOmOptions* omOptions, int omType)
{
    ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs = GetModelInitOmOptionsFuncs();
    if (modelInitOmOptionsFuncs->setOmType != NULL) {
        modelInitOmOptionsFuncs->setOmType(omOptions, omType);
    }
}

void HIAI_MR_ModelInitOmOptions_SetOmOutDir(HIAI_MR_ModelInitOmOptions* omOptions, const char* omOutDir)
{
    ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs = GetModelInitOmOptionsFuncs();
    if (modelInitOmOptionsFuncs->setOmOutDir != NULL) {
        modelInitOmOptionsFuncs->setOmOutDir(omOptions, omOutDir);
    }
}

int HIAI_MR_ModelInitOmOptions_SetOmAttr(HIAI_MR_ModelInitOmOptions* omOptions, const char* key, const char* value)
{
    ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs = GetModelInitOmOptionsFuncs();
    if (modelInitOmOptionsFuncs->setOmAttr != NULL) {
        return modelInitOmOptionsFuncs->setOmAttr(omOptions, key, value);
    }
    return -1;
}

void HIAI_MR_ModelInitOmOptions_Destroy(HIAI_MR_ModelInitOmOptions** omOptions)
{
    ModelInitOmOptionsFuncs* modelInitOmOptionsFuncs = GetModelInitOmOptionsFuncs();
    if (modelInitOmOptionsFuncs->destroyFunc != NULL) {
        return modelInitOmOptionsFuncs->destroyFunc(omOptions);
    }
}

void HIAI_MR_ModelInitOptions_SetOmOptions(HIAI_MR_ModelInitOptions* options, HIAI_MR_ModelInitOmOptions* omOptions)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->setOmOptions != NULL) {
        modelInitOptionsFuncs->setOmOptions(options, omOptions);
    }
}

HIAI_MR_ModelInitOmOptions* HIAI_MR_ModelInitOptions_GetOmOptions(HIAI_MR_ModelInitOptions* options)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->getOmOptions != NULL) {
        return modelInitOptionsFuncs->getOmOptions(options);
    }
    return NULL;
}

void HIAI_MR_ModelInitOptions_SetBuildOptions(
    HIAI_MR_ModelInitOptions* options, HIAI_MR_ModelBuildOptions* buildOptions)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->setBuildOptions != NULL) {
        modelInitOptionsFuncs->setBuildOptions(options, buildOptions);
    }
}

HIAI_MR_ModelBuildOptions* HIAI_MR_ModelInitOptions_GetBuildOptions(const HIAI_MR_ModelInitOptions* options)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->getBuildOptions != NULL) {
        return modelInitOptionsFuncs->getBuildOptions(options);
    }
    return NULL;
}

void HIAI_MR_ModelInitOptions_Destroy(HIAI_MR_ModelInitOptions** options)
{
    ModelInitOptionsFuncs* modelInitOptionsFuncs = GetModelInitOptionsFuncs();
    if (modelInitOptionsFuncs->destroy != NULL) {
        modelInitOptionsFuncs->destroy(options);
    }
}
