/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FRAMEWORK_MODEL_RUNTIME_DIRECT_DIRECT_MODEL_MANAGER_UTIL_H
#define FRAMEWORK_MODEL_RUNTIME_DIRECT_DIRECT_MODEL_MANAGER_UTIL_H

#include <string>
#include <memory>

#include "framework/c/compatible/HIAIModelManager.h"
#include "framework/c/compatible/hiai_model_buffer.h"
#include "c/hcl/hiai_execute_option_types.h"
#include "infra/base/base_buffer.h"

namespace hiai {

struct ModelLoadInfo {
    std::string modelName {""};
    std::string modelFile {""};
    std::shared_ptr<BaseBuffer> modelBuffer {nullptr};
    HIAI_PerfMode perf {HIAI_PerfMode::HIAI_PERFMODE_UNSET};
};

using FreeBufferFunc = std::function<void(HIAI_ModelBuffer*)>;
class DirectModelManagerUtil {
public:
    DirectModelManagerUtil() = default;
    ~DirectModelManagerUtil() = default;

    static HIAI_ModelManager* CreateModelManager(HIAI_ModelManagerListenerLegacy* listener);
    static void DestroyModelManager(HIAI_ModelManager** manager);

    static int LoadModel(HIAI_ModelManager* manager, const ModelLoadInfo& options);
    static int LoadModel(HIAI_ModelManager* manager, HIAI_ModelBuffer* buffer);
    static int UnLoadModel(HIAI_ModelManager* manager);

    static std::unique_ptr<HIAI_ModelBuffer, FreeBufferFunc> CreateModelBuffer(const ModelLoadInfo& loadInfo);
};
} // namespace hiai
#endif // FRAMEWORK_MODEL_RUNTIME_DIRECT_DIRECT_MODEL_MANAGER_UTIL_H