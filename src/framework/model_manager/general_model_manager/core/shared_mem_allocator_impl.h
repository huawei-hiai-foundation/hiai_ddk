/**
 * Copyright 2022-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_INC_MODEL_MANAGER_SHARED_MEM_ALLOCATOR_IMPL_H
#define FRAMEWORK_INC_MODEL_MANAGER_SHARED_MEM_ALLOCATOR_IMPL_H

#include "model_manager/model_manager_ext.h"
#include "c/hcl/hiai_native_handle.h"

namespace hiai {
class SharedMemAllocatorImpl : public ISharedMemAllocator {
public:
    SharedMemAllocatorImpl(HIAI_ModelManagerMemAllocator_AllocateFun allocateFun,
        HIAI_ModelManagerMemAllocator_FreeFun freeFun, void* config);
    SharedMemAllocatorImpl() = default;
    ~SharedMemAllocatorImpl() override;

public:
    std::vector<NativeHandle> Allocate(size_t size) override;
    void Free(std::vector<NativeHandle>& handles) override;

private:
    static const uint32_t NATIVE_HANDLE_MAX = 10; /* 10: NativeHandle specification */

private:
    HIAI_ModelManagerMemAllocator_AllocateFun allocateFun_ {nullptr};
    HIAI_ModelManagerMemAllocator_FreeFun freeFun_ {nullptr};
    void* config_ {nullptr};
};
} // namespace hiai

#endif