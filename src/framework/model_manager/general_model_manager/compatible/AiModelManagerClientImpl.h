/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_MANAGER_CLIENT_IMPL_H
#define FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_MANAGER_CLIENT_IMPL_H

#include <mutex>

#include "compatible/HiAiModelManagerService.h"
#include "model_manager/model_manager.h"

namespace hiai {
class AiModelMngerClientImpl {
public:
    AiModelMngerClientImpl();
    virtual ~AiModelMngerClientImpl();

    AIStatus Init(std::shared_ptr<AiModelManagerClientListener> listener);

    AIStatus Load(std::vector<std::shared_ptr<AiModelDescription>>& pmodelDesc);

    AIStatus Process(AiContext& context, std::vector<std::shared_ptr<AiTensor>>& pinputTensor,
        std::vector<std::shared_ptr<AiTensor>>& poutputTensor, uint32_t timeout, int32_t& piStamp);

    AIStatus CheckModelCompatibility(AiModelDescription& pmodelDesc, bool& pisModelCompatibility);

    AIStatus GetModelIOTensorDim(const std::string& pmodelName, std::vector<TensorDimension>& pinputTensor,
        std::vector<TensorDimension>& poutputTensor);

    AIStatus GetModelAippPara(const std::string& modelName, std::vector<std::shared_ptr<AippPara>>& aippPara);

    AIStatus GetModelAippPara(
        const std::string& modelName, uint32_t index, std::vector<std::shared_ptr<AippPara>>& aippPara);

    char* GetVersion();

    AIStatus UnLoadModel();

    AIStatus SetModelPriority(const std::string& modelName, ModelPriority modelPriority);

    void Cancel(const std::string& modelName);

private:
    bool inited_ = false;
    // 保存模型对应的ModelManager
    std::map<std::string, std::shared_ptr<IModelManager>> modelManagerMap_;
    // 保存模型对应的BuiltModel
    std::map<std::string, std::shared_ptr<IBuiltModel>> builtModelMap_;
    // 客户端注册的listener
    std::shared_ptr<AiModelManagerClientListener> listener_ = nullptr;
    // 注册给模型管家的listener, 内部含客户端注册的listener
    std::shared_ptr<IModelManagerListener> listenerImpl_ = nullptr;
    std::mutex modelManagerMutex_;
};
}

#endif