/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AiModelManagerClientImpl.h"

#include <unistd.h>
#include <atomic>
#include <algorithm>
#include <climits>

#include "framework/infra/log/log.h"
#include "compatible/AiTensor.h"
#include "framework/infra/log/log_fmk_interface.h"
#include "infra/base/securestl.h"
#include "model_manager/model_manager_types.h"
#include "model_builder/model_builder.h"
#include "model_builder/om/model_build_options_util.h"
#include "model_runtime/core/hiai_model_runtime_repo.h"
#include "infra/base/process_util.h"
#include "infra/om/stats/ai_stats_log_builder.h"
#include "model_manager/model_manager_aipp.h"
#include "model/built_model_aipp.h"
#include "compatible/AippTensor.h"
#include "tensor/compatible/AiTensorImpl.h"
#include "tensor/compatible/AippTensorImpl.h"
#include "util/version_util.h"

namespace hiai {
class AiTensorWrapper : public AiTensor {
public:
    explicit AiTensorWrapper(std::shared_ptr<INDTensorBuffer> tensor) : tensor_(tensor)
    {
    }
    ~AiTensorWrapper() override
    {
    }
    void* GetBuffer() const override
    {
        return tensor_ == nullptr ? nullptr : tensor_->GetData();
    }

    uint32_t GetSize() const override
    {
        return tensor_ == nullptr ? 0 : tensor_->GetSize();
    }

private:
    using AiTensor::GetTensorDimension;
    using AiTensor::Init;
    using AiTensor::SetTensorDimension;

private:
    std::shared_ptr<INDTensorBuffer> tensor_ {nullptr};
};
class ManagerListenerImpl : public IModelManagerListener {
public:
    explicit ManagerListenerImpl(std::shared_ptr<AiModelManagerClientListener> listener) : listenerImpl_(listener)
    {
    }

    ~ManagerListenerImpl() override = default;

    void OnRunDone(const Context& context, Status result,
        std::vector<std::shared_ptr<INDTensorBuffer>>& outputs) override
    {
        H_LOG_INTERFACE_FILTER(ITF_COUNT);
        if (listenerImpl_ == nullptr) {
            FMK_LOGE("listenerImpl_ is null");
            return;
        }

        AiContext contextV1;
        std::map<std::string, std::string> keyValues = context.GetContent();
        for (auto& it : keyValues) {
            contextV1.AddPara(it.first, it.second);
        }

        std::vector<std::shared_ptr<AiTensor>> tensors;
        for (const auto& it : outputs) {
            auto tmp = make_shared_nothrow<AiTensorWrapper>(it);
            if (tmp == nullptr) {
                return;
            }
            tensors.push_back(tmp);
        }

        int32_t piStamp = std::stoi(context.GetValue("task_id"));
        listenerImpl_->OnProcessDone(contextV1, result, tensors, piStamp);
    }

    void OnServiceDied() override
    {
        H_LOG_INTERFACE_FILTER(ITF_COUNT);
        if (listenerImpl_ == nullptr) {
            FMK_LOGE("listenerImpl_ is null");
            return;
        }
        listenerImpl_->OnServiceDied();
    }

private:
    std::shared_ptr<AiModelManagerClientListener> listenerImpl_;
    // 保存推理前AiTensor地址回调给客户端
};

class AIModelMngerClientTaskIdGenerator {
public:
    AIModelMngerClientTaskIdGenerator() = delete;
    ~AIModelMngerClientTaskIdGenerator() = delete;

    /*
     * @brief 生成模型id，模型id保证唯一.
     *
     */
    static uint32_t Generate();
};

static std::atomic<uint32_t> taskIdMaxNum = {0};

uint32_t AIModelMngerClientTaskIdGenerator::Generate()
{
    const int32_t ID_OFFSET = 100000000;
    taskIdMaxNum = (taskIdMaxNum + 1) % ID_OFFSET;
    return taskIdMaxNum;
}

static void GetModelBuildOptions(const AiModelDescription* modelDesc, ModelBuildOptions& buildOptions)
{
    PrecisionMode precisionMode;
    modelDesc->GetPrecisionMode(precisionMode);
    buildOptions.precisionMode = precisionMode;
    DynamicShapeConfig dynamicShapeConfig;
    modelDesc->GetDynamicShapeConfig(dynamicShapeConfig);
    buildOptions.dynamicShapeConfig = dynamicShapeConfig;
    std::vector<TensorDimension> inputDims;
    modelDesc->GetInputDims(inputDims);
    std::vector<NDTensorDesc> inputTensorDescs;
    for (auto& dim : inputDims) {
        NDTensorDesc desc;
        desc.dims = {static_cast<int32_t>(dim.GetNumber()), static_cast<int32_t>(dim.GetChannel()),
            static_cast<int32_t>(dim.GetHeight()), static_cast<int32_t>(dim.GetWidth())};
        desc.dataType = DataType::FLOAT32;
        desc.format = Format::NCHW;
        inputTensorDescs.emplace_back(desc);
    }
    buildOptions.inputTensorDescs = inputTensorDescs;
    ModelDeviceConfig modelDeviceConfig;
    buildOptions.modelDeviceConfig = modelDeviceConfig;
    buildOptions.formatMode = FormatMode::USE_NCHW;
    buildOptions.tuningStrategy = modelDesc->GetTuningStrategy();
}

static AIStatus CovertTensorDimension(
    const std::vector<NDTensorDesc>& inputTensor, std::vector<TensorDimension>& outputTensor)
{
    if (inputTensor.empty()) {
        FMK_LOGE("tensor is empty");
        return AI_FAILED;
    }

    for (auto& it : inputTensor) {
        if (it.dims.size() != 4) {
            FMK_LOGE("invalid dims size %zu, CovertTensorDimension failed", it.dims.size());
            return AI_FAILED;
        }
        TensorDimension dim;
        dim.SetNumber(it.dims[0]);
        dim.SetChannel(it.dims[1]);
        dim.SetHeight(it.dims[2]);
        dim.SetWidth(it.dims[3]);
        outputTensor.push_back(dim);
    }
    return AI_SUCCESS;
}

static void SetPerfMode(int32_t perfMode, ModelInitOptions& initOptions)
{
    initOptions.perfMode = static_cast<PerfMode>(perfMode);
    if (perfMode == static_cast<int32_t>(AiModelDescription_Frequency_MEDIUM_BAND_MEDIUM_COMPUTE_UNIT)) {
        initOptions.bandMode = static_cast<PerfMode>(2);
        initOptions.perfMode = static_cast<PerfMode>(2);
    }
}

static void Convert2Context(AiContext& aiContext, Context& context)
{
    std::vector<std::string> keys;
    AIStatus ret = aiContext.GetAllKeys(keys);
    if (ret != AI_SUCCESS) {
        FMK_LOGW("GetAllKeys return %d", ret);
        return;
    }

    for (auto& key : keys) {
        context.SetValue(key, aiContext.GetPara(key));
    }
}

AiModelMngerClientImpl::AiModelMngerClientImpl()
{
}

AiModelMngerClientImpl::~AiModelMngerClientImpl()
{
}

AIStatus AiModelMngerClientImpl::Init(std::shared_ptr<AiModelManagerClientListener> listener)
{
    inited_ = true;
    listener_ = listener;
    return AI_SUCCESS;
}

AIStatus AiModelMngerClientImpl::Load(std::vector<std::shared_ptr<AiModelDescription>>& pmodelDesc)
{
    if (!inited_) {
        FMK_LOGE("not init, load failed");
        return AI_FAILED;
    }

    if (listener_ != nullptr) {
        listenerImpl_ = make_shared_nothrow<ManagerListenerImpl>(listener_);
        if (listenerImpl_ == nullptr) {
            FMK_LOGE("malloc ManagerListenerImpl return null, load failed");
            return AI_FAILED;
        }
    }

    std::vector<std::string> modelNameList;
    bool isRepeatLoadorError = false;

    for (auto& modelDesc : pmodelDesc) {
        isRepeatLoadorError = true;
        if (modelDesc == nullptr) {
            FMK_LOGE("modelDesc is nullptr");
            break;
        }
        std::string modelName = modelDesc->GetName();
        // 查询之前是否已加载该模型
        std::map<std::string, std::shared_ptr<IModelManager>>::iterator it = modelManagerMap_.find(modelName);
        if (it != modelManagerMap_.end()) {
            FMK_LOGE("%s model already loaded, load failed", modelName.c_str());
            break;
        }

        // 查询当前模型列表中模型名是否重复
        std::vector<std::string>::iterator iter = find(modelNameList.begin(), modelNameList.end(), modelName);
        if (iter != modelNameList.end()) {
            FMK_LOGE("same model name %s, load failed", modelName.c_str());
            break;
        }

        // 创建模型管家
        std::shared_ptr<IModelManager> modelManager = CreateModelManager();
        if (modelManager == nullptr) {
            FMK_LOGE("CreateModelManager return null, load failed");
            break;
        }

        std::shared_ptr<IBuffer> modelBuffer =
            CreateLocalBuffer(modelDesc->GetModelBuffer(), modelDesc->GetModelNetSize(), false);
        if (modelBuffer == nullptr) {
            FMK_LOGE("CreateLocalBuffer return null, load failed");
            break;
        }

        std::shared_ptr<IBuiltModel> builtModel = nullptr;

        ModelBuildOptions buildOptions;
        GetModelBuildOptions(modelDesc.get(), buildOptions);

        ModelInitOptions initOptions;
        int32_t perfMode = modelDesc->GetFrequency();
        SetPerfMode(perfMode, initOptions);

        Status ret = FAILURE;

        if (ModelBuildOptionsUtil::IsHasBuildOptions(buildOptions) && IsHclModelRuntimeCanBuild()) {
            std::shared_ptr<IModelBuilder> modelBuilder = CreateModelBuilder();
            if (modelBuilder == nullptr) {
                FMK_LOGE("CreateModelBuilder return null, load failed");
                break;
            }
            ret = modelBuilder->Build(buildOptions, modelName, modelBuffer, builtModel);
            if (ret != SUCCESS || builtModel == nullptr) {
                FMK_LOGE("build model failed in load", ret);
                break;
            }
        } else {
            builtModel = CreateBuiltModel();
            if (builtModel == nullptr) {
                FMK_LOGE("CreateBuiltModel return null, load failed");
                break;
            }

            ret = builtModel->RestoreFromBuffer(modelBuffer);
            if (ret != SUCCESS) {
                FMK_LOGE("RestoreFromBuffer return %d, load failed", ret);
                break;
            }

            (void)builtModel->SetName(modelName);
            initOptions.buildOptions = buildOptions;
        }

        // 加载模型
        ret = modelManager->Init(initOptions, builtModel, listenerImpl_);
        if (ret != SUCCESS) {
            FMK_LOGE("manager Init return %d, load failed", ret);
            break;
        }
        isRepeatLoadorError = false;
        modelNameList.emplace_back(modelName);
        std::lock_guard<std::mutex> lock(modelManagerMutex_);
        modelManagerMap_.insert(std::map<std::string, std::shared_ptr<IModelManager>>::value_type(modelName, modelManager));
        builtModelMap_.insert(std::map<std::string, std::shared_ptr<IBuiltModel>>::value_type(modelName, builtModel));
    }

    // 模型重复或加载失败场景,卸载所有模型
    if (isRepeatLoadorError) {
        UnLoadModel();
        return AI_FAILED;
    }

    int32_t uid = getpid();
    std::string processName = hiai::GetProcessName();
    AiStatsLogBuilder statslogbuilder;
    statslogbuilder.Stats(uid, AI_STATS_HIAI_MNGR, "InterfaceV1Load", processName.c_str(), AI_SUCCESS);

    return AI_SUCCESS;
}

AIStatus AiModelMngerClientImpl::Process(AiContext& context, std::vector<std::shared_ptr<AiTensor>>& pinputTensor,
    std::vector<std::shared_ptr<AiTensor>>& poutputTensor, uint32_t timeout, int32_t& piStamp)
{
    if (!inited_) {
        FMK_LOGE("not init, Process failed");
        return AI_FAILED;
    }

    std::string modelName = context.GetPara("model_name");
    if (modelName.empty()) {
        FMK_LOGE("Process failed, modelName is empty");
        return AI_FAILED;
    }

    std::map<std::string, std::shared_ptr<IModelManager>>::iterator it = modelManagerMap_.find(modelName);
    if (it == modelManagerMap_.end()) {
        FMK_LOGE("%s not loaded", modelName.c_str());
        return AI_FAILED;
    }

    std::vector<std::shared_ptr<INDTensorBuffer>> inputs;
    std::vector<std::shared_ptr<INDTensorBuffer>> outputs;
    std::vector<std::shared_ptr<IAIPPPara>> aippParasV2;
    std::vector<std::shared_ptr<AippPara>> aippParasV1;

    for (auto& inTensor : pinputTensor) {
        std::shared_ptr<AippTensor> aippTensor = std::dynamic_pointer_cast<AippTensor>(inTensor);
        if (aippTensor != nullptr && aippTensor->GetAiTensor() != nullptr) {
            inputs.push_back(aippTensor->GetAiTensor()->impl_->tensor_);
            aippParasV1 = aippTensor->GetAippParas();
            for (auto& para : aippParasV1) {
                aippParasV2.push_back(para->GetAIPPPara());
            }
        } else {
            inputs.push_back(inTensor->impl_->tensor_);
        }
    }

    for (auto& outTensor : poutputTensor) {
        if (outTensor == nullptr) {
            FMK_LOGE("outTensor is nullptr.");
            return AI_FAILED;
        }
        outputs.push_back(outTensor->impl_->tensor_);
    }

    if (inputs.size() == 0 || outputs.size() == 0) {
        FMK_LOGE("Invalid input or output.");
        return AI_FAILED;
    }

    piStamp = static_cast<int32_t>(AIModelMngerClientTaskIdGenerator::Generate());
    Context context_;
    Convert2Context(context, context_);
    context_.SetValue("task_id", std::to_string(piStamp));
    Status ret = FAILURE;
    if (it->second != nullptr) {
        // 判断是否为AIPP模型
        if (aippParasV2.empty()) {
            if (listener_ == nullptr) {
                ret = it->second->Run(inputs, outputs);
            } else {
                ret = it->second->RunAsync(context_, inputs, outputs, timeout);
            }
        } else {
            std::shared_ptr<IModelManagerAipp> managerAipp = std::dynamic_pointer_cast<IModelManagerAipp>(it->second);
            if (managerAipp != nullptr) {
                ret = managerAipp->RunAippModel(context_, inputs, aippParasV2, outputs, timeout);
            }
        }

        if (ret != SUCCESS) {
            FMK_LOGE("run %s failed", modelName.c_str());
            ret = AI_FAILED;
        }
    }
    return static_cast<AIStatus>(ret);
}

AIStatus AiModelMngerClientImpl::CheckModelCompatibility(AiModelDescription& pmodelDesc, bool& pisModelCompatibility)
{
    std::shared_ptr<IBuffer> modelBuffer = CreateLocalBuffer(pmodelDesc.GetModelBuffer(),
        pmodelDesc.GetModelNetSize(), false);
    if (modelBuffer == nullptr) {
        FMK_LOGE("CreateLocalBuffer return null, %s failed", __func__);
        return AI_FAILED;
    }

    std::shared_ptr<IBuiltModel> builtModel = nullptr;
    builtModel = CreateBuiltModel();
    if (builtModel == nullptr) {
        FMK_LOGE("builtModel is null");
        return AI_FAILED;
    }

    auto ret = builtModel->RestoreFromBuffer(modelBuffer);
    if (ret != AI_SUCCESS) {
        FMK_LOGE("RestoreFromBuffer failed, return %d", ret);
        return AI_FAILED;
    }

    return builtModel->CheckCompatibility(pisModelCompatibility);
}

AIStatus AiModelMngerClientImpl::GetModelIOTensorDim(const std::string& pmodelName, std::vector<TensorDimension>& pinputTensor,
    std::vector<TensorDimension>& poutputTensor)
{
    // 查询是否已加载该模型
    std::map<std::string, std::shared_ptr<IBuiltModel>>::iterator it = builtModelMap_.find(pmodelName);
    if (it == builtModelMap_.end()) {
        FMK_LOGE("%s not loaded, %s failed", pmodelName.c_str(), __func__);
        return AI_FAILED;
    }
    std::vector<NDTensorDesc> inTensorDescs = it->second->GetInputTensorDescs();
    std::vector<NDTensorDesc> outTensorDescs = it->second->GetOutputTensorDescs();
    if (CovertTensorDimension(inTensorDescs, pinputTensor) != AI_SUCCESS) {
        return AI_FAILED;
    }
    if (CovertTensorDimension(outTensorDescs, poutputTensor) != AI_SUCCESS) {
        return AI_FAILED;
    }

    return AI_SUCCESS;
}

static AIStatus GetModelAippParaFromIndex(std::map<std::string, std::shared_ptr<IBuiltModel>> builtModelMap,
    const std::string& modelName, int32_t index, std::vector<std::shared_ptr<AippPara>>& aippParas)
{
    if (index >= INT_MAX) {
        FMK_LOGE("index is too large.");
        return AI_FAILED;
    }
    // 查询是否已加载该模型
    std::map<std::string, std::shared_ptr<IBuiltModel>>::iterator it = builtModelMap.find(modelName);
    if (it == builtModelMap.end()) {
        FMK_LOGE("%s not loaded", modelName.c_str());
        return AI_FAILED;
    }

    std::shared_ptr<IBuiltModelAipp> builtModelAipp = std::dynamic_pointer_cast<IBuiltModelAipp>(it->second);
    if (builtModelAipp == nullptr) {
        FMK_LOGE("is not a aipp model[%s]", modelName.c_str());
        return AI_FAILED;
    }

    uint32_t aippParaNum;
    uint32_t batchCount;
    auto ret = builtModelAipp->GetTensorAippInfo(index, &aippParaNum, &batchCount);
    if (ret != AI_SUCCESS) {
        FMK_LOGE("%s GetTensorAippInfo index %d return %d", modelName.c_str(), index, ret);
        return ret;
    }

    std::vector<std::shared_ptr<IAIPPPara>> tensorAippParas;
    ret = builtModelAipp->GetTensorAippPara(index, tensorAippParas);
    if (ret != AI_SUCCESS) {
        FMK_LOGE("%s GetTensorAippPara index %d return %d", modelName.c_str(), index, ret);
        return ret;
    }

    aippParas.resize(tensorAippParas.size());
    for (size_t i = 0; i < tensorAippParas.size(); i++) {
        aippParas[i] = make_shared_nothrow<AippPara>();
        if (aippParas[i] == nullptr) {
            FMK_LOGE("new AippPara i=%d failed", i);
            return AI_FAILED;
        }
        aippParas[i]->SetAIPPPara(tensorAippParas[i]);
    }
    return AI_SUCCESS;
}

AIStatus AiModelMngerClientImpl::GetModelAippPara(const std::string& modelName, std::vector<std::shared_ptr<AippPara>>& aippPara)
{
    return GetModelAippParaFromIndex(builtModelMap_, modelName, -1, aippPara);
}

AIStatus AiModelMngerClientImpl::GetModelAippPara(
    const std::string& modelName, uint32_t index, std::vector<std::shared_ptr<AippPara>>& aippPara)
{
    return GetModelAippParaFromIndex(builtModelMap_, modelName, index, aippPara);
}

char* AiModelMngerClientImpl::GetVersion()
{
    char* romVersion = const_cast<char*>(VersionUtil::GetVersion());
    return romVersion;
}

AIStatus AiModelMngerClientImpl::UnLoadModel()
{
    std::lock_guard<std::mutex> lock(modelManagerMutex_);
    for (auto& it : modelManagerMap_) {
        if (it.second != nullptr) {
            it.second->DeInit();
        }
    }

    modelManagerMap_.clear();
    builtModelMap_.clear();
    return AI_SUCCESS;
}

AIStatus AiModelMngerClientImpl::SetModelPriority(const std::string& modelName, ModelPriority modelPriority)
{
    if (modelPriority != ModelPriority::PRIORITY_LOW && modelPriority != ModelPriority::PRIORITY_MIDDLE &&
        modelPriority != ModelPriority::PRIORITY_HIGH) {
        FMK_LOGE("invalid priority[%d]", static_cast<int32_t>(modelPriority));
        return AI_FAILED;
    }

    // 查询是否已加载该模型
    std::map<std::string, std::shared_ptr<IModelManager>>::iterator it = modelManagerMap_.find(modelName);
    if (it == modelManagerMap_.end()) {
        FMK_LOGE("%s not loaded", modelName.c_str());
        return AI_FAILED;
    }

    return it->second->SetPriority(modelPriority);
}

void AiModelMngerClientImpl::Cancel(const std::string& modelName)
{
    // 查询是否已加载该模型
    std::map<std::string, std::shared_ptr<IModelManager>>::iterator it = modelManagerMap_.find(modelName);
    if (it == modelManagerMap_.end()) {
        FMK_LOGW("%s not loaded", modelName.c_str());
        return;
    }

    it->second->Cancel();
}
}