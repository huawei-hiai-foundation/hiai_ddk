/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MemBufferImpl.h"
namespace hiai {
void* MemBufferImpl::GetMemBufferData()
{
    return data_;
}

uint32_t MemBufferImpl::GetMemBufferSize()
{
    return size_;
}

void MemBufferImpl::SetMemBufferSize(uint32_t size)
{
    size_ = size;
}
void MemBufferImpl::SetMemBufferData(void* data)
{
    data_ = data;
}
void MemBufferImpl::SetServerMem(void* serverMem)
{
    servermem_ = serverMem;
}
void MemBufferImpl::SetAppAllocFlag(bool isAppAlloc)
{
    isAppAlloc_ = isAppAlloc;
}
void* MemBufferImpl::GetServerMem()
{
    return servermem_;
}
bool MemBufferImpl::GetAppAllocFlag()
{
    return isAppAlloc_;
}
}