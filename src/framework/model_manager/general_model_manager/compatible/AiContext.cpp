/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compatible/HiAiModelManagerType.h"

#include <string>
#include <vector>

#include "framework/infra/log/log_fmk_interface.h"
#include "AiContextImpl.h"
#include "infra/base/assertion.h"
#include "infra/base/securestl.h"

namespace hiai {
AiContext::AiContext()
{
    impl_ = hiai::make_shared_nothrow<AiContextImpl>();
}

AiContext::~AiContext()
{
}

std::string AiContext::GetPara(const std::string& key) const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, "");
    return impl_->GetPara(key);
}

void AiContext::AddPara(const std::string& key, const std::string& value)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->AddPara(key, value);
}

void AiContext::SetPara(const std::string& key, const std::string& value)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->SetPara(key, value);
}

void AiContext::DelPara(const std::string& key)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->DelPara(key);
}

void AiContext::ClearPara()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->ClearPara();
}

AIStatus AiContext::GetAllKeys(std::vector<std::string>& keys)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetAllKeys(keys);
}
} // namespace hiai
