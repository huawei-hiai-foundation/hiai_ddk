/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AiModelBuilderImpl.h"
#include "infra/base/assertion.h"
#include "model_builder/compatible/BuildOptionUtil.h"
#include "model_manager/general_model_manager/compatible/MembufferUtil.h"

namespace hiai {
AiModelBuilderImpl::AiModelBuilderImpl()
{
    modelBuilder_ = CreateModelBuilder();
    HIAI_EXPECT_NOT_NULL_VOID(modelBuilder_);
}

AIStatus AiModelBuilderImpl::BuildModel(
    const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer, uint32_t& poutputModelSize)
{
    HIAI_EXPECT_NOT_NULL_R(modelBuilder_, AI_NOT_INIT);
    BuildOptions options;
    return BuildModel(pinputMemBuffer, poutputModelBuffer, poutputModelSize, options);
}
    
AIStatus AiModelBuilderImpl::BuildModel(const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer,
    uint32_t& poutputModelSize, const BuildOptions& options)
{
    HIAI_EXPECT_TRUE_R((pinputMemBuffer.size() == 1), AI_INVALID_PARA);
    if (pinputMemBuffer[0] == nullptr || pinputMemBuffer[0]->GetMemBufferData() == nullptr ||
        pinputMemBuffer[0]->GetMemBufferSize() == 0 || poutputModelBuffer == nullptr) {
        return AI_INVALID_PARA;
    }

    HIAI_EXPECT_NOT_NULL_R(modelBuilder_, AI_NOT_INIT);

    std::shared_ptr<IBuffer> inputBuffer =
        CreateLocalBuffer(pinputMemBuffer[0]->GetMemBufferData(), pinputMemBuffer[0]->GetMemBufferSize(), false);
    std::shared_ptr<IBuffer> outputBuffer =
        CreateLocalBuffer(poutputModelBuffer->GetMemBufferData(), poutputModelBuffer->GetMemBufferSize(), false);

    HIAI_EXPECT_TRUE_R((inputBuffer != nullptr && outputBuffer != nullptr), AI_INVALID_PARA);

    ModelBuildOptions modelBuildOptions = BuildOptionUtil::Convert2ModelBuildOptions(options);
    std::shared_ptr<IBuiltModel> builtModel = nullptr;
    (void)modelBuilder_->Build(modelBuildOptions, "Default", inputBuffer, builtModel);

    size_t realSize = 0;
    Status ret = builtModel->SaveToExternalBuffer(outputBuffer, realSize);
    if (ret == SUCCESS) {
        poutputModelSize = static_cast<uint32_t>(realSize);
    }
    return static_cast<AIStatus>(ret);
}

MemBuffer* AiModelBuilderImpl::ReadBinaryProto(const std::string path)
{
    return MembufferUtil::InputMemBufferCreate(path);
}

MemBuffer* AiModelBuilderImpl::ReadBinaryProto(void* data, uint32_t size)
{
    return MembufferUtil::InputMemBufferCreate(data, size);
}

MemBuffer* AiModelBuilderImpl::InputMemBufferCreate(void* data, uint32_t size)
{
    return MembufferUtil::InputMemBufferCreate(data, size);
}

MemBuffer* AiModelBuilderImpl::InputMemBufferCreate(const std::string path)
{
        return MembufferUtil::InputMemBufferCreate(path);
}

MemBuffer* AiModelBuilderImpl::OutputMemBufferCreate(
    const int32_t framework, const std::vector<MemBuffer*>& pinputMemBuffer)
{
    return MembufferUtil::OutputMemBufferCreate(framework, pinputMemBuffer);
}

void AiModelBuilderImpl::MemBufferDestroy(MemBuffer* membuf)
{
    MembufferUtil::MemBufferDestroy(membuf);
}

AIStatus AiModelBuilderImpl::MemBufferExportFile(
    MemBuffer* membuf, const uint32_t pbuildSize, const std::string pbuildPath)
{
    return MembufferUtil::MemBufferExportFile(membuf, pbuildSize, pbuildPath);
}
}
