/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compatible/HiAiModelManagerService.h"
#include "compatible/hiai_base_types_cpt.h"
#include "model_builder/model_builder_types.h"
#include "framework/infra/log/log_fmk_interface.h"

#include "AiModelDescriptionImpl.h"
#include "infra/base/assertion.h"
#include "infra/base/securestl.h"

namespace hiai {

AiModelDescription::AiModelDescription(const std::string& pmodelName, const int32_t frequency, const int32_t framework,
    const int32_t pmodelType, const int32_t pdeviceType)
{
    impl_ = hiai::make_shared_nothrow<AiModelDescriptionImpl>(pmodelName, frequency, framework, pmodelType,
        pdeviceType);
}

AiModelDescription::~AiModelDescription()
{
}

const std::string& AiModelDescription::GetName() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    static const std::string emptyString = "";
    HIAI_EXPECT_NOT_NULL_R(impl_, emptyString);
    return impl_->GetName();
}

int32_t AiModelDescription::GetFrequency() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetFrequency();
}

int32_t AiModelDescription::GetFramework() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetFramework();
}

int32_t AiModelDescription::GetModelType() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetModelType();
}

int32_t AiModelDescription::GetDeviceType() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetDeviceType();
}

AIStatus AiModelDescription::GetDynamicShapeConfig(DynamicShapeConfig& dynamicShape) const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetDynamicShapeConfig(dynamicShape);
}

AIStatus AiModelDescription::SetDynamicShapeConfig(const DynamicShapeConfig& dynamicShape)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetDynamicShapeConfig(dynamicShape);
}

AIStatus AiModelDescription::GetInputDims(std::vector<TensorDimension>& inputDims) const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetInputDims(inputDims);
}

AIStatus AiModelDescription::SetInputDims(const std::vector<TensorDimension>& inputDims)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetInputDims(inputDims);
}

void* AiModelDescription::GetModelBuffer() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->GetModelBuffer();
}

AIStatus AiModelDescription::SetModelBuffer(const void* data, uint32_t size)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetModelBuffer(data, size);
}

AIStatus AiModelDescription::SetModelPath(const std::string& modelPath)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetModelPath(modelPath);
}

uint32_t AiModelDescription::GetModelNetSize() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetModelNetSize();
}

const std::string& AiModelDescription::GetModelPath() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    static const std::string emptyString = "";
    HIAI_EXPECT_NOT_NULL_R(impl_, emptyString);
    return impl_->GetModelPath();
}

AIStatus AiModelDescription::GetPrecisionMode(PrecisionMode& precisionMode) const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetPrecisionMode(precisionMode);
}

AIStatus AiModelDescription::SetPrecisionMode(const PrecisionMode& precisionMode)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetPrecisionMode(precisionMode);
}

AIStatus AiModelDescription::SetTuningStrategy(const TuningStrategy& tuningStrategy)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetTuningStrategy(tuningStrategy);
}

TuningStrategy AiModelDescription::GetTuningStrategy() const
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, TuningStrategy::OFF);
    return impl_->GetTuningStrategy();
}
} // namespace hiai
