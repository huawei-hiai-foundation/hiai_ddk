/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_DESCRIPTION_IMPL_H
#define FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_DESCRIPTION_IMPL_H

#include "compatible/HiAiModelManagerService.h"
#include "model_builder/model_builder_types.h"

namespace hiai {
class AiModelDescriptionImpl {
public:
    AiModelDescriptionImpl(const std::string& pmodelName, const int32_t frequency, const int32_t framework,
        const int32_t pmodelType, const int32_t pdeviceType);

    virtual ~AiModelDescriptionImpl();

    const std::string& GetName() const;
    void* GetModelBuffer() const;
    AIStatus SetModelBuffer(const void* data, uint32_t size);
    AIStatus SetModelPath(const std::string& modelPath);
    const std::string& GetModelPath() const;
    int32_t GetFrequency() const;
    int32_t GetFramework() const;
    int32_t GetModelType() const;
    int32_t GetDeviceType() const;
    uint32_t GetModelNetSize() const;
    AIStatus GetDynamicShapeConfig(DynamicShapeConfig& dynamicShape) const;
    AIStatus SetDynamicShapeConfig(const DynamicShapeConfig& dynamicShape);
    AIStatus GetInputDims(std::vector<TensorDimension>& inputDims) const;
    AIStatus SetInputDims(const std::vector<TensorDimension>& inputDims);
    AIStatus GetPrecisionMode(PrecisionMode& precisionMode) const;
    AIStatus SetPrecisionMode(const PrecisionMode& precisionMode);
    AIStatus SetTuningStrategy(const TuningStrategy& tuningStrategy);
    TuningStrategy GetTuningStrategy() const;
private:
    std::string model_name_;
    int32_t frequency_ {0};
    int32_t framework_ {0};
    int32_t modelType_ {0};
    int32_t deviceType_ {0};
    void* modelNetBuffer_ {nullptr};
    uint32_t modelNetSize_ {0};
    std::string modelNetKey_;
    std::string modelPath_;
    DynamicShapeConfig dynamicShape_;
    std::vector<TensorDimension> inputDims_;
    PrecisionMode precisionMode_ {PrecisionMode::PRECISION_MODE_FP32};
    TuningStrategy tuningStrategy_ {TuningStrategy::OFF};
};
}

#endif