/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_BUILDER_IMPL_H
#define FRAMEWORK_MODEL_MANAGER_COMPATIBLE_AI_MODEL_BUILDER_IMPL_H

#include "compatible/MemBuffer.h"
#include "model_builder/model_builder.h"
#include "compatible/HiAiModelBuilderType.h"

namespace hiai {
class AiModelBuilderImpl {
public:
    explicit AiModelBuilderImpl();
    virtual ~AiModelBuilderImpl() = default;

    AIStatus BuildModel(
        const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer, uint32_t& poutputModelSize);

    AIStatus BuildModel(const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer,
        uint32_t& poutputModelSize, const BuildOptions& options);

    MemBuffer* ReadBinaryProto(const std::string path);

    MemBuffer* ReadBinaryProto(void* data, uint32_t size);

    MemBuffer* InputMemBufferCreate(void* data, uint32_t size);

    MemBuffer* InputMemBufferCreate(const std::string path);

    MemBuffer* OutputMemBufferCreate(const int32_t framework, const std::vector<MemBuffer*>& pinputMemBuffer);

    void MemBufferDestroy(MemBuffer* membuf);

    AIStatus MemBufferExportFile(MemBuffer* membuf, const uint32_t pbuildSize, const std::string pbuildPath);

private:
    std::shared_ptr<IModelBuilder> modelBuilder_ {nullptr};
};
} // namespace hiai

#endif