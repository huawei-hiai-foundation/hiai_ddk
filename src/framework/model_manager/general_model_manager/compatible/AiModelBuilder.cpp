/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compatible/AiModelBuilder.h"

#include <memory>
#include <functional>

#include "infra/base/securestl.h"
#include "framework/infra/log/log.h"
#include "framework/c/compatible/hiai_mem_buffer.h"
#include "framework/infra/log/log_fmk_interface.h"

#include "model_manager/general_model_manager/compatible/MembufferUtil.h"
#include "model_builder/compatible/BuildOptionUtil.h"
#include "AiModelBuilderImpl.h"
#include "infra/base/assertion.h"
namespace hiai {

using HIAI_ModelManager = struct HIAI_ModelManager;
using HIAI_ModelManagerListener = struct HIAI_ModelManagerListener;

AiModelBuilder::AiModelBuilder(std::shared_ptr<AiModelMngerClient> client)
{
    (void)client;
    impl_ = hiai::make_shared_nothrow<AiModelBuilderImpl>();
}

AiModelBuilder::~AiModelBuilder()
{
}

AIStatus AiModelBuilder::BuildModel(
    const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer, uint32_t& poutputModelSize)
{
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->BuildModel(pinputMemBuffer, poutputModelBuffer, poutputModelSize);
}

AIStatus AiModelBuilder::BuildModel(const std::vector<MemBuffer*>& pinputMemBuffer, MemBuffer* poutputModelBuffer,
    uint32_t& poutputModelSize, const BuildOptions& options)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->BuildModel(pinputMemBuffer, poutputModelBuffer, poutputModelSize, options);
}

MemBuffer* AiModelBuilder::ReadBinaryProto(const std::string path)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->ReadBinaryProto(path);
}

MemBuffer* AiModelBuilder::ReadBinaryProto(void* data, uint32_t size)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->ReadBinaryProto(data, size);
}

MemBuffer* AiModelBuilder::InputMemBufferCreate(void* data, uint32_t size)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->InputMemBufferCreate(data, size);
}

MemBuffer* AiModelBuilder::InputMemBufferCreate(const std::string path)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->InputMemBufferCreate(path);
}

MemBuffer* AiModelBuilder::OutputMemBufferCreate(
    const int32_t framework, const std::vector<MemBuffer*>& pinputMemBuffer)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->OutputMemBufferCreate(framework, pinputMemBuffer);
}

void AiModelBuilder::MemBufferDestroy(MemBuffer* membuf)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    impl_->MemBufferDestroy(membuf);
}

AIStatus AiModelBuilder::MemBufferExportFile(MemBuffer* membuf, const uint32_t pbuildSize, const std::string pbuildPath)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->MemBufferExportFile(membuf, pbuildSize, pbuildPath);
}

} // namespace hiai