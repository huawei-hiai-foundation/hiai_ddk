/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AiModelDescriptionImpl.h"
#include "framework/infra/log/log.h"
#include "model_builder/model_builder_types.h"

namespace hiai {
AiModelDescriptionImpl::AiModelDescriptionImpl(const std::string& pmodelName, const int32_t frequency,
    const int32_t framework, const int32_t pmodelType, const int32_t pdeviceType)
{
    this->model_name_ = pmodelName;
    this->frequency_ = frequency;
    this->framework_ = framework;
    this->modelType_ = pmodelType;
    this->deviceType_ = pdeviceType;
}

AiModelDescriptionImpl::~AiModelDescriptionImpl()
{
}

const std::string& AiModelDescriptionImpl::GetName() const
{
    return this->model_name_;
}

void* AiModelDescriptionImpl::GetModelBuffer() const
{
    return modelNetBuffer_;
}

AIStatus AiModelDescriptionImpl::SetModelBuffer(const void* data, uint32_t size)
{
    if (data == nullptr) {
        FMK_LOGE("AiModelDescription SetModelBuffer failed, data can not be null");
        return AI_INVALID_PARA;
    }
    modelNetBuffer_ = const_cast<void*>(data);
    modelNetSize_ = size;
    modelPath_ = "";
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::SetModelPath(const std::string& modelPath)
{
    modelPath_ = modelPath;
    modelNetBuffer_ = nullptr;
    modelNetSize_ = 0;
    return AI_SUCCESS;
}

const std::string& AiModelDescriptionImpl::GetModelPath() const
{
    return modelPath_;
}

int32_t AiModelDescriptionImpl::GetFrequency() const
{
    return this->frequency_;
}

int32_t AiModelDescriptionImpl::GetFramework() const
{
    return this->framework_;
}

int32_t AiModelDescriptionImpl::GetModelType() const
{
    return this->modelType_;
}

int32_t AiModelDescriptionImpl::GetDeviceType() const
{
    return this->deviceType_;
}

uint32_t AiModelDescriptionImpl::GetModelNetSize() const
{
    return modelNetSize_;
}

AIStatus AiModelDescriptionImpl::GetDynamicShapeConfig(DynamicShapeConfig& dynamicShape) const
{
    dynamicShape = dynamicShape_;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::SetDynamicShapeConfig(const DynamicShapeConfig& dynamicShape)
{
    if (!dynamicShape.enable) {
        FMK_LOGI("not set dynamic shape config");
        dynamicShape_ = dynamicShape;
        return AI_SUCCESS;
    }

    if (dynamicShape.cacheMode != static_cast<CacheMode>(CacheMode::CACHE_BUILDED_MODEL) &&
        dynamicShape.cacheMode != static_cast<CacheMode>(CacheMode::CACHE_LOADED_MODEL)) {
        FMK_LOGE("dynamic shape cache mode is error");
        return AI_FAILED;
    }
    if (dynamicShape.maxCachedNum < hiai::MIN_DYNAMIC_SHAPE_CACHE_NUM ||
        dynamicShape.maxCachedNum > hiai::MAX_DYNAMIC_SHAPE_CACHE_NUM) {
        FMK_LOGE("max cached num should be from %d to %d", hiai::MIN_DYNAMIC_SHAPE_CACHE_NUM,
            hiai::MAX_DYNAMIC_SHAPE_CACHE_NUM);
        return AI_FAILED;
    }
    dynamicShape_ = dynamicShape;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::GetInputDims(std::vector<TensorDimension>& inputDims) const
{
    inputDims = inputDims_;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::SetInputDims(const std::vector<TensorDimension>& inputDims)
{
    inputDims_ = inputDims;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::GetPrecisionMode(PrecisionMode& precisionMode) const
{
    precisionMode = precisionMode_;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::SetPrecisionMode(const PrecisionMode& precisionMode)
{
    if (precisionMode != PrecisionMode::PRECISION_MODE_FP32 &&
        precisionMode != PrecisionMode::PRECISION_MODE_FP16) {
        FMK_LOGE("precision mode is error");
        return AI_FAILED;
    }
    precisionMode_ = precisionMode;
    return AI_SUCCESS;
}

AIStatus AiModelDescriptionImpl::SetTuningStrategy(const TuningStrategy& tuningStrategy)
{
    if (tuningStrategy < TuningStrategy::OFF ||
        tuningStrategy > TuningStrategy::ON_CLOUD_TUNING) {
        FMK_LOGE("tuning strategy is error");
        return AI_FAILED;
    }
    tuningStrategy_ = tuningStrategy;
    return AI_SUCCESS;
}

TuningStrategy AiModelDescriptionImpl::GetTuningStrategy() const
{
    return tuningStrategy_;
}
}