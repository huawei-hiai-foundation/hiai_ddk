/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AiContextImpl.h"
#include "framework/infra/log/log.h"

namespace hiai {
AiContextImpl::AiContextImpl()
{
}
AiContextImpl::~AiContextImpl()
{
}

std::string AiContextImpl::GetPara(const std::string& key) const
{
    auto iter = paras_.find(key);
    return (iter == paras_.end()) ? "" : iter->second;
}

void AiContextImpl::AddPara(const std::string& key, const std::string& value)
{
    paras_.insert(std::pair<std::string, std::string>(key, value));
}

void AiContextImpl::SetPara(const std::string& key, const std::string& value)
{
    paras_[key] = value;
}

void AiContextImpl::DelPara(const std::string& key)
{
    paras_.erase(key);
}

void AiContextImpl::ClearPara()
{
    paras_.clear();
}

AIStatus AiContextImpl::GetAllKeys(std::vector<std::string>& keys)
{
    if (paras_.empty()) {
        FMK_LOGE("AiContext GetAllKeys failed, paras_ is empty");
        return AI_INVALID_PARA;
    }

    for (auto iter = paras_.begin(); iter != paras_.end(); iter++) {
        keys.push_back(iter->first);
    }

    return AI_SUCCESS;
}
}