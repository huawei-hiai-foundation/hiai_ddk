/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compatible/HiAiModelManagerService.h"

#include "compatible/AiTensor.h"
#include "infra/base/securestl.h"
#include "framework/infra/log/log_fmk_interface.h"
#include "AiModelManagerClientImpl.h"
#include "infra/base/assertion.h"

using namespace std;

namespace hiai {
AiModelMngerClient::AiModelMngerClient()
{
    impl_ = hiai::make_shared_nothrow<AiModelMngerClientImpl>();
}

AiModelMngerClient::~AiModelMngerClient()
{
}

AIStatus AiModelMngerClient::Init(std::shared_ptr<AiModelManagerClientListener> listener)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->Init(listener);
}

// 1. 模型名不能重复
// 2. 单个加载失败,卸载全部并返回失败
AIStatus AiModelMngerClient::Load(std::vector<std::shared_ptr<AiModelDescription>>& pmodelDesc)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->Load(pmodelDesc);
}

AIStatus AiModelMngerClient::Process(AiContext& context, std::vector<std::shared_ptr<AiTensor>>& pinputTensor,
    std::vector<std::shared_ptr<AiTensor>>& poutputTensor, uint32_t timeout, int32_t& piStamp)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->Process(context, pinputTensor, poutputTensor, timeout, piStamp);
}

AIStatus AiModelMngerClient::CheckModelCompatibility(AiModelDescription& pmodelDesc, bool& pisModelCompatibility)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->CheckModelCompatibility(pmodelDesc, pisModelCompatibility);
}

AIStatus AiModelMngerClient::GetModelIOTensorDim(const std::string& pmodelName,
    std::vector<TensorDimension>& pinputTensor, std::vector<TensorDimension>& poutputTensor)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetModelIOTensorDim(pmodelName, pinputTensor, poutputTensor);
}

AIStatus AiModelMngerClient::GetModelAippPara(
    const std::string& modelName, std::vector<std::shared_ptr<AippPara>>& aippPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetModelAippPara(modelName, aippPara);
}

AIStatus AiModelMngerClient::GetModelAippPara(
    const std::string& modelName, uint32_t index, std::vector<std::shared_ptr<AippPara>>& aippPara)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->GetModelAippPara(modelName, index, aippPara);
}

char* AiModelMngerClient::GetVersion()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->GetVersion();
}

AIStatus AiModelMngerClient::UnLoadModel()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->UnLoadModel();
}

AIStatus AiModelMngerClient::SetModelPriority(const std::string& modelName, ModelPriority modelPriority)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, AI_NOT_INIT);
    return impl_->SetModelPriority(modelName, modelPriority);
}

void AiModelMngerClient::Cancel(const std::string& modelName)
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_VOID(impl_);
    return impl_->Cancel(modelName);
}

} // namespace hiai
