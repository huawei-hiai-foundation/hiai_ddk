/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compatible/MemBuffer.h"
#include "framework/infra/log/log_fmk_interface.h"

#include "MemBufferImpl.h"
#include "infra/base/assertion.h"
#include "infra/base/securestl.h"

namespace hiai {
MemBuffer::MemBuffer()
{
    impl_ = hiai::make_shared_nothrow<MemBufferImpl>();
}

void* MemBuffer::GetMemBufferData()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, nullptr);
    return impl_->GetMemBufferData();
}

uint32_t MemBuffer::GetMemBufferSize()
{
    H_LOG_INTERFACE_FILTER(ITF_COUNT);
    HIAI_EXPECT_NOT_NULL_R(impl_, 0);
    return impl_->GetMemBufferSize();
}

} // namespace hiai
