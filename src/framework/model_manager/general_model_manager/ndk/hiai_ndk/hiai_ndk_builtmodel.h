/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_BUILTMODEL_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_BUILTMODEL_H
 
#include "hiai_foundation/inner_api/hiai_builtmodel_inner.h"
#include "base/error_types.h"
 
namespace hiai {
Status HIAI_NDK_HiAIBuiltmodel_GetFmMemorySize(OH_NNCompilation* compilation, uint64_t* fmSize);
}
 
#endif