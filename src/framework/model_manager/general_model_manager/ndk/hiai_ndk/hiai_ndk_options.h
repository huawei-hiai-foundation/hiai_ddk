/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_OPTIONS_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_OPTIONS_H

#include "neural_network_runtime/neural_network_core.h"

#include "base/error_types.h"
#include "hiai_foundation/hiai_options.h"
#include "hiai_foundation/inner_api/hiai_options_inner.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_align.h"

namespace hiai {

Status HIAI_NDK_HiAIOptions_SetInputTensorShapes(OH_NNCompilation* compilation,
    NN_TensorDesc* inputTensorDescs[], size_t shapeCount);

Status HIAI_NDK_HiAIOptions_SetFormatMode(OH_NNCompilation* compilation, HiAI_FormatMode formatMode);

Status HIAI_NDK_HiAIOptions_SetDynamicShapeStatus(OH_NNCompilation* compilation, HiAI_DynamicShapeStatus status);

Status HIAI_NDK_HiAIOptions_SetDynamicShapeMaxCache(OH_NNCompilation* compilation, size_t maxCacheCount);

Status HIAI_NDK_HiAIOptions_SetDynamicShapeCacheMode(OH_NNCompilation* compilation, HiAI_DynamicShapeCacheMode mode);

Status HIAI_NDK_HiAIOptions_SetOperatorDeviceOrder(OH_NNCompilation* compilation,
    const char* operatorName, HiAI_ExecuteDevice* executeDevices, size_t deviceCount);

Status HIAI_NDK_HiAIOptions_SetModelDeviceOrder(OH_NNCompilation* compilation,
    HiAI_ExecuteDevice* executeDevices, size_t deviceCount);

Status HIAI_NDK_HiAIOptions_SetFallbackMode(OH_NNCompilation* compilation, HiAI_FallbackMode fallbackMode);

Status HIAI_NDK_HiAIOptions_SetDeviceMemoryReusePlan(OH_NNCompilation* compilation,
    HiAI_DeviceMemoryReusePlan deviceMemoryReusePlan);

Status HIAI_NDK_HiAIOptions_SetTuningStrategy(OH_NNCompilation* compilation, HiAI_TuningStrategy tuningStrategy);

Status HIAI_NDK_HiAIOptions_SetQuantConfig(OH_NNCompilation* compilation, void* data, size_t size);

Status HIAI_NDK_HiAIOptions_SetTuningMode(OH_NNCompilation* compilation, HiAI_TuningMode tuningMode);

Status HIAI_NDK_HiAIOptions_SetTuningCacheDir(OH_NNCompilation* compilation, const char* cacheDir);

Status HIAI_NDK_HiAIOptions_SetBandMode(OH_NNCompilation* nnCompilation, HiAI_BandMode bandMode);

HiAI_BandMode HIAI_NDK_HiAIOptions_GetBandMode(const OH_NNCompilation* nnCompilation);

Status HIAI_NDK_HiAIOptions_SetAsyncModeEnable(OH_NNCompilation* nnCompilation, bool isEnable);

bool HIAI_NDK_HiAIOptions_GetAsyncModeEnable(const OH_NNCompilation* nnCompilation);

Status HIAI_NDK_HiAIOptions_SetCustomOpPath(OH_NNCompilation* compilation, const char* customPath);

Status HIAI_NDK_HiAIOptions_SetAllocate(OH_NNCompilation* compilation, onAllocate allocateFunc);

Status HIAI_NDK_HiAIOptions_SetFree(OH_NNCompilation* compilation, onFree freeFunc);

Status HIAI_NDK_HiAIOptions_SetUserData(OH_NNCompilation* compilation, void* userData);
}

#endif