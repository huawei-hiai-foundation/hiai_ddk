/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_builtmodel.h"
 
#include "infra/base/assertion.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"
 
namespace hiai {
Status HIAI_NDK_HiAIBuiltmodel_GetFmMemorySize(OH_NNCompilation* compilation, uint64_t* fmSize)
{
    auto getFmSizeFunc = reinterpret_cast<decltype(HMS_HiAIBuiltModel_GetFmMemorySize)*>(
        NDKProxy::GetSymbol("HMS_HiAIBuiltModel_GetFmMemorySize"));
    HIAI_EXPECT_NOT_NULL(getFmSizeFunc);
    HIAI_EXPECT_TRUE(getFmSizeFunc(compilation, fmSize) == OH_NN_SUCCESS);
    return SUCCESS;
}
}