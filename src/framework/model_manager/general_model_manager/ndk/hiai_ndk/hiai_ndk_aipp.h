/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_AIPP_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_AIPP_H

#include "hiai_foundation/hiai_aipp_param.h"
#include "base/error_types.h"

namespace hiai {

HiAI_AippParam* HIAI_NDK_HiAIAippParam_Create(uint32_t batchNum);
void HIAI_NDK_HiAIAippParam_Destroy(HiAI_AippParam** aippParam);

void* HIAI_NDK_HiAIAippParam_GetData(HiAI_AippParam* aippParam);
uint32_t HIAI_NDK_HiAIAippParam_GetDataSize(HiAI_AippParam* aippParam);
uint32_t HIAI_NDK_HiAIAippParam_GetBatchCount(HiAI_AippParam* aippParam);

Status HIAI_NDK_HiAIAippParam_SetInputIndex(HiAI_AippParam* aippParam, uint32_t inputIndex);
int HIAI_NDK_HiAIAippParam_GetInputIndex(HiAI_AippParam* aippParam);

Status HIAI_NDK_HiAIAippParam_SetInputAippIndex(HiAI_AippParam* aippParam, uint32_t inputAippIndex);
int HIAI_NDK_HiAIAippParam_GetInputAippIndex(HiAI_AippParam* aippParam);

Status HIAI_NDK_HiAIAippParam_SetCscConfig(HiAI_AippParam* aippParam,
    HiAI_ImageFormat inputFormat, HiAI_ImageFormat outputFormat, HiAI_ImageColorSpace space);
Status HIAI_NDK_HiAIAippParam_GetCscConfig(HiAI_AippParam* aippParam,
    HiAI_ImageFormat* inputFormat, HiAI_ImageFormat* outputFormat, HiAI_ImageColorSpace* space);

Status HIAI_NDK_HiAIAippParam_SetChannelSwapConfig(HiAI_AippParam* aippParam, bool rbuvSwapSwitch, bool axSwapSwitch);
Status HIAI_NDK_HiAIAippParam_GetChannelSwapConfig(HiAI_AippParam* aippParam, bool* rbuvSwapSwitch, bool* axSwapSwitch);

Status HIAI_NDK_HiAIAippParam_SetSingleBatchMultiCrop(HiAI_AippParam* aippParam, bool singleBatchMultiCrop);
bool HIAI_NDK_HiAIAippParam_GetSingleBatchMultiCrop(HiAI_AippParam* aippParam);

Status HIAI_NDK_HiAIAippParam_SetCropConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t startPosW, uint32_t startPosH, uint32_t croppedW, uint32_t croppedH);
Status HIAI_NDK_HiAIAippParam_GetCropConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t* startPosW, uint32_t* startPosH, uint32_t* croppedW, uint32_t* croppedH);

Status HIAI_NDK_HiAIAippParam_SetResizeConfig(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t resizedW, uint32_t resizedH);
Status HIAI_NDK_HiAIAippParam_GetResizeConfig(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t* resizedW, uint32_t* resizedH);

Status HIAI_NDK_HiAIAippParam_SetPadConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t leftPadSize, uint32_t rightPadSize, uint32_t topPadSize, uint32_t bottomPadSize);
Status HIAI_NDK_HiAIAippParam_GetPadConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t* leftPadSize, uint32_t* rightPadSize, uint32_t* topPadSize, uint32_t* bottomPadSize);

Status HIAI_NDK_HiAIAippParam_SetChannelPadding(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t paddingValues[], uint32_t channelCount);
Status HIAI_NDK_HiAIAippParam_GetChannelPadding(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t paddingValues[], uint32_t channelCount);

Status HIAI_NDK_HiAIAippParam_SetDtcMeanPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t meanPixel[], uint32_t channelCount);
Status HIAI_NDK_HiAIAippParam_GetDtcMeanPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t meanPixel[], uint32_t channelCount);

Status HIAI_NDK_HiAIAippParam_SetDtcMinPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float minPixel[], uint32_t channelCount);
Status HIAI_NDK_HiAIAippParam_GetDtcMinPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float minPixel[], uint32_t channelCount);

Status HIAI_NDK_HiAIAippParam_SetDtcVarReciPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float varReciPixel[], uint32_t channelCount);
Status HIAI_NDK_HiAIAippParam_GetDtcVarReciPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float varReciPixel[], uint32_t channelCount);

Status HIAI_NDK_HiAIAippParam_SetRotationAngle(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float rotationAngle);
Status HIAI_NDK_HiAIAippParam_GetRotationAngle(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float* rotationAngle);

Status HIAI_NDK_HiAIAippParam_SetInputFormat(HiAI_AippParam* aippParam, HiAI_ImageFormat inputFormat);
HiAI_ImageFormat HIAI_NDK_HiAIAippParam_GetInputFormat(HiAI_AippParam* aippParam);

Status HIAI_NDK_HiAIAippParam_SetInputShape(
    HiAI_AippParam* aippParam, uint32_t srcImageW, uint32_t srcImageH);
Status HIAI_NDK_HiAIAippParam_GetInputShape(
    HiAI_AippParam* aippParam, uint32_t* srcImageW, uint32_t* srcImageH);
}
#endif