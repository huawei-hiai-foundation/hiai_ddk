/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_nativehandle.h"

#include "infra/base/assertion.h"
#include "framework/infra/log/log.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"

namespace hiai {
HiAI_NativeHandle* HIAI_NDK_HiAINativeHandle_Create(int fd, int size, int offset)
{
    auto createNativeHandleFunc = reinterpret_cast<decltype(HMS_HiAINativeHandle_Create)*>(
        NDKProxy::GetSymbol("HMS_HiAINativeHandle_Create"));
    HIAI_EXPECT_NOT_NULL_R(createNativeHandleFunc, nullptr);
    HiAI_NativeHandle* handle = createNativeHandleFunc(fd, size, offset);
    HIAI_EXPECT_TRUE_R(handle != nullptr, nullptr);
    return handle;
}

void HIAI_NDK_HiAINativeHandle_Destroy(HiAI_NativeHandle** handle)
{
    auto destroyNativeHandleFunc = reinterpret_cast<decltype(HMS_HiAINativeHandle_Destroy)*>(
        NDKProxy::GetSymbol("HMS_HiAINativeHandle_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(destroyNativeHandleFunc);
    destroyNativeHandleFunc(handle);
}
}
