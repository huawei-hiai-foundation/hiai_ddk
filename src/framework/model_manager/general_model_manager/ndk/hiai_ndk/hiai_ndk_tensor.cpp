/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_tensor.h"

#include "infra/base/assertion.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"

namespace hiai {
Status HIAI_NDK_HiAITensor_SetAippParams(std::vector<NN_Tensor*> tensor, std::vector<HiAI_AippParam*> aippParams)
{
    HIAI_EXPECT_TRUE(!tensor.empty());

    auto setAippParamsFunc = reinterpret_cast<decltype(HMS_HiAITensor_SetAippParams)*>(
        NDKProxy::GetSymbol("HMS_HiAITensor_SetAippParams"));
    HIAI_EXPECT_NOT_NULL(setAippParamsFunc);
    OH_NN_ReturnCode retCode = setAippParamsFunc(tensor[0], aippParams.data(), aippParams.size());
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAITensor_SetCacheStatus(NN_Tensor* tensor, uint8_t cacheStatus)
{
    HIAI_EXPECT_NOT_NULL(tensor);
    auto setCacheStatusFunc = reinterpret_cast<decltype(HMS_HiAITensor_SetCacheStatus)*>(
        NDKProxy::GetSymbol("HMS_HiAITensor_SetCacheStatus"));
    HIAI_EXPECT_NOT_NULL(setCacheStatusFunc);
    HIAI_EXPECT_TRUE(setCacheStatusFunc(tensor, cacheStatus) == OH_NN_SUCCESS);
    return SUCCESS;
}

Status HIAI_NDK_HiAITensor_GetCacheStatus(NN_Tensor* tensor, uint8_t* cacheStatus)
{
    HIAI_EXPECT_NOT_NULL(tensor);
    HIAI_EXPECT_NOT_NULL(cacheStatus);
    auto getCacheStatusFunc = reinterpret_cast<decltype(HMS_HiAITensor_GetCacheStatus)*>(
        NDKProxy::GetSymbol("HMS_HiAITensor_GetCacheStatus"));
    HIAI_EXPECT_NOT_NULL(getCacheStatusFunc);
    HIAI_EXPECT_TRUE(getCacheStatusFunc(tensor, cacheStatus) == OH_NN_SUCCESS);
    return SUCCESS;
}
}
