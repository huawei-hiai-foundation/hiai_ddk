/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_executor.h"

#include "infra/base/assertion.h"
#include "framework/infra/log/log.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"

namespace hiai {
Status HIAI_NDK_HiAIExecutor_InitWeights(OH_NNExecutor *executor, const char* weightDir)
{
    HIAI_EXPECT_NOT_NULL(executor);
    auto initWeightsFunc = reinterpret_cast<decltype(HMS_HiAIExecutor_InitWeights)*>(
        NDKProxy::GetSymbol("HMS_HiAIExecutor_InitWeights"));
    HIAI_EXPECT_NOT_NULL(initWeightsFunc);
    OH_NN_ReturnCode retCode = initWeightsFunc(executor, weightDir);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIExecutor_GetWeightBuffer(OH_NNExecutor *executor, const char* weightName, void** data, size_t* size)
{
    HIAI_EXPECT_NOT_NULL(executor);
    HIAI_EXPECT_NOT_NULL(weightName);
    auto getWeightBufferFunc = reinterpret_cast<decltype(HMS_HiAIExecutor_GetWeightBuffer)*>(
        NDKProxy::GetSymbol("HMS_HiAIExecutor_GetWeightBuffer"));
    HIAI_EXPECT_NOT_NULL(getWeightBufferFunc);
    OH_NN_ReturnCode retCode = getWeightBufferFunc(executor, weightName, data, size);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status  HIAI_NDK_HiAIExecutor_FlushWeight(OH_NNExecutor *executor, const char* weightName, size_t offset, size_t size)
{
    HIAI_EXPECT_NOT_NULL(executor);
    HIAI_EXPECT_NOT_NULL(weightName);
    auto flushWeightFunc = reinterpret_cast<decltype(HMS_HiAIExecutor_FlushWeight)*>(
        NDKProxy::GetSymbol("HMS_HiAIExecutor_FlushWeight"));
    HIAI_EXPECT_NOT_NULL(flushWeightFunc);
    OH_NN_ReturnCode retCode = flushWeightFunc(executor, weightName, offset, size);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status  HIAI_NDK_HiAIExecutor_GetModelID(OH_NNExecutor *executor, uint32_t* modelId)
{
    HIAI_EXPECT_NOT_NULL(executor);
    HIAI_EXPECT_NOT_NULL(modelId);
    auto getModelIDFunc = reinterpret_cast<decltype(HMS_HiAIExecutor_GetModelID)*>(
        NDKProxy::GetSymbol("HMS_HiAIExecutor_GetModelID"));
    HIAI_EXPECT_NOT_NULL(getModelIDFunc);
    OH_NN_ReturnCode retCode = getModelIDFunc(executor, modelId);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}
}