/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"

#include "framework/infra/log/log.h"

namespace hiai {
NDKProxy::NDKProxy()
{
    hiaiHelper_.Init("/system/lib64/ndk/libhiai_foundation.so");
    nncoreHelper_.Init("/system/lib64/ndk/libneural_network_core.so");
}

NDKProxy* NDKProxy::GetInstance()
{
    static NDKProxy instance;
    return &instance;
}

NDKProxy::~NDKProxy()
{
}

void* NDKProxy::GetSymbol(std::string symbol)
{
    if (!GetInstance()->nncoreHelper_.IsValid() || !GetInstance()->hiaiHelper_.IsValid()) {
        FMK_LOGE("nncoreHelper_ or hiaiHelper_ is invalid.");
        return nullptr;
    }

    const std::string OH_NN_PREFIX = "OH_NN";
    if (symbol.substr(0, OH_NN_PREFIX.size()) == OH_NN_PREFIX) {
        return GetInstance()->nncoreHelper_.GetSymbol(symbol.c_str());
    }
    return GetInstance()->hiaiHelper_.GetSymbol(symbol.c_str());
}
} // namespace hiai