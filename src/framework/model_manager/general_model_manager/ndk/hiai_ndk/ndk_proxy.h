/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_FOUNDATION_NDK_PROXY_NDK_PROXY_H
#define HIAI_FOUNDATION_NDK_PROXY_NDK_PROXY_H

#include <string>
#include "infra/base/dynamic_load_helper.h"

namespace hiai {
class NDKProxy {
public:
    static void* GetSymbol(std::string symbol);

private:
    NDKProxy();
    ~NDKProxy();
    NDKProxy(const NDKProxy&) = delete;
    NDKProxy& operator=(const NDKProxy&) = delete;

    static NDKProxy* GetInstance();

private:
    DynamicLoadHelper nncoreHelper_;
    DynamicLoadHelper hiaiHelper_;
};
} // namespace hiai
#endif // HIAI_FOUNDATION_NDK_PROXY_NDK_PROXY_H
