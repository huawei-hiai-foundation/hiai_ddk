/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_TENSOR_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_TENSOR_H

#include <vector>

#include "hiai_foundation/hiai_tensor.h"
#include "hiai_foundation/inner_api/hiai_tensor_inner.h"
#include "base/error_types.h"

namespace hiai {
Status HIAI_NDK_HiAITensor_SetAippParams(std::vector<NN_Tensor*> tensor, std::vector<HiAI_AippParam*> aippParams);


Status HIAI_NDK_HiAITensor_SetCacheStatus(NN_Tensor* tensor, uint8_t cacheStatus);


Status HIAI_NDK_HiAITensor_GetCacheStatus(NN_Tensor* tensor, uint8_t* cacheStatus);
}

#endif