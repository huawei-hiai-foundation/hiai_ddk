/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_CREATE_ITF_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_CREATE_ITF_H

#include "model_manager/model_manager_ext.h"
#include "model/built_model.h"
#include "model_builder/model_builder.h"
#include "tensor/nd_tensor_buffer.h"
#include "tensor/aipp_para.h"
#include "tensor/image_tensor_buffer.h"
#include "tensor/core/image_tensor_buffer_info.h"

namespace hiai {
std::shared_ptr<IModelManager> CreateModelManagerFromNDK();

std::shared_ptr<IModelManagerExt> CreateModelManagerExtFromNDK();

std::shared_ptr<IBuiltModel> CreateBuiltModelFromNDK();

std::shared_ptr<IModelBuilder> CreateModelBuilderFromNDK();

std::shared_ptr<INDTensorBuffer> CreateNDTensorBufferFromNDK(const NDTensorDesc& tensorDesc);

std::shared_ptr<INDTensorBuffer> CreateNDTensorBufferFromNDK(const NDTensorDesc& tensorDesc,
    const void* data, size_t dataSize);

std::shared_ptr<INDTensorBuffer> CreateNDTensorBufferFromNDK(const NDTensorDesc& tensorDesc,
    const NativeHandle& handle);

void* CreateHIAINDTensorBufferFromNDK(const NDTensorDesc& tensorDesc, size_t dataSize);

void* CreateHIAINDTensorBufferFromNDK(const NDTensorDesc& tensorDesc, const NativeHandle& handle);

void* GetRawBufferFromNDK(const std::shared_ptr<INDTensorBuffer>& buffer);

std::shared_ptr<IAIPPPara> CreateAIPPParaFromNDK(uint32_t batchCount);

std::shared_ptr<IImageTensorBuffer> CreateImageTensorBufferFromNDK(ImageTensorBufferInfo bufferInfo,
    void* ndTensor, NDTensorDesc desc, ImageColorSpace colorSpace, int32_t rotation);

bool GetEnableCropFromNDK(std::shared_ptr<IAIPPPara> aippParaBase, uint32_t batchIndex);

bool GetEnableResizeFromNDK(std::shared_ptr<IAIPPPara> aippParaBase, uint32_t batchIndex);

bool GetEnableCscFromNDK(std::shared_ptr<IAIPPPara> aippParaBase);

bool GetEnablePaddingFromNDK(std::shared_ptr<IAIPPPara> aippParaBase, uint32_t batchIndex);

}

#endif