/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_helper.h"

#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"
#include "infra/base/assertion.h"

namespace hiai {
const char* HIAI_NDK_GetVersion()
{
    auto getVersionFunc = reinterpret_cast<decltype(HMS_HiAI_GetVersion)*>(
        NDKProxy::GetSymbol("HMS_HiAI_GetVersion"));

    return getVersionFunc == nullptr ? nullptr : getVersionFunc();
}

HiAI_Compatibility HIAI_NDK_HiAICompatibility_CheckFromFile(const char* file)
{
    auto checkFromFileFunc = reinterpret_cast<decltype(HMS_HiAICompatibility_CheckFromFile)*>(
        NDKProxy::GetSymbol("HMS_HiAICompatibility_CheckFromFile"));
    HIAI_EXPECT_TRUE_R(checkFromFileFunc != nullptr, HiAI_Compatibility::HIAI_COMPATIBILITY_INCOMPATIBLE);

    return checkFromFileFunc(file);
}

HiAI_Compatibility HIAI_NDK_HiAICompatibility_CheckFromBuffer(const void* data, size_t size)
{
    auto checkFromBufferFunc = reinterpret_cast<decltype(HMS_HiAICompatibility_CheckFromBuffer)*>(
        NDKProxy::GetSymbol("HMS_HiAICompatibility_CheckFromBuffer"));
    HIAI_EXPECT_TRUE_R(checkFromBufferFunc != nullptr, HiAI_Compatibility::HIAI_COMPATIBILITY_INCOMPATIBLE);

    return checkFromBufferFunc(data, size);
}
}
