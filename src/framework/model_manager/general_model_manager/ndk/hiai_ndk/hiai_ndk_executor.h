/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_EXECUTOR_H
#define HIAI_FOUNDATION_HIAI_NDK_HIAI_NDK_EXECUTOR_H

#include "hiai_foundation/inner_api/hiai_executor_inner.h"
#include "base/error_types.h"

namespace hiai {
Status HIAI_NDK_HiAIExecutor_InitWeights(OH_NNExecutor *executor, const char* weightDir);

Status HIAI_NDK_HiAIExecutor_GetWeightBuffer(OH_NNExecutor *executor, const char* weightName, void** data, size_t* size);

Status  HIAI_NDK_HiAIExecutor_FlushWeight(OH_NNExecutor *executor, const char* weightName, size_t offset, size_t size);

Status  HIAI_NDK_HiAIExecutor_GetModelID(OH_NNExecutor *executor, uint32_t* modelId);
}

#endif