/**
 * Copyright 2024-2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_aipp.h"

#include "model_manager/general_model_manager/ndk/hiai_ndk/ndk_proxy.h"
#include "infra/base/assertion.h"

namespace hiai {

HiAI_AippParam* HIAI_NDK_HiAIAippParam_Create(uint32_t batchNum)
{
    auto createAippParamFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_Create)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_Create"));
    HIAI_EXPECT_NOT_NULL_R(createAippParamFunc, nullptr);

    return createAippParamFunc(batchNum);
}

void HIAI_NDK_HiAIAippParam_Destroy(HiAI_AippParam** aippParam)
{
    auto destoryAippParamFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_Destroy)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_Destroy"));
    HIAI_EXPECT_NOT_NULL_VOID(destoryAippParamFunc);

    destoryAippParamFunc(aippParam);
    aippParam = nullptr;
}

void* HIAI_NDK_HiAIAippParam_GetData(HiAI_AippParam* aippParam)
{
    auto getRawBufferFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetData)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetData"));
    HIAI_EXPECT_NOT_NULL_R(getRawBufferFunc, nullptr);

    return getRawBufferFunc(aippParam);
}

uint32_t HIAI_NDK_HiAIAippParam_GetDataSize(HiAI_AippParam* aippParam)
{
    auto getRawBufferSizeFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetDataSize)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetDataSize"));
    HIAI_EXPECT_NOT_NULL_R(getRawBufferSizeFunc, 0);

    return getRawBufferSizeFunc(aippParam);
}

uint32_t HIAI_NDK_HiAIAippParam_GetBatchCount(HiAI_AippParam* aippParam)
{
    auto getBatchCountFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetBatchCount)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetBatchCount"));
    HIAI_EXPECT_NOT_NULL_R(getBatchCountFunc, 0);

    return getBatchCountFunc(aippParam);
}

Status HIAI_NDK_HiAIAippParam_SetInputIndex(HiAI_AippParam* aippParam, uint32_t inputIndex)
{
    auto setInputIndexFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetInputIndex)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetInputIndex"));
    HIAI_EXPECT_NOT_NULL(setInputIndexFunc);

    OH_NN_ReturnCode retCode = setInputIndexFunc(aippParam, inputIndex);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

int HIAI_NDK_HiAIAippParam_GetInputIndex(HiAI_AippParam* aippParam)
{
    auto getInputIndexFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetInputIndex)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetInputIndex"));
    HIAI_EXPECT_NOT_NULL_R(getInputIndexFunc, -1);

    return getInputIndexFunc(aippParam);
}

Status HIAI_NDK_HiAIAippParam_SetInputAippIndex(HiAI_AippParam* aippParam, uint32_t inputAippIndex)
{
    auto setInputAippIndexFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetInputAippIndex)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetInputAippIndex"));
    HIAI_EXPECT_NOT_NULL(setInputAippIndexFunc);

    OH_NN_ReturnCode retCode = setInputAippIndexFunc(aippParam, inputAippIndex);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

int HIAI_NDK_HiAIAippParam_GetInputAippIndex(HiAI_AippParam* aippParam)
{
    auto getInputAippIndexFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetInputAippIndex)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetInputAippIndex"));
    HIAI_EXPECT_NOT_NULL_R(getInputAippIndexFunc, -1);

    return getInputAippIndexFunc(aippParam);
}

Status HIAI_NDK_HiAIAippParam_SetCscConfig(HiAI_AippParam* aippParam,
    HiAI_ImageFormat inputFormat, HiAI_ImageFormat outputFormat, HiAI_ImageColorSpace space)
{
    auto setCscParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetCscConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetCscConfig"));
    HIAI_EXPECT_NOT_NULL(setCscParaFunc);

    OH_NN_ReturnCode retCode = setCscParaFunc(aippParam, inputFormat, outputFormat, space);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetCscConfig(HiAI_AippParam* aippParam,
    HiAI_ImageFormat* inputFormat, HiAI_ImageFormat* outputFormat, HiAI_ImageColorSpace* space)
{
    auto getCscParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetCscConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetCscConfig"));
    HIAI_EXPECT_NOT_NULL(getCscParaFunc);

    OH_NN_ReturnCode retCode = getCscParaFunc(aippParam, inputFormat, outputFormat, space);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetChannelSwapConfig(
    HiAI_AippParam* aippParam, bool rbuvSwapSwitch, bool axSwapSwitch)
{
    auto setChannelSwapParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetChannelSwapConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetChannelSwapConfig"));
    HIAI_EXPECT_NOT_NULL(setChannelSwapParaFunc);

    OH_NN_ReturnCode retCode = setChannelSwapParaFunc(aippParam, rbuvSwapSwitch, axSwapSwitch);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetChannelSwapConfig(
    HiAI_AippParam* aippParam, bool* rbuvSwapSwitch, bool* axSwapSwitch)
{
    auto getChannelSwapParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetChannelSwapConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetChannelSwapConfig"));
    HIAI_EXPECT_NOT_NULL(getChannelSwapParaFunc);

    OH_NN_ReturnCode retCode = getChannelSwapParaFunc(aippParam, rbuvSwapSwitch, axSwapSwitch);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetSingleBatchMultiCrop(
    HiAI_AippParam* aippParam, bool singleBatchMultiCrop)
{
    auto setSingleBatchMultiCropFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetSingleBatchMultiCrop)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetSingleBatchMultiCrop"));
    HIAI_EXPECT_NOT_NULL(setSingleBatchMultiCropFunc);

    OH_NN_ReturnCode retCode = setSingleBatchMultiCropFunc(aippParam, singleBatchMultiCrop);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

bool HIAI_NDK_HiAIAippParam_GetSingleBatchMultiCrop(HiAI_AippParam* aippParam)
{
    auto getSingleBatchMultiCropFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetSingleBatchMultiCrop)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetSingleBatchMultiCrop"));
    HIAI_EXPECT_NOT_NULL_R(getSingleBatchMultiCropFunc, false);

    return getSingleBatchMultiCropFunc(aippParam);
}

Status HIAI_NDK_HiAIAippParam_SetCropConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t startPosW, uint32_t startPosH, uint32_t croppedW, uint32_t croppedH)
{
    auto setCropPosFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetCropConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetCropConfig"));
    HIAI_EXPECT_NOT_NULL(setCropPosFunc);

    OH_NN_ReturnCode retCode = setCropPosFunc(aippParam, batchIndex, startPosW, startPosH, croppedW, croppedH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetCropConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t* startPosW, uint32_t* startPosH, uint32_t* croppedW, uint32_t* croppedH)
{
    auto getCropPosFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetCropConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetCropConfig"));
    HIAI_EXPECT_NOT_NULL(getCropPosFunc);

    OH_NN_ReturnCode retCode = getCropPosFunc(aippParam, batchIndex, startPosW, startPosH, croppedW, croppedH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetResizeConfig(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t resizedW, uint32_t resizedH)
{
    auto setResizeParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetResizeConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetResizeConfig"));
    HIAI_EXPECT_NOT_NULL(setResizeParaFunc);

    OH_NN_ReturnCode retCode = setResizeParaFunc(aippParam, batchIndex, resizedW, resizedH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetResizeConfig(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t* resizedW, uint32_t* resizedH)
{
    auto getResizeParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetResizeConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetResizeConfig"));
    HIAI_EXPECT_NOT_NULL(getResizeParaFunc);

    OH_NN_ReturnCode retCode = getResizeParaFunc(aippParam, batchIndex, resizedW, resizedH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetPadConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t leftPadSize, uint32_t rightPadSize, uint32_t topPadSize, uint32_t bottomPadSize)
{
    auto setPadConfigFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetPadConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetPadConfig"));
    HIAI_EXPECT_NOT_NULL(setPadConfigFunc);

    OH_NN_ReturnCode retCode = setPadConfigFunc(aippParam, batchIndex,
        leftPadSize, rightPadSize, topPadSize, bottomPadSize);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetPadConfig(HiAI_AippParam* aippParam, uint32_t batchIndex,
    uint32_t* leftPadSize, uint32_t* rightPadSize, uint32_t* topPadSize, uint32_t* bottomPadSize)
{
    auto getPadConfigFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetPadConfig)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetPadConfig"));
    HIAI_EXPECT_NOT_NULL(getPadConfigFunc);

    OH_NN_ReturnCode retCode = getPadConfigFunc(aippParam, batchIndex,
        leftPadSize, rightPadSize, topPadSize, bottomPadSize);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetChannelPadding(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t paddingValues[], uint32_t channelCount)
{
    auto setPadChannelValueFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetChannelPadding)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetChannelPadding"));
    HIAI_EXPECT_NOT_NULL(setPadChannelValueFunc);

    OH_NN_ReturnCode retCode = setPadChannelValueFunc(aippParam, batchIndex, paddingValues, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetChannelPadding(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t paddingValues[], uint32_t channelCount)
{
    auto getPadChannelValueFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetChannelPadding)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetChannelPadding"));
    HIAI_EXPECT_NOT_NULL(getPadChannelValueFunc);

    OH_NN_ReturnCode retCode = getPadChannelValueFunc(aippParam, batchIndex, paddingValues, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetDtcMeanPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t meanPixel[], uint32_t channelCount)
{
    auto setDtcPixelMeanParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetDtcMeanPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetDtcMeanPixel"));
    HIAI_EXPECT_NOT_NULL(setDtcPixelMeanParaFunc);

    OH_NN_ReturnCode retCode = setDtcPixelMeanParaFunc(aippParam, batchIndex, meanPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetDtcMeanPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, uint32_t meanPixel[], uint32_t channelCount)
{
    auto getDtcPixelMeanParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetDtcMeanPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetDtcMeanPixel"));
    HIAI_EXPECT_NOT_NULL(getDtcPixelMeanParaFunc);

    OH_NN_ReturnCode retCode = getDtcPixelMeanParaFunc(aippParam, batchIndex, meanPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetDtcMinPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float minPixel[], uint32_t channelCount)
{
    auto setDtcPixelMinParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetDtcMinPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetDtcMinPixel"));
    HIAI_EXPECT_NOT_NULL(setDtcPixelMinParaFunc);

    OH_NN_ReturnCode retCode = setDtcPixelMinParaFunc(aippParam, batchIndex, minPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetDtcMinPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float minPixel[], uint32_t channelCount)
{
    auto getDtcPixelMinParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetDtcMinPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetDtcMinPixel"));
    HIAI_EXPECT_NOT_NULL(getDtcPixelMinParaFunc);

    OH_NN_ReturnCode retCode = getDtcPixelMinParaFunc(aippParam, batchIndex, minPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetDtcVarReciPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float varReciPixel[], uint32_t channelCount)
{
    auto setDtcPixelVarReciParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetDtcVarReciPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetDtcVarReciPixel"));
    HIAI_EXPECT_NOT_NULL(setDtcPixelVarReciParaFunc);

    OH_NN_ReturnCode retCode = setDtcPixelVarReciParaFunc(aippParam, batchIndex, varReciPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetDtcVarReciPixel(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float varReciPixel[], uint32_t channelCount)
{
    auto getDtcPixelVarReciParaFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetDtcVarReciPixel)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetDtcVarReciPixel"));
    HIAI_EXPECT_NOT_NULL(getDtcPixelVarReciParaFunc);

    OH_NN_ReturnCode retCode = getDtcPixelVarReciParaFunc(aippParam, batchIndex, varReciPixel, channelCount);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetRotationAngle(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float rotationAngle)
{
    auto setRotateAngleFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetRotationAngle)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetRotationAngle"));
    HIAI_EXPECT_NOT_NULL(setRotateAngleFunc);

    OH_NN_ReturnCode retCode = setRotateAngleFunc(aippParam, batchIndex, rotationAngle);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetRotationAngle(
    HiAI_AippParam* aippParam, uint32_t batchIndex, float* rotationAngle)
{
    auto getRotateAngleFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetRotationAngle)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetRotationAngle"));
    HIAI_EXPECT_NOT_NULL(getRotateAngleFunc);

    OH_NN_ReturnCode retCode = getRotateAngleFunc(aippParam, batchIndex, rotationAngle);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_SetInputFormat(HiAI_AippParam* aippParam, HiAI_ImageFormat inputFormat)
{
    auto setInputFormatFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetInputFormat)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetInputFormat"));
    HIAI_EXPECT_NOT_NULL(setInputFormatFunc);

    OH_NN_ReturnCode retCode = setInputFormatFunc(aippParam, inputFormat);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

HiAI_ImageFormat HIAI_NDK_HiAIAippParam_GetInputFormat(HiAI_AippParam* aippParam)
{
    auto getInputFormatFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetInputFormat)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetInputFormat"));
    HIAI_EXPECT_NOT_NULL_R(getInputFormatFunc, HIAI_IMAGE_FORMAT_INVALID);

    return getInputFormatFunc(aippParam);
}

Status HIAI_NDK_HiAIAippParam_SetInputShape(
    HiAI_AippParam* aippParam, uint32_t srcImageW, uint32_t srcImageH)
{
    auto setInputShapeFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_SetInputShape)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_SetInputShape"));
    HIAI_EXPECT_NOT_NULL(setInputShapeFunc);

    OH_NN_ReturnCode retCode = setInputShapeFunc(aippParam, srcImageW, srcImageH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

Status HIAI_NDK_HiAIAippParam_GetInputShape(
    HiAI_AippParam* aippParam, uint32_t* srcImageW, uint32_t* srcImageH)
{
    auto setInputShapeFunc = reinterpret_cast<decltype(HMS_HiAIAippParam_GetInputShape)*>(
        NDKProxy::GetSymbol("HMS_HiAIAippParam_GetInputShape"));
    HIAI_EXPECT_NOT_NULL(setInputShapeFunc);

    OH_NN_ReturnCode retCode = setInputShapeFunc(aippParam, srcImageW, srcImageH);
    return retCode == OH_NN_SUCCESS ? SUCCESS : FAILURE;
}

}