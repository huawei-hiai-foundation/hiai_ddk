/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "aipp_input_converter.h"

#include <map>
#include <functional>

#include "tensor/image_tensor_buffer.h"
#include "securec.h"
#include "framework/common/fmk_error_codes.h"
#include "framework/infra/log/log.h"

namespace hiai {
template <typename T>
static T* GetAippParam(size_t size, void* tensor)
{
    if (size != sizeof(T)) {
        FMK_LOGE("para size not equal!");
        return nullptr;
    }
    return static_cast<T*>(tensor);
}

bool HasDynamicPara(hiai::AippPreprocessConfig& aippConfig, size_t type, size_t& idx)
{
    for (int32_t i = 0; i < aippConfig.configDataCnt; i++) {
        if ((static_cast<uint32_t>(aippConfig.configDataInfo[i].type) == type) &&
            (aippConfig.configDataInfo[i].idx >= 0)) {
            idx = static_cast<size_t>(static_cast<uint32_t>(aippConfig.configDataInfo[i].idx));
            return true;
        }
    }
    return false;
}

bool CheckIndexValid(size_t idx, const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    return idx >= 0 && idx < inputs.size();
}

static Status SetCropPara(
    hiai::AippPreprocessConfig& aippConfig, std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    (void)aippConfig;
    if ((size - sizeof(hiai::ImageFormat)) % sizeof(CropPara) != 0) {
        FMK_LOGE("para size is invalid!");
        return hiai::FAILED;
    }
    size_t dyCropParaNum = (size - sizeof(hiai::ImageFormat)) / sizeof(CropPara);
    uint8_t* paraData = static_cast<uint8_t*>(tensor) + sizeof(hiai::ImageFormat);

    size_t offSize = 0;
    for (size_t i = 0; i < dyCropParaNum; i++) {
        CropPara* cropPara = reinterpret_cast<CropPara*>(reinterpret_cast<void*>(paraData + offSize));
        if (aippPara->SetCropPara(cropPara->batchIndex, std::move(*cropPara)) != hiai::SUCCESS) {
            return hiai::FAILED;
        }
        offSize += sizeof(CropPara);
    }
    return hiai::SUCCESS;
}

static Status PrepareCropParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IMAGE_CROP_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetCropPara(aippConfig, aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippPara->GetSingleBatchMultiCrop()) {
            FMK_LOGE("when it is single batch multiCrop, crop must be dynamic para.");
            return hiai::FAILED;
        }
        if (aippConfig.aippParamInfo.enableCrop) {
            return aippPara->SetCropPara(std::move(aippConfig.aippParamInfo.cropPara));
        }
    }
    return hiai::SUCCESS;
}

static Status SetChannelSwapPara(std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    auto para = GetAippParam<ChannelSwapPara>(size, tensor);
    if (para == nullptr) {
        FMK_LOGE("channel swap para is nullptr");
        return hiai::FAILED;
    }

    return aippPara->SetChannelSwapPara(std::move(*para));
}

static Status PrepareChannelSwapParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IMAGE_CHANNEL_SWAP_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetChannelSwapPara(aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        aippPara->SetChannelSwapPara(std::move(aippConfig.aippParamInfo.channelSwapPara));
    }
    return hiai::SUCCESS;
}

static Status SetCSCPara(
    hiai::AippPreprocessConfig& aippConfig, std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    hiai::CscMatrixPara* matrixPara = GetAippParam<CscMatrixPara>(size, tensor);
    if (matrixPara != nullptr) {
        return aippPara->SetCscPara(*matrixPara);
    }

    hiai::CscPara* para = GetAippParam<CscPara>(size, tensor);
    if (para != nullptr) {
        if (para->outputFormat != aippConfig.aippParamInfo.cscMatrixPara.outputFormat) {
            FMK_LOGE("csc output format error!");
            return hiai::FAILED;
        }
        return aippPara->SetCscPara(para->outputFormat, para->imageColorSpace);
    }
    return hiai::FAILED;
}

static Status PrepareCscParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IMAGE_COLOR_SPACE_CONVERTION_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetCSCPara(aippConfig, aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippConfig.aippParamInfo.enableCsc) {
            return aippPara->SetCscPara(aippConfig.aippParamInfo.cscMatrixPara);
        }
    }
    return hiai::SUCCESS;
}

static Status SetResizePara(
    hiai::AippPreprocessConfig& aippConfig, std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    (void)aippConfig;
    if ((size - sizeof(hiai::ImageFormat)) % sizeof(ResizePara) != 0) {
        FMK_LOGE("para size is invalid!");
        return hiai::FAILED;
    }
    size_t dyResizeParaNum = (size - sizeof(hiai::ImageFormat)) / sizeof(ResizePara);
    uint8_t* paraData = static_cast<uint8_t*>(tensor) + sizeof(hiai::ImageFormat);

    size_t offSize = 0;
    for (size_t i = 0; i < dyResizeParaNum; i++) {
        ResizePara* resizePara = reinterpret_cast<ResizePara*>(reinterpret_cast<void*>(paraData + offSize));
        if (aippPara->SetResizePara(resizePara->batchIndex, std::move(*resizePara)) != hiai::SUCCESS) {
            return hiai::FAILED;
        }
        offSize += sizeof(ResizePara);
    }
    return hiai::SUCCESS;
}

static Status PrepareResizeParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IMAGE_RESIZE_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetResizePara(aippConfig, aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippConfig.aippParamInfo.enableResize) {
            return aippPara->SetResizePara(std::move(aippConfig.aippParamInfo.resizePara));
        }
    }
    return hiai::SUCCESS;
}

static Status SetDtcPara(std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    if ((size - sizeof(hiai::ImageFormat)) % sizeof(DtcPara) != 0) {
        FMK_LOGE("para size is invalid!");
        return hiai::FAILED;
    }
    size_t dyDtcParaNum = (size - sizeof(hiai::ImageFormat)) / sizeof(DtcPara);
    uint8_t* paraData = static_cast<uint8_t*>(tensor) + sizeof(hiai::ImageFormat);

    size_t offSize = 0;
    for (size_t i = 0; i < dyDtcParaNum; i++) {
        DtcPara* dtcPara = reinterpret_cast<DtcPara*>(reinterpret_cast<void*>(paraData + offSize));
        if (aippPara->SetDtcPara(dtcPara->batchIndex, std::move(*dtcPara)) != hiai::SUCCESS) {
            return hiai::FAILED;
        }
        offSize += sizeof(DtcPara);
    }
    return hiai::SUCCESS;
}

static Status PrepareDtcParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IMAGE_DATA_TYPE_CONVERSION_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetDtcPara(aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippConfig.aippParamInfo.enableDtc) {
            return aippPara->SetDtcPara(std::move(aippConfig.aippParamInfo.dtcPara));
        }
    }
    return hiai::SUCCESS;
}

static Status SetPaddingPara(std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    if ((size - sizeof(hiai::ImageFormat)) % sizeof(PadPara) != 0) {
        FMK_LOGE("para size is invalid!");
        return hiai::FAILED;
    }
    size_t dyPadParaNum = (size - sizeof(hiai::ImageFormat)) / sizeof(PadPara);
    uint8_t* paraData = static_cast<uint8_t*>(tensor) + sizeof(hiai::ImageFormat);

    size_t offSize = 0;
    for (size_t i = 0; i < dyPadParaNum; i++) {
        PadPara* padPara = reinterpret_cast<PadPara*>(reinterpret_cast<void*>(paraData + offSize));
        if (aippPara->SetPaddingPara(padPara->batchIndex, std::move(*padPara)) != hiai::SUCCESS) {
            return hiai::FAILED;
        }
        offSize += sizeof(PadPara);
    }
    return hiai::SUCCESS;
}

static Status PreparePaddingParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IAMGE_PADDING_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetPaddingPara(aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippConfig.aippParamInfo.enablePadding) {
            return aippPara->SetPaddingPara(std::move(aippConfig.aippParamInfo.paddingPara));
        }
    }
    return hiai::SUCCESS;
}

static Status SetRotatePara(std::shared_ptr<IAIPPPara>& aippPara, size_t size, void* tensor)
{
    if ((size - sizeof(hiai::ImageFormat)) % sizeof(RotatePara) != 0) {
        FMK_LOGE("para size is invalid!");
        return hiai::FAILED;
    }
    size_t dyRotateParaNum = (size - sizeof(hiai::ImageFormat)) / sizeof(RotatePara);
    uint8_t* paraData = static_cast<uint8_t*>(tensor) + sizeof(hiai::ImageFormat);

    size_t offSize = 0;
    for (size_t i = 0; i < dyRotateParaNum; i++) {
        RotatePara* rotatePara = reinterpret_cast<RotatePara*>(reinterpret_cast<void*>(paraData + offSize));
        if (aippPara->SetRotatePara(rotatePara->batchIndex, std::move(*rotatePara)) != hiai::SUCCESS) {
            return hiai::FAILED;
        }
        offSize += sizeof(RotatePara);
    }
    return hiai::SUCCESS;
}

static Status PrepareRotateParam(std::shared_ptr<IAIPPPara>& aippPara, hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs)
{
    size_t idx;
    if (HasDynamicPara(aippConfig, AIPP_FUNC_IAMGE_ROTATION_V2, idx)) {
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        return SetRotatePara(aippPara, inputs[idx]->GetSize(), inputs[idx]->GetData());
    } else {
        if (aippConfig.aippParamInfo.enableRotate) {
            return aippPara->SetRotatePara(std::move(aippConfig.aippParamInfo.rotatePara));
        }
    }
    return hiai::SUCCESS;
}

static void SetInputParam(hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs, std::shared_ptr<IAIPPPara>& aippPara)
{
    std::shared_ptr<IImageTensorBuffer> imageTensorBuffer =
        std::dynamic_pointer_cast<IImageTensorBuffer>(inputs[aippConfig.graphDataIdx]);
    if (imageTensorBuffer != nullptr) {
        aippPara->SetInputFormat(imageTensorBuffer->Format());
        std::vector<int32_t> shape = {imageTensorBuffer->Width(), imageTensorBuffer->Height()};
        aippPara->SetInputShape(shape);
    }
    aippPara->SetInputIndex(aippConfig.tensorDataIdx);
}

static Status ConvertParams(hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs, std::shared_ptr<IAIPPPara>& aippPara)
{
    using SetAippPramFunc = std::function<hiai::Status(std::shared_ptr<IAIPPPara>&,
        hiai::AippPreprocessConfig & aippConfig, const std::vector<std::shared_ptr<INDTensorBuffer>>&)>;
    std::vector<SetAippPramFunc> convertFuncList {PrepareCropParam, PrepareChannelSwapParam, PrepareCscParam,
        PrepareResizeParam, PrepareDtcParam, PreparePaddingParam, PrepareRotateParam};

    for (const auto& func : convertFuncList) {
        if (func(aippPara, aippConfig, inputs) != SUCCESS) {
            return FAILED;
        }
    }

    return SUCCESS;
}

bool AllAippParaEqual(std::vector<size_t>& dyBatchNumVec)
{
    bool allBatchCountEqual = true;
    for (size_t i = 0; i < dyBatchNumVec.size(); i++) {
        for (size_t j = i + 1; j < dyBatchNumVec.size(); j++) {
            if (dyBatchNumVec[i] != dyBatchNumVec[j]) {
                allBatchCountEqual = false;
                break;
            }
        }
        if (!allBatchCountEqual) {
            break;
        }
    }
    return allBatchCountEqual;
}

Status GetAippParamBatch(hiai::AippPreprocessConfig& aippConfig,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs, uint32_t& batchNum)
{
    std::map<int, int> paraSizeMap = {
        {AIPP_FUNC_IMAGE_CROP_V2, sizeof(CropPara)},
        {AIPP_FUNC_IMAGE_CHANNEL_SWAP_V2, sizeof(ChannelSwapPara)},
        {AIPP_FUNC_IMAGE_COLOR_SPACE_CONVERTION_V2, sizeof(CscPara)},
        {AIPP_FUNC_IMAGE_RESIZE_V2, sizeof(ResizePara)},
        {AIPP_FUNC_IMAGE_DATA_TYPE_CONVERSION_V2, sizeof(DtcPara)},
        {AIPP_FUNC_IAMGE_ROTATION_V2, sizeof(RotatePara)},
        {AIPP_FUNC_IAMGE_PADDING_V2, sizeof(PadPara)}};

    std::vector<size_t> dyBatchNumVec;
    for (int i = 0; i < aippConfig.configDataCnt; i++) {
        int32_t idx =  aippConfig.configDataInfo[i].idx;
        if (!CheckIndexValid(idx, inputs)) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        if (aippConfig.configDataInfo[i].type == AIPP_FUNC_IMAGE_CHANNEL_SWAP_V2 ||
            aippConfig.configDataInfo[i].type == AIPP_FUNC_IMAGE_COLOR_SPACE_CONVERTION_V2) {
            continue;
        }
        size_t paraSize = inputs[idx]->GetSize();
        if ((paraSize - sizeof(hiai::ImageFormat)) %
                static_cast<uint32_t>(paraSizeMap[aippConfig.configDataInfo[i].type]) != 0) {
            FMK_LOGE("para size is invalid!");
            return hiai::FAILED;
        }
        batchNum = (paraSize - sizeof(hiai::ImageFormat)) /
            static_cast<uint32_t>(paraSizeMap[aippConfig.configDataInfo[i].type]);
        dyBatchNumVec.push_back(batchNum);
    }

    if (!AllAippParaEqual(dyBatchNumVec)) {
        FMK_LOGE("aipp dynamic input params number is not equal");
        return hiai::FAILED;
    }
    return SUCCESS;
}

static Status ExtractAippPreprocessConfig(const CustomModelData& customModelData, size_t& dynamicInputCount,
    std::vector<hiai::AippPreprocessConfig>& aippConfigVec)
{
    size_t aippNodeCnt = customModelData.value.size() / sizeof(hiai::AippPreprocessConfig);
    for (size_t i = 0; i < aippNodeCnt; i++) {
        hiai::AippPreprocessConfig aippConfig;
        size_t size = sizeof(hiai::AippPreprocessConfig);
        int ret = memcpy_s(&aippConfig, size,
            reinterpret_cast<const void*>(
                reinterpret_cast<const char*>(customModelData.value.data()) + i * size),
            size);
        if (ret != 0) {
            FMK_LOGE("ExtractAippPreprocessConfig faliled");
            return FAILED;
        }
        if (aippConfig.configDataCnt >= 0) {
            dynamicInputCount += static_cast<size_t>(static_cast<uint32_t>(aippConfig.configDataCnt));
        }
        aippConfigVec.push_back(aippConfig);
    }
    return SUCCESS;
}


static Status ConvertInputs2DataInptusParaInputs(std::vector<hiai::AippPreprocessConfig>& aippConfigs,
    const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs,
    std::vector<std::shared_ptr<INDTensorBuffer>>& dataInputs, std::vector<std::shared_ptr<IAIPPPara>>& paraInputs)
{
    std::vector<bool> inputsVisited(inputs.size(), false);
    for (size_t i = 0; i < aippConfigs.size(); i++) {
        if (aippConfigs[i].graphDataIdx < 0 || static_cast<uint32_t>(aippConfigs[i].graphDataIdx) >= inputs.size()) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        if (aippConfigs[i].tensorDataIdx < 0 ||
            static_cast<uint32_t>(aippConfigs[i].tensorDataIdx) >= dataInputs.size()) {
            FMK_LOGE("inputs size error");
            return hiai::FAILED;
        }
        dataInputs[aippConfigs[i].tensorDataIdx] = inputs[aippConfigs[i].graphDataIdx];
        inputsVisited[aippConfigs[i].graphDataIdx] = true;

        uint32_t batchNum = 0;
        if (GetAippParamBatch(aippConfigs[i], inputs, batchNum) != SUCCESS) {
            FMK_LOGE("aipp param batch number is invalid");
            return hiai::FAILED;
        }
        int32_t imageBatch =
            (std::dynamic_pointer_cast<IImageTensorBuffer>(inputs[aippConfigs[i].graphDataIdx]))->Batch();
        std::shared_ptr<IAIPPPara> aippPara = CreateAIPPPara(batchNum);
        if (aippPara == nullptr) {
            FMK_LOGE("aippPara is invalid");
            return hiai::FAILED;
        }

        if (imageBatch == 1 && batchNum > 1) {
            FMK_LOGI("set aipp SingleBatchMultiCrop");
            aippPara->SetSingleBatchMultiCrop(true);
        }
        SetInputParam(aippConfigs[i], inputs, aippPara);
        aippPara->SetInputAippIndex(i);
        if (ConvertParams(aippConfigs[i], inputs, aippPara) != SUCCESS) {
            return FAILED;
        }

        paraInputs.push_back(aippPara);

        for (int32_t j = 0; j < aippConfigs[i].configDataCnt; j++) {
            inputsVisited[aippConfigs[i].configDataInfo[j].idx] = true;
        }
    }

    size_t j = 0;
    for (size_t i = 0; i < dataInputs.size(); i++) {
        if (dataInputs[i] != nullptr) {
            continue;
        }

        for (; j < inputsVisited.size(); j++) {
            if (!inputsVisited[j]) {
                dataInputs[i] = inputs[j];
                inputsVisited[j] = true;
                break;
            }
        }
    }

    return SUCCESS;
}

Status AippInputConverter::ConvertInputs(const std::vector<std::shared_ptr<INDTensorBuffer>>& inputs,
    const CustomModelData& customModelData, std::vector<std::shared_ptr<INDTensorBuffer>>& dataInputs,
    std::vector<std::shared_ptr<IAIPPPara>>& paraInputs)
{
    std::vector<AippPreprocessConfig> aippConfigs;
    size_t dynamicInputCount = 0;

    if (ExtractAippPreprocessConfig(customModelData, dynamicInputCount, aippConfigs) != SUCCESS) {
        return hiai::FAILED;
    }
    if (inputs.size() <= dynamicInputCount) {
        FMK_LOGE("inputs size error");
        return hiai::FAILED;
    }
    dataInputs.resize(inputs.size() - dynamicInputCount);
    if (ConvertInputs2DataInptusParaInputs(aippConfigs, inputs, dataInputs, paraInputs) != SUCCESS) {
        return hiai::FAILED;
    }

    return SUCCESS;
}

static NDTensorDesc MakeNDTesnorDescWithType(int type)
{
    static std::map<int, int> size {
        {AIPP_FUNC_IMAGE_CROP_V2, sizeof(CropPara)},
        {AIPP_FUNC_IMAGE_CHANNEL_SWAP_V2, sizeof(ChannelSwapPara)},
        {AIPP_FUNC_IMAGE_COLOR_SPACE_CONVERTION_V2, sizeof(CscPara)},
        {AIPP_FUNC_IMAGE_RESIZE_V2, sizeof(ResizePara)},
        {AIPP_FUNC_IMAGE_DATA_TYPE_CONVERSION_V2, sizeof(DtcPara)},
        {AIPP_FUNC_IAMGE_PADDING_V2, sizeof(PadPara)},
    };

    NDTensorDesc ndTensorDesc;

    const auto& iter = size.find(type);
    if (iter == size.cend()) {
        return ndTensorDesc;
    }

    std::vector<int32_t> dims {1, iter->second, 1, 1};
    ndTensorDesc.format = Format::NCHW;
    ndTensorDesc.dataType = DataType::UINT8;
    ndTensorDesc.dims = dims;

    return ndTensorDesc;
}

static void ConvertInputTesnor2NewInputTesnor(std::vector<AippPreprocessConfig>& aippConfig,
    std::vector<NDTensorDesc>& dataInputTensorDesc, std::vector<NDTensorDesc>& modelInputTensor,
    std::vector<bool>& dataInputTensorVisited, std::vector<bool>& modelInputTensorVisited)
{
    for (size_t i = 0; i < aippConfig.size(); i++) {
        modelInputTensor[aippConfig[i].graphDataIdx] = dataInputTensorDesc[aippConfig[i].tensorDataIdx];
        modelInputTensorVisited[aippConfig[i].graphDataIdx] = true;
        dataInputTensorVisited[aippConfig[i].tensorDataIdx] = true;
        for (int32_t j = 0; j < aippConfig[i].configDataCnt; j++) {
            modelInputTensor[aippConfig[i].configDataInfo[j].idx] =
                MakeNDTesnorDescWithType(aippConfig[i].configDataInfo[j].type);
            modelInputTensorVisited[aippConfig[i].configDataInfo[j].idx] = true;
        }
    }
}

static void MakeInputTensor(std::vector<AippPreprocessConfig>& aippConfig,
    std::vector<NDTensorDesc>& dataInputTensorDesc, std::vector<NDTensorDesc>& modelInputTensor)
{
    std::vector<bool> dataInputTensorVisited(dataInputTensorDesc.size(), false);
    std::vector<bool> modelInputTensorVisited(dataInputTensorDesc.size(), false);

    ConvertInputTesnor2NewInputTesnor(
        aippConfig, dataInputTensorDesc, modelInputTensor, dataInputTensorVisited, modelInputTensorVisited);

    size_t j = 0;
    for (size_t i = 0; i < modelInputTensor.size(); i++) {
        if (modelInputTensorVisited[i]) {
            continue;
        }

        for (; j < dataInputTensorDesc.size(); j++) {
            if (!dataInputTensorVisited[j]) {
                modelInputTensor[i] = dataInputTensorDesc[j];
                dataInputTensorVisited[j] = true;
                break;
            }
        }
    }
}

Status AippInputConverter::ConvertInputTensorDesc(
    const CustomModelData& customModelData, std::vector<NDTensorDesc>& inputTensorDescVec)
{
    std::vector<NDTensorDesc> modelInputTensor;
    std::vector<hiai::AippPreprocessConfig> aippConfigVec;
    size_t dynamicInputCount = 0;
    if (customModelData.type.empty() || strcmp(customModelData.type.c_str(), AIPP_PREPROCESS_TYPE) != 0) {
        return SUCCESS;
    }

    if (ExtractAippPreprocessConfig(customModelData, dynamicInputCount, aippConfigVec) != SUCCESS) {
        return hiai::FAILED;
    }
    modelInputTensor.resize(dynamicInputCount + inputTensorDescVec.size());
    MakeInputTensor(aippConfigVec, inputTensorDescVec, modelInputTensor);

    inputTensorDescVec.assign(modelInputTensor.begin(), modelInputTensor.end());
    return SUCCESS;
}
} // namespace hiai
