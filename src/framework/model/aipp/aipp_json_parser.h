/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2024. All rights reserved.
 * Description: version definiation for model_run_tool and data_proc_tool.
 */
#ifndef FRAMEWORK_MODEL_AIPP_JSON_PARSER_H
#define FRAMEWORK_MODEL_AIPP_JSON_PARSER_H

#include <string>
#include <nlohmann/json.hpp>

namespace hiai {
std::vector<std::string> Split(const std::string& inputStr, const std::string& delimiter);
// trim from both ends (in place)
void Trim(std::string& s);

class AippJsonParser {
public:
    static bool ReadFileToJson(const std::string& file, nlohmann::json& j);
    static bool VarifyNodeInJson(const nlohmann::json& oriJson, const std::string& key);
    static bool GetNodeFromJson(const nlohmann::json& oriJson, const std::string& key, nlohmann::json& destJson);
    static bool GetNodeListFromJson(
        const nlohmann::json& oriJson, const std::string& key, std::vector<nlohmann::json>& destJson);
    static bool GetStringFromJson(const nlohmann::json& oriJson, const std::string& key, std::string& val);
    static bool GetIntFromJson(const nlohmann::json& oriJson, const std::string& key, int& val);
    static bool GetFloatFromJson(const nlohmann::json& oriJson, const std::string& key, float& val);
    static bool GetBoolFromJson(const nlohmann::json& oriJson, const std::string& key, bool& val);
};
}
#endif
