/**
 * Copyright 2019-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "util/version_util.h"
#include "c/hcl/hiai_version.h"

#ifdef __OHOS__
#include "model_manager/general_model_manager/ndk/ndk_util/ndk_util.h"
#include "model_manager/general_model_manager/ndk/hiai_ndk/hiai_ndk_helper.h"
#endif

namespace hiai {
const char* VersionUtil::GetVersion()
{
#ifdef __OHOS__
    if (!NDKUtil::CanDlopenVendorSo()) {
        return HIAI_NDK_GetVersion();
    }
#endif
    return HIAI_MR_GetVersion();
}
} // namespace hiai