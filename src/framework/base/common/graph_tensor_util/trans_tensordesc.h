/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: trans tensordesc
 */
#ifndef TRANS_TENSORDESC_H
#define TRANS_TENSORDESC_H
#include "graph/tensor.h"
#include "base/common/tensor_util/trans_tensor.h"
#include "framework/model/base_buffer.h"
namespace hiai {

HCS_API_EXPORT hiai::Status InitTensorDescriptor(const ge::TensorDesc& tensor, ccTensor_t& ccTensor);

HCS_API_EXPORT hiai::Status TransTensor(const ge::TensorDesc& xDesc, const ge::BaseBuffer& input,
    const ge::TensorDesc& yDesc, ge::BaseBuffer& output);

}
#endif