/**
 * Copyright 2023-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef CAST_CHECK_UTILS_H_
#define CAST_CHECK_UTILS_H_

#include <vector>

#include "framework/common/types.h"
#include "framework/graph/core/cgraph/compute_graph.h"
#include "framework/graph/core/node/node.h"
#include "framework/common/fmk_error_codes.h"

#include "base/common/tensor_util/trans_tensor.h"

namespace hiai {
class CastCheckUtils {
public:
    CastCheckUtils() = default;
    ~CastCheckUtils() = default;
    static hiai::Status CastBuildVerify(std::shared_ptr<ge::ComputeGraph>& computeGraph);
    static bool CheckInputOutputTensor(const ge::TensorDesc& inputDesc, const ge::TensorDesc& outputDesc);

private:
    static hiai::Status CheckInputOutput(const ge::Node& node);
    static bool CheckDataType(ge::DataType_t xType, ge::DataType_t yType);
    static bool CheckFormat(const ge::TensorDesc&  inputDesc, const ge::TensorDesc&  outputDesc);
    static bool CheckDims(const ge::TensorDesc&  inputDesc, const ge::TensorDesc&  outputDesc);
    static hiai::Status TransferDim(const std::vector<int64_t>& dims, std::vector<int64_t>& newDims);
    static bool CheckDimValue(ge::Format inputFormat, ge::Format outputFormat,
        const std::vector<int64_t>& inputDims, const std::vector<int64_t>& outputDims);
};
} // namespace hiai
#endif // CAST_CHECK_UTILS_H_
