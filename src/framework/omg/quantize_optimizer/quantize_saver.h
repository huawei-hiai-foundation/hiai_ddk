/*
 * Copyright (c) Hisilicon Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description: quantize saver
 */
#ifndef OMG_QUANTIZE_OPTIMIZERH_QUANTIZE_SAVER
#define OMG_QUANTIZE_OPTIMIZERH_QUANTIZE_SAVER

#include "base/error_types.h"

#include "framework/graph/core/node/node.h"

#include "omg/quantize_optimizer/quantize_types.h"

namespace hiai {
class QuantizeSaver {
public:
    static Status SaveOpQuantV1Params(
        QuantizeConfig& quantizeConfig, ge::Node* node, int64_t weightDataAddr = 0);
    static Status SaveOpQuantV2Params(
        QuantizeV2Config& quantizeV2Config, ge::Node* node, int64_t weightDataAddr = 0);
};
} // namespace hiai

#endif // OMG_QUANTIZE_OPTIMIZERH_QUANTIZE_SAVER
