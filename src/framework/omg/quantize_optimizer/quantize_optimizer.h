/*
 * Copyright (c) Hisilicon Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description: 设置轻量化配置
 */
#ifndef DOMI_OMG_COMPRESS_COMPRESS_OPTIMIZERH
#define DOMI_OMG_COMPRESS_COMPRESS_OPTIMIZERH

#include "base/error_types.h"
#include "framework/graph/core/cgraph/compute_graph.h"
#include "framework/model/base_buffer.h"

namespace hiai {
class QuantizeOptimizer {
public:
    /**
     *  @brief 解析轻量化参数配置并且设置到对应对象中
     */
    Status Optimize(const char* file, ge::ComputeGraphPtr graph);
    Status Optimize(ge::BaseBuffer& quantizeBuffer, ge::ComputeGraphPtr graph);
    /**
     *  @brief IR API定义量化算子属性映射
     */
    Status SetIRQuantizeInfos(ge::ComputeGraphPtr graph);
};
} // namespace hiai

#endif
