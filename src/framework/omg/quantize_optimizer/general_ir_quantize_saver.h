/*
 * Copyright (c) Hisilicon Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description: general ir quantize sever
 */
#ifndef OMG_QUANTIZE_OPTIMIZERH_GENERAL_IR_QUANTIZE_SAVER
#define OMG_QUANTIZE_OPTIMIZERH_GENERAL_IR_QUANTIZE_SAVER

#include "base/error_types.h"

#include "framework/graph/core/node/node.h"

#include "omg/quantize_optimizer/quantize_types.h"

namespace hiai {
class GeneralIRQuantizeSaver {
public:
    static Status SaveOpQuantParams(
        const QuantizeV2Config& quantizeConfig, ge::Node* node, int64_t weightDataAddr = 0);
};
} // namespace hiai

#endif // OMG_QUANTIZE_OPTIMIZERH_GENERAL_IR_QUANTIZE_SAVER
