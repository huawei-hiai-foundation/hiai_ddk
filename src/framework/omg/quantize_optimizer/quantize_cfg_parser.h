/*
 * Copyright (c) Hisilicon Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description: quantize config parser
 */
#ifndef FRAMEWORK_OMG_QUANTIZE_OPTIMIZER_QUANTIZE_CFG_PARSER_H
#define FRAMEWORK_OMG_QUANTIZE_OPTIMIZER_QUANTIZE_CFG_PARSER_H

#include <string>

#include "base/error_types.h"

namespace hiai {
struct ModelLightWeightParams;

class QuantizeCfgParser {
public:
    /*
     *  @ingroup domi_omg
     *  @brief 解析轻量化参数配置
     */
    static hiai::Status ParseConfigFromFile(const std::string& file, ModelLightWeightParams& params);
    static hiai::Status ParseConfigFromBuffer(uint8_t* buffer, size_t size, ModelLightWeightParams& params);
};
} // namespace hiai

#endif // FRAMEWORK_OMG_QUANTIZE_OPTIMIZER_QUANTIZE_CFG_PARSER_H