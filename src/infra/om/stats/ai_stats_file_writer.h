/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: ai_stats_file_writer.h
 */

#ifndef INFRA_OM_STATS_FILE_WRITER_H
#define INFRA_OM_STATS_FILE_WRITER_H

#include <string>

#include "infra/om/stats/ai_stats_log_common.h"

namespace hiai {
class FileWriter {
public:
    FileWriter() = default;
    ~FileWriter() = default;
    static int Write(const AiStatsEngineType type, std::string message);
};
} // namespace hiai
#endif