/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2023. All rights reserved.
 * Description: ai stats log timer class
 */

#include "infra/base/assertion.h"
#include "infra/base/ai_thread.h"
#include "infra/base/api_export.h"
#include "infra/om/stats/ai_stats_log_timer.h"

namespace hiai {
AiStatsLogTimer* AiStatsLogTimer::instance_ = nullptr;
std::mutex AiStatsLogTimer::instanceLock_;
INFRA_API_EXPORT AiStatsLogTimer* AiStatsLogTimer::GetInstance()
{
    std::lock_guard<std::mutex> lck(instanceLock_);
    if (instance_ == nullptr) {
        instance_ = new (std::nothrow) AiStatsLogTimer();
        HIAI_EXPECT_NOT_NULL_R(instance_, nullptr);
    }
    return instance_;
}

INFRA_API_EXPORT void AiStatsLogTimer::StartStatsLogTimer()
{
    (void)AIThread_Create(AiStatsLogTimer::Start, this);
}

void* AiStatsLogTimer::Start(void* arg)
{
    auto* p = static_cast<AiStatsLogTimer*>(arg);
    HIAI_EXPECT_NOT_NULL_R(p, nullptr);
    while (1) {
        AIThread_Sleep_For(p->seconds_);
        HIAI_EXPECT_NOT_NULL_R(p->pProc_, nullptr);
        STATS_LOGD("AiStatsLogTimer::Start:has sleep %d seconds_,call Proc->Timeout", p->seconds_);
        p->pProc_->Timeout();
    }
    return nullptr;
}
} // namespace ai
