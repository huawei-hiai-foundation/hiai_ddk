/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: ai stats log manager class
 */

#ifndef INFRA_OM_STATS_LOG_FILE_WRITER_H
#define INFRA_OM_STATS_LOG_FILE_WRITER_H

#include <mutex>
#include <string>
#include <vector>
#include <atomic>
#include <map>
#include <pthread.h>
#include <condition_variable>

#include "infra/om/stats/ai_stats_log_common.h"

#include "ai_stats_file_writer.h"

namespace hiai {
/*
 * 该类用于将各engine缓存的buffer内容写入文件，该类会启动一个后台线程，这样避免了engine直接写文件的耗时
 */
class AiStatsLogFileWriter {
public:
    static AiStatsLogFileWriter* GetInstance();

    // 定时器超时时调用。内部调DoAsyncWrite异步写日志
    void CopyBuffer(std::map<pid_t, std::map<std::string, AiStatsData>> buffer, AiStatsEngineType type);
#ifdef STATS_SERVER
    // 客户端向服务端发RPC请求，在服务端调用。内部调DoAsyncWrite异步写日志
    void WriteMessage(const AiStatsEngineType type, std::string message);
#endif
    // 客户端退出，同步写日志
    void WriteToFile(const AiStatsEngineType type, std::map<pid_t, std::map<std::string, AiStatsData>>& logs);

private:
    AiStatsLogFileWriter();
    ~AiStatsLogFileWriter();
    AiStatsLogFileWriter(const AiStatsLogFileWriter&) = delete;
    AiStatsLogFileWriter operator=(const AiStatsLogFileWriter&) = delete;

    static void* Start(void* arg);
    void DoAsyncWrite();
#ifdef STATS_SERVER
    // 异步函数中调用的同步写文件
    void WriteMessageToFile();
#endif

private:
#ifdef STATS_SERVER
    // 客户端向服务端发RPC请求的数据存储
    std::vector<std::pair<AiStatsEngineType, std::string>> messageBuffer_;
#endif
    // 超时数据堆积的数据存储
    std::map<AiStatsEngineType, std::map<pid_t, std::map<std::string, AiStatsData>>> engineBufferMap_;
    std::mutex engineBufferMapLock_;

    int threadCnt_ = 0;
};
} // end namespace hiai
#endif // INFRA_OM_STATS_LOG_FILE_WRITER_H
