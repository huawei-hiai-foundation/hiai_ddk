/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2017-2020. All rights reserved.
 * Description: ai local file writer
 */

#include "ai_stats_file_writer.h"

#include <iostream>
#include <fstream>

#include "infra/om/stats/ai_stats_log_utils.h"
#include "infra/om/stats/ai_stats_log_common.h"

namespace {
const uint32_t STATS_LOG_MAX_SIZE = 4 * 1024 * 1024;
}  // namespace

namespace hiai {
int FileWriter::Write(const AiStatsEngineType type, std::string message)
{
    std::string file = AiStatsLogUtils::GetTodayLogFileName(type);
    STATS_LOGI("file:%s", file.c_str());
    if (AiStatsLogCfgUtils::GetFileSize(file) >= STATS_LOG_MAX_SIZE) {
        STATS_LOGE("LocalFileWriter::Write [%s] size has surpass max size", file.c_str());
        return -1;
    }
    std::ofstream logStream(file.c_str(), std::ofstream::app);
    if (!logStream.is_open()) {
        STATS_LOGE("LocalFileWriter::Write open[%s] failed.", file.c_str());
        return -1;
    }

    logStream << message;
    logStream.close();
    return 0;
}
};  // namespace hiai
