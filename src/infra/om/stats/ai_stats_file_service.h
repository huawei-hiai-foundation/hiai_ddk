/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2023. All rights reserved.
 * Description: ai_stats_file_service.h
 */

#ifndef INFRA_OM_STATS_FILE_SERVICE_H
#define INFRA_OM_STATS_FILE_SERVICE_H

#include <atomic>
#include <mutex>
#include <functional>

#include "base/error_types.h"

#include "infra/rpc/rpc_request.h"
#include "infra/base/api_export.h"

namespace hiai {
class INFRA_API_EXPORT AIStatsFileService : public hiai::rpc::IRpcRequestHandler {
public:
    AIStatsFileService() = default;
    ~AIStatsFileService() override = default;

private:
    hiai::Status Execute(int32_t pid, int32_t commandID, const std::vector<hiai::rpc::RpcHandle>& inputs,
        std::vector<hiai::rpc::RpcHandle>& outputs) override;
    void OnClientDie(pid_t pid) override;
    int32_t GetModuleID() const override;
    hiai::Status SetListener(int pid, const std::vector<hiai::rpc::RpcHandle>& paras,
        const std::shared_ptr<hiai::rpc::IRPCListener> rpcListener) override;
    hiai::Status DestroyFd(std::vector<hiai::rpc::RpcHandle>& outputs) override;
};
} // namespace hiai
#endif
